package shopmode

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, AddShopMode)
	routes.Post("/{id:string}", auth.TokenMDW, UpdateShopMode)
	routes.Get("/", auth.TokenMDW, GetShopMode)
}
