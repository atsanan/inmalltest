package shopmode

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddShopMode for admin add data.
func AddShopMode(ctx iris.Context) {
	shopShopModels := models.ShopModels{}
	if parseErr := ctx.ReadJSON(&shopShopModels); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return

	}

	_id := bson.NewObjectId()
	update := bson.M{
		"_id":                    _id,
		"shopModeNameEng":        shopShopModels.ShopModeNameEng,
		"shopModeNameThai":       shopShopModels.ShopModeNameThai,
		"shopModeNameChi1":       shopShopModels.ShopModeNameChi1,
		"shopModeNameChi2":       shopShopModels.ShopModeNameChi2,
		"shopModeDetailEng":      shopShopModels.ShopModeDetailEng,
		"shopModeDetailThai":     shopShopModels.ShopModeDetailThai,
		"shopModeDetailChi1":     shopShopModels.ShopModeDetailChi1,
		"shopModeDetailChi2":     shopShopModels.ShopModeDetailChi2,
		"shopModeAssertMode":     shopShopModels.ShopModeAssertMode,
		"shopModelAssertVersion": shopShopModels.ShopModeAssertVersion,
		"createAt":               time.Now(),
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopModeCollection)

	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	shopShopModelsData := bson.M{}
	if findErr := coll.Find(bson.M{"_id": _id}).One(&shopShopModelsData); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, shopShopModelsData)

}
