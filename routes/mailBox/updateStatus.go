package mailBox

import (
	"fmt"
	"inmallgame/app/utils"
	"strconv"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

func updateStatus(ctx iris.Context) {
	coinActive := ctx.PostValue("coinActive")
	diamondActive := ctx.PostValue("diamondActive")
	isGetItem := ctx.PostValue("isGetItem")
	isGetMDefault := ctx.PostValue("isGetMDefault")
	isActive := ctx.PostValue("isActive")
	itemActive := ctx.PostValue("itemActive")
	ID := ctx.Params().Get("Id")
	monsterActive := ctx.PostValue("monsterActive")
	err := validation.Errors{
		"id": validation.Validate(ID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	update := bson.M{}
	coin, errBool := strconv.ParseBool(coinActive)
	if errBool == nil {
		update["coinActive"] = coin
	}

	diamond, errBool := strconv.ParseBool(diamondActive)
	if errBool == nil {
		update["diamondActive"] = diamond
	}

	item, errBool := strconv.ParseBool(isGetItem)
	if errBool == nil {
		update["isGetItem"] = item
	}

	mDefault, errBool := strconv.ParseBool(isGetMDefault)
	if errBool == nil {
		update["isGetMDefault"] = mDefault
	}

	monster, errBool := strconv.ParseBool(monsterActive)
	if errBool == nil {
		update["monsterActive"] = monster
	}

	active, errBool := strconv.ParseBool(isActive)
	if errBool == nil {
		update["isActive"] = active
	}

	item, errBool = strconv.ParseBool(itemActive)
	if errBool == nil {
		update["itemActive"] = item
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(ID)}, bson.M{
		"$currentDate": bson.M{"lastModified": true},
		"$set":         update,
	})

	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	pipe := coll.Pipe([]bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(ID)}},
	},
	)

	result := bson.M{}

	if err := pipe.One(&result); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, result)
}
