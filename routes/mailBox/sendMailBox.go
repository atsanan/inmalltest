package mailBox

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

func sendMailBox(ctx iris.Context) {
	mailBox := models.MailBox{}
	if parseErr := ctx.ReadJSON(&mailBox); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	session := database.GetMgoSession()
	config := utils.ConfigParser(ctx)
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)

	opt := option.WithCredentialsFile("inmallgotchimons.json")
	app, errfire := firebase.NewApp(context.Background(), nil, opt)
	if errfire != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
	}

	context := context.Background()
	client, errMes := app.Messaging(context)
	if errMes != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
	}
	stringMap := map[string]string{
		"type":         "inbox",
		"by":           "system",
		"linkListName": mailBox.LinkListname,
	}
	data := []bson.M{}
	if len(mailBox.ListUser) > 0 {
		for _, v := range mailBox.ListUser {
			user := models.User{}
			if err := coll.Find(bson.M{"_id": v}).One(&user); err == nil {

				if user.PushMessagesAccessToken != "" {

					message := &messaging.Message{
						Notification: &messaging.Notification{
							Title: mailBox.Title,
							Body:  mailBox.Detail,
							//Data:  dataJson,
						},
						Data:  stringMap,
						Token: user.PushMessagesAccessToken,
						//Topic: "InMallGotchiMons",
						//Condition: condition,
						//Topic:topic,
						//Condition: condition,
					}

					response, errSend := client.Send(context, message)
					if errSend != nil {
						res := bson.M{"response": errSend}
						data = append(data, res)
					} else {
						res := bson.M{"response": response,
							"PushMessagesAccessToken": user.PushMessagesAccessToken}
						data = append(data, res)
					}
				}
			}
		}
	}else{

		message := &messaging.Message{
			Notification: &messaging.Notification{
				Title: mailBox.Title,
				Body:  mailBox.Detail,
				//Data:  dataJson,
			},
			Data:  stringMap,
			Topic: "InMallGotchiMons",
			//Condition: condition,
			//Topic:topic,
			//Condition: condition,
		}

		response, errSend := client.Send(context, message)
		if errSend != nil {
			res := bson.M{"response": errSend}
			data = append(data, res)
		} else {
			res := bson.M{"response": response}
			data = append(data, res)
		}
	}
	result := bson.M{
		"user":    data,
		"mailBok": mailBox,
	}
	utils.ResponseSuccess(ctx, result)

}
