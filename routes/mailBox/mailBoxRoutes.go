package mailBox

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Get("/byPlayer/{playerId:string}", auth.TokenMDW, getMailBoxByPlayer)
	routes.Get("/{playerId:string}/{linkListName:string}", auth.TokenMDW, getMailBoxByPlayerAndLinkListName)
	routes.Post("/softDeleteList/", auth.TokenMDW, SoftDeleteItemMailBoxList)
	routes.Post("/deleteList/", auth.TokenMDW, DeleteMailBoxList)
	routes.Get("/deleteList/", auth.TokenMDW, GetDeleteMailBoxList)
	routes.Post("/updateStatus/{Id:string}", auth.TokenMDW, updateStatus)
	routes.Post("/sendMailBox/", sendMailBox)
	routes.Get("/remove/{Id:string}", auth.TokenMDW, removeMailBox)
}
