package mailBox

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/kataras/iris"

	"github.com/globalsign/mgo/bson"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

func getMailBoxByPlayerAndLinkListName(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	linkListName := ctx.Params().Get("linkListName")
	err := validation.Errors{
		"friendId":     validation.Validate(playerID, validation.Required, is.MongoID),
		"linkListName": validation.Validate(linkListName, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	mailBox := bson.M{}
	objQuery := []bson.M{}
	objQuery = append(objQuery,
		bson.M{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID),
			"linkListName": linkListName,
		}})

	pipe := coll.Pipe(objQuery)
	pipe.One(&mailBox)

	utils.ResponseSuccess(ctx, mailBox)
}
