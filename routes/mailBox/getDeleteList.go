package mailBox

import (

	// "github.com/globalsign/mgo"

	"fmt"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/models"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// GetDeleteMailBoxPlayerList route.
func GetDeleteMailBoxList(ctx iris.Context) {

	linkListName := ctx.URLParam("linkListName")
	field := ctx.URLParam("field")
	query := bson.M{}
	if linkListName != "" {
		query["linkListName"] = linkListName
	}
	query["createAt"] = bson.M{"$exists": true}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)

	mailBox := []models.MailBox{}
	if findErr := coll.Find(query).All(&mailBox); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "", fmt.Sprint(findErr))
		return
	}

	settingColl := session.DB(config.DbName).C(config.DBCollection.SettingCollection)
	setting := bson.M{}
	pipeSetting := settingColl.Pipe([]bson.M{})
	errSetting := pipeSetting.One(&setting)
	if errSetting != nil {
		return
	}

	checkday := 0
	if field != "" {
		checkday = setting[field].(int)
	}
	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)

	oids := make([]bson.ObjectId, 0)
	for _, v := range mailBox {
		createAt := v.CreateAt.In(location)
		hs := now.Sub(createAt).Hours() / 24

		if checkday > 0 {
			if hs > float64(checkday) {
				oids = append(oids, v.ID)
			}
		}
	}

	_, err := coll.RemoveAll(bson.M{"_id": bson.M{"$in": oids}})
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{"message": "success delete MailBox"})

}
