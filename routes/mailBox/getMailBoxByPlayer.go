package mailBox

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func getMailBoxByPlayer(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit := ctx.URLParamIntDefault("limit", 100)

	playerID := ctx.Params().Get("playerId")
	if validateErr := utils.ValidateMongoID("playerId", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	objQuery := []bson.M{}
	objQuery = append(objQuery, bson.M{"$match": bson.M{
		"playerId": bson.ObjectIdHex(playerID),
		"isActive": true,
	}})

	objQuery = append(objQuery, bson.M{"$sort": bson.M{
		"_id": -1,
	}})

	pipe := coll.Pipe(objQuery)
	mailBox := []bson.M{}
	mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	if countErr != nil {
		result := map[string]interface{}{
			"pages":     0,
			"pageIndex": page,
			"pageLimit": limit,
			"mailBox":   mailBox,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}
	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe = coll.Pipe(objQuery)
	pipe.All(&mailBox)

	originalLink := fmt.Sprintf("/api/v1/mailBox/byPlayer/%s", playerID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"mailBox":   mailBox,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}
