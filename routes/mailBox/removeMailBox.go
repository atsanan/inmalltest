package mailBox

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

func removeMailBox(ctx iris.Context) {

	ID := ctx.Params().Get("Id")

	err := validation.Errors{
		"id": validation.Validate(ID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	mongoErr := coll.Remove(bson.M{"_id": bson.ObjectIdHex(ID)})

	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to remove data", fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{"message": "success remove mailBox"})

}
