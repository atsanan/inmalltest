package mailBox

import (

	// "github.com/globalsign/mgo"
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/models"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// SoftDeleteItemMailBoxList route.
func SoftDeleteItemMailBoxList(ctx iris.Context) {

	MailBox := models.MailBox{}
	if parseErr := ctx.ReadJSON(&MailBox); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	_, err := coll.UpdateAll(bson.M{"_id": bson.M{"$in": MailBox.ListIdMailBox}}, bson.M{"$set": bson.M{"isActive": false}})
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, MailBox, fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{"message": "success soft delete mail box"})

}
