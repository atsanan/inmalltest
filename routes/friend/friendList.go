package friend

import (
	"fmt"
	"inmallgame/app/utils"
	"strconv"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

//friendList routes
func friendList(ctx iris.Context) {
	friendID := ctx.Params().Get("friendId")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit := ctx.URLParamIntDefault("limit", 100)
	friendActiveStatus := ctx.URLParam("friendActiveStatus")

	err := validation.Errors{
		"friendID":           validation.Validate(friendID, validation.Required, is.MongoID),
		"friendActiveStatus": validation.Validate(friendActiveStatus, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	status, _ := strconv.ParseInt(friendActiveStatus, 10, 32)

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.FriendCollection)
	objQuery := []bson.M{
		{"$match": bson.M{
			"friendId":           bson.ObjectIdHex(friendID),
			"friendActiveStatus": status,
		},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "playerId",
				"foreignField": "_id",
				"as":           "player",
			},
		},
		{"$unwind": "$player"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "friendId",
				"foreignField": "_id",
				"as":           "friend",
			},
		},
		{"$unwind": "$friend"},
	}

	pipe := coll.Pipe(objQuery)

	mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
		return
	}
	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)
	friendList := []bson.M{}
	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe = coll.Pipe(objQuery)
	findErr := pipe.All(&friendList)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	originalLink := fmt.Sprintf("/api/v1/friends/friend/%s", friendID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"friend":    friendList,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}
