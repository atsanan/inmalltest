package friend

import (
	"fmt"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

// AddFriend  Router
func AddFriend(ctx iris.Context) {
	playerID := ctx.PostValue("playerId")
	friendID := ctx.PostValue("friendId")

	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
		"friendId": validation.Validate(friendID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	findFriend := bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.FriendCollection)
	objQuery := []bson.M{}
	friend := coll.Find(bson.M{"$or": []bson.M{
		bson.M{
			"friendId": bson.ObjectIdHex(friendID),
			"playerId": bson.ObjectIdHex(playerID),
		},
		bson.M{
			"friendId": bson.ObjectIdHex(playerID),
			"playerId": bson.ObjectIdHex(friendID),
		},
	}}).One(&findFriend)
	if friend != nil {

		_id := bson.NewObjectId()
		insert := bson.M{
			"_id":                _id,
			"friendId":           bson.ObjectIdHex(friendID),
			"playerId":           bson.ObjectIdHex(playerID),
			"friendActiveStatus": 1,
			"createAt":           time.Now(),
		}

		objQuery = append(objQuery, bson.M{"$match": bson.M{
			"_id": _id,
		}})

		mongoErr := coll.Insert(insert)
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			return
		}

	}

	if findFriend["friendActiveStatus"] == 0 {
		mongoErr := coll.Update(bson.M{"_id": findFriend["_id"]}, bson.M{
			"$currentDate": bson.M{"lastModified": true},
			"$set": bson.M{
				"friendId":           bson.ObjectIdHex(friendID),
				"playerId":           bson.ObjectIdHex(playerID),
				"friendActiveStatus": 1,
			},
		})

		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}
		objQuery = append(objQuery, bson.M{"$match": bson.M{
			"_id": findFriend["_id"],
		}})

	} else if findFriend["friendActiveStatus"] == 1 {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "add friend duplicate friend Request", fmt.Sprint(""))
		return
	} else if findFriend["friendActiveStatus"] == 2 {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "add friend duplicate friend approved", fmt.Sprint(""))
		return
	}

	objQuery = append(objQuery,
		bson.M{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "playerId",
				"foreignField": "_id",
				"as":           "player",
			},
		},
		bson.M{"$unwind": "$player"},
		bson.M{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "friendId",
				"foreignField": "_id",
				"as":           "friend",
			},
		},
		bson.M{"$unwind": "$friend"},
	)
	data := bson.M{}

	collfind := session.DB(config.DbName).C(config.DBCollection.FriendCollection)
	pipe := collfind.Pipe(objQuery)
	findErr := pipe.One(&data)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, data)
}
