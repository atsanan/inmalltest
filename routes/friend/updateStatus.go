package friend

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"strconv"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

func updateStatus(ctx iris.Context) {
	ID := ctx.Params().Get("Id")
	friendStatus := ctx.PostValue("friendActiveStatus")
	err := validation.Errors{
		"Id":                 validation.Validate(ID, validation.Required, is.MongoID),
		"friendActiveStatus": validation.Validate(friendStatus, validation.Required, is.Int),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	i64, _ := strconv.Atoi(friendStatus)
	data := bson.M{
		"friendActiveStatus": i64,
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.FriendCollection)
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(ID)}, bson.M{
		"$currentDate": bson.M{"lastModified": true},
		"$set":         data,
	})

	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "update errors", fmt.Sprint(mongoErr))
		return
	}
	findFriendData := bson.M{}

	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(ID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "playerId",
				"foreignField": "_id",
				"as":           "player",
			},
		},
		{"$unwind": "$player"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "friendId",
				"foreignField": "_id",
				"as":           "friend",
			},
		},
		{"$unwind": "$friend"},
	}
	pipe := coll.Pipe(objQuery)
	findFriendErr := pipe.One(&findFriendData)
	if findFriendErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findFriendErr))
		return
	}

	result := map[string]interface{}{

		"friend": findFriendData,
		// "paging": map[string]interface{}{
		// 	"next":     nextPage,
		// 	"previous": previousPage,
		// },
	}

	utils.ResponseSuccess(ctx, result)
}
