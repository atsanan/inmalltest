package friend

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

//ConfirmFriend router
func ConfirmFriend(ctx iris.Context) {
	playerID := ctx.PostValue("playerId")
	friendID := ctx.PostValue("friendId")

	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
		"friendId": validation.Validate(friendID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	condiition := bson.M{
		"friendId":           bson.ObjectIdHex(friendID),
		"playerId":           bson.ObjectIdHex(playerID),
		"friendActiveStatus": 0,
	}
	friendFriend := bson.M{}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.FriendCollection)
	mongoErr := coll.Update(condiition, bson.M{
		"$currentDate": bson.M{"lastModified": true},
		"$set": bson.M{
			"friendActiveStatus": 1,
		},
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	findErr := coll.Pipe(
		[]bson.M{
			{"$match": bson.M{
				"friendId": bson.ObjectIdHex(friendID),
				"playerId": bson.ObjectIdHex(playerID),
			},
			},
			{
				"$lookup": bson.M{
					"from":         config.DBCollection.PlayerCollection,
					"localField":   "playerId",
					"foreignField": "_id",
					"as":           "player",
				},
			},
			{"$unwind": "$player"},
			{
				"$lookup": bson.M{
					"from":         config.DBCollection.PlayerCollection,
					"localField":   "friendId",
					"foreignField": "_id",
					"as":           "friend",
				},
			},
			{"$unwind": "$friend"},
		},
	).One(&friendFriend)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, friendFriend)
}
