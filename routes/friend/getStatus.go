package friend

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

//getStatus routes
func getStatus(ctx iris.Context) {

	playerID := ctx.Params().Get("playerId")
	friendID := ctx.Params().Get("friendId")
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
		"friendId": validation.Validate(friendID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.FriendCollection)
	findFriendData := bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{
			"$or": []bson.M{
				bson.M{
					"playerId": bson.ObjectIdHex(playerID),
					"friendId": bson.ObjectIdHex(friendID),
				},
				bson.M{
					"friendId": bson.ObjectIdHex(playerID),
					"playerId": bson.ObjectIdHex(friendID),
				},
			},
		},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "playerId",
				"foreignField": "_id",
				"as":           "player",
			},
		},
		{"$unwind": "$player"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "friendId",
				"foreignField": "_id",
				"as":           "friend",
			},
		},
		{"$unwind": "$friend"},
	}

	pipe := coll.Pipe(objQuery)
	findFriendErr := pipe.One(&findFriendData)
	if findFriendErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findFriendErr))
		return
	}

	utils.ResponseSuccess(ctx, findFriendData)
}
