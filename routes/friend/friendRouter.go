package friend

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, AddFriend)
	routes.Get("/addFriendList/{friendId: string}", auth.TokenMDW, AddFriendList)
	routes.Get("/friend/{friendId: string}", auth.TokenMDW, friendList)
	routes.Get("/friendList/{playerId: string}", auth.TokenMDW, playerAndFriendList)
	routes.Get("/player/{playerId: string}", auth.TokenMDW, playerList)
	routes.Get("/status/{playerId: string}/{friendId: string}", auth.TokenMDW, getStatus)
	routes.Post("/confirmFriend", auth.TokenMDW, ConfirmFriend)
	routes.Post("/updateStatus/{Id: string}", auth.TokenMDW, updateStatus)
}
