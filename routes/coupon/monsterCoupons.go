package coupon

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

//monsterCoupons router
func monsterCoupons(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	itemCategoryID := ctx.URLParam("itemCategoryId")

	err := validation.Errors{
	//	"itemCategoryId":  validation.Validate(itemCategoryID, validation.Required, is.MongoID),
		"player": validation.Validate(playerID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemShopCollection)
	collShop := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	ChannelCollection := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)
	mallFloorCollection := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	mallCollection := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	itemShop := []bson.M{}

	objQuery := []bson.M{
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
	}

	if itemCategoryID !=""{
		objQuery = append(objQuery, bson.M{"$match": bson.M{
			"item.itemCategoryId": bson.ObjectIdHex(itemCategoryID),
		},
		})

	}
	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&itemShop)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	data := []bson.M{}
	for k, v := range itemShop {
		
		shop := bson.M{}
		err := collShop.Find(bson.M{"_id": v["shopId"]}).One(&shop)
		if err == nil {

			channelCheck := []bson.M{}
			channel := []bson.M{}
			ChannelCollection.Find(bson.M{"shopId": shop["_id"]}).All(&channel)
			if len(channel) > 0 {

				for kc, vc := range channel {
					jc := []bson.M{}
					joinChannelColl.Pipe([]bson.M{
						{
							"$match": bson.M{"channelId": vc["_id"]},
						},
						{
							"$lookup": bson.M{
								"from":         config.DBCollection.MonsterPlayerCollection,
								"localField":   "mPlayerId",
								"foreignField": "_id",
								"as":           "mPlayerId",
							},
						},
						{"$unwind": "$mPlayerId"},
						{
							"$match": bson.M{"mPlayerId.playerId": bson.ObjectIdHex(playerID)},
						},
					}).All(&jc)
					channel[kc]["joinChannel"] = jc
					if len(jc) > 0 {
						channelCheck = append(channelCheck, bson.M{"check": 1})
					}
				}

				shop["channel"] = channel
				mallFloor := bson.M{}
				errMallFloor := mallFloorCollection.Find(bson.M{"_id": shop["mallFloorId"]}).One(&mallFloor)

				mall := bson.M{}

				if errMallFloor == nil {
					mallCollection.Find(bson.M{"_id": mallFloor["mallId"]}).One(&mall)
				}

				itemShop[k]["mall"] = mall
				itemShop[k]["mallFloor"] = mallFloor
				itemShop[k]["shop"] = shop
				if len(channelCheck) > 0 {
					data = append(data, itemShop[k])
				}
			}
		}
	}	
	result := bson.M{}
	result["coupon"] = data

	utils.ResponseSuccess(ctx, result)
}