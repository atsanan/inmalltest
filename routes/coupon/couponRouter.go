package coupon

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Get("/list", auth.TokenMDW, GetCouponList)
	routes.Get("/detail", auth.TokenMDW, GetDetail)
	routes.Post("/getCoupon", auth.TokenMDW, GetCoupon)
	routes.Post("/pending", auth.TokenMDW, GetCouponPending)
	routes.Post("/recive", auth.TokenMDW, ReciveCoupon)
	routes.Post("/redeem", auth.TokenMDW, GetRedeem)
	routes.Get("/favorites/{playerId:string}", auth.TokenMDW, monsterCoupons)
}
