package coupon

import (
	"fmt"
	"inmallgame/app/utils"

	"github.com/kataras/iris"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

func ReciveCoupon(ctx iris.Context) {
	ID := ctx.PostValue("id")
	err := validation.Errors{
		"id": validation.Validate(ID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)

	errUpdate := coll.Update(bson.M{
		"_id": bson.ObjectIdHex(ID),
	}, bson.M{
		"$currentDate": bson.M{"couponDatetime1": true},
	})
	if errUpdate != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not Update", fmt.Sprint("update"))
		return
	}

	data := bson.M{}
	findErr := coll.Find(bson.M{
		"_id": bson.ObjectIdHex(ID),
	}).One(&data)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{"couponPlayer": data})

}
