package coupon

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

type CouponData struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty"`
	DiamondInclude  int           `json:"diamondInclude" bson:"diamondInclude"`
	IsReachDateTime bool          `json:"isReachDateTime" bson:"isReachDateTime"`

	ExpiredDate time.Time `json:"expireDate" bson:"expireDate"`
}

func GetCouponList(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	mapIndoorFloorKey := ctx.URLParamTrim("indoorKey")
	mallID := ctx.URLParamTrim("mallId")
	partnerType := ctx.URLParamTrim("partnerType")
	isHighlight := ctx.URLParamTrim("isHighlight")
	itemCategoryID := ctx.URLParamTrim("itemCategoryId")
	itemSubCategory := ctx.URLParamTrim("itemSubCategory")
	coin := ctx.URLParam("coin")
	diamond:= ctx.URLParam("diamond")
	
	distance := 0
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	lat := ctx.URLParamTrim("lat")
	long := ctx.URLParamTrim("long")
	err := validation.Errors{
		//"lat":  validation.Validate(lat, validation.Required, is.Float),
		"itemCategoryId": validation.Validate(itemCategoryID, is.MongoID),
		"mallId": validation.Validate(mallID, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemCollection)

	condition := []bson.M{}
	if long != "" && lat != "" {
		distance = ctx.URLParamIntDefault("distance", 5000)
		longConv, _ := strconv.ParseFloat(long, 64)
		latConv, _ := strconv.ParseFloat(lat, 64)
		var coordinates [2]float64
		coordinates[0] = longConv
		coordinates[1] = latConv
		condition = append(condition, bson.M{
			"$geoNear": bson.M{
				"near":          bson.M{"type": "Point", "coordinates": coordinates},
				"maxDistance":   distance,
				"distanceField": "dist.calculated",
				"spherical":     true,
			},
		})
	} else {

		condition = append(condition, bson.M{"$sort": bson.M{
			"itemOrder": -1,
		},
		})

	}
	condition = append(condition, bson.M{
		"$match": bson.M{
			"isActive": true,
		},
	})
	if partnerType != "" {
		condition = append(condition, bson.M{
			"$match": bson.M{
				"partnerTypes.partnerTypeName": partnerType,
			},
		})
	}
	boolIsHighlight, _ := strconv.ParseBool(isHighlight)
	if isHighlight != "" {
		condition = append(condition, bson.M{
			"$match": bson.M{
				"isHighlight": boolIsHighlight,
			},
		})
	}
	if itemSubCategory != ""{
		condition = append(condition, bson.M{
			"$match": bson.M{
				"subCategory": itemSubCategory,
			},
		})
	}
	// baseQuery = append(baseQuery, bson.M{
	// 	"$lookup": bson.M{
	// 		"from":         config.DBCollection.MallFloorCollection,
	// 		"localField":   "mallFloorId",
	// 		"foreignField": "_id",
	// 		"as":           "mallFloor",
	// 	},
	// }, bson.M{
	// 	"$unwind": "$mallFloor",
	// })

	// condition = append(condition, bson.M{
	// 	"$lookup": bson.M{
	// 		"from":         config.DBCollection.CouponItemCollection,
	// 		"localField":   "_id",
	// 		"foreignField": "itemId",
	// 		"as":           "couponItem",
	// 	},
	// })
	// condition = append(condition, bson.M{
	// 	"$match": bson.M{
	// 		"couponItem.itemPlayerId": bson.M{"$exists": false},
	// 		"couponItem": bson.M{"$not":bson.M{"$size": 0,}},
	// 	},
	// })

	// condition = append(condition, bson.M{
	// 	"$lookup": bson.M{
	// 		"from":         config.DBCollection.ItemShopCollection,
	// 		"localField":   "_id",
	// 		"foreignField": "itemId",
	// 		"as":           "itemShop",
	// 	},
	// },
	// )
	condition = append(condition, bson.M{
		"$lookup": bson.M{
			"from":         config.DBCollection.ShopCollection,
			"localField":   "shopSelect",
			"foreignField": "_id",
			"as":           "shop",
		},
	}, bson.M{
		"$unwind": "$shop",
	})

	// condition = append(condition, bson.M{
	// 	"$lookup": bson.M{
	// 		"from":         config.DBCollection.MallFloorCollection,
	// 		"localField":   "shop.mallFloorId",
	// 		"foreignField": "_id",
	// 		"as":           "mallFloor",
	// 	},
	// }, bson.M{
	// 	"$unwind": "$mallFloor",
	// })

	if mallID != "" {
		collItemShop := session.DB(config.DbName).C(config.DBCollection.ItemShopCollection)
		itemShop := []bson.M{}
		if mongoErr := collItemShop.Pipe([]bson.M{
			{
				"$match": bson.M{"mallId": bson.ObjectIdHex(mallID)},
			},
		}).All(&itemShop); mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
			return
		}
		oids := make([]bson.ObjectId, len(itemShop))
		if len(itemShop) > 0 {
			for i := range itemShop {
				oids[i] = itemShop[i]["itemId"].(bson.ObjectId)
			}
		}
		condition = append(condition,
			bson.M{
				"$match": bson.M{"_id": bson.M{"$in": oids}},
			})
	}

	condition = append(condition, bson.M{
		"$lookup": bson.M{
			"from":         config.DBCollection.PartnerTypesCollection,
			"localField":   "partnerTypeId",
			"foreignField": "_id",
			"as":           "partnerTypes",
		},
	})

	if mapIndoorFloorKey != "" {
		condition = append(condition, bson.M{"$match": bson.M{
			"mallFloor.mapIndoorFloorKey": mapIndoorFloorKey,
		},
		})
	}

	if itemCategoryID !=""{
		condition = append(condition, bson.M{"$match": bson.M{
			"itemCategoryId": bson.ObjectIdHex(itemCategoryID),
		},
		})

	}
	parseCoin, _ := strconv.ParseInt(coin, 10, 32)
	if coin !=""{
		condition = append(condition, bson.M{"$match": bson.M{
			"coin": parseCoin,
		},
		})
	}

	parseDiamond, _ := strconv.ParseInt(diamond, 10, 32)

	if diamond !=""{
		condition = append(condition, bson.M{"$match": bson.M{
			"diamond": parseDiamond,
		},
		})
	}

	

	condition = append(condition, bson.M{
		"$project": bson.M{
			"_id":                  1,
			"itemName":             1, //           bson.M{"$first": "$itemName"},
			"itemDetail":           1, //          bson.M{"$first": "$itemDetail"},
			"itemAssertImageSlot1": 1, // bson.M{"$first": "$itemAssertImageSlot1"},
			"itemAssertImageSlot2": 1, // bson.M{"$first": "$itemAssertImageSlot2"},
			"nameReport":           1, // bson.M{"$first": "$nameReport"},
			"diamondInclude":       1,
			"isReachDateTime":      1, // bson.M{"$first": "$isReachDateTime"},
			"expireDate":           1, //bson.M{"$first": "$expireDate"},
			"coin":        1,
			"diamond":        1,
			"shop.shopName":        1,
		},
	})
	coupon := []bson.M{}
	if itemErr := coll.Pipe(condition).All(&coupon); itemErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(itemErr))
		return
	}

	Coupon := []CouponData{}

	if itemErr := coll.Pipe(condition).All(&Coupon); itemErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(itemErr))
		return
	}

	// nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	//
	couponItemColl := session.DB(config.DbName).C(config.DBCollection.CouponItemCollection)

	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)
	data := []bson.M{}
	for K, v := range Coupon {
		couponCount, _ := couponItemColl.Find(bson.M{
			"itemId":       v.ID,
			"itemPlayerId": bson.M{"$exists": false},
		}).Count()
		ExpiredDateTime := Coupon[K].ExpiredDate
		if couponCount > 0 {
			if v.IsReachDateTime == false {
				coupon[K]["isOutOfStock"] = false
				data = append(data, coupon[K])
			} else {
				coupon[K]["isOutOfStock"] = false
				if ExpiredDateTime.Year() > now.Year() {
					data = append(data, coupon[K])
				} else if ExpiredDateTime.Year() == now.Year() {

					if int(ExpiredDateTime.Month()) > int(now.Month()) {
						data = append(data, coupon[K])
					} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
						if ExpiredDateTime.Day() > now.Day() {
							data = append(data, coupon[K])
						} else if ExpiredDateTime.Day() == now.Day() {
							if ExpiredDateTime.Hour() > now.Hour() {
								data = append(data, coupon[K])
							} else {
								if ExpiredDateTime.Hour() == now.Hour() {
									if ExpiredDateTime.Minute() >= now.Minute() {
										data = append(data, coupon[K])

									}
								}
							}
						}
					}
				}

				// if Coupon[K].ExpiredDate.After(now) {
				// 	coupon[K]["isProcessExpired"] = false

				// 	data = append(data, coupon[K])
				// }
			}
		} else {
			if v.DiamondInclude > 0 {
				if v.IsReachDateTime == false {
					coupon[K]["isOutOfStock"] = true
					data = append(data, coupon[K])
				} else {
					coupon[K]["isOutOfStock"] = true
					if ExpiredDateTime.Year() > now.Year() {
						data = append(data, coupon[K])
					} else if ExpiredDateTime.Year() == now.Year() {
						if int(ExpiredDateTime.Month()) > int(now.Month()) {
							data = append(data, coupon[K])
						} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
							if ExpiredDateTime.Day() > now.Day() {
								data = append(data, coupon[K])
							} else if ExpiredDateTime.Day() == now.Day() {
								if ExpiredDateTime.Hour() > now.Hour() {
									data = append(data, coupon[K])
								} else {
									if ExpiredDateTime.Hour() == now.Hour() {
										if ExpiredDateTime.Minute() > now.Minute() {
											data = append(data, coupon[K])

										}
									}
								}
							}
						}
					}

					// if Coupon[K].ExpiredDate.After(now) {
					// 	coupon[K]["IsProcessExpired"] =true

					// 	data = append(data, coupon[K])
					// }
				}
			}
		}
	}

	limitData := len(data)
	if len(data) > offset+limit {
		limitData = offset + limit
	}
	count := len(data)
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v1/coupon/list?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)

	// // this should give you time in location
	// t := time.Now().In(location)
	// // location, errTime := time.LoadLocation("GMT")
	// // if errTime != nil {
	// // 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(errTime))
	// // 	return
	// // }

	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["coupon"] = data[offset:limitData]

	utils.ResponseSuccess(ctx, result)

}
