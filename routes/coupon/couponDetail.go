package coupon

import (
	"fmt"
	"inmallgame/app/utils"

	"github.com/kataras/iris"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

func GetDetail(ctx iris.Context) {
	ID := ctx.URLParamTrim("id")
	uniqueID := ctx.URLParamTrim("uniqueId")
	playerID := ctx.URLParamTrim("playerId")
	err := validation.Errors{
		"id":       validation.Validate(ID, validation.Required, is.MongoID),
		"playerId": validation.Validate(playerID, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemCollection)
	objQuery := []bson.M{}
	objQuery = append(objQuery, bson.M{"$match": bson.M{
		//"isActive": true,
		"_id":      bson.ObjectIdHex(ID),
	},
	})

	coupon := bson.M{}
	findErr := coll.Pipe(objQuery).One(&coupon)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusOK, nil, fmt.Sprint(findErr))
		return
	}

	couponItemColl := session.DB(config.DbName).C(config.DBCollection.CouponItemCollection)
	countMax, _ := couponItemColl.Find(bson.M{"itemId": bson.ObjectIdHex(ID)}).Count()
	couponCount, _ := couponItemColl.Find(bson.M{
		"itemId":       bson.ObjectIdHex(ID),
		"itemPlayerId": bson.M{"$exists": false},
	}).Count()
	coupon["couponCodeCountMax"] = countMax
	coupon["couponCodeCount"] = couponCount
	coupon["isOutOfStock"] = couponCount <= 0
	condition := []bson.M{
		{"$match": bson.M{"itemId": bson.ObjectIdHex(ID)}},
		{"$lookup": bson.M{
			"from":         config.DBCollection.MallCollection,
			"localField":   "mallId",
			"foreignField": "_id",
			"as":           "mall",
		},
		},
		{
			"$unwind": "$mall",
		},
		{"$lookup": bson.M{
			"from":         config.DBCollection.MallFloorCollection,
			"localField":   "mallFloorId",
			"foreignField": "_id",
			"as":           "mallFloor",
		},
		},
		{
			"$unwind": "$mallFloor",
		},
		{"$lookup": bson.M{
			"from":         config.DBCollection.ShopCollection,
			"localField":   "shopId",
			"foreignField": "_id",
			"as":           "shop",
		},
		},
		{
			"$unwind": "$shop",
		},
		{
			"$group": bson.M{
				"_id":      "$_id",
				"mallId":   bson.M{"$first": "$mall._id"},
				"mallName": bson.M{"$first": "$mall.mallName"},
				"shopId":   bson.M{"$first": "$shop._id"},
				"shopName": bson.M{"$first": "$shop.shopName"},
				"location": bson.M{"$first": "$shop.location"},
			},
		},
	}
	itemShop := []bson.M{}
	collItemShop := session.DB(config.DbName).C(config.DBCollection.ItemShopCollection)
	findErrShop := collItemShop.Pipe(condition).All(&itemShop)
	if findErrShop != nil {
		utils.ResponseFailure(ctx, iris.StatusOK, nil, fmt.Sprint(findErrShop))
		return
	}
	coupon["shops"] = itemShop

	shop := bson.M{}
	if coupon["shopSelect"] != nil {
		conditionShop := []bson.M{
			{"$match": bson.M{
				"_id": coupon["shopSelect"],
			},
			},
			{"$lookup": bson.M{
				"from":         config.DBCollection.MallFloorCollection,
				"localField":   "mallFloorId",
				"foreignField": "_id",
				"as":           "mallFloor",
			},
			},
			{
				"$unwind": "$mallFloor",
			},
			{"$lookup": bson.M{
				"from":         config.DBCollection.MallCollection,
				"localField":   "mallFloor.mallId",
				"foreignField": "_id",
				"as":           "mall",
			},
			},
			{
				"$unwind": "$mall",
			},
			{
				"$group": bson.M{
					"_id":      "$_id",
					"mallId":   bson.M{"$first": "$mall._id"},
					"mallName": bson.M{"$first": "$mall.mallName"},
					"shopId":   bson.M{"$first": "$_id"},
					"shopName": bson.M{"$first": "$shopName"},
					"location": bson.M{"$first": "$location"},
				},
			},
		}
		CollShops := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
		errShop := CollShops.Pipe(conditionShop).One(&shop)
		if errShop != nil {
			utils.ResponseFailure(ctx, iris.StatusOK, nil, fmt.Sprint(errShop))
			return
		}
	}
	coupon["shop"] = shop
	couponPlayer := bson.M{}

	collItemPlayer := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	objQueryPlayer := []bson.M{}

	if playerID != "" {
		objQueryPlayer = append(objQueryPlayer, bson.M{"$match": bson.M{
			"isActive": true,
			"itemId":   bson.ObjectIdHex(ID),
			"playerId": bson.ObjectIdHex(playerID),
		},
		})
		findErrPlayer := collItemPlayer.Pipe(objQueryPlayer).One(&couponPlayer)
		if findErrPlayer != nil {
			utils.ResponseSuccess(ctx, bson.M{"coupon": coupon, "couponPlayer": couponPlayer})
			return
		}
	} else if uniqueID != "" {
		objQueryPlayer = append(objQueryPlayer, bson.M{"$match": bson.M{
			"isActive": true,
			"itemId":   bson.ObjectIdHex(ID),
			"uniqueId": uniqueID,
		},
		})

		findErrPlayer := collItemPlayer.Pipe(objQueryPlayer).One(&couponPlayer)
		if findErrPlayer != nil {
			utils.ResponseSuccess(ctx, bson.M{"coupon": coupon, "couponPlayer": couponPlayer})
			return
		}
	}

	utils.ResponseSuccess(ctx, bson.M{"coupon": coupon, "couponPlayer": couponPlayer})
}
