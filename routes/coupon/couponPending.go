package coupon

import (
	"fmt"
	"inmallgame/app/utils"
	"time"

	"github.com/kataras/iris"

	"inmallgame/app/data-access"

	//"encoding/json"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

func GetCouponPending(ctx iris.Context) {
	ID := ctx.PostValue("id")
	playerID := ctx.PostValue("playerId")
	err := validation.Errors{
		"id":       validation.Validate(ID, validation.Required, is.MongoID),
		"playerId": validation.Validate(playerID, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	couponItemColl := session.DB(config.DbName).C(config.DBCollection.CouponItemCollection)

	uniqueItem := bson.M{}
	findUnique := coll.Find(bson.M{
		"itemId":   bson.ObjectIdHex(ID),
		"playerId": bson.ObjectIdHex(playerID),
	}).One(&uniqueItem)
	if findUnique == nil {
		couponCode := bson.M{}
		findErrCouponCode := couponItemColl.Find(bson.M{
			"itemId":       bson.ObjectIdHex(ID),
			"itemPlayerId": bson.M{"$exists": false},
		}).One(&couponCode)
		isOutOfStock := false
		if findErrCouponCode != nil {
			isOutOfStock = true
		}
		uniqueItem["isOutOfStock"] = isOutOfStock
		utils.ResponseSuccess(ctx, bson.M{"couponPlayer": uniqueItem})
		return
		// utils.ResponseFailure(ctx, iris.StatusBadRequest, "data unique", "data unique")
		// return
	}
	couponCode := bson.M{}
	findErrCouponCode := couponItemColl.Find(bson.M{
		"itemId":       bson.ObjectIdHex(ID),
		"itemPlayerId": bson.M{"$exists": false},
	}).One(&couponCode)
	if findErrCouponCode != nil {
		utils.ResponseSuccess(ctx, bson.M{
				"isOutOfStock":   true,
				"couponPlayer": bson.M{
			// "_id":            "",
			// "itemId":         "",
			// "playerId":       "",
			// "isActive":       true,
			// "isCoupon":       true,
			// "couponGiftId":   "",
			// "couponPassword": "",
			// "couponHashKey":  "",
			// "couponQRImage":  "",
			// "createAt":       "",
			"isOutOfStock":   true,
		},
	
	})
		return
	}

	colltem := session.DB(config.DbName).C(config.DBCollection.ItemCollection)
	item := bson.M{}
	findErrItem := colltem.Find(bson.M{
		"_id": bson.ObjectIdHex(ID),
	}).One(&item)
	if findErrItem != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not Found", fmt.Sprint(findErrItem))
		return
	}

	// dataJson := item["itemDetailEffect"].(string)
	// test := []byte(dataJson)

	// stringMap := map[string]string{}
	// errStr := json.Unmarshal(test, &stringMap)
	// if errStr != nil {

	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errStr))
	// }

	_id := bson.NewObjectId()
	errUpdate := couponItemColl.Update(bson.M{
		"_id": couponCode["_id"],
	},
		bson.M{
			"$set":         bson.M{"itemPlayerId": _id},
			"$currentDate": bson.M{"datetimeGetCoupon": true},
		})
	if errUpdate != nil {

		utils.ResponseSuccess(ctx, bson.M{"isOutOfStock": true})
		return
	}

	update := bson.M{
		"_id":            _id,
		"itemId":         bson.ObjectIdHex(ID),
		"playerId":       bson.ObjectIdHex(playerID),
		"isActive":       true,
		"isCoupon":       true,
		"isPlayerBuy":    false,
		"couponGiftId":   couponCode["code"],
		"couponPassword": ID,
		"couponHashKey":  "",
		"couponQRImage":  "",
		"createAt":       time.Now(),
	}
	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	data := bson.M{}

	findErr := coll.Find(bson.M{"_id": _id}).One(&data)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	data["isOutOfStock"] = false
	utils.ResponseSuccess(ctx, bson.M{"couponPlayer": data})
}
