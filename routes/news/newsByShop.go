package news

import (
	"fmt"
	"strconv"
	"time"

	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

//ListNewsByShop router
func ListNewsByShop(ctx iris.Context) {

	shopID := ctx.Params().Get("shopId")
	isNearby := ctx.URLParam("isNearBy")
	if validateErr := utils.ValidateMongoID("shopId", shopID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	// pageIndex, _ := ctx.URLParamInt("page")
	// pageLimit, _ := ctx.URLParamInt("limit")

	minAge, _ := ctx.URLParamInt("minAge")
	maxAge, _ := ctx.URLParamInt("maxAge")

	//offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	News := []bson.M{}

	query := bson.M{}
	query["shopId"] = bson.ObjectIdHex(shopID)
	if isNearby != "" {
		b, errBool := strconv.ParseBool(isNearby)
		if errBool != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errBool))
			return
		}
		query["isNearBy"] = b
	}
	objQuery := []bson.M{
		{"$match": query},
		// {
		// 	"$lookup": bson.M{
		// 		"from":         config.DBCollection.WildItemCollection,
		// 		"localField":   "wildItemId",
		// 		"foreignField": "_id",
		// 		"as":           "wildItem",
		// 	},
		// },
		// {"$unwind": "$wildItem"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.NewsTypeCollection,
				"localField":   "newsType",
				"foreignField": "_id",
				"as":           "newsType",
			},
		},
		{"$unwind": "$newsType"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "shopId",
				"foreignField": "_id",
				"as":           "shop",
			},
		},
		{"$unwind": "$shop"},
	}

	ageQuery := bson.M{}
	if minAge > 0 {
		ageQuery["minAge"] = bson.M{"$gte": minAge}
	}

	if maxAge > 0 {
		ageQuery["maxAge"] = bson.M{"$lte": maxAge}
	}
	ageQuery["newsActive"] = true
	objQuery = append(objQuery, bson.M{"$match": ageQuery})

	// mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	// if countErr != nil {
	// 	utils.ResponseSuccess(ctx,
	// 		bson.M{"news": News,
	// 			"pageIndex": pageIndex,
	// 			"pageLimit": pageLimit,
	// 			"pages":     1,
	// 			"paging": bson.M{
	// 				"next":     "",
	// 				"previous": "",
	// 			}})
	// 	return
	// }

	//count := mcount["count"].(int)
	//pages := utils.CalcPages(count, limit)

	//objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&News)
	if findErr != nil {
		utils.ResponseSuccess(ctx,
			bson.M{
				"news": News,
				// "pageIndex": pageIndex,
				// "pageLimit": pageLimit,
				"pages": 1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return
	}

	checkNews := []models.News{}
	pipe.All(&checkNews)

	data := []bson.M{}
	now := time.Now()
	for k, v := range News {
		if v["isReachDateTime"].(bool) == false {
			data = append(data, News[k])
		} else {
			ExpiredDateTime := checkNews[k].ExpiredDateTime

			if ExpiredDateTime.After(now) || ExpiredDateTime.Equal(now) {
				data = append(data, News[k])
			}

		}
	}
	// linkF := "/api/v1/news/byShop?page=%d&limit=%d"
	// nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := bson.M{} //utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["news"] = data
	utils.ResponseSuccess(ctx, result)
}
 