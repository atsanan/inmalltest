package news

import (
	"fmt"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//AddNews router
func AddNews(ctx iris.Context) {
	news := models.News{}
	if parseErr := ctx.ReadJSON(&news); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return

	}

	_id := bson.NewObjectId()
	update := bson.M{
		"_id":              _id,
		"newsNameEng":      news.NewsNameEng,
		"newsNameNameThai": news.NewsNameThai,
		"newsNameNameChi1": news.NewsNameChi1,
		"newsNameChi2":     news.NewsNameChi1,
		"newsDetailEng":    news.NewsDetailEng,
		"newsDetailThai":   news.NewsDetailThai,
		"newsDetailChi1":   news.NewsDetailChi1,
		"newsDetailChi2":   news.NewsDetailChi2,
		"newsActive":       news.NewsActive,
		"filenameImage1":   news.FilenameImage1,
		"filenameImage2":   news.FilenameImage2,
		"shopId":           news.ShopID,
		"location":         news.Location,
		"newsTypeId":       news.NewsTypeID,
		"wildItemId":       news.WildItemID,
		"createAt":         time.Now(),
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)

	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	News := bson.M{}
	if findErr := coll.Find(bson.M{"_id": _id}).One(&News); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, News)

}
