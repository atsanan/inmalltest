package news

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

//NewsDetail router
func NewsDetail(ctx iris.Context) {
	ID := ctx.Params().Get("id")
	if validateErr := utils.ValidateMongoID("id", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	objQuery := []bson.M{
		{"$match":bson.M{"_id":bson.ObjectIdHex(ID)}},
		// {
		// 	"$lookup": bson.M{
		// 		"from":         config.DBCollection.WildItemCollection,
		// 		"localField":   "wildItemId",
		// 		"foreignField": "_id",
		// 		"as":           "wildItem",
		// 	},
		// },
		// {"$unwind": "$wildItem"},
		// {
		// 	"$lookup": bson.M{
		// 		"from":         config.DBCollection.ShopCollection,
		// 		"localField":   "shopId",
		// 		"foreignField": "_id",
		// 		"as":           "shop",
		// 	},
		// },
		// {"$unwind": "$shop"},
		// {
		// 	"$lookup": bson.M{
		// 		"from":         config.DBCollection.NewsTypeCollection,
		// 		"localField":   "newsType",
		// 		"foreignField": "_id",
		// 		"as":           "newsType",
		// 	},
		// },
		// {"$unwind": "$newsType"},
	}

	pipe := coll.Pipe(objQuery)

	news := bson.M{}
	findErr := pipe.One(&news)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, news)

}
