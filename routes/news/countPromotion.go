package news

import (
	"fmt"
	"time"

	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"inmallgame/app/models"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

//CountPromotion router
func CountPromotion(ctx iris.Context) {
	mallID := ctx.Params().Get("mallId")
	if validateErr := utils.ValidateMongoID("mallId", mallID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	objQuery := []bson.M{
		{
			"$match": bson.M{"newsActive": true},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "shopId",
				"foreignField": "_id",
				"as":           "shop",
			},
		},
		{"$unwind": "$shop"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallFloorCollection,
				"localField":   "shop.mallFloorId",
				"foreignField": "_id",
				"as":           "mallFloor",
			},
		},
		{"$unwind": "$mallFloor"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.NewsTypeCollection,
				"localField":   "newsType",
				"foreignField": "_id",
				"as":           "newsType",
			},
		},
		{"$unwind": "$newsType"},
		{"$match": bson.M{"newsType.newsTypeName.eng": "Promotion",
			"mallFloor.mallId": bson.ObjectIdHex(mallID),
		}},
	}
	news := []bson.M{}
	checkNews := []models.News{}
	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&news)
	result := bson.M{}
	now := time.Now()
	if findErr != nil {
		result["countPromotion"] = 0
	} else {
		pipe.All(&checkNews)
		count := 0
		for k, v := range news {

			if v["isReachDateTime"].(bool) == false {
				count += 1
			} else {
				StartDateTime := checkNews[k].StartDateTime
				ExpiredDateTime := checkNews[k].ExpiredDateTime

				if StartDateTime.Before(now) || StartDateTime.Equal(now) {
					if ExpiredDateTime.After(now) || ExpiredDateTime.Equal(now) {
						count += 1
					}
				}

			}
		}
		result["countPromotion"] = count

	}

	utils.ResponseSuccess(ctx, result)
}
