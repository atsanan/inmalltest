package news

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

func sendNewsNotification(ctx iris.Context) {

	ID := ctx.Params().Get("id")
	pushTokenUser := ctx.URLParam("pushTokenUser")
	pushUrl := ctx.URLParam("pushUrl")
	if validateErr := utils.ValidateMongoID("id", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	collUser := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	collPlayer := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(ID)}},
	}

	pipe := coll.Pipe(objQuery)

	news := map[string]map[string]string{}
	findErr := pipe.One(&news)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	newsFind := map[string]string{}
	findErr2 := pipe.One(&newsFind)
	if findErr2 != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr2))
		return
	}

	opt := option.WithCredentialsFile("inmallgotchimons.json")
	app, errfire := firebase.NewApp(context.Background(), nil, opt)
	if errfire != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
	}

	context := context.Background()
	client, errMes := app.Messaging(context)
	if errMes != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
	}

	stringMap := map[string]string{
		"type":           "news",
		"id":             ID,
		"titleEng":       news["newsName"]["eng"],
		"bodyEng":        news["newsDetail"]["eng"],
		"filenameImage1": newsFind["filenameImage1"],
		"filenameImage2": newsFind["filenameImage2"],
	}
	if pushUrl != "" {
		stringMap["pushUrl"] = pushUrl
	}
	//	condition := "'stock-GOOG' in topics || 'industry-tech' in topics"
	result := bson.M{}
	if pushTokenUser == "" {
		message := &messaging.Message{
			Notification: &messaging.Notification{
				Title: news["newsName"]["eng"],
				Body:  news["newsDetail"]["eng"],
				//Data:  dataJson,
			},
			Data:  stringMap,
			Topic: "InMallGotchiMons",
			//Condition: condition,
			//Topic:topic,
			//Condition: condition,
		}
		response, errSend := client.Send(context, message)
		if errSend != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errSend))
			return
		}
		mongoErr := collPlayer.Update(bson.M{}, bson.M{
			"$set":         bson.M{"newsLastId": bson.ObjectIdHex(ID)},
			"$currentDate": bson.M{"lastModified": true}})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			return
		}

		result["response"] = response

	} else {
		message := &messaging.Message{
			Notification: &messaging.Notification{
				Title: news["newsName"]["eng"],
				Body:  news["newsDetail"]["eng"],
				//Data:  dataJson,
			},
			Data:  stringMap,
			Token: pushTokenUser,
			//Topic: "InMallGotchiMons",
			//Condition: condition,
			//Topic:topic,
			//Condition: condition,
		}

		response, errSend := client.Send(context, message)
		if errSend != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errSend))
			return
		}

		user := bson.M{}
		findUser := collUser.Find(bson.M{"pushMessagesAccessToken": pushTokenUser}).One(&user)
		if findUser != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr2))
			return
		}

		mongoErr := collPlayer.Update(bson.M{"userId": user["_id"]}, bson.M{
			"$set":         bson.M{"newsLastId": bson.ObjectIdHex(ID)},
			"$currentDate": bson.M{"lastModified": true}})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			return
		}
		result["response"] = response

	}

	result["pushTokenUser"] = pushTokenUser
	result["stringMap"] = stringMap
	utils.ResponseSuccess(ctx, result)
	ctx.Redirect("https://www.google.co.th")

}
