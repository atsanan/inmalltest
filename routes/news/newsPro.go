package news

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

//MonsterPromotion router
func MonsterPromotionFigBug(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	if validateErr := utils.ValidateMongoID("playerId", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}
	// pageIndex, _ := ctx.URLParamInt("page")
	// pageLimit, _ := ctx.URLParamInt("limit")

	//offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	collShop := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	ChannelCollection := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)
	// mallFloorCollection := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	// mallCollection := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	News := []bson.M{}

	objQuery := []bson.M{
		{
			"$match": bson.M{"newsActive": true},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.NewsTypeCollection,
				"localField":   "newsType",
				"foreignField": "_id",
				"as":           "newsType",
			},
		},
		{"$unwind": "$newsType"},
		{
			"$match": bson.M{"newsType.newsTypeName.eng": "Promotion"},
		},
	}

	// mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	// if countErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
	// 	return
	// }

	//count := mcount["count"].(int)
	//pages := utils.CalcPages(count, limit)

	//objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&News)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}
	data := []bson.M{}
	for k, v := range News {

		shop := bson.M{}
		err := collShop.Find(bson.M{"_id": v["shopId"]}).One(&shop)
		if err == nil {
			channel := []bson.M{}
			ChannelCollection.Find(bson.M{"shopId": shop["_id"]}).All(&channel)

			if len(channel) > 0 {

				channelCheck := []bson.M{}
				for kc, vc := range channel {
					jc := []bson.M{}
					joinChannelColl.Pipe([]bson.M{
						{
							"$match": bson.M{"channelId": vc["_id"], "playerId": bson.ObjectIdHex(playerID)},
						},
					}).All(&jc)
					channel[kc]["joinChannel"] = jc
					if len(jc) > 0 {
						channelCheck = append(channelCheck, bson.M{"check": 1})
					}
				}
				shop["channel"] = channel

				News[k]["shop"] = shop
				if len(channelCheck) > 0 {
					data = append(data, News[k])

				}

			}
		}
	}

	result := map[string]interface{}{
		"news": data,
		// "paging": map[string]interface{}{
		// 	"next":     nextPage,
		// 	"previous": previousPage,
		// },
	}

	utils.ResponseSuccess(ctx, result)
}
