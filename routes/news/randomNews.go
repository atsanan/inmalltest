package news

import (

	// "log"

	"math/rand"
	"time"

	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	//"math/rand"
	"strconv"
)

func RanDomNews(ctx iris.Context) {
	newsType := ctx.Params().Get("newsType")

	if validateErr := utils.ValidateMongoID("newsType", newsType); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	amount := ctx.URLParamTrim("amount")
	mallID := ctx.URLParam("mallId")
	err := validation.Errors{
		"amount": validation.Validate(amount, validation.Required, is.Int),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	amountRandom, _ := strconv.Atoi(amount)

	//  pageIndex, _ := ctx.URLParamInt("page")
	// pageLimit, _ := ctx.URLParamInt("limit")

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	News := []bson.M{}

	condition := bson.M{"newsType": bson.ObjectIdHex(newsType), "newsActive": true}
	if mallID != "" {
		collShop := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
		itemShop := []bson.M{}
		if mongoErr := collShop.Pipe([]bson.M{
			{
				"$lookup": bson.M{
					"from":         config.DBCollection.MallFloorCollection,
					"localField":   "mallFloorId",
					"foreignField": "_id",
					"as":           "mallFloor",
				},
			},
			{"$unwind": "$mallFloor"},
			{
				"$match": bson.M{"mallFloor.mallId": bson.ObjectIdHex(mallID)},
			},
		}).All(&itemShop); mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
			return
		}
		oids := make([]bson.ObjectId, len(itemShop))
		if len(itemShop) > 0 {
			for i := range itemShop {
				oids[i] = itemShop[i]["_id"].(bson.ObjectId)
			}
		}
		condition["shopId"] = bson.M{"$in": oids}
		// objQuery = append(objQuery,
		// 	bson.M{
		// 		"$match": bson.M{"shopId": },
		// 	})
	}

	objQuery := []bson.M{
		{"$match": condition},
		//{"$sample": bson.M{"size": amountRandom}},
	}

	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&News)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}
	checkNews := []models.News{}
	pipe.All(&checkNews)
	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)
	data := []bson.M{}
	for k, v := range News {

		if v["isReachDateTime"].(bool) == false {
			data = append(data, News[k])
		} else {
			//StartDateTime := checkNews[k].StartDateTime
			ExpiredDateTime := checkNews[k].ExpiredDateTime
			if ExpiredDateTime.Year() > now.Year() {
				data = append(data, News[k])
			} else if ExpiredDateTime.Year() == now.Year() {

				if int(ExpiredDateTime.Month()) > int(now.Month()) {
					data = append(data, News[k])
				} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
					if ExpiredDateTime.Day() > now.Day() {
						data = append(data, News[k])
					} else if ExpiredDateTime.Day() == now.Day() {
						if ExpiredDateTime.Hour() > now.Hour() {
							data = append(data, News[k])
						} else {
							if ExpiredDateTime.Hour() == now.Hour() {

								if ExpiredDateTime.Minute() > now.Minute() {
									data = append(data, News[k])
								}
							}
						}
					}
				}
			}
		}
	}
	newsData := []bson.M{}

	if len(data) > 0 {
		for i := 0; i < amountRandom; i++ {
		rand := rand.Intn(len(data))
		newsData = append(newsData, data[rand])
		data = append(data[:rand], data[rand+1:]...)

		//data = append(data[:rand], data[rand+1:]...)
		// 	sum += i
		}
	}

	// _, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	// if countErr != nil {

	// 	result := map[string]interface{}{
	// 		"news": news,
	// 	}

	// 	utils.ResponseSuccess(ctx, result)
	// }

	result := map[string]interface{}{
		"news": newsData,
		//"condition": condition,
	}

	utils.ResponseSuccess(ctx, result)
}
