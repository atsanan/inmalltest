package news

import (
	"fmt"
	"strconv"

	// "log"
	"time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

//ListNews router
func ListNews(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	minAge, _ := ctx.URLParamInt("minAge")
	maxAge, _ := ctx.URLParamInt("maxAge")

	search := ctx.URLParam("search")
	mallID := ctx.URLParam("mallId")
	mainTypes := ctx.URLParam("mainTypes")
	lat := ctx.URLParamTrim("lat")
	long := ctx.URLParamTrim("long")
	distance := ctx.URLParamIntDefault("distance", 5000)
	isHighlight := ctx.URLParamTrim("isHighlight")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	objQuery := []bson.M{}
	News := []bson.M{}

	if long != "" && lat != "" {
		longConv, _ := strconv.ParseFloat(long, 64)
		latConv, _ := strconv.ParseFloat(lat, 64)
		var coordinates [2]float64
		coordinates[0] = longConv
		coordinates[1] = latConv
		objQuery = append(objQuery, bson.M{
			"$geoNear": bson.M{
				"near":          bson.M{"type": "Point", "coordinates": coordinates},
				"maxDistance":   distance,
				"distanceField": "dist.calculated",
				"spherical":     true,
			},
		})

	}else{

		objQuery = append(objQuery, bson.M{"$sort": bson.M{
			"order":-1,
		}})
	}

	boolIsHighlight, _ := strconv.ParseBool(isHighlight)
	if isHighlight != "" {
		objQuery = append(objQuery, bson.M{
			"$match": bson.M{
				"isHighlight": boolIsHighlight,
			},
		})
	}

	if mainTypes != "" {
		objQuery = append(objQuery,
			bson.M{"$match": bson.M{"mainTypes": mainTypes}})
	}

	objQuery = append(objQuery,
		bson.M{
			"$match": bson.M{"newsActive": true},
		},
		bson.M{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "shopId",
				"foreignField": "_id",
				"as":           "shop",
			},
		},
		bson.M{"$unwind": "$shop"},
		bson.M{
			"$lookup": bson.M{
				"from":         config.DBCollection.NewsTypeCollection,
				"localField":   "newsType",
				"foreignField": "_id",
				"as":           "newsType",
			},
		},
		bson.M{"$unwind": "$newsType"},
		bson.M{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallFloorCollection,
				"localField":   "shop.mallFloorId",
				"foreignField": "_id",
				"as":           "mallFloor",
			},
		},
		bson.M{"$unwind": "$mallFloor"},
	)

	// })

	ageQuery := bson.M{}
	if minAge > 0 {
		ageQuery["minAge"] = bson.M{"$gte": minAge}
	}

	if maxAge > 0 {
		ageQuery["maxAge"] = bson.M{"$lte": maxAge}
	}

	objQuery = append(objQuery, bson.M{"$match": ageQuery})

	if mallID != "" {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"mallFloor.mallId": bson.ObjectIdHex(mallID)}})
	}

	if search != "" {
		//	baseQuery = append(baseQuery, bson.M{"$match": bson.M{"shop.shopName.eng": bson.M{"$regex":search}}})
		objQuery = append(objQuery, bson.M{"$match": bson.M{"$or": []bson.M{
			bson.M{"shop.shopName.eng": bson.M{"$regex": search, "$options": "i"}},
			bson.M{"shop.shopName.thai": bson.M{"$regex": search, "$options": "i"}},
			bson.M{"shop.shopName.chi1": bson.M{"$regex": search, "$options": "i"}},
			bson.M{"shop.shopName.chi2": bson.M{"$regex": search, "$options": "i"}},
		},
		},
		},
		)
	}

	// mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	// if countErr != nil {
	// 	result := map[string]interface{}{
	// 		"pages":     "",
	// 		"pageIndex": page,
	// 		"pageLimit": limit,
	// 		"news":      News,
	// 		"paging": map[string]interface{}{
	// 			"next":     "",
	// 			"previous": "",
	// 		},
	// 	}

	// 	utils.ResponseSuccess(ctx, result)
	// 	return
	// }

	//objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&News)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	linkF := "api/v1/news?page=%d&limit=%d"
	if mallID != "" {
		linkF += "&mallId=" + mallID
	}

	if search != "" {
		linkF += "&search=" + search
	}

	if mainTypes != "" {
		linkF += "&mainTypes=" + mainTypes
	}
	if long != "" && lat != "" {

		dis := strconv.Itoa(distance)
		linkF += "&long=" + long + "&lat=" + lat + "&distance=" + dis
	}
	checkNews := []models.News{}
	pipe.All(&checkNews)
	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)
	data := []bson.M{}
	for k, v := range News {

		if v["isReachDateTime"].(bool) == false {
			data = append(data, News[k])
		} else {
			//StartDateTime := checkNews[k].StartDateTime
			ExpiredDateTime := checkNews[k].ExpiredDateTime
			if ExpiredDateTime.Year() > now.Year() {
				data = append(data, News[k])
			} else if  ExpiredDateTime.Year() == now.Year(){
			
				if int(ExpiredDateTime.Month()) > int(now.Month()) {
					data = append(data, News[k])
				} else if int(ExpiredDateTime.Month()) == int(now.Month()){
					if ExpiredDateTime.Day() > now.Day() {
						data = append(data, News[k])
					}else if ExpiredDateTime.Day() == now.Day(){
						if ExpiredDateTime.Hour() > now.Hour() {
							data = append(data, News[k])
						}else{
							if ExpiredDateTime.Hour() == now.Hour() {
		
								if ExpiredDateTime.Minute() > now.Minute() {
									data = append(data, News[k])
								}
							}
						}
					}
				} 
			}
		}
	}
	count := len(data)
	pages := utils.CalcPages(count, limit)

	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)

	limitData := len(data)
	if len(data) > offset+limit {
		limitData = offset + limit
	}
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"news":      data[offset:limitData],
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}

// ListPromotion router
func ListPromotion(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	minAge, _ := ctx.URLParamInt("minAge")
	maxAge, _ := ctx.URLParamInt("maxAge")

	search := ctx.URLParam("search")
	mallID := ctx.URLParam("mallId")
	mainTypes := ctx.URLParam("mainTypes")
	_, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	News := []bson.M{}

	objQuery := []bson.M{
		{
			"$match": bson.M{"newsActive": true},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "shopId",
				"foreignField": "_id",
				"as":           "shop",
			},
		},
		{"$unwind": "$shop"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.NewsTypeCollection,
				"localField":   "newsType",
				"foreignField": "_id",
				"as":           "newsType",
			},
		},
		{"$unwind": "$newsType"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallFloorCollection,
				"localField":   "shop.mallFloorId",
				"foreignField": "_id",
				"as":           "mallFloor",
			},
		},
		{"$unwind": "$mallFloor"},
	}

	ageQuery := bson.M{}
	if minAge > 0 {
		ageQuery["minAge"] = bson.M{"$gte": minAge}
	}

	if maxAge > 0 {
		ageQuery["maxAge"] = bson.M{"$lte": maxAge}
	}

	objQuery = append(objQuery, bson.M{"$match": ageQuery})

	if mallID != "" {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"mallFloor.mallId": bson.ObjectIdHex(mallID)}})
	}

	if search != "" {
		//	baseQuery = append(baseQuery, bson.M{"$match": bson.M{"shop.shopName.eng": bson.M{"$regex":search}}})
		objQuery = append(objQuery, bson.M{"$match": bson.M{"$or": []bson.M{
			bson.M{"shop.shopName.eng": bson.M{"$regex": search, "$options": "i"}},
			bson.M{"shop.shopName.thai": bson.M{"$regex": search, "$options": "i"}},
			bson.M{"shop.shopName.chi1": bson.M{"$regex": search, "$options": "i"}},
			bson.M{"shop.shopName.chi2": bson.M{"$regex": search, "$options": "i"}},
		},
		},
		},
		)
	}

	if mainTypes != "" {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"mainTypes": mainTypes}})
	}

	mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	if countErr != nil {
		result := map[string]interface{}{
			"pages":     "",
			"pageIndex": page,
			"pageLimit": limit,
			"promotion": News,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}

	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	//objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&News)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	linkF := "api/v2/promotion/list?page=%d&limit=%d"
	if mallID != "" {
		linkF += "&mallId=" + mallID
	}

	if search != "" {
		linkF += "&search=" + search
	}

	if mainTypes != "" {
		linkF += "&mainTypes=" + mainTypes
	}

	checkNews := []models.News{}
	pipe.All(&checkNews)
	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)
	data := []bson.M{}
	for k, _ := range News {
		isReachDateTime := checkNews[k].IsReachDateTime
		if isReachDateTime == false {
			data = append(data, News[k])
		} else {
			ExpiredDateTime := checkNews[k].ExpiredDateTime
			if ExpiredDateTime.Year() > now.Year() {
				data = append(data, News[k])
			} else if  ExpiredDateTime.Year() == now.Year(){
			
				if int(ExpiredDateTime.Month()) > int(now.Month()) {
					data = append(data, News[k])
				} else if int(ExpiredDateTime.Month()) == int(now.Month()){
					if ExpiredDateTime.Day() > now.Day() {
						data = append(data, News[k])
					}else if ExpiredDateTime.Day() == now.Day(){
						if ExpiredDateTime.Hour() > now.Hour() {
							data = append(data, News[k])
						}else{
							if ExpiredDateTime.Hour() == now.Hour() {
		
								if ExpiredDateTime.Minute() > now.Minute() {
									data = append(data, News[k])
								}
							}
						}
					}
				} 
			}

		}
	}
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"promotion": data,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}
