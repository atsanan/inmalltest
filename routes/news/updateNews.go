package news

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/mitchellh/mapstructure"
)

func validateParams(newsID string) error {
	err := validation.Errors{
		"id": validation.Validate(newsID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		return err
	}
	return nil
}

//UpdateNews router
func UpdateNews(ctx iris.Context) {
	newsID := ctx.Params().Get("id")

	if err := validateParams(newsID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	update := map[string]interface{}{}
	if parseErr := ctx.ReadJSON(&update); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(parseErr))
		return
	}

	news := models.News{}
	decodeErr := mapstructure.Decode(update, &news)
	if decodeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "decode error", fmt.Sprint(decodeErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)

	// if update["wildItemId"] != "" {
	// 	WideItemID, _ := update["wildItemId"].(string)
	// 	update["wildItemId"] = bson.ObjectIdHex(WideItemID)
	// }

	// if update["shopId"] != "" {
	// 	shopID, _ := update["shopId"].(string)
	// 	update["shopId"] = bson.ObjectIdHex(shopID)
	// }

	// if update["newsType"] != "" {
	// 	newsType, _ := update["newsType"].(string)
	// 	update["newsType"] = bson.ObjectIdHex(newsType)
	// }

	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(newsID)}, bson.M{
		"$set":         update,
		"$currentDate": bson.M{"lastModified": true}})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	News := models.News{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(newsID)}).One(&News)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, News)
}
