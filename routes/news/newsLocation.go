package news

import (
	"fmt"
	// "log"
	//"time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
	"strconv"
)

//PromotionByLocation router
func PromotionByLocation(ctx iris.Context) {
	lat := ctx.URLParamTrim("lat")
	long := ctx.URLParamTrim("long")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	// if pageLimit == 10 {
	// 	pageLimit = 20
	// }
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)
	err := validation.Errors{
		"lat":  validation.Validate(lat, validation.Required, is.Float),
		"long": validation.Validate(long, validation.Required, is.Float),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	longConv, _ := strconv.ParseFloat(long, 64)
	latConv, _ := strconv.ParseFloat(lat, 64)

	var coordinates [2]float64
	coordinates[0] = longConv
	coordinates[1] = latConv

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	news := []bson.M{}
	objQuery := []bson.M{
		{
			"$geoNear": bson.M{
				"near":          bson.M{"type": "Point", "coordinates": coordinates},
				"maxDistance":   5000,
				"distanceField": "dist.calculated",
				"spherical":     true,
			},
			
		},
		{"$sort": bson.M{"dist.calculated": 1}},
		{
			"$match":bson.M{"newsActive": true},
		},
	}
	mcount, _ := utils.CalcPipeCount(ctx, objQuery, coll)
	pages := utils.CalcPagesByPipeResult(mcount, limit)
	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)
	if mongoErr := pipe.All(&news); mongoErr != nil {
		result := map[string]interface{}{
			"pages":     pages,
			"pageIndex": page,
			"pageLimit": limit,
			"promotion": news,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)

	}

	originalLink := fmt.Sprintf("api/v2/promotion/byLocation/?lat=%s&long=%s", lat, long)
	linkF := originalLink + "?page=%d&limit=%d"

	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["promotion"] = news

	utils.ResponseSuccess(ctx, result)
}
