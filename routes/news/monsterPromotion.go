package news

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

//MonsterPromotion router
func MonsterPromotion(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	if validateErr := utils.ValidateMongoID("playerId", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}
	// pageIndex, _ := ctx.URLParamInt("page")
	// pageLimit, _ := ctx.URLParamInt("limit")

	//offset, limit, _ := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsCollection)
	collShop := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	ChannelCollection := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)
	mallFloorCollection := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	mallCollection := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	News := []bson.M{}

	objQuery := []bson.M{
		{
			"$match": bson.M{"newsActive": true},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.NewsTypeCollection,
				"localField":   "newsType",
				"foreignField": "_id",
				"as":           "newsType",
			},
		},
		{"$unwind": "$newsType"},
		{
			"$match": bson.M{"newsType.newsTypeName.eng": "Promotion"},
		},
	}

	// mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	// if countErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
	// 	return
	// }

	//objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&News)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}
	data := []bson.M{}
	for k, v := range News {

		shop := bson.M{}
		err := collShop.Find(bson.M{"_id": v["shopId"]}).One(&shop)
		if err == nil {

			channelCheck := []bson.M{}
			channel := []bson.M{}
			ChannelCollection.Find(bson.M{"shopId": shop["_id"]}).All(&channel)
			if len(channel) > 0 {

				for kc, vc := range channel {
					jc := []bson.M{}
					joinChannelColl.Pipe([]bson.M{
						{
							"$match": bson.M{"channelId": vc["_id"]},
						},
						{
							"$lookup": bson.M{
								"from":         config.DBCollection.MonsterPlayerCollection,
								"localField":   "mPlayerId",
								"foreignField": "_id",
								"as":           "mPlayerId",
							},
						},
						{"$unwind": "$mPlayerId"},
						{
							"$match": bson.M{"mPlayerId.playerId": bson.ObjectIdHex(playerID)},
						},
					}).All(&jc)
					channel[kc]["joinChannel"] = jc
					if len(jc) > 0 {
						channelCheck = append(channelCheck, bson.M{"check": 1})
					}
				}

				shop["channel"] = channel
				mallFloor := bson.M{}
				errMallFloor := mallFloorCollection.Find(bson.M{"_id": shop["mallFloorId"]}).One(&mallFloor)

				mall := bson.M{}

				if errMallFloor == nil {
					mallCollection.Find(bson.M{"_id": mallFloor["mallId"]}).One(&mall)
				}

				News[k]["mall"] = mall
				News[k]["mallFloor"] = mallFloor
				News[k]["shop"] = shop
				if len(channelCheck) > 0 {
					data = append(data, News[k])
				}
			}
		}
	}

	// pages := utils.CalcPages(len(data), limit)
	// originalLink := fmt.Sprintf("/api/v1/news/monsterPromotion/%s", playerID)
	// linkF := originalLink + "?page=%d&limit=%d"
	// nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	// result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result := bson.M{}
	//if len(data) > offset+limit {
	result["news"] = data
	// } else {
	// 	if offset < len(data) {
	// 		result["news"] = data[offset:len(data)]
	// 	} else {
	// 		result["news"] = []bson.M{}
	// 	}
	// }

	utils.ResponseSuccess(ctx, result)
}
