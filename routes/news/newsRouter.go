package news

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Get("/", auth.TokenMDW, ListNews)
	routes.Get("/list", auth.TokenMDW, ListPromotion)
	routes.Post("/add", auth.TokenMDW, AddNews)
	routes.Get("/sendNews/{id:string}", sendNewsNotification)
	routes.Post("/{id:string}", auth.TokenMDW, UpdateNews)
	routes.Get("/byShop/{shopId:string}", auth.TokenMDW, ListNewsByShop)
	routes.Get("/{id:string}", auth.TokenMDW, NewsDetail)
	routes.Get("/random/{newsType:string}", auth.TokenMDW, RanDomNews)
	routes.Get("/byLocation/", auth.TokenMDW, PromotionByLocation)
	routes.Get("/monsterPromotion/{playerId:string}", auth.TokenMDW, MonsterPromotion)
	routes.Get("/monsterPromotionFigBug/{playerId:string}", auth.TokenMDW, MonsterPromotionFigBug)
	routes.Get("/countPromotion/{mallId:string}", auth.TokenMDW, CountPromotion)

	//routes.Get("/monsterCoupon/{playerId:string}", auth.TokenMDW, monsterCoupons)

}
