package tellFriend

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	validation "github.com/go-ozzo/ozzo-validation"


	// // "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
	"github.com/go-ozzo/ozzo-validation/is"
	// "log"
	// "encoding/json"
	"strconv"
)


// AddAward route.
func AddAward(ctx iris.Context) {
	playerID := ctx.PostValue("playerId")
	tellFriendRewardCount := ctx.PostValue("tellFriendRewardCount")
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required,is.MongoID),
		"tellFriendRewardCount": validation.Validate(tellFriendRewardCount, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	collPlayer := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	tellFriendRewardCountUpdate, err := strconv.Atoi(tellFriendRewardCount)
	if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "coin should be integer", "")
			return
	}
	mongoPlayerErr := collPlayer.Update(bson.M{"_id": bson.ObjectIdHex(playerID),}, bson.M{
		"$set": bson.M{
			"tellFriendRewardCount":tellFriendRewardCountUpdate,
		},
	})
	
	if mongoPlayerErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update Player Fail", fmt.Sprint(mongoPlayerErr))
		return
	}

	playerReturn:=PlayerTellFriend{}
	errPlayerReturn := collPlayer.Find(bson.M{"_id":bson.ObjectIdHex(playerID)}).One(&playerReturn)
	if errPlayerReturn != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errPlayerReturn))
		return
	}
	utils.ResponseSuccess(ctx, bson.M{
		"player":playerReturn,
		"message":"Success Update tellFriendRewardCount"})
}