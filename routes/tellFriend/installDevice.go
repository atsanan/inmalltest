package tellFriend

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	validation "github.com/go-ozzo/ozzo-validation"


	// // "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	"time"
	// "encoding/json"
	// "strconv"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"golang.org/x/net/context"
	"google.golang.org/api/option"

	
)

//News sturct
type Device struct {
	PlayerID                 bson.ObjectId `json:"playerId" bson:"playerId"`
	InstallDateTime        time.Time `json:"installDateTime" bson:"installDateTime"`
}
// InstallDevice route.
func InstallDevice(ctx iris.Context) {
	deviceUnique := ctx.PostValue("deviceUnique")
	err := validation.Errors{
		"deviceUnique": validation.Validate(deviceUnique, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.TellFriendLinkCollection)
	collPlayer := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	tellFriend := []bson.M{}
    errTellFriend:= coll.Pipe(
	[]bson.M{
		{"$match":bson.M{
			"deviceUnique":   deviceUnique,
			"installDateTime":time.Time{},
			"clicKLinkDateTime":bson.M{"$ne":time.Time{}},
		},
		},
		{"$lookup": bson.M{
			"from":         config.DBCollection.PlayerCollection,
			"localField":   "playerId",
			"foreignField": "_id",
			"as":           "player",
		},
		},
		{"$unwind": "$player"},
		{"$lookup": bson.M{
			"from":         config.DBCollection.UserCollection,
			"localField":   "player.userId",
			"foreignField": "_id",
			"as":           "user",
		},
		},
		{"$unwind": "$user"},
		{"$addFields":bson.M{
			"pushMessagesAccessToken":bson.M{"$concat": []string{"$user.pushMessagesAccessToken",},},
			"playerId":"$player._id", 
			"installDateTime":"$installDateTime",
		},
		},
		{
			"$project": bson.M{
				"_id":                  1,
				"pushMessagesAccessToken":1,
				"playerId":        1,
				"installDateTime":1,
			},
		},	
	}).All(&tellFriend)
	if errTellFriend != nil {
		utils.ResponseFailure(ctx, iris.StatusNotFound, nil, fmt.Sprint(errTellFriend))
		return
	}


	//player:=bson.M{}
	registrationTokens := []string{}
	playerID := []bson.ObjectId{}
	for _, player := range tellFriend{
		playerID=append(playerID,player["playerId"].(bson.ObjectId))
		registrationTokens = append(registrationTokens, player["pushMessagesAccessToken"].(string))
	}
	collSetting := session.DB(config.DbName).C(config.DBCollection.SettingCollection)
	setting:=bson.M{}

    querySetting:=[]bson.M{
			{"$addFields":bson.M{
				"tellFriendAddCoinStep3Message":
				bson.M{"$concat": []string{"$tellFriendAddCoinStep3Message.eng",},}, 
			},
			},
			{
				"$project": bson.M{
					"_id":                  1,
					"tellFriendAddCoinStep3Message":        1,
					"tellFriendAddCoinStep3Value":        1,
				},
			},
		
	}
	errSetting := collSetting.Pipe(querySetting).One(&setting)
	if errSetting != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errSetting))
		return
	}

	// now := time.Now()
	// if tellFriend.InstallDateTime.Year() < now.Year(){
		_,mongoErr := coll.UpdateAll(bson.M{"deviceUnique":   deviceUnique,"installDateTime":time.Time{},}, bson.M{
			"$currentDate": bson.M{"installDateTime": true}})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			return
		}

		_,mongoPlayerErr := collPlayer.UpdateAll(bson.M{"_id":bson.M{ "$in":   playerID,}}, bson.M{
			"$inc":         bson.M{"coin": setting["tellFriendAddCoinStep3Value"].(int)},
		})
		if mongoPlayerErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update Player Fail", fmt.Sprint(mongoPlayerErr))
			return
		}

	opt := option.WithCredentialsFile("inmallgotchimons.json")
	app, errfire := firebase.NewApp(context.Background(), nil, opt)
	if errfire != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
	}

	context := context.Background()
	client, errMes := app.Messaging(context)
	if errMes != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
	}

	stringMap := map[string]string{
		"type":setting["tellFriendAddCoinStep3Message"].(string),
	}

		message := &messaging.MulticastMessage{
			Notification: &messaging.Notification{
				Title: setting["tellFriendAddCoinStep3Message"].(string),
				Body:  setting["tellFriendAddCoinStep3Message"].(string),
				//Data:  dataJson,
			},
			Data:  stringMap,
			Tokens: registrationTokens,
		}

		response, errSend := client.SendMulticast(context, message)
		if errSend != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint("user un click link"))
			return
		}
		utils.ResponseSuccess(ctx, bson.M{
			"isSend":true,
			"message":"Success Save Install",
			"response":response,
			"tellFriend":tellFriend,
			
		})
		return
	// }

	// utils.ResponseSuccess(ctx, bson.M{
	// 	"isSend":false,
	// 	"message":"Device Install Fail",})
	

}