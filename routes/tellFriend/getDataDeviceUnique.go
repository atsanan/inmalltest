package tellFriend

import (
	"inmallgame/app/utils"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func GetDataDeviceUnique(ctx iris.Context){
	
	utils.ResponseSuccess(ctx, bson.M{ "ip": ctx.RemoteAddr(),})

}
