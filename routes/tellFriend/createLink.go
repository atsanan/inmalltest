package tellFriend

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"math/rand"

	// // "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)
type PlayerTellFriend struct {
	PlayerID                 bson.ObjectId `json:"_id" bson:"_id"`
	TellFriendRewardCount int `json:"tellFriendRewardCount" bson:"tellFriendRewardCount"`
	TellFriendCount int `json:"tellFriendCount" bson:"tellFriendCount"`
	TellFriendRewardDateTime time.Time `json:"tellFriendRewardDateTime" bson:"tellFriendRewardDateTime"`
}
// CreateLink route.
func CreateLink(ctx iris.Context) {
	playerID := ctx.PostValue("playerId")
	blockDeviceUnique := ctx.PostValue("blockDeviceUnique")
	err := validation.Errors{
	//	"blockDeviceUnique": validation.Validate(blockDeviceUnique, validation.Required,),
		"playerId": validation.Validate(playerID, validation.Required,is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.TellFriendLinkCollection)

	collPlayer := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	player:=PlayerTellFriend{}
	errPlayer := collPlayer.Find(bson.M{"_id":bson.ObjectIdHex(playerID)}).One(&player)
	if errPlayer != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errPlayer))
		return
	}

	dateLastLoginReward := player.TellFriendRewardDateTime
	dateLastLoginRewardCheck := dateLastLoginReward.Format("2006-01-02")

	now := time.Now()
	nowCheck := now.Format("2006-01-02")

	if nowCheck != dateLastLoginRewardCheck {
		mongoPlayerErr := collPlayer.Update(bson.M{"_id": bson.ObjectIdHex(playerID),}, bson.M{
			"$currentDate": bson.M{"tellFriendRewardDateTime": true},
			"$inc": bson.M{
				"tellFriendCount":1,
			},
		})
		
		if mongoPlayerErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update Player Fail", fmt.Sprint(mongoPlayerErr))
			return
		}	
	}

	
	playerReturn:=PlayerTellFriend{}
	errPlayerReturn := collPlayer.Find(bson.M{"_id":bson.ObjectIdHex(playerID)}).One(&playerReturn)
	if errPlayerReturn != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errPlayerReturn))
		return
	}
	linkListName:=RandomString(13)
	
	save := bson.M{
		"linkListName":linkListName,
		"playerId":bson.ObjectIdHex(playerID),
		"deviceUnique":nil,
		"blockDeviceUnique":blockDeviceUnique,
		"clicKLinkDateTime":time.Time{},
		"installDateTime":time.Time{},
		"createAt":time.Now(),
	}

	if blockDeviceUnique !=""{
		save["blockDeviceUnique"]=blockDeviceUnique
	}
	mongoErr := coll.Insert(save)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{
		"linkListName":linkListName,
		"player":playerReturn,
	})
	//detail := ctx.PostValue("detail")
}

func RandomString(n int) string {
	var letter = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	b := make([]rune, n)
	for i := range b {
		b[i] = letter[rand.Intn(len(letter))]
	}
	return string(b)
}