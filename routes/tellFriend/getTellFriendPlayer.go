package tellFriend

import (
	"fmt"
	//"time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	

	// // "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

func GetTellFrinedPlayer(ctx iris.Context) {
	playerID := ctx.Params().Get("id")
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required,is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	collPlayer := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	player:=PlayerTellFriend{}
	errPlayer := collPlayer.Find(bson.M{"_id":bson.ObjectIdHex(playerID)}).One(&player)
	if errPlayer != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errPlayer))
		return
	}


	utils.ResponseSuccess(ctx, bson.M{
		"player":player,
	})

}