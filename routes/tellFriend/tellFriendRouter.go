package tellFriend

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// // For authorize user.
	// routes.Get("/list", auth.TokenMDW, GetStoreList)
	routes.Post("/createLink", auth.TokenMDW, CreateLink)
	routes.Post("/installDevice", auth.TokenMDW, InstallDevice)
	routes.Post("/editReward", auth.TokenMDW, AddAward)
	routes.Get("/{id:string}", auth.TokenMDW, GetTellFrinedPlayer)
	routes.Get("/notification/{id:string}", SendNotification)


	
}
