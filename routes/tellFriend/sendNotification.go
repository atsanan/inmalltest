package tellFriend

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	// // "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	//"time"
	// "encoding/json"
	// "strconv"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

func SendNotification(ctx iris.Context) {

	playerID := ctx.Params().Get("id")
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required,is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	collPlayer := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	player:=bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(playerID)}},
		{"$lookup": bson.M{
			"from":         config.DBCollection.UserCollection,
			"localField":   "userId",
			"foreignField": "_id",
			"as":           "user",
		},
		},
		{"$unwind": "$user"},
		{"$addFields":bson.M{
			"pushMessagesAccessToken":
			bson.M{"$concat": []string{"$user.pushMessagesAccessToken",},}, 
		},
		},
		{
			"$project": bson.M{
				"_id":                  1,
				"pushMessagesAccessToken":        1,
			},
		},	
	}

	errPlayerReturn := collPlayer.Pipe(objQuery).One(&player)
	if errPlayerReturn != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errPlayerReturn))
		return
	}

	collSetting := session.DB(config.DbName).C(config.DBCollection.SettingCollection)
	setting:=bson.M{}

    querySetting:=[]bson.M{
			{"$addFields":bson.M{
				"tellFriendAddCoinStep2Message":
				bson.M{"$concat": []string{"$tellFriendAddCoinStep2Message.eng",},}, 
			},
			},
			{
				"$project": bson.M{
					"_id":                  1,
					"tellFriendAddCoinStep2Message":        1,
				},
			},
		
	}
	errSetting := collSetting.Pipe(querySetting).One(&setting)
	if errSetting != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errSetting))
		return
	}
	opt := option.WithCredentialsFile("inmallgotchimons.json")
	app, errfire := firebase.NewApp(context.Background(), nil, opt)
	if errfire != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
	}

	context := context.Background()
	client, errMes := app.Messaging(context)
	if errMes != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
	}

	stringMap := map[string]string{
		"type":setting["tellFriendAddCoinStep2Message"].(string),
	}

	message := &messaging.Message{
		Notification: &messaging.Notification{
			Title: setting["tellFriendAddCoinStep2Message"].(string),
			Body:  setting["tellFriendAddCoinStep2Message"].(string),
			//Data:  dataJson,
		},
		Data:  stringMap,
		Token:player["pushMessagesAccessToken"].(string),
		//Topic: "InMallGotchiMons",
		//Condition: condition,
		//Topic:topic,
		//Condition: condition,
	}

	response, errSend := client.Send(context, message)
	if errSend != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errSend))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{
		"response":response,
	})
	
}