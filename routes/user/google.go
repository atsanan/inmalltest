package user

import (
	"fmt"
	"log"
	"time"

	// "log"
	// "encoding/json"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"github.com/parnurzeal/gorequest"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"inmallgame/routes/auth"
	"inmallgame/routes/player"

	"inmallgame/app/helpers"
)

/*
{
  "family_name": "Rattajak",
  "name": "Nattapon Rattajak",
  "picture": "https://lh4.googleusercontent.com/-4nZ1oDyeOMw/AAAAAAAAAAI/AAAAAAAAES4/dtwY0SvbSg8/photo.jpg",
  "locale": "en",
  "gender": "male",
  "email": "rattajak.n@gmail.com",
  "link": "https://plus.google.com/+NattaponRattajak",
  "given_name": "Nattapon",
  "id": "108294695677105089532",
  "verified_email": true
}
*/

// Google OAuth2
type GoogleClaim struct {
	Email         string `json:"email"`
	Picture       string `json:"picture"`
	Gender        string `json:"gender"`
	FamilyName    string `json:"family_name"`
	Link          string `json:"link"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	Locale        string `json:"locale"`
	ID            string `json:"id"`
	VerifiedEmail bool   `json:"verified_email"`
}

// GoogleLogin login or register.
// https://developers.google.com/identity/sign-in/web/backend-auth
func GoogleLogin(ctx iris.Context) {
	token := ctx.PostValue("authenToken")

	if token == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, "authenToken field cannot empty.")
		return
	}

	claim := GoogleClaim{}

	bearer := fmt.Sprintf("Bearer %s", token)
	request := gorequest.New()
	resp, _, errs := request.Get("https://www.googleapis.com/oauth2/v2/userinfo").
		Set("Authorization", bearer).EndStruct(&claim)
	if errs != nil || resp.StatusCode != 200 {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Get googleapis data fail.", fmt.Sprint(errs))
		return
	}

	log.Println("GoogleApi response", resp.Status, resp.StatusCode)
	log.Println("Here is my google id:", claim)
	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	// Todo.
	// Find already registered authId && authTypeId.
	// If not yet, register new one, sign new accessToken and create new player profile.
	// If account already have just signing with normal login flow.
	var user = models.User{}
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if findErr := coll.Find(bson.M{"authenId": claim.ID, "authenTypeId": models.GoogleType}).One(&user); findErr != nil {
		fmt.Println("Not yet register, Just register new account.", user.ID.Hex(), findErr)

		RegisterNewGoogleAccount(ctx, claim)
		return
	} else if user.ID != "" {
		fmt.Println("Already registered, Just signing...")
		mongoErr := coll.Update(bson.M{"authenId": claim.ID}, bson.M{
			"$inc":         bson.M{"time": 1},
			"$currentDate": bson.M{"lastModified": true},
		})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}
		signToken, signErr := auth.GenerateAccessToken(user)
		if signErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Sign Token Fail.", fmt.Sprint(signErr))
			return
		}

		utils.ResponseSuccess(ctx, signToken)
	}
}

// RegisterNewGoogleAccount when this id not yet registered.
func RegisterNewGoogleAccount(ctx iris.Context, claim GoogleClaim) {
	var user = models.User{
		Email:           claim.Email,
		Password:        "",
		Name:            claim.Name,
		Firstname:       claim.GivenName,
		Lastname:        claim.FamilyName,
		Gender:          claim.Gender,
		Birthdate:       "",
		Picture:         claim.Picture,
		DeviceID:        "",
		AuthenTypeID:    models.GoogleType,
		AuthenID:        claim.ID,
		Roles:           []string{"user"},
		Time:            1,
		IndexReward:     0,
		LastLogin:       time.Now(),
		LastLoginReward: time.Now(),
		CreateAt:        time.Now(),
		Verified:        true,
		VerifiedAt:      time.Now(),
		LastModified:    time.Now(),
	}

	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if err := coll.Insert(user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Insert new account fail, Please try again.", fmt.Sprint(err))
		return
	}

	queryUser := models.User{}
	if findErr := coll.Find(bson.M{"authenId": claim.ID, "authenTypeId": models.GoogleType}).One(&queryUser); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Cannot get your account info.", fmt.Sprint(findErr))
		return
	}

	if createPlayerErr := player.CreateDefaultPlayer(ctx, queryUser.ID); createPlayerErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Register new account success but create default player fail.", fmt.Sprint(createPlayerErr))
		return
	}

	signToken, signErr := auth.GenerateAccessToken(queryUser)
	if signErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Sign Token Fail.", fmt.Sprint(signErr))
		return
	}

	isLoginReward, loginReward, index := helpers.CreateLoginReward(ctx, queryUser)

	mongoErr := coll.Update(bson.M{"_id": queryUser.ID}, bson.M{
		"$inc": bson.M{"indexReward": index},
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}
	ctx.JSON(iris.Map{"isSuccess": true, "data": signToken,
		"isLoginReward": isLoginReward, "loginReward": loginReward,
	})
	return
}
