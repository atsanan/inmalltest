package user

import (
	"fmt"
	"time"
	// "encoding/json"
	"github.com/kataras/iris"

	"github.com/globalsign/mgo/bson"
	// "log"
	// "time"
	"inmallgame/app/controller"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"inmallgame/routes/player"
)

// Register user.
func Register(ctx iris.Context) {
	email, password := ctx.PostValue("email"), ctx.PostValue("password")
	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	var user = models.User{
		ID:           bson.NewObjectId(),
		Email:        email,
		Password:     password,
		Roles:        []string{"user"},
		AuthenTypeID: models.TraditionalType,

		CreateAt: time.Now(),
		Verified: false,
	}
	if err := user.Validate(); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	var session = database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	// Find email already register first.
	num, notFound := coll.Find(bson.M{"email": user.Email}).Count()
	if notFound != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(notFound))
		return
	}
	if num > 0 {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Email already used.", "")
		return
	}

	if err := coll.Insert(user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "", fmt.Sprint(err))
		return
	}

	if createPlayerErr := player.CreateDefaultPlayer(ctx, user.ID); createPlayerErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Register new account success but create default player fail.", fmt.Sprint(createPlayerErr))
		return
	}

	// encrypt value to base64
	cryptoText := controller.Encrypt(config.GeneratedLinkKey, email)

	// Send verification email.
	SendEmail(ctx, user.Email, cryptoText)
	utils.ResponseSuccess(ctx, iris.Map{
		"success": true,
		"message": "Verification email will send to you as " + user.Email,
		"secret":  cryptoText,
	})
}
