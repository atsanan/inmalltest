package user

import (
	"fmt"
	// "log"
	"time"
	// "encoding/json"

	"github.com/globalsign/mgo/bson"
	fb "github.com/huandu/facebook"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"inmallgame/app/helpers"
	"inmallgame/routes/auth"
	"inmallgame/routes/player"
)

type FBClaim struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Birthday  string `json:"birthday"`
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Gender    string `json:"gender"`
}

// FacebookLogin login or register.
// More info please read
// https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/
// https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-protocols-oauth-code#jwt-token-claims
func FacebookLogin(ctx iris.Context) {
	token := ctx.PostValue("authenToken")

	if token == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, "authenToken field cannot empty.")
		return
	}

	res, err := fb.Get("/me", fb.Params{
		"fields":       "id,name,age_range,birthday,email,first_name,last_name,gender",
		"access_token": token,
	})
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Get facebook data fail.", fmt.Sprint(err))
		return
	}

	var fbData = FBClaim{}
	res.Decode(&fbData)
	fmt.Println("Here is my Facebook id:", fbData)

	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	// Todo.
	// Find already registered authId && authTypeId.
	// If not yet, register new one, sign new accessToken and create new player profile.
	// If account already have just signing with normal login flow.
	var user = models.User{}
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if findErr := coll.Find(bson.M{"authenId": fbData.ID, "authenTypeId": models.FacebookType}).One(&user); findErr != nil {
		fmt.Println("Not yet register, Just register new account.", user.ID.Hex(), findErr)

		RegisterNewFBAccount(ctx, fbData)
		return
	} else if user.ID != "" {

		fmt.Println("Already registered, Just signing...")

		mongoErr := coll.Update(bson.M{"_id": user.ID}, bson.M{
			//"$set":         bson.M{"indexReward": index},
			"$inc":         bson.M{"time": 1},
			"$currentDate": bson.M{"lastLogin": true},
		})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}
		checkLoginReeard := models.User{}
		if findErr := coll.Find(bson.M{"_id": user.ID}).One(&checkLoginReeard); findErr != nil {
			return
		}
		isLoginReward, _, loginReward, index := helpers.CheckUpDateLoginReward(ctx, checkLoginReeard)

		errLoginReward := coll.Update(bson.M{"_id": user.ID}, bson.M{
			"$set":         bson.M{"indexReward": index},
			"$currentDate": bson.M{"lastLoginReward": true},
		})
		if errLoginReward != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}
		signToken, signErr := auth.GenerateAccessToken(user)
		if signErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Sign Token Fail.", fmt.Sprint(signErr))
			return
		}

		ctx.JSON(iris.Map{"isSuccess": true, "data": signToken,
			"isLoginReward": isLoginReward, "loginReward": loginReward,
		})

		return
	}
}

// RegisterNewFBAccount when this fb_id not yet registered.
func RegisterNewFBAccount(ctx iris.Context, fbData FBClaim) {
	var user = models.User{
		Email:           fbData.Email,
		Password:        "",
		Name:            fbData.Name,
		Firstname:       fbData.FirstName,
		Lastname:        fbData.LastName,
		Gender:          fbData.Gender,
		Birthdate:       fbData.Birthday,
		AuthenTypeID:    models.FacebookType,
		AuthenID:        fbData.ID,
		Roles:           []string{"user"},
		Time:            1,
		DeviceID:        "",
		IndexReward:     0,
		LastLogin:       time.Now(),
		LastLoginReward: time.Now(),
		CreateAt:        time.Now(),
		Verified:        true,
		VerifiedAt:      time.Now(),
		LastModified:    time.Now(),
	}

	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if err := coll.Insert(user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Insert new account fail, Please try again.", fmt.Sprint(err))
		return
	}

	var queryUser = models.User{}
	if findErr := coll.Find(bson.M{"authenId": fbData.ID, "authenTypeId": models.FacebookType}).One(&queryUser); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Cannot get your account info.", fmt.Sprint(findErr))
		return
	}

	if createPlayerErr := player.CreateDefaultPlayer(ctx, queryUser.ID); createPlayerErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Register new account success but create default player fail.", fmt.Sprint(createPlayerErr))
		return
	}

	signToken, signErr := auth.GenerateAccessToken(queryUser)
	if signErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Sign Token Fail.", fmt.Sprint(signErr))
		return
	}

	isLoginReward, loginReward, index := helpers.CreateLoginReward(ctx, queryUser)

	mongoErr := coll.Update(bson.M{"_id": queryUser.ID}, bson.M{
		"$inc": bson.M{"indexReward": index},
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}
	ctx.JSON(iris.Map{"isSuccess": true, "data": signToken,
		"isLoginReward": isLoginReward, "loginReward": loginReward,
	})
	return
	//utils.ResponseSuccess(ctx, signToken)
}

// RegisterNewFBAccount when this fb_id not yet registered.
func RegisterNewFBAccountForm(ctx iris.Context, claim GoogleClaim) {
	var user = models.User{
		Email:           claim.Email,
		Password:        "",
		Name:            claim.Name,
		Firstname:       claim.GivenName,
		Lastname:        claim.FamilyName,
		Gender:          claim.Gender,
		Birthdate:       "",
		Picture:         claim.Picture,
		DeviceID:        "",
		AuthenTypeID:    models.FacebookType,
		AuthenID:        claim.ID,
		Roles:           []string{"user"},
		Time:            1,
		IndexReward:     0,
		LastLogin:       time.Now(),
		LastLoginReward: time.Now(),
		CreateAt:        time.Now(),
		Verified:        true,
		VerifiedAt:      time.Now(),
		LastModified:    time.Now(),
	}

	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if err := coll.Insert(user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Insert new account fail, Please try again.", fmt.Sprint(err))
		return
	}

	var queryUser = models.User{}
	if findErr := coll.Find(bson.M{"authenId": claim.ID, "authenTypeId": models.FacebookType}).One(&queryUser); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Cannot get your account info.", fmt.Sprint(findErr))
		return
	}

	if createPlayerErr := player.CreateDefaultPlayer(ctx, queryUser.ID); createPlayerErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Register new account success but create default player fail.", fmt.Sprint(createPlayerErr))
		return
	}

	signToken, signErr := auth.GenerateAccessToken(queryUser)
	if signErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Sign Token Fail.", fmt.Sprint(signErr))
		return
	}

	isLoginReward, loginReward, index := helpers.CreateLoginReward(ctx, queryUser)

	mongoErr := coll.Update(bson.M{"_id": queryUser.ID}, bson.M{
		"$inc": bson.M{"indexReward": index},
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}
	ctx.JSON(iris.Map{"isSuccess": true, "data": signToken,
		"isLoginReward": isLoginReward, "loginReward": loginReward,
	})
	return
	//utils.ResponseSuccess(ctx, signToken)
}
