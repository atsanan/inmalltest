package user

import (
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"inmallgame/routes/auth"

	"fmt"
)

// Login with username, password ...
func Login(ctx iris.Context) {
	email, password := ctx.PostValue("email"), ctx.PostValue("password")
	user := models.User{
		Email:    email,
		Password: password,
	}
	if err := user.Validate(); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if err := coll.Find(bson.M{"email": email, "password": password}).One(&user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "NoContent", fmt.Sprint(err))
		return
	}

	ss, err := auth.GenerateAccessToken(user)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Sign Token Fail.", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, ss)
}
