package user

import (
	"inmallgame/app/controller"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

// ResendActivationEmail use for send email to user again and again.
func ResendActivationEmail(ctx iris.Context) {
	email := ctx.PostValue("email")
	if email == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, "Missing email field")
		return
	}

	user := models.User{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	filter := bson.M{"email": email}
	if err := coll.Find(filter).One(&user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, err.Error())
		return
	}

	// encrypt value to base64
	cryptoText := controller.Encrypt(config.GeneratedLinkKey, user.Email)

	SendEmail(ctx, email, cryptoText)

	utils.ResponseSuccess(ctx, iris.Map{
		"success": true,
		"message": "Verification email will send to you as " + user.Email,
		"secret":  cryptoText,
	})
}
