package user

import (
	"time"
	// "encoding/json"
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/controller"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

// VerifyAccount use for email activation process.
func VerifyAccount(ctx iris.Context) {
	secret := ctx.PostValue("secret")
	config := utils.ConfigParser(ctx)

	if secret == "" {
		fmt.Println("Missing secret data")
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, "Missing secret data")
		return
	}

	email := controller.Decrypt(config.GeneratedLinkKey, secret)

	var session = database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	filter := bson.M{"email": email}
	change := bson.M{"$set": bson.M{"verified": true, "verifiedAt": time.Now()},
		"$currentDate": bson.M{"lastModified": true}}
	if err := coll.Update(filter, change); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, err.Error())
		return
	}

	var user = models.User{}
	if err := coll.Find(filter).Select(bson.M{"password": 0}).One(&user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, err.Error())
		return
	}

	utils.ResponseSuccess(ctx, user)
}
