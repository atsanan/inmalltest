package user

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/helpers"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"inmallgame/routes/player"
	"inmallgame/routes/auth"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"time"
)

func LineFormData(ctx iris.Context) {
	claim := GoogleClaim{}
	if parseErr := ctx.ReadJSON(&claim); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	user := models.User{}
	config := utils.ConfigParser(ctx)

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if findErr := coll.Find(bson.M{"authenId": claim.ID, "authenTypeId": models.LineType}).One(&user); findErr != nil {
		fmt.Println("Not yet register, Just register new account.", user.ID.Hex(), findErr)
		RegisterNewLineAccount(ctx, claim)
		return
	} else if user.ID != "" {

		fmt.Println("Already registered, Just signing...")
		mongoErr := coll.Update(bson.M{"_id": user.ID}, bson.M{
			"$inc":         bson.M{"time": 1},
			"$currentDate": bson.M{"lastLogin": true},
		})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}

		checkLoginReeard := models.User{}
		if findErr := coll.Find(bson.M{"_id": user.ID}).One(&checkLoginReeard); findErr != nil {
			return
		}

		isLoginReward, _, loginReward, index := helpers.CheckUpDateLoginReward(ctx, checkLoginReeard)

		errLoginReward := coll.Update(bson.M{"_id": user.ID}, bson.M{
			"$set":         bson.M{"indexReward": index},
			"$currentDate": bson.M{"lastLoginReward": true},
		})
		
		if errLoginReward != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}
		signToken, signErr := auth.GenerateAccessToken(user)
		if signErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Sign Token Fail.", fmt.Sprint(signErr))
			return
		}

		ctx.JSON(iris.Map{"isSuccess": true, "data": signToken,
			"isLoginReward": isLoginReward, "loginReward": loginReward,
		})
		return
	}

	//ctx.JSON(bson.M{"test": claim})
}



// RegisterNewLineAccount when this id not yet registered.
func RegisterNewLineAccount(ctx iris.Context, claim GoogleClaim) {
	var user = models.User{
		Email:           claim.Email,
		Password:        "",
		Name:            claim.Name,
		Firstname:       claim.GivenName,
		Lastname:        claim.FamilyName,
		Gender:          claim.Gender,
		Birthdate:       "",
		Picture:         claim.Picture,
		DeviceID:        "",
		AuthenTypeID:    models.LineType,
		AuthenID:        claim.ID,
		Roles:           []string{"user"},
		Time:            1,
		IndexReward:     0,
		LastLogin:       time.Now(),
		LastLoginReward: time.Now(),
		CreateAt:        time.Now(),
		Verified:        true,
		VerifiedAt:      time.Now(),
		LastModified:    time.Now(),
	}

	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if err := coll.Insert(user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Insert new account fail, Please try again.", fmt.Sprint(err))
		return
	}

	queryUser := models.User{}
	if findErr := coll.Find(bson.M{"authenId": claim.ID, "authenTypeId": models.LineType}).One(&queryUser); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Cannot get your account info.", fmt.Sprint(findErr))
		return
	}

	if createPlayerErr := player.CreateDefaultPlayer(ctx, queryUser.ID); createPlayerErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Register new account success but create default player fail.", fmt.Sprint(createPlayerErr))
		return
	}

	signToken, signErr := auth.GenerateAccessToken(queryUser)
	if signErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Sign Token Fail.", fmt.Sprint(signErr))
		return
	}
	
	//isLoginReward, _, loginReward, index := helpers.CheckUpDateLoginReward(ctx, queryUser)

		// errLoginReward := coll.Update(bson.M{"_id": user.ID}, bson.M{
		// 	"$set":         bson.M{"indexReward": index},
		// 	"$currentDate": bson.M{"lastLoginReward": true},
		// })
	isLoginReward, loginReward, index := helpers.CreateLoginReward(ctx, queryUser)

	mongoErr := coll.Update(bson.M{"_id": queryUser.ID}, bson.M{
		"$inc": bson.M{"indexReward": index},
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}
	ctx.JSON(iris.Map{"isSuccess": true, "data": signToken,
	  "isLoginReward": isLoginReward, "loginReward": loginReward,
	  "test":1,
	})
	return
}
