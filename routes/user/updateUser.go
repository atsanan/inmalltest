package user

import (
	// "encoding/json"

	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func UpdateUser(ctx iris.Context) {
	userID := ctx.Params().Get("id")
	gender := ctx.PostValue("gender")
	birthdate := ctx.PostValue("birthdate")
	firstname := ctx.PostValue("firstname")
	lastname := ctx.PostValue("lastname")
	pushMessagesAccessToken := ctx.PostValue("pushMessagesAccessToken")
	deviceID := ctx.PostValue("deviceId")
	update := bson.M{}

	if gender != "" {
		update["gender"] = gender
	}

	if birthdate != "" {
		update["birthdate"] = birthdate
	}

	if firstname != "" {
		update["firstname"] = firstname
	}

	if lastname != "" {
		update["lastname"] = lastname
	}

	//if pushMessagesAccessToken != "" {
	update["pushMessagesAccessToken"] = pushMessagesAccessToken
	//}

	if deviceID != "" {
		update["deviceId"] = deviceID
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(userID)}, bson.M{
		"$set":         update,
		"$currentDate": bson.M{"lastModified": true},
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	result := bson.M{}
	if err := coll.Find(bson.M{"_id": bson.ObjectIdHex(userID)}).Select(bson.M{"password": 0}).One(&result); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, err.Error())
		return
	}

	utils.ResponseSuccess(ctx, result)

}
