package user

import (
	"fmt"
	"inmallgame/app/structs"
	"inmallgame/app/utils"
	"log"
	"net/smtp"

	"github.com/kataras/iris"
)

// SendEmail handle send validate email job.
func SendEmail(ctx iris.Context, email string, secret string) {
	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	// Set up authentication information.
	auth := smtp.PlainAuth("", config.SMTPConfig.AuthUser, config.SMTPConfig.AuthPass, config.SMTPConfig.Server)

	mail := appStruct.Mail{}
	mail.To = []string{email}
	mail.Sender = config.SMTPConfig.DefaultSender
	mail.Subject = "Plase Verify your registration"
	mail.Body = fmt.Sprintf("InMall game sent this email with secret link for you to verification process: %s", secret)

	messageBody := mail.BuildMessage()
	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	msg := []byte(messageBody)
	err := smtp.SendMail(config.SMTPConfig.Server+":"+config.SMTPConfig.Port,
		auth, config.SMTPConfig.DefaultSender, mail.To, msg)
	if err != nil {
		log.Fatalf("SendMail Fail %s", err)
	}
}
