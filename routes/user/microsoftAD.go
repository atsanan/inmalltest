package user

import (
	"fmt"
	// "log"
	// "time"
	"encoding/json"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/kataras/iris"
	// "github.com/globalsign/mgo/bson"

	// "inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
)

type ADClaim struct {
	Exp        int      `json:"exp"`
	Nbf        int      `json:"nbf"`
	Ver        string   `json:"ver"`
	Iss        string   `json:"iss"`
	Sub        string   `json:"sub"`
	Aud        string   `json:"aud"`
	Nonce      string   `json:"nonce"`
	Iat        int      `json:"iat"`
	AuthTime   int      `json:"auth_time"`
	GivenName  string   `json:"given_name"`
	FamilyName string   `json:"family_name"`
	Name       string   `json:"name"`
	Idp        string   `json:"idp"`
	Oid        string   `json:"oid"`
	Emails     []string `json:"emails"`
	Tfp        string   `json:"tfp"`
}

// MicrosoftAD login or register.
// More info please read
// https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/
// https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-protocols-oauth-code#jwt-token-claims
func MicrosoftAD(ctx iris.Context) {
	token := ctx.PostValue("JWTToken")

	if token == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, "JWTToken field cannot empty.")
		return
	}

	arr := strings.Split(token, ".")
	// _h, _p, _s := arr[0], arr[1], arr[2]
	pbytes, err := jwt.DecodeSegment(arr[1])
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	payload := string(pbytes)
	data := ADClaim{}
	if json.Unmarshal([]byte(payload), &data); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	fmt.Println("data: ", data)

	// c := ctx.Values().Get("config")
	// config, _ := c.(utils.Configuration)

	// session := database.GetMgoSession()
	// coll := session.DB(config.DbName).C(config.UserCollection)
	// if err := coll.Find(bson.M{"email": email, "password": password}).One(&user); err != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "NoContent", err)
	// 	return
	// }

	utils.ResponseSuccess(ctx, data)
}
