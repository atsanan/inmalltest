package user

import (

	// "log"

	// "encoding/json"

	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/kataras/iris"

	"github.com/globalsign/mgo/bson"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"

	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

func CheckUpDateLoginReward(ctx iris.Context, user models.User) (bool, map[string]interface{}, map[string]interface{}) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	LoginRewardColl := session.DB(config.DbName).C(config.DBCollection.LoginRewardCollection)
	MailBoxColl := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	PlayerColl := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	isLoginReward := true
	currentDate := map[string]interface{}{}
	currentDate["lastLogin"] = true
	dateLastLoginReward := user.LastLoginReward
	LastLoginReward := dateLastLoginReward.Format("2006-01-02")

	dateLastLogin := user.LastLogin
	LastLogin := dateLastLogin.Format("2006-01-02")
	loginReward := bson.M{}

	opt := option.WithCredentialsFile("inmallgotchimons.json")
	app, errfire := firebase.NewApp(context.Background(), nil, opt)
	if errfire != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
	}

	context := context.Background()
	client, errMes := app.Messaging(context)
	if errMes != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
	}

	if LastLoginReward == LastLogin {
		isLoginReward = false
	} else {
		player := bson.M{}
		if findErr := PlayerColl.Find(bson.M{"userId": user.ID}).One(&player); findErr != nil {
			isLoginReward = false
		} else if findErr := LoginRewardColl.Find(bson.M{"indexReward": user.IndexReward}).One(&loginReward); findErr != nil {
			isLoginReward = false

		} else {
			create := bson.M{
				"playerId":      player["_id"],
				"userId":        user.ID,
				"title":         loginReward["title"],
				"detail":        loginReward["detail"],
				"itemId":        loginReward["itemId"],
				"itemActive":    loginReward["itemActive"],
				"mDefaultId":    loginReward["mDefaultId"],
				"monsterActive": loginReward["monsterActive"],
				"diamond":       loginReward["diamond"],
				"diamondActive": loginReward["diamondActive"],
				"imageName":     loginReward["imageName"],
				"thumbnail":     loginReward["thumbnail"],
				"linkListName":  loginReward["linkListName"],
				"isActive":      true,
				"createAt":      time.Now(),
			}

			mongoErr := MailBoxColl.Insert(create)
			if mongoErr != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
				//return
			}

			stringMap := map[string]string{
				"type":         "inbox",
				"by":           "system",
				"linkListName": "1234",
			}

			message := &messaging.Message{
				Notification: &messaging.Notification{
					Title: loginReward["title"].(string),
					Body:  loginReward["detail"].(string),
					//Data:  dataJson,
				},
				Data:  stringMap,
				Token: user.PushMessagesAccessToken,
				//Topic: "InMallGotchiMons",
				//Condition: condition,
				//Topic:topic,
				//Condition: condition,
			}

			_, errSend := client.Send(context, message)
			if errSend == nil {
				currentDate["lastLoginReward"] = true
			}
		}
		//if user.IndexReward
	}

	return isLoginReward, currentDate, loginReward

}

func test() bool {
	return true
}
