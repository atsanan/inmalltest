package user

import (
	// "github.com/dgrijalva/jwt-go"
	"github.com/kataras/iris"

	"github.com/globalsign/mgo/bson"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"log"
	// "time"

	"fmt"
)

// GetUser will return user data.
func GetUser(ctx iris.Context) {
	myClaim, err := utils.TokenParser(ctx)
	userID := ctx.URLParam("userId")
	if userID != "" {
		if err := utils.ValidateMongoID("userId", userID); err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
			return
		}
		myClaim.ID = userID
	}
	if err != nil || myClaim.ID == "" {
		log.Printf("MyClaim Err %e", err)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	config := utils.ConfigParser(ctx)

	user := bson.M{}

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if err := coll.Find(bson.M{"_id": bson.ObjectIdHex(myClaim.ID)}).Select(bson.M{"password": 0}).One(&user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "NoContent", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, user)
}

// PostUser use to update user data.
func PostUser(ctx iris.Context) {
	myClaim, err := utils.TokenParser(ctx)
	userID := ctx.URLParam("userId")
	if userID != "" {
		if err := utils.ValidateMongoID("userId", userID); err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
			return
		}
		myClaim.ID = userID
	}
	if err != nil || myClaim.ID == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
	}
	config := utils.ConfigParser(ctx)

	user := models.User{}
	if err := ctx.ReadJSON(&user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	m := bson.M{}
	if user.Firstname != "" {
		m["firstname"] = user.Firstname
	}
	if user.Lastname != "" {
		m["lastname"] = user.Lastname
	}
	if user.Gender != "" {
		m["gender"] = user.Gender
	}
	if user.Name != "" {
		m["name"] = user.Name
	}

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)

	colQuerier := bson.M{"_id": bson.ObjectIdHex(myClaim.ID)}
	change := bson.M{"$set": m, "$currentDate": bson.M{"lastModified": true}}
	if err = coll.Update(colQuerier, change); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, err.Error())
		return
	}

	result := bson.M{}
	if err := coll.Find(bson.M{"_id": bson.ObjectIdHex(myClaim.ID)}).Select(bson.M{"password": 0}).One(&result); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, err.Error())
		return
	}

	utils.ResponseSuccess(ctx, result)
}
