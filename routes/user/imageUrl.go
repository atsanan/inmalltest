package user

import (
	// "log"
	// "encoding/json"
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"
	"inmallgame/app/models"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//ImageGoogle
type Image struct {
	Entry Photothumbnail `json:"entry"`
}
type Photothumbnail struct {
	Thumbnail T `json:"gphoto$thumbnail"`
}
type T struct {
	T string `json:"$t"`
}

func ImageUrl(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")

	if validateErr := utils.ValidateMongoID("playerId", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "", fmt.Sprint(validateErr))
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	player := map[string]map[string]interface{}{}
	errPlayer := coll.Pipe(
		[]bson.M{
			{"$match": bson.M{
				"_id": bson.ObjectIdHex(playerID),
			},
			},
			{
				"$lookup": bson.M{
					"from":         config.DBCollection.UserCollection,
					"localField":   "userId",
					"foreignField": "_id",
					"as":           "user",
				},
			},
			{"$unwind": "$user"},
		},
	).One(&player)

	if errPlayer != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errPlayer))
		return
	}

	authenTypeId := player["user"]["authenTypeId"]

	if authenTypeId == models.FacebookType {
		url := fmt.Sprintf("https://graph.facebook.com/%s/picture?type=large&width=512&height=512", player["user"]["authenId"])
		utils.ResponseSuccess(ctx, bson.M{"url": url})
	} else if authenTypeId == models.GoogleType {

		utils.ResponseSuccess(ctx, bson.M{"url": player["user"]["picture"]})
	} else if authenTypeId == models.LineType {

		utils.ResponseSuccess(ctx, bson.M{"url": player["user"]["picture"]})
	} else {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint("type user not google or facebook"))
	}
}
