package user

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/helpers"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"inmallgame/routes/auth"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func FacebookFormData(ctx iris.Context) {
	claim := GoogleClaim{}
	if parseErr := ctx.ReadJSON(&claim); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	user := models.User{}
	config := utils.ConfigParser(ctx)

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	if findErr := coll.Find(bson.M{"authenId": claim.ID, "authenTypeId": models.FacebookType}).One(&user); findErr != nil {
		fmt.Println("Not yet register, Just register new account.", user.ID.Hex(), findErr)
		RegisterNewFBAccountForm(ctx, claim)
		return
	} else if user.ID != "" {

		fmt.Println("Already registered, Just signing...")
		mongoErr := coll.Update(bson.M{"_id": user.ID}, bson.M{
			"$inc":         bson.M{"time": 1},
			"$currentDate": bson.M{"lastLogin": true},
		})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}

		checkLoginReeard := models.User{}
		if findErr := coll.Find(bson.M{"_id": user.ID}).One(&checkLoginReeard); findErr != nil {
			return
		}
		isLoginReward, _, loginReward, index := helpers.CheckUpDateLoginReward(ctx, checkLoginReeard)

		errLoginReward := coll.Update(bson.M{"_id": user.ID}, bson.M{
			"$set":         bson.M{"indexReward": index},
			"$currentDate": bson.M{"lastLoginReward": true},
		})
		if errLoginReward != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}
		signToken, signErr := auth.GenerateAccessToken(user)
		if signErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Sign Token Fail.", fmt.Sprint(signErr))
			return
		}

		ctx.JSON(iris.Map{"isSuccess": true, "data": signToken,
			"isLoginReward": isLoginReward, "loginReward": loginReward,
		})
		return
	}

	//ctx.JSON(bson.M{"test": claim})
}
