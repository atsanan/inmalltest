package user

import (
	"fmt"
	"time"
	// "encoding/json"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "log"
	// "time"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

// ResetPassword use for generate and send email to user email.
func ResetPassword(ctx iris.Context) {
	email, password := ctx.PostValue("email"), ctx.PostValue("password")
	c := ctx.Values().Get("config")
	config, _ := c.(utils.Configuration)

	var user = models.User{
		Email:    email,
		Password: password,
		CreateAt: time.Now(),
	}
	err := user.Validate()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "", fmt.Sprint(err))
		return
	}

	var session = database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	// Find email already register first.
	num, notFound := coll.Find(bson.M{"email": user.Email}).Count()

	if notFound != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(notFound))
		return
	}

	if num > 0 {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Email already used.", "")
		return
	}

	if err := coll.Insert(user); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, map[string]bool{"success": true})
}
