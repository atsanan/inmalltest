package qrCode

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"time"

	// firebase "firebase.google.com/go"
	// "firebase.google.com/go/messaging"
	// "golang.org/x/net/context"
	// "google.golang.org/api/option"
)

//addQrcodePlayer
func addQrcodePlayer(ctx iris.Context) {
	qrCode := ctx.PostValue("qrCode")
	playerID := ctx.PostValue("playerId")
	shopID := ctx.PostValue("shopId")
	err := validation.Errors{
		"qrCode":  validation.Validate(qrCode, validation.Required,),
		"playerId":  validation.Validate(playerID, validation.Required,is.MongoID),
		"shopId":  validation.Validate(shopID,is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.QrCodeCollection)
	
	result:=bson.M{}
	errQuery := coll.Pipe([]bson.M{
		{"$match": bson.M{"code": qrCode}},
		{"$addFields":bson.M{
			"titleMessage":bson.M{"$concat": []string{"$titleMessage.eng",},},
			"bodyMessage":bson.M{"$concat": []string{"$bodyMessage.eng",},},
			"coin":bson.M{"$toInt":"$coinInclude" ,},
			"code":bson.M{"$toString":"$code" ,},
		},
		},
		{
			"$project": bson.M{
				"_id":                  1,
				"titleMessage":        1,
				"bodyMessage":1,
				"coin":        1,
				"code":        1,
				
			},
		},
	}).One(&result)
	if errQuery != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errQuery))
		return
	}

	collPlayer := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	player:=bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(playerID)}},
		{"$addFields":bson.M{
			"coin":bson.M{"$toInt":"$coin" ,}, 
		},
		},
		{
			"$project": bson.M{
				"_id":                  1,
				"coin":        1,
			},
		},	
	}

	errPlayerReturn := collPlayer.Pipe(objQuery).One(&player)
	if errPlayerReturn != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errPlayerReturn))
		return
	}

	
	collQrCodePlayer := session.DB(config.DbName).C(config.DBCollection.QrCodePlayerCollection)
	qrCodePlayerQuery := []bson.M{}
	isScan :=false
	if shopID ==""{
		qrCodePlayerQuery=append(qrCodePlayerQuery, 
		bson.M{"$match":bson.M{
				"playerId": bson.ObjectIdHex(playerID),
				"code": qrCode,
				"shopId": bson.M{"$exists": false},
		}})
	}else{
		qrCodePlayerQuery=append(qrCodePlayerQuery, 
			bson.M{"$match":bson.M{
					"playerId": bson.ObjectIdHex(playerID),
					"code": qrCode,
					"shopId": bson.ObjectIdHex(shopID),
			}})
	}
	qrCodePlayer:=bson.M{}
	coin:=player["coin"].(int)
	errQrCodePlayerPlayerReturn := collQrCodePlayer.Pipe(qrCodePlayerQuery).One(&qrCodePlayer)

	if 	errQrCodePlayerPlayerReturn != nil {
	
		save:=bson.M{
				"qrCodeId":result["_id"],
				"playerId": bson.ObjectIdHex(playerID),
				"code": qrCode,
				"created_at":time.Now(),
		}

		if shopID !=""{
			save["shopId"]=bson.ObjectIdHex(shopID)
		}
		errorSave:=collQrCodePlayer.Insert(save)
		if errorSave !=nil{
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errQrCodePlayerPlayerReturn))
			return
		}
		mongoErr := collPlayer.Update(bson.M{"_id": bson.ObjectIdHex(playerID)}, bson.M{
			"$inc":         bson.M{"coin": result["coin"].(int)},
			"$currentDate": bson.M{"lastLogin": true},
		})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}

		coin+=result["coin"].(int)
		// opt := option.WithCredentialsFile("inmallgotchimons.json")
		// app, errfire := firebase.NewApp(context.Background(), nil, opt)
		// if errfire != nil {
		// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
		// }

		// context := context.Background()
		// client, errMes := app.Messaging(context)
		// if errMes != nil {
		// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
		// }
		// stringMap := map[string]string{
		// 	"type":"qrCodePlayer",
		// 	"message":result["titleMessage"].(string),
		// 	"playerId":playerID,
		// }

		// message := &messaging.Message{
		// 	Notification: &messaging.Notification{
		// 		Title: result["titleMessage"].(string),
		// 		Body:  result["bodyMessage"].(string),
		// 		//Data:  dataJson,
		// 	},
		// 	Data:  stringMap,
		// 	Token: player["pushMessagesAccessToken"].(string),
		// }

		// res,errSend:=client.Send(context, message)
		// if errSend == nil {
		// 	//errorLog=fmt.Sprint(errSend)
		// 	// utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errSend))
		// 	// return
		//  }
	
		isScan=true
	
	}


	utils.ResponseSuccess(ctx, bson.M{
		"isScan":isScan,
		"coin":coin,
	})
	

}