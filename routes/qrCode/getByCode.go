package qrCode

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	validation "github.com/go-ozzo/ozzo-validation"
)

//getByCode
func getByCode(ctx iris.Context) {
	
	qrCode := ctx.Params().Get("qrCode")
	err := validation.Errors{
		"qrCode":  validation.Validate(qrCode, validation.Required,),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.QrCodeCollection)
	result:=bson.M{}
	errQuery := coll.Pipe([]bson.M{
		{"$match": bson.M{"code": qrCode,"isActive":true,}},
		{"$lookup": bson.M{
			"from":         config.DBCollection.QrShopCollection,
			"localField":   "_id" ,
			"foreignField":"qrCodeId",
			"as":           "qrShops",
		},
		},
		{"$lookup": bson.M{
			"from":         config.DBCollection.ShopCollection,
			"localField":   "qrShops.shopId" ,
			"foreignField":"_id",
			"as":           "shops",
		},
		},
		{"$lookup": bson.M{
			"from":         config.DBCollection.QrItemCollection,
			"localField":   "_id" ,
			"foreignField":"qrCodeId",
			"as":           "qrItems",
		},
		},
		{"$lookup": bson.M{
			"from":         config.DBCollection.ItemCollection,
			"localField":   "qrItems.itemId" ,
			"foreignField":"_id",
			"as":           "items",
		},
		},
		{"$project": bson.M{
			"qrShops": 0,
			"qrItems": 0,
		},
		},
	}).One(&result)
	if errQuery != nil {
		utils.ResponseSuccess(ctx, result)
		return
	}

	utils.ResponseSuccess(ctx, result)
}