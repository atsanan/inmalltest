package qrCode

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Get("/{qrCode:string}", auth.TokenMDW, getByCode)
	routes.Post("/addQrCodePlayer", auth.TokenMDW, addQrcodePlayer)
}
