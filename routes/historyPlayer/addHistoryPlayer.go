package historyPlayer

import (
	"fmt"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

func AddHistoryPlayer(ctx iris.Context){
	playerID := ctx.PostValue("playerId")
	shopID := ctx.PostValue("shopId")
	itemID := ctx.PostValue("itemId")
	activityType := ctx.PostValue("activityType")
	tag := ctx.PostValue("tag")
	line1 := ctx.PostValue("line1")
	line2 := ctx.PostValue("line2")
	line3 := ctx.PostValue("line3")
	line4 := ctx.PostValue("line4")
	iconType,errIconType := ctx.PostValueInt("iconType")
	
	if errIconType != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errIconType))
		return
	}
	
	err := validation.Errors{
		"playerId":           validation.Validate(playerID, validation.Required, is.MongoID),
		"shopId":           validation.Validate(shopID, is.MongoID),
		"itemId":           validation.Validate(itemID, is.MongoID),
		//"iconType":           validation.Validate(iconType, validation.Required),
		//"activityType":           validation.Validate(activityType, validation.Required),
		//"title":           validation.Validate(title, validation.Required),
		//"detail":           validation.Validate(detail, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.HistoryPlayerCollection)
	collItem := session.DB(config.DbName).C(config.DBCollection.ItemCollection)
	collShop := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	
	_id := bson.NewObjectId()

    save:=bson.M{
		"_id":_id,
		"playerId":bson.ObjectIdHex(playerID),
		"iconType":iconType,
		"activityType":activityType,
		"tag":tag,
		"line1":line1,
		"line2":line2,
		"line3":line3,
		"line4":line4,
		// "title":title,
		// "detail":detail,
		"created_at":time.Now(),
	}	

	if shopID !=""{
		save["shopId"]=bson.ObjectIdHex(shopID)
		objQuery := []bson.M{
			{"$match": bson.M{"_id": bson.ObjectIdHex(shopID)}},
			{
				"$project": bson.M{
					"_id":                  1,
					"shopName":             1, 
					"filenameLogo1":             1,  
			},
		},
		}
	
		shop := bson.M{}
		pipe := collShop.Pipe(objQuery)
		findErr := pipe.One(&shop)
		if findErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
			return
		}
		save["shop"]=shop
	}

	if itemID !=""{
		save["itemId"]=bson.ObjectIdHex(itemID)

		objQuery := []bson.M{
			{"$match": bson.M{"_id": bson.ObjectIdHex(itemID)}},
			{
				"$project": bson.M{
					"_id":                  1,
					"itemName":             1, 
					"itemAssertImageSlot1":             1, 
					"itemAssertImageSlot2":             1, 
			},
		},
		}
	
		item := bson.M{}
		pipe := collItem.Pipe(objQuery)
		findErr := pipe.One(&item)
		if findErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
			return
		}
		save["item"]=item
	}

	mongoErr := coll.Insert(save)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	result := bson.M{}
	findErr := coll.Find(bson.M{"_id": _id}).One(&result)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, result)

}