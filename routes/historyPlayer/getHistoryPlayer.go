package historyPlayer

import (
	"fmt"
	"inmallgame/app/utils"
	//"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

//HistoryPlayer routes
func HistoryPlayerList(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	activityType := ctx.URLParam("activityType")
	tag := ctx.URLParam("tag")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	iconType, _ := ctx.URLParamInt("iconType")
	err := validation.Errors{
		"playerID":           validation.Validate(playerID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.HistoryPlayerCollection)
	//collShop := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	list:=[]bson.M{}

	objQuery:=[]bson.M{
		{"$match":bson.M{
			"playerId":bson.ObjectIdHex(playerID),
			},
		},
	}
	
	if activityType !=""{
		objQuery = append(objQuery, bson.M{"$match":bson.M{
			"activityType":activityType,
		}})
	}

	if iconType > -1 {
		objQuery = append(objQuery, bson.M{"$match":bson.M{
			"iconType":iconType,
		}})
	}
	
	if tag !=""{
		objQuery = append(objQuery, bson.M{"$match":bson.M{
			"tag":tag,
		}})
	}
	

	mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	if countErr != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"historyPlayer":   []bson.M{},
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}
	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)
	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.All(&list)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}


	// for k,v :=range list{
	// 	shop:=bson.M{}
	// 	if v["shopId"]!= nil {
	// 		collShop.Pipe([]bson.M{
	// 			{"$match":bson.M{"_id":v["shopId"],},},
	// 			{"$project":bson.M{
	// 				"shopName":1,
	// 				"filenameLogo1":1,
	// 				"filenameLogo2":1,
	// 			},},
	// 		}).One(&shop)
			
	// 	}
	// 	list[k]["shop"]=shop
	// }

	linkF := "/api/v1/historyPlayer/list/"+playerID+"?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["historyPlayer"] = list

	utils.ResponseSuccess(ctx, result)
}