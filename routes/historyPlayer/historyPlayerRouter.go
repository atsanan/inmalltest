package historyPlayer

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, AddHistoryPlayer)
	routes.Get("/list/{playerId:string}", auth.TokenMDW, HistoryPlayerList)
}
