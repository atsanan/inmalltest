package player

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/helpers"
	"inmallgame/app/utils"

	// "log"
	// "time"
	// "encoding/json"
	"fmt"
	"strings"
)

// Player will serve playerInfo.
func Player(ctx iris.Context) {
	myClaim, _ := utils.TokenParser(ctx)
	config := utils.ConfigParser(ctx)
	userID := ctx.URLParam("userId")
	if myClaim.ID == "" && userID != "" {
		if err := utils.ValidateMongoID("userId", userID); err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
			return
		}
		myClaim.ID = userID
	}

	params := ctx.URLParams()
	fields := params["fields"]
	if fields != "" {
		selects := strings.Split(fields, ",")

		session := database.GetMgoSession()
		coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

		result := map[string]interface{}{}
		user := bson.M{}
		if id, _ := gubrak.IndexOf(selects, "user"); id >= 0 {
			usercoll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
			if err := usercoll.Find(bson.M{"_id": bson.ObjectIdHex(myClaim.ID)}).Select(bson.M{"password": 0}).One(&user); err != nil {
				fmt.Println("Not found user data, Something went wrong.")
			}

			result["user"] = user
		}

		player := bson.M{}
		if id, _ := gubrak.IndexOf(selects, "player"); id >= 0 {
			if err := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).One(&player); err != nil {
				fmt.Println("Not found, this will create default item and find it again.", myClaim)

				createErr := CreateDefaultPlayer(ctx, bson.ObjectIdHex(myClaim.ID))
				if createErr != nil {
					utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(createErr))
					return
				}

				findErr := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).One(&player)
				if findErr != nil {
					utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(findErr))
					return
				}
			}

			result["player"] = player
		}

		utils.ResponseSuccess(ctx, result)
	} else {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "No fields selection.", "")
		return
	}
}

// Register setup default
// Player -> authenTypeId = 1
// Player -> playerName = “undefined”
// Player -> teamPlayerId = -1
// Player -> coin = 500
// Player -> diamond = 0
// Player -> costumeSelectId = -1

// CreateDefaultPlayer when new registered user.
func CreateDefaultPlayer(ctx iris.Context, userID bson.ObjectId) error {
	player := helpers.DefaultPlayerModel(userID)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	playerColl := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	index := mgo.Index{
		Key:        []string{"userId"},
		Unique:     true,
		Background: true, // See notes.
	}
	createIndexErr := playerColl.EnsureIndex(index)
	if createIndexErr != nil {
		fmt.Println("createIndexErr: ", createIndexErr)
	}

	if err := playerColl.Insert(player); err != nil {
		return err
	}

	return nil
}
