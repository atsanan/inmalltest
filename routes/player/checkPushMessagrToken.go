package player

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
	

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"

	// "github.com/globalsign/mgo/bson"
	// "github.com/kataras/iris"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	// "log"
	// "time"
	// "encoding/json"
)

// CheckPushToken will serve get mothod.
func CheckPushToken(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	player := bson.M{}
	pipe := coll.Pipe([]bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(playerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.UserCollection,
				"localField":   "userId",
				"foreignField": "_id",
				"as":           "user",
			},
		},
		{"$unwind": "$user"},
		{
			"$group": bson.M{
				"_id":                     "$_id",
				"pushMessagesAccessToken": bson.M{"$first": "$user.pushMessagesAccessToken"},
			},
		},
	},
	)
	findErr := pipe.One(&player)

	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	settingColl := session.DB(config.DbName).C(config.DBCollection.SettingCollection)
	setting := bson.M{}
	pipeSetting := settingColl.Pipe([]bson.M{})
	errSetting := pipeSetting.One(&setting)
	if errSetting != nil {

		return
	}

	opt := option.WithCredentialsFile("inmallgotchimons.json")
	app, errfire := firebase.NewApp(context.Background(), nil, opt)
	if errfire != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
	}

	context := context.Background()
	client, errMes := app.Messaging(context)
	if errMes != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
	}

	stringMap := map[string]string{
		"title": setting["couponTimeOutMessageTitle"].(string),
		"body":  setting["couponTimeOutMessageBody"].(string),
	}
	message := &messaging.Message{
		Notification: &messaging.Notification{
			Title: setting["couponTimeOutMessageTitle"].(string),
			Body:  setting["couponTimeOutMessageBody"].(string),
			//Data:  dataJson,
		},
		Data:  stringMap,
		Token: player["pushMessagesAccessToken"].(string),
		//Topic: "InMallGotchiMons",
		//Condition: condition,
		//Topic:topic,
		//Condition: condition,
	}

	response,errorSend := client.Send(context, message)
	if errorSend != nil{
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errorSend))
	}
	utils.ResponseSuccess(ctx, response)

}
