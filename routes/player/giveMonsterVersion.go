package player

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	// "log"
	"time"
	// "encoding/json"
)

// getDataPlayer will serve get mothod.
func giveMonsterVersion(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	GiveMonsterColl := session.DB(config.DbName).C(config.DBCollection.GiveMonsterCollection)
	monstersDefaultColl := session.DB(config.DbName).C(config.DBCollection.MonstersDefaultCollection)
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)

	giveMonster := bson.M{}

	giveMonsterErr := GiveMonsterColl.Pipe([]bson.M{}).One(&giveMonster)

	if giveMonsterErr != nil {
		utils.ResponseFailure(ctx, iris.StatusNotFound, nil, fmt.Sprint(giveMonsterErr))
		return
	}

	player := bson.M{}

	playerErr := coll.Pipe([]bson.M{
		{"$match": bson.M{
			"_id": bson.ObjectIdHex(playerID),
		},
		},
	}).One(&player)

	if playerErr != nil {
		utils.ResponseFailure(ctx, iris.StatusNotFound, nil, fmt.Sprint(playerErr))
		return
	}

	if player["giveVersion"].(int) == giveMonster["giveVersion"].(int) {
		utils.ResponseFailure(ctx, iris.StatusOK, nil, "Player Current Version")
		return
	} else {
		mongoErr := coll.Update(
			bson.M{"_id": bson.ObjectIdHex(playerID)},
			bson.M{"$set": bson.M{"giveVersion": giveMonster["giveVersion"].(int)},
				"$currentDate": bson.M{"lastModified": true}})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}
		//player["giveVersion"] = giveMonster["giveVersion"].(int)
		monstersDefaultReturn := bson.M{}
		monstersDefaultSave := bson.M{}

		monsterErr := monstersDefaultColl.Pipe([]bson.M{
			{"$match": bson.M{
				"_id": giveMonster["mDefaultId"],
			},
			},
			{
				"$group": bson.M{
					"_id":          "$_id",
					"mDefaultName": bson.M{"$first": "$mDefaultName.eng"},
				},
			},
		}).One(&monstersDefaultSave)

		if monsterErr != nil {
			utils.ResponseFailure(ctx, iris.StatusNotFound, nil, fmt.Sprint(monsterErr))
			return
		}

		monsterReturnErr := monstersDefaultColl.Pipe([]bson.M{
			{"$match": bson.M{
				"_id": giveMonster["mDefaultId"],
			},
			},
		}).One(&monstersDefaultReturn)

		if monsterReturnErr != nil {
			utils.ResponseFailure(ctx, iris.StatusNotFound, nil, fmt.Sprint(monsterReturnErr))
			return
		}

		insert := bson.M{
			"playerId":                 bson.ObjectIdHex(playerID),
			"mDefaultId":               monstersDefaultSave["_id"],
			"mPlayerName":              monstersDefaultSave["mDefaultName"],
			"mPlayerQuest1Datetime":    time.Now(),
			"mPlayerQuest2Datetime":    time.Now(),
			"mPlayerQuest3Datetime":    time.Now(),
			"mPlayerExpDatetime":       time.Now(),
			"mPlayerFoodDatetime":      time.Now(),
			"mPlayerHappinessDatetime": time.Now(),
			"mPlayerHealthDatetime":    time.Now(),
			"mPlayerNewDateTime":       time.Now(),
			"mPlayerLastDateTime":      time.Now(),
			"mPlayerHabit":             0,
			"mPlayerStatusExp":         0,
			"mPlayerStatusFood":        0,
			"mPlayerStatusHappiness":   0,
			"mPlayerStatusHealth":      0,
			"mPlayerStatusQuest1":      0,
			"mPlayerStatusQuest2":      0,
			"mPlayerStatusQuest3":      0,
			"mPlayerHealthId":          nil,
			"mPlayerHappyId":           nil,
			"mPlayerFoodId":            nil,
			"createAt":                 time.Now(),
		}

		if mongoErr := monsterPlayerColl.Insert(insert); mongoErr != nil {

			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			return
		}

		utils.ResponseSuccess(ctx, bson.M{"giveMonster": giveMonster, "monstersDefault": monstersDefaultReturn})
	}

}
