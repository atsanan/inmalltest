package player

import (
	"fmt"

	// "github.com/globalsign/mgo"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"github.com/mitchellh/mapstructure"

	"inmallgame/app/utils"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// UpdateCostumeList route.
func UpdateCostumeList(ctx iris.Context) {

	playerID := ctx.Params().Get("id")

	if validateErr := utils.ValidateMongoID("id", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "", fmt.Sprint(validateErr))
	}

	update := models.Player{}
	if parseErr := ctx.ReadJSON(&update); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	player := models.Player{}

	decodeErr := mapstructure.Decode(update, &player)
	if decodeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(decodeErr))
		return
	}

	if validateErr := player.ValidateAllCostumeList(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}

	newData := bson.M{}
	newData["costumeList"] = player.CostumeList

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	mongoErr := coll.Update(
		bson.M{"_id": bson.ObjectIdHex(playerID)},
		bson.M{"$set": newData, "$currentDate": bson.M{"lastModified": true}})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, player, fmt.Sprint(mongoErr))
		return
	}

	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(playerID)}).One(&player)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, player)

}
