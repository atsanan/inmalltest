package player

import (
	//"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"inmallgame/app/data-access"
	//"inmallgame/app/helpers"
	"inmallgame/app/utils"
	"inmallgame/app/models"
	// "log"
	"time"
	// "encoding/json"
	"fmt"
	"strconv"

)

type Share struct {
	ID             bson.ObjectId `json:"id" bson:"_id,omitempty"`
	PlayerID       bson.ObjectId `json:"playerId" bson:"playerId"`
	ItemID         bson.ObjectId `json:"itemId" bson:"itemId"`
	CoinUpdate     bool			 `json:"coinUpdate" bson:"coinUpdate"`
	LastModified   time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}


func shareCouponAddCoin(ctx iris.Context){
	playerID := ctx.PostValue("playerId")
	itemID := ctx.PostValue("itemId")
	addCoin := ctx.PostValue("addCoin")
	
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
		"itemId": validation.Validate(itemID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	// if coin == "" && diamond == "" {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "coin or diamond should not empty field.", "")
	// 	return
	// }

	coinInt, _ := strconv.Atoi(addCoin)
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShareCouponAddCoinCollection)
	collPlayer := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	share:=Share{}
	pipe := coll.Pipe([]bson.M{
		{"$match":bson.M{
			"itemId": bson.ObjectIdHex(itemID),
			"playerId": bson.ObjectIdHex(playerID),
		  },
		},
	})
	errShare := pipe.One(&share)
	if errShare != nil {
		utils.ResponseSuccess(ctx, bson.M{
			"coinUpdate":false,
			"coin":0,
			"diamond":0,
			"addCoin":coinInt,
		})
		return
    }

	coinUpdate:=share.CoinUpdate
	UpdatePlayer:=bson.M{}	
	UpdatePlayer["coin"] = coinInt

	if coinUpdate == true {

		if err := collPlayer.Update(bson.M{"_id": bson.ObjectIdHex(playerID),},
				bson.M{
					"$inc":         UpdatePlayer,
					"$currentDate": bson.M{
						"lastModified": true,
					},
				}); err != nil {
					fmt.Println("Update fail.", err)
					utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update fail.", fmt.Sprint(err))
					return
		}

		if errCoupon := coll.Update(bson.M{
			"itemId": bson.ObjectIdHex(itemID),
			"playerId": bson.ObjectIdHex(playerID),
		},
		bson.M{
			"$set": bson.M{"coinUpdate":false,},
			"$currentDate": bson.M{
				"updated_at": true,
			},
		}); errCoupon != nil {
			fmt.Println("Update fail.", errCoupon)
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update fail.", fmt.Sprint(errCoupon))
			return
		}

	}
	playerReturn:=models.Player{}
	pipePlayer := collPlayer.Pipe([]bson.M{
		{"$match":bson.M{
			"_id": bson.ObjectIdHex(playerID)},
		},
	})
	errPlayer := pipePlayer.One(&playerReturn)
	if errPlayer!= nil {
		utils.ResponseFailure(ctx, 404, nil, fmt.Sprint(errPlayer))
		return
	}

	
	
	utils.ResponseSuccess(ctx, bson.M{
		"coinUpdate":coinUpdate,
		"coin":playerReturn.Coin,
		"diamond":playerReturn.Diamond,
		"addCoin":coinInt,
	})

	

}