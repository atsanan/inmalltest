package player

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	"strconv"
)

// Costume will serve get mothod.
func Costume(ctx iris.Context) {
	myClaim, err := utils.TokenParser(ctx)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, err.Error())
		return
	}

	config := utils.ConfigParser(ctx)

	player := map[string]interface{}{}

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	if err := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).Select(bson.M{
		"costumeSelectId": 1,
		"costumeList":     1,
	}).One(&player); err != nil {
		fmt.Println("Not found.", myClaim)

		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, player)
}

// UpdateCostume will serve post mothod.
func UpdateCostume(ctx iris.Context) {
	costumeSelectID := ctx.FormValue("costumeSelectId")
	if costumeSelectID == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "costumeSelectId should not empty field.", "")
		return
	}

	costumeID, err := strconv.Atoi(costumeSelectID)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "costumeSelectId should be integer", "")
		return
	}

	myClaim, _ := utils.TokenParser(ctx)
	config := utils.ConfigParser(ctx)
	player := models.Player{}

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	if err := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).Select(bson.M{
		"costumeList": 1,
	}).One(&player); err != nil {
		fmt.Println("Not found.", myClaim)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	id, _ := gubrak.IndexOf(player.CostumeList, costumeID)
	if id < 0 {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Cannot update costumeSelectId cause costumeList not yet contained.", fmt.Sprint(err))
		return
	}

	update := bson.M{}
	update["costumeSelectId"] = costumeID
	if err := coll.Update(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)},
		bson.M{
			"$set":         update,
			"$currentDate": bson.M{"lastModified": true},
		}); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(err))
		return
	}

	if err := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).One(&player); err != nil {
		fmt.Println("Not found.", myClaim)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, player)
}

// NewCostume will serve post mothod.
func NewCostume(ctx iris.Context) {
	costumeSelectID := ctx.FormValue("costumeSelectId")
	if costumeSelectID == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "costumeSelectId should not empty field.", "")
		return
	}
	costumeID, err := strconv.Atoi(costumeSelectID)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "costumeSelectId should be integer", "")
		return
	}

	myClaim, _ := utils.TokenParser(ctx)
	config := utils.ConfigParser(ctx)
	player := models.Player{}

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	if err := coll.Update(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)},
		bson.M{
			"$addToSet":    bson.M{"costumeList": costumeID},
			"$set":         bson.M{"costumeSelectId": costumeID},
			"$currentDate": bson.M{"lastModified": true},
		}); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(err))
		return
	}

	if err := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).One(&player); err != nil {
		fmt.Println("Not found.", myClaim)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, player)
}
