package player

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

func validatePlayer(ctx iris.Context, playerID string, player *map[string]interface{}) error {
	if validateErr := utils.ValidateMongoID("id", playerID); validateErr != nil {
		return validateErr
	}
	if err := ctx.ReadJSON(&player); err != nil {
		return err
	}

	playerModel := models.Player{}
	if decodeErr := mapstructure.Decode(player, &playerModel); decodeErr != nil {
		return decodeErr
	}
	if validateErr := playerModel.ValidateAll(); validateErr != nil {
		return validateErr
	}

	return nil
}

// UpdatePlayer will serve player/{id}.
func UpdatePlayer(ctx iris.Context) {
	playerID := ctx.Params().Get("id")
	player := map[string]interface{}{}
	if validateErr := validatePlayer(ctx, playerID, &player); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	mongoErr := coll.Update(
		bson.M{"_id": bson.ObjectIdHex(playerID)},
		bson.M{"$set": player, "$currentDate": bson.M{"lastModified": true}})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(playerID)}).One(&player)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, player)
}
