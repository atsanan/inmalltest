package player

import (

	// "github.com/globalsign/mgo"

	"fmt"
	"inmallgame/app/utils"
	"strconv"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
	// "log"
	// "time"
	// "encoding/json"
)

func AddPlayerLog(ctx iris.Context) {
	title := ctx.PostValue("title")
	detail := ctx.PostValue("detail")
	coin := ctx.PostValue("coin")
	diamond := ctx.PostValue("diamond")
	money := ctx.PostValue("money")
	playerID := ctx.PostValue("playerId")
	typeData := ctx.PostValue("type")
	itemID := ctx.PostValue("itemId")
	mDefaultID := ctx.PostValue("mDefaultId")
	err := validation.Errors{
		"title":      validation.Validate(title, validation.Required),
		"detail":     validation.Validate(detail, validation.Required),
		"tpye":       validation.Validate(typeData, validation.Required),
		"playerId":   validation.Validate(playerID, validation.Required, is.MongoID),
		"itemId":     validation.Validate(itemID, is.MongoID),
		"mDefaultId": validation.Validate(mDefaultID, is.MongoID),
	}.Filter()
	_id := bson.NewObjectId()
	create := bson.M{
		"_id":      _id,
		"createAt": time.Now(),
		"playerId": bson.ObjectIdHex(playerID),
		"title":    title,
		"detail":   detail,
		"type":     typeData,
	}
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	if coin != "" {
		coinInt, err := strconv.Atoi(coin)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "coin should be integer", "")
			return
		}

		create["coin"] = coinInt
	}

	if diamond != "" {
		diamondInt, err := strconv.Atoi(diamond)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "diamond should be integer", "")
			return
		}

		create["diamond"] = diamondInt
	}


	create["money"] = money
	

	if itemID != "" {
		create["itemId"] = bson.ObjectIdHex(itemID)
	}

	if mDefaultID != "" {
		create["mDefaultId"] = bson.ObjectIdHex(mDefaultID)
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerLogsCollection)
	if err := coll.Insert(create); err != nil {
		fmt.Println("insert fail.", err)

		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update fail.", fmt.Sprint(err))
		return
	}
	log := bson.M{}
	if findErr := coll.Find(bson.M{"_id": _id}).One(&log); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, log)
}
