package player

import (
	//"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"strconv"
	"inmallgame/app/data-access"
	//"inmallgame/app/helpers"
	"inmallgame/app/utils"
	"inmallgame/app/models"
	// "log"
	"time"
	// "encoding/json"
	"fmt"
)

func shareShopAddCoin(ctx iris.Context){
	playerID := ctx.PostValue("playerId")
	shopID := ctx.PostValue("shopId")
	coin:= ctx.PostValue("addCoin")  
	diamond := ctx.PostValue("addDiamond")
	
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
		"shopId": validation.Validate(shopID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	if coin == "" && diamond == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "coin or diamond should not empty field.", "")
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShareShopAddCoinCollection)
	collPlayer := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	settingColl := session.DB(config.DbName).C(config.DBCollection.SettingCollection)
	pipeSetting := settingColl.Pipe([]bson.M{})
	setting:=bson.M{}
	errSetting := pipeSetting.One(&setting)
	if errSetting != nil {
		return
	}
	
	update:=bson.M{}

	share:=bson.M{}
	pipePlayer := coll.Pipe([]bson.M{
		{"$match":bson.M{
			"shopId": bson.ObjectIdHex(shopID),
			"playerId": bson.ObjectIdHex(playerID)},},
	})
	errShare := pipePlayer.One(&share)
	if errShare != nil {
	
		update["playerId"] = bson.ObjectIdHex(playerID)
		update["shopId"] = bson.ObjectIdHex(shopID)
		update["created_at"]=time.Now()
		update["lastModified"]=time.Now()
		mongoErr := coll.Insert(update)
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			//return
		}
	}

	couponTimeOutHour := setting["couponShareTimeOutHours"].(int)
	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)

	player:=models.Player{}
	pipePlayer = collPlayer.Pipe([]bson.M{
		{"$match":bson.M{
			"_id": bson.ObjectIdHex(playerID)},},
	})
	errPlayer := pipePlayer.One(&player)
	if errPlayer!= nil {
		utils.ResponseFailure(ctx, 404, nil, fmt.Sprint(errPlayer))
		return
	}


	LastModified := player.LastModified.In(location)
	hs := now.Sub(LastModified).Hours()

	coinReturn:=0
	diamondReturn:=0
	if coin != "" {
		coinInt, err := strconv.Atoi(coin)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "coin should be integer", "")
			return
		}
		update["coin"] = coinInt
		coinReturn=coinInt
	}else{
		update["coin"] = 0
	}
	if diamond != "" {
		diamondInt, err := strconv.Atoi(diamond)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "diamond should be integer", "")
			return
		}
		update["diamond"] = diamondInt
		diamondReturn=diamondInt
	}else{
		update["diamond"] = 0
	}
	
	coinUpdate:=false
	diamondUpdate:=false
	if hs > float64(couponTimeOutHour) {
		if coin != "" {
			coinUpdate=true
		}
		if diamond != "" {
			diamondUpdate=true

		}	

		if err := collPlayer.Update(bson.M{"_id": bson.ObjectIdHex(playerID),},
				bson.M{
					"$inc":         update,
					"$currentDate": bson.M{"lastModified": true},
				}); err != nil {
				fmt.Println("Update fail.", err)

				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update fail.", fmt.Sprint(err))
				return
			}
	
		
	}

	playerReturn:=models.Player{}
	pipePlayer = collPlayer.Pipe([]bson.M{
		{"$match":bson.M{
			"_id": bson.ObjectIdHex(playerID)},},
	})
	errPlayer = pipePlayer.One(&playerReturn)
	if errPlayer!= nil {
		utils.ResponseFailure(ctx, 404, nil, fmt.Sprint(errPlayer))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{
		"diamond":playerReturn.Diamond,
		"diamonUpdate":diamondUpdate,
		"addDiamond":diamondReturn,
		"coinUpdate":coinUpdate,
		"coin":playerReturn.Coin,
		"addCoin":coinReturn,
	})

}