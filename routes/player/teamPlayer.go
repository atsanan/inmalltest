package player

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	"strconv"
)

// TeamPlayer @deprecated route.
func GetTeamPlayer(ctx iris.Context) {
	myClaim, _ := utils.TokenParser(ctx)
	config := utils.ConfigParser(ctx)

	player := map[string]interface{}{}

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	if err := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).Select(bson.M{"teamPlayerId": 1}).One(&player); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, player)
}

// UpdateTeamPlayer will serve post mothod.
func UpdateTeamPlayer(ctx iris.Context) {
	teamID := ctx.FormValue("teamPlayerId")
	if teamID == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "teamPlayerId should not empty field.", "")
		return
	}
	teamPlayerID, err := strconv.Atoi(teamID)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "teamPlayerID should be integer", "")
		return
	}

	myClaim, _ := utils.TokenParser(ctx)
	config := utils.ConfigParser(ctx)
	player := models.Player{}

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	if err := coll.Update(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)},
		bson.M{
			"$set":         bson.M{"teamPlayerId": teamPlayerID},
			"$currentDate": bson.M{"lastModified": true},
		}); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(err))
		return
	}

	if err := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).One(&player); err != nil {
		fmt.Println("Not found.", myClaim)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, player)
}
