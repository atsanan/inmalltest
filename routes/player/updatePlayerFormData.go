package player

import (
	"fmt"
	"strings"

	// "log"
	// "time"
	// "encoding/json"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"strconv"
)

// UpdateFormData will serve player/{id}.
func UpdatePlayerFormData(ctx iris.Context) {
	playerID := ctx.Params().Get("id")

	if validateErr := utils.ValidateMongoID("id", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "", fmt.Sprint(validateErr))
	}

	gender := ctx.PostValue("gender")
	teamPlayerID := ctx.PostValue("teamPlayerId")
	costumeSelectID := ctx.PostValue("costumeSelectId")
	playerName := ctx.PostValue("playerName")
	coin, diamond := ctx.FormValue("coin"), ctx.FormValue("diamond")
	err := validation.Errors{
		"teamPlayerId":    validation.Validate(teamPlayerID, is.MongoID),
		"costumeSelectId": validation.Validate(costumeSelectID, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	costumeList := ctx.PostValue("costumeList")

	update := bson.M{}

	if costumeList != "" {
		selects := strings.Split(costumeList, ",")

		// list := make([]string, len(selects))
		// fieldLen := len(selects)
		// if fieldLen > 0 {
		// for i := 0; i < fieldLen; i++ {
		// 	if selects[i] != "" {

		// 		list = append(list, selects[i][5:])
		// 	}
		// }

		update["costumeList"] = selects
		///}
	}

	if gender != "" {
		update["gender"] = gender
	}

	if teamPlayerID != "" {
		update["teamPlayerId"] = bson.ObjectIdHex(teamPlayerID)
	}
	if playerName != "" {
		update["playerName"] = playerName
	}

	if costumeSelectID != "" {
		update["costumeSelectId"] = bson.ObjectIdHex(costumeSelectID)
	}

	if coin != "" {
		coinInt, err := strconv.Atoi(coin)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "coin should be integer", "")
			return
		}

		update["coin"] = coinInt
	}
	if diamond != "" {
		diamondInt, err := strconv.Atoi(diamond)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "diamond should be integer", "")
			return
		}

		update["diamond"] = diamondInt
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	mongoErr := coll.Update(
		bson.M{"_id": bson.ObjectIdHex(playerID)},
		bson.M{"$set": update, "$currentDate": bson.M{"lastModified": true}})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	player := bson.M{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(playerID)}).One(&player)
	if findErr != nil {

		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
	}

	utils.ResponseSuccess(ctx, player)
}
