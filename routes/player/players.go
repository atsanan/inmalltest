package player

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AllPlayers will serve playerInfo.
func AllPlayers(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	players := []models.Player{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	count, _ := coll.Count()
	pages := utils.CalcPages(count, limit)

	err := coll.Find(bson.M{}).Skip(offset).Limit(limit).Select(bson.M{"password": 0}).All(&players)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	linkF := "/api/v1/players?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"players":   players,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}
