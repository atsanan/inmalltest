package player

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	"strconv"
)

// CoinAndDiamond will serve get mothod.
func CoinAndDiamond(ctx iris.Context) {
	myClaim, err := utils.TokenParser(ctx)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, err.Error())
		return
	}

	config := utils.ConfigParser(ctx)

	player := map[string]interface{}{}

	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	if err := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).Select(bson.M{"coin": 1, "diamond": 1}).One(&player); err != nil {
		fmt.Println("Not found.", myClaim)

		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, player)
}

// UpdateCoinAndDiamond will serve post mothod.
func UpdateCoinAndDiamond(ctx iris.Context) {
	coin, diamond := ctx.FormValue("coin"), ctx.FormValue("diamond")
	if coin == "" && diamond == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "coin or diamond should not empty field.", "")
		return
	}

	update := bson.M{}
	if coin != "" {
		coinInt, err := strconv.Atoi(coin)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "coin should be integer", "")
			return
		}

		update["coin"] = coinInt
	}
	if diamond != "" {
		diamondInt, err := strconv.Atoi(diamond)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "diamond should be integer", "")
			return
		}

		update["diamond"] = diamondInt
	}

	myClaim, err := utils.TokenParser(ctx)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Parsing token fail.", err.Error())
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	if err := coll.Update(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)},
		bson.M{
			"$set":         update,
			"$currentDate": bson.M{"lastModified": true},
		}); err != nil {
		fmt.Println("Update fail.", err)

		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update fail.", fmt.Sprint(err))
		return
	}

	player := models.Player{}
	if err := coll.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).One(&player); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, player)
}
