package player

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Get("/info", auth.TokenMDW, Player)
	routes.Get("/coinAndDiamond", auth.TokenMDW, CoinAndDiamond)
	routes.Post("/coinAndDiamond", auth.TokenMDW, UpdateCoinAndDiamond)
	routes.Post("/addLog", auth.TokenMDW, AddPlayerLog)
	routes.Get("/costume", auth.TokenMDW, Costume)
	routes.Post("/costume", auth.TokenMDW, UpdateCostume)
	routes.Post("/costume/new", auth.TokenMDW, NewCostume)
	routes.Get("/teamplayer", auth.TokenMDW, GetTeamPlayer)
	routes.Post("/teamplayer", auth.TokenMDW, UpdateTeamPlayer)
	routes.Post("/{id: string}", auth.TokenMDW, UpdatePlayer)
	routes.Post("/update/{id: string}", auth.TokenMDW, UpdatePlayerFormData)
	routes.Post("/updateCostumeList/{id: string}", auth.TokenMDW, UpdateCostumeList)
	routes.Get("/all", auth.TokenMDW, AllPlayers)
	routes.Get("/{playerId: string}", auth.TokenMDW, getDataPlayer)
	//routes.Post("/sendMessage", auth.TokenMDW, SendNotification)
	routes.Get("/giveMonsterVersion/{playerId: string}", auth.TokenMDW, giveMonsterVersion)
	routes.Get("/checkPushToken/{playerId: string}", auth.TokenMDW, CheckPushToken)

	routes.Post("/shareCouponAddCoin/", shareCouponAddCoin)
	routes.Post("/shareShopAddCoin", auth.TokenMDW, shareShopAddCoin)

}
