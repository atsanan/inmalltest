package player

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
	// "log"
	// "time"
	// "encoding/json"
)

// getDataPlayer will serve get mothod.
func getDataPlayer(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	player := bson.M{}
	pipe := coll.Pipe([]bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(playerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.UserCollection,
				"localField":   "userId",
				"foreignField": "_id",
				"as":           "user",
			},
		},
		{"$unwind": "$user"},
	},
	)
	findErr := pipe.One(&player)

	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, player)

}
