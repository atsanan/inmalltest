package chat

import (
	"fmt"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"

	// "github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

// AddMessage  Router
func AddMessage(ctx iris.Context) {

	// chat := models.Chat{}
	// if parseErr := ctx.ReadJSON(&chat); parseErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
	// 	return
	// }
	PlayerID := ctx.PostValue("playerId")
	FriendID := ctx.PostValue("friendId")
	Message := ctx.PostValue("message")
	MessageCode := ctx.PostValue("messageCode")

	// if validateErr := item.ValidateAll(); validateErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
	// 	return
	// }

	err := validation.Errors{
		"playerId": validation.Validate(PlayerID, validation.Required),
		"friendId": validation.Validate(FriendID, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	findFriend := bson.M{}
	insertChat := bson.M{}

	chatID := bson.NewObjectId()
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.FriendCollection)
	collChat := session.DB(config.DbName).C(config.DBCollection.ChatCollection)
	objQuery := []bson.M{}
	friend := coll.Find(bson.M{"$or": []bson.M{
		bson.M{
			"friendId": bson.ObjectIdHex(FriendID),
			"playerId": bson.ObjectIdHex(PlayerID),
		},
		bson.M{
			"friendId": bson.ObjectIdHex(FriendID),
			"playerId": bson.ObjectIdHex(PlayerID),
		},
	}}).One(&findFriend)
	if friend != nil {

		_id := bson.NewObjectId()
		insert := bson.M{
			"_id":                _id,
			"friendId":           bson.ObjectIdHex(FriendID),
			"playerId":           bson.ObjectIdHex(PlayerID),
			"friendActiveStatus": 0,
			"createAt":           time.Now(),
		}

		insertChat = bson.M{
			"_id":         chatID,
			"message":     Message,
			"friendId":    bson.ObjectIdHex(FriendID),
			"playerId":    bson.ObjectIdHex(PlayerID),
			"messageCode": MessageCode,
			"chartRoom":   _id,
			"createAt":    time.Now(),
		}

		objQuery = append(objQuery, bson.M{"$match": bson.M{
			"_id": chatID,
		}})

		mongoErr := coll.Insert(insert)
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			return
		}

		mongoChatErr := collChat.Insert(insertChat)
		if mongoChatErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert Chat data", fmt.Sprint(mongoChatErr))
			return
		}

	}

	if findFriend["_id"] != nil {

		insertChat = bson.M{
			"_id":         chatID,
			"message":     Message,
			"friendId":    bson.ObjectIdHex(FriendID),
			"playerId":    bson.ObjectIdHex(PlayerID),
			"messageCode": MessageCode,
			"chartRoom":   findFriend["_id"],
			"createAt":    time.Now(),
		}
		mongoChatErr := collChat.Insert(insertChat)
		if mongoChatErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert Chat data", fmt.Sprint(mongoChatErr))
			return
		}

		objQuery = append(objQuery, bson.M{"$match": bson.M{
			"_id": chatID,
		}})

	}

	objQuery = append(objQuery,
		bson.M{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "playerId",
				"foreignField": "_id",
				"as":           "player",
			},
		},
		bson.M{"$unwind": "$player"},
		bson.M{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "friendId",
				"foreignField": "_id",
				"as":           "friend",
			},
		},
		bson.M{"$unwind": "$friend"},
	)
	data := bson.M{}

	pipe := collChat.Pipe(objQuery)
	findErr := pipe.One(&data)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(objQuery))
		return
	}

	utils.ResponseSuccess(ctx, data)
}
