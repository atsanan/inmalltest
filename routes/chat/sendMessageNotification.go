package chat

import (
	"encoding/json"
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/kataras/iris"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

func sendMessageNotification(ctx iris.Context) {

	//player,friend,title,body,dataJson

	PlayerID := ctx.PostValue("playerId")
	FriendID := ctx.PostValue("friendId")
	Title := ctx.PostValue("title")
	Body := ctx.PostValue("body")
	dataJson := ctx.PostValue("dataJson")
	test := []byte(dataJson)

	stringMap := map[string]string{}
	errStr := json.Unmarshal(test, &stringMap)
	if errStr != nil {

		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errStr))
	}

	err := validation.Errors{
		"playerId": validation.Validate(PlayerID, validation.Required),
		"friendId": validation.Validate(FriendID, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)

	player := map[string]map[string]string{}
	errPlayer := coll.Pipe(
		[]bson.M{
			{"$match": bson.M{
				"_id": bson.ObjectIdHex(FriendID),
			},
			},
			{
				"$lookup": bson.M{
					"from":         config.DBCollection.UserCollection,
					"localField":   "userId",
					"foreignField": "_id",
					"as":           "user",
				},
			},
			{"$unwind": "$user"},
		},
	).One(&player)

	if errPlayer != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errPlayer))
		return
	}
	opt := option.WithCredentialsFile("inmallgotchimons.json")
	app, errfire := firebase.NewApp(context.Background(), nil, opt)
	if errfire != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
	}

	context := context.Background()
	client, errMes := app.Messaging(context)
	if errMes != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
	}

	registrationToken := player["user"]["pushMessagesAccessToken"]
	//topic := "highScores"
	//condition := "'stock-GOOG' in topics || 'industry-tech' in topics"

	if registrationToken == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint("pushMessagesAccessToken is Empty"))

	} else {

		message := &messaging.Message{
			Notification: &messaging.Notification{
				Title: Title,
				Body:  Body,
				//Data:  dataJson,
			},
			Data: stringMap,
			//Topic:topic,
			Token: registrationToken,
			//Condition: condition,
		}

		response, errSend := client.Send(context, message)
		if errSend != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errSend))
		}

		data := bson.M{}
		data["path"] = response
		utils.ResponseSuccess(ctx, data)
	}
}
