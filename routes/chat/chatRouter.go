package chat

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter will serve as router handler.
func InitRouter(chatRoutes router.Party) {
	chatRoutes.Post("/addMessage", auth.TokenMDW, AddMessage)
	chatRoutes.Post("/sendMessageNotification", auth.TokenMDW, sendMessageNotification)
	chatRoutes.Get("/chatList/{playerId:string}/{friendId:string}", auth.TokenMDW, FeedChat)
}
