package chat

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

//FeedChat routes
func FeedChat(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	friendID := ctx.Params().Get("friendId")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	err := validation.Errors{
		"playerID": validation.Validate(playerID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ChatCollection)
	objQuery := []bson.M{
		{"$match": bson.M{
			"$or": []bson.M{
				bson.M{
					"playerId": bson.ObjectIdHex(playerID),
					"friendId": bson.ObjectIdHex(friendID),
				},
				bson.M{
					"friendId": bson.ObjectIdHex(playerID),
					"playerId": bson.ObjectIdHex(friendID),
				},
			},
		},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.FriendCollection,
				"localField":   "chartRoom",
				"foreignField": "_id",
				"as":           "chat",
			},
		},
		{"$unwind": "$chat"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "playerId",
				"foreignField": "_id",
				"as":           "player",
			},
		},
		{"$unwind": "$player"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "friendId",
				"foreignField": "_id",
				"as":           "friend",
			},
		},
		{"$unwind": "$friend"},
	}

	pipe := coll.Pipe(objQuery)

	mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(countErr))
		return
	}
	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)
	friendList := []bson.M{}
	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe = coll.Pipe(objQuery)
	findErr := pipe.All(&friendList)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	originalLink := fmt.Sprintf("/api/v1/chat/chatList/%s/%s", playerID, friendID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"chat":      friendList,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}
