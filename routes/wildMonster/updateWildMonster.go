package wildMonster

import (
	"fmt"

	"github.com/kataras/iris"

	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

// "encoding/json"

// "github.com/globalsign/mgo"

// "github.com/novalagung/gubrak"
// "github.com/fatih/structs"

// "github.com/imdario/mergo"

// "time"
// "strconv"

//UpdateItemWildMonster function
func UpdateItemWildMonster(ctx iris.Context) {
	wildMonsterID := ctx.Params().Get("id")
	itemID := ctx.PostValue("itemId")
	err := validation.Errors{
		"wildMonsterId": validation.Validate(wildMonsterID, validation.Required, is.MongoID),
		"itemId":        validation.Validate(itemID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
	}

	update := bson.M{
		"itemId": bson.ObjectIdHex(itemID),
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	wMonsterColl := session.DB(config.DbName).C(config.DBCollection.WildMonsterCollection)
	mongoErr := wMonsterColl.Update(bson.M{"_id": bson.ObjectIdHex(wildMonsterID)}, bson.M{
		"$set":         update,
		"$currentDate": bson.M{"lastModified": true},
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	result := bson.M{}
	if err := wMonsterColl.Find(bson.M{"_id": bson.ObjectIdHex(wildMonsterID)}).One(&result); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, err.Error())
		return
	}

	utils.ResponseSuccess(ctx, result)

}
