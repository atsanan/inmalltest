package wildMonster

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Get("/byZone", auth.TokenMDW, GetWildMonstersByZone)

	routes.Get("/monstersInstantiate/{playerId:string}", auth.TokenMDW, monstersInstantiate)

	routes.Get("/byZoneAndPlayer", auth.TokenMDW, GetWildMonstersByZoneAndPlayer)
	routes.Post("/pick", auth.TokenMDW, CatchWildMonster)
	routes.Get("/count/{itemId:string}", GetWildMonsterCountAndCountMax)
	// For admin
	routes.Post("/", AddWildMonster)
	routes.Post("/{id:string}", UpdateItemWildMonster)
}
