package wildMonster

import (
	// "encoding/json"
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	// "github.com/fatih/structs"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	// "github.com/imdario/mergo"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "time"
	// "strconv"
)

func validateCatchWildMonster(wildMonsterID string, playerID string) error {
	err := validation.Errors{
		"wildMonsterId": validation.Validate(wildMonsterID, validation.Required, is.MongoID),
		"playerId":      validation.Validate(playerID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		return err
	}

	return nil
}

// CatchWildMonster for admin use.
func CatchWildMonster(ctx iris.Context) {
	wildMonsterID := ctx.PostValue("wildMonsterId")
	playerID := ctx.PostValue("playerId")

	if err := validateCatchWildMonster(wildMonsterID, playerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	// Find wildMonster.
	wMonster := models.WildMonster{}
	wMonsterColl := session.DB(config.DbName).C(config.DBCollection.WildMonsterCollection)

	if findErr := wMonsterColl.Find(bson.M{"_id": bson.ObjectIdHex(wildMonsterID)}).One(&wMonster); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(findErr))
		return
	}

	// Check wildMonster count before allow player catch.
	if wMonster.Count > 0 {
		pMonster := models.MonsterPlayer{
			ID:                       bson.NewObjectId(),
			PlayerID:                 bson.ObjectIdHex(playerID),
			MDefaultID:               wMonster.MDefaultID,
			MPlayerNewDateTime:       time.Now(),
			MPlayerLastDateTime:      time.Now(),
			MPlayerFoodDatetime:      time.Now(),
			MPlayerHealthDatetime:    time.Now(),
			MPlayerHappinessDatetime: time.Now(),
			MPlayerQuest1Datetime:    time.Now(),
			MPlayerQuest2Datetime:    time.Now(),
			MPlayerQuest3Datetime:    time.Now(),
			MPlayerExpDatetime:       time.Now(),
			CreateAt:                 time.Now(),
			LastModified:             time.Now(),
		}

		resp := bson.M{}
		if wMonster.IsPickToBag == true {

			mPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
			if mongoErr := mPlayerColl.Insert(pMonster); mongoErr != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
				return
			}

			pipe := mPlayerColl.Pipe([]bson.M{
				{"$match": bson.M{"_id": pMonster.ID}},
				{
					"$lookup": bson.M{
						"from":         config.DBCollection.MonstersDefaultCollection,
						"localField":   "mDefaultId",
						"foreignField": "_id",
						"as":           "monstersDefault",
					},
				},
				{"$unwind": "$monstersDefault"},
			})

			if err := pipe.One(&resp); err != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to pipe data", fmt.Sprint(err))
				return
			}

			// Reduce wildMonster pool.
			if updateErr := wMonsterColl.Update(bson.M{"_id": bson.ObjectIdHex(wildMonsterID)},
				bson.M{"$inc": bson.M{"count": -1}}); updateErr != nil {
				// Revoke previous transaction.
				mPlayerColl.Remove(bson.M{"_id": pMonster.ID})

				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(updateErr))
				return
			}
		} else {
			// Reduce wildMonster pool.
			if updateErr := wMonsterColl.Update(bson.M{"_id": bson.ObjectIdHex(wildMonsterID)},
				bson.M{"$inc": bson.M{"count": -1}}); updateErr != nil {
				// Revoke previous transaction.
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(updateErr))
				return
			}
		}

		utils.ResponseSuccess(ctx, iris.Map{"monster": resp})
	} else {
		utils.ResponseSuccess(ctx, iris.Map{"message": "Cannot catch wildMonster because out of limit"})
	}
}
