package wildMonster

import (
	"fmt"
	"math/rand"
	"time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	//"time"
	// "encoding/json
)

// GetWildMonstersByZoneAndPlayer will serve as router.
func GetWildMonstersByZoneAndPlayer(ctx iris.Context) {
	zoneID := ctx.URLParam("zoneTypeId")
	isRealPosition := ctx.URLParam("isRealPosition")
	ID := ctx.URLParam("id")
	playerID := ctx.URLParam("playerId")
	monstersLimit := ctx.URLParamIntDefault("limit", 5)

	err := validation.Errors{
		"playerId":   validation.Validate(playerID, validation.Required, validation.NotNil, is.MongoID),
		"zoneTypeId": validation.Validate(zoneID, validation.Required, validation.NotNil, is.MongoID),
		//"isRealPosition": validation.Validate(isRealPosition, validation.Required),
		"limit": validation.Validate(monstersLimit, validation.NotNil),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	query := []bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	//query = append(query, bson.M{"zoneTypeId": bson.ObjectIdHex(zoneID)})
	zoneTypeColl := session.DB(config.DbName).C(config.DBCollection.ZoneTypeCollection)
	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	shopColl := session.DB(config.DbName).C(config.DBCollection.ShopCollection)

	zoneTypes := map[string]map[string]string{}
	zoneTypesQuery := bson.M{"_id": bson.ObjectIdHex(zoneID)}
	mongoErr := zoneTypeColl.Find(zoneTypesQuery).One(&zoneTypes)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}
	qu := bson.M{}

	zoneTypeName := zoneTypes["zoneTypeName"]

	world := bson.M{}
	worldErr := zoneTypeColl.Find(bson.M{"zoneTypeName.eng": "World"}).One(&world)
	if worldErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(worldErr))
		return
	}

	qu["isActive"] = true

	if zoneTypeName["eng"] != "World" {

		errId := validation.Errors{
			"id": validation.Validate(ID, is.MongoID),
		}.Filter()
		if errId != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
			return
		}

		mallZone := bson.M{}
		mallErr := zoneTypeColl.Find(bson.M{"zoneTypeName.eng": "InMall"}).One(&mallZone)
		if mallErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mallErr))
			return
		}

		mallFloorZone := bson.M{}
		mallFloorErr := zoneTypeColl.Find(bson.M{"zoneTypeName.eng": "InMall Floor"}).One(&mallFloorZone)
		if mallFloorErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mallFloorErr))
			return
		}

		shopZone := bson.M{}
		shopErr := zoneTypeColl.Find(bson.M{"zoneTypeName.eng": "InMall Shop"}).One(&shopZone)
		if shopErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(shopErr))
			return
		}

		if ID != "" {

			if zoneTypeName["eng"] == "InMall" {
				query = append(query, bson.M{"zoneTypeId": world["_id"]})
				query = append(query, bson.M{"toMall": bson.ObjectIdHex(ID), "zoneTypeId": mallZone["_id"]})
			}

			if zoneTypeName["eng"] == "InMall Floor" {
				query = append(query, bson.M{"zoneTypeId": world["_id"]})
				mallFloor := bson.M{}
				mongoErr := mallFloorColl.Find(bson.M{"_id": bson.ObjectIdHex(ID)}).One(&mallFloor)
				if mongoErr != nil {
					utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
					return
				}
				query = append(query, bson.M{"toMall": mallFloor["mallId"],
					"zoneTypeId": mallZone["_id"],
				})
				query = append(query, bson.M{"toMallFloor": bson.ObjectIdHex(ID),
					"zoneTypeId": mallFloorZone["_id"],
				})
			}

			if zoneTypeName["eng"] == "InMall Shop" {

				query = append(query, bson.M{"zoneTypeId": world["_id"]})

				shop := bson.M{}
				mongoErr := shopColl.Find(bson.M{"_id": bson.ObjectIdHex(ID)}).One(&shop)
				if mongoErr != nil {
					utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
					return
				}

				query = append(query, bson.M{"toMallFloor": shop["mallFloorId"],
					"zoneTypeId": mallFloorZone["_id"],
				})

				mallFloor := bson.M{}
				mallFloorErr := mallFloorColl.Find(bson.M{"_id": shop["mallFloorId"]}).One(&mallFloor)
				if mallFloorErr != nil {
					utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mallFloorErr))
					return
				}

				query = append(query, bson.M{"toMall": mallFloor["mallId"],
					"zoneTypeId": mallZone["_id"],
				})

				query = append(query, bson.M{"toShop": bson.ObjectIdHex(ID),
					"zoneTypeId": shopZone["_id"],
				})
			}
			qu["$or"] = query
		} else {
			if zoneTypeName["eng"] == "InMall" {
				query = append(query, bson.M{"zoneTypeId": world["_id"]})
				query = append(query, bson.M{"zoneTypeId": mallZone["_id"]})

				qu["$or"] = query
			}

			if zoneTypeName["eng"] == "InMall Floor" {
				query = append(query, bson.M{"zoneTypeId": world["_id"]})
				query = append(query, bson.M{"zoneTypeId": mallZone["_id"]})
				query = append(query, bson.M{"zoneTypeId": mallFloorZone["_id"]})
				qu["$or"] = query

			}

			if zoneTypeName["eng"] == "InMall Shop" {
				query = append(query, bson.M{"zoneTypeId": world["_id"]})
				query = append(query, bson.M{"zoneTypeId": mallZone["_id"]})
				query = append(query, bson.M{"zoneTypeId": mallFloorZone["_id"]})
				query = append(query, bson.M{"zoneTypeId": shopZone["_id"]})

				qu["$or"] = query

			}

		}
	} else {
		qu["zoneTypeId"] = world["_id"]
	}

	wildMonsterColl := session.DB(config.DbName).C(config.DBCollection.WildMonsterCollection)
	itemPlayercoll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	if isRealPosition == "False" {
		qu["isRealPosition"] = false
	}
	qu["isFDGetCoupon"] = bson.M{"$ne": true}
	pipe := wildMonsterColl.Pipe([]bson.M{
		{"$match": qu},
		{"$match": bson.M{"count": bson.M{"$gt": 0}}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MonstersDefaultCollection,
				"localField":   "mDefaultId",
				"foreignField": "_id",
				"as":           "monsterDefault",
			},
		},
		{"$unwind": "$monsterDefault"},
		{"$limit": monstersLimit},
	})

	monsters := []bson.M{}
	if mongoErr := pipe.All(&monsters); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to pipe data", fmt.Sprint(mongoErr))
		return
	}

	monsterModels := []models.WildMonster{}
	if mongoErr := pipe.All(&monsterModels); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to pipe data", fmt.Sprint(mongoErr))
		return
	}

	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)
	data := []bson.M{}

	min := 1
	max := 100
	for K, v := range monsters {
		rand := rand.Intn(max-min) + min

		if rand <= v["pickPercent"].(int) && v["pickPercent"].(int) > 0 {
			//StartDateTime := monsterModels[K].StartDateTime
			ExpiredDateTime := monsterModels[K].ExpiredDateTime

			if v["itemId"] != nil {
				if v["itemStock"].(int) > 0 {
					itemPlayer := bson.M{}
					err := itemPlayercoll.Find(bson.M{"itemId": v["itemId"], "playerId": bson.ObjectIdHex(playerID)}).One(&itemPlayer)
					if err != nil {
						if v["isReachDateTime"].(bool) == false {
							data = append(data, monsters[K])
						} else {
							if ExpiredDateTime.Year() > now.Year() {
								data = append(data, monsters[K])
							} else if ExpiredDateTime.Year() == now.Year() {

								if int(ExpiredDateTime.Month()) > int(now.Month()) {
									data = append(data, monsters[K])
								} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
									if ExpiredDateTime.Day() > now.Day() {
										data = append(data, monsters[K])
									} else if ExpiredDateTime.Day() == now.Day() {
										if ExpiredDateTime.Hour() > now.Hour() {
											data = append(data, monsters[K])
										} else {
											if ExpiredDateTime.Hour() == now.Hour() {

												if ExpiredDateTime.Minute() > now.Minute() {
													data = append(data, monsters[K])
												}
											}
										}
									}
								}
							}
						}
					} else {
						if itemPlayer["count"].(int) < v["itemStock"].(int) {
							if v["isReachDateTime"].(bool) == false {
								data = append(data, monsters[K])
							} else {
								if ExpiredDateTime.Year() > now.Year() {
									data = append(data, monsters[K])
								} else if ExpiredDateTime.Year() == now.Year() {

									if int(ExpiredDateTime.Month()) > int(now.Month()) {
										data = append(data, monsters[K])
									} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
										if ExpiredDateTime.Day() > now.Day() {
											data = append(data, monsters[K])
										} else if ExpiredDateTime.Day() == now.Day() {
											if ExpiredDateTime.Hour() > now.Hour() {
												data = append(data, monsters[K])
											} else {
												if ExpiredDateTime.Hour() == now.Hour() {

													if ExpiredDateTime.Minute() > now.Minute() {
														data = append(data, monsters[K])
													}
												}
											}
										}
									}
								}
							}
						}
					}
				} else {
					if v["isReachDateTime"].(bool) == false {
						data = append(data, monsters[K])
					} else {
						if ExpiredDateTime.Year() > now.Year() {
							data = append(data, monsters[K])
						} else if ExpiredDateTime.Year() == now.Year() {

							if int(ExpiredDateTime.Month()) > int(now.Month()) {
								data = append(data, monsters[K])
							} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
								if ExpiredDateTime.Day() > now.Day() {
									data = append(data, monsters[K])
								} else if ExpiredDateTime.Day() == now.Day() {
									if ExpiredDateTime.Hour() > now.Hour() {
										data = append(data, monsters[K])
									} else {
										if ExpiredDateTime.Hour() == now.Hour() {

											if ExpiredDateTime.Minute() > now.Minute() {
												data = append(data, monsters[K])
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				if v["isReachDateTime"].(bool) == false {
					data = append(data, monsters[K])
				} else {
					if ExpiredDateTime.Year() > now.Year() {
						data = append(data, monsters[K])
					} else if ExpiredDateTime.Year() == now.Year() {

						if int(ExpiredDateTime.Month()) > int(now.Month()) {
							data = append(data, monsters[K])
						} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
							if ExpiredDateTime.Day() > now.Day() {
								data = append(data, monsters[K])
							} else if ExpiredDateTime.Day() == now.Day() {
								if ExpiredDateTime.Hour() > now.Hour() {
									data = append(data, monsters[K])
								} else {
									if ExpiredDateTime.Hour() == now.Hour() {

										if ExpiredDateTime.Minute() > now.Minute() {
											data = append(data, monsters[K])
										}
									}
								}
							}
						}
					}

				}
			}

		}

	}

	result := map[string]interface{}{
		"monsters": data,
	}
	utils.ResponseSuccess(ctx, result)
}
