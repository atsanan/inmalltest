package wildMonster

import (
	"fmt"
	"log"
	"time"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

func validateWildMonster(ctx iris.Context, wMonster *models.WildMonster) error {
	if parseErr := ctx.ReadJSON(&wMonster); parseErr != nil {
		return parseErr
	}

	if validateErr := wMonster.ValidateAll(); validateErr != nil {
		return validateErr
	}

	return nil
}

// AddWildMonster for admin use.
func AddWildMonster(ctx iris.Context) {
	wMonster := models.WildMonster{}

	if parseErr := validateWildMonster(ctx, &wMonster); parseErr != nil {
		log.Println("parseErr", parseErr)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	wMonster.ID = bson.NewObjectId()
	wMonster.CreateAt = time.Now()

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.WildMonsterCollection)
	mongoErr := coll.Insert(wMonster)
	if mongoErr != nil {
		log.Println("mongoErr", mongoErr)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	monsters := models.WildMonster{}
	if findErr := coll.Find(bson.M{"_id": wMonster.ID}).One(&monsters); findErr != nil {
		log.Println("findErr", findErr)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, monsters)
}
