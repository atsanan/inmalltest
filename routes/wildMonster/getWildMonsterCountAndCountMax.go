package wildMonster

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

// GetWildMonsterCountAndCountMax find by Player .
func GetWildMonsterCountAndCountMax(ctx iris.Context) {
	itemID := ctx.Params().Get("itemId")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	err := validation.Errors{
		"itemId": validation.Validate(itemID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.WildMonsterCollection)

	objQuery := []bson.M{
		{"$match": bson.M{"itemId": bson.ObjectIdHex(itemID)}},
		{"$project": bson.M{"count": 1, "countMax": 1}},
	}
	wildMonster := []bson.M{}
	mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	if countErr != nil {
		utils.ResponseSuccess(ctx,
			bson.M{"wildMonster": wildMonster,
				"pageIndex": pageIndex,
				"pageLimit": pageLimit,
				"pages":     1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return
	}

	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&wildMonster)
	if findErr != nil {
		utils.ResponseSuccess(ctx,
			bson.M{
				"wildMonster": wildMonster,
				"pageIndex":   pageIndex,
				"pageLimit":   pageLimit,
				"pages":       1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return
	}

	linkF := fmt.Sprintf("/api/v1/wildMonster/count/%s?page=%d&limit=%d", itemID, page, limit)
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["wildMonster"] = wildMonster
	utils.ResponseSuccess(ctx, result)
}
