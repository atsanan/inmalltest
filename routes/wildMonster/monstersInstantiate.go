package wildMonster

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"inmallgame/app/models"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"strconv"
	// "log"
	// "time"
	// "encoding/json
)

func monstersInstantiate(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	lat := ctx.URLParamTrim("lat")
	long := ctx.URLParamTrim("long")
	distance := ctx.URLParamIntDefault("distance", 5000)
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, validation.NotNil, is.MongoID),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	//offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.WildMonsterCollection)
	zoneTypeColl := session.DB(config.DbName).C(config.DBCollection.ZoneTypeCollection)
	mallColl := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	shopColl := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	itemPlayerColl := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)

	wildMonster := []bson.M{}
	objQuery := []bson.M{
		// {
		// 	"$lookup": bson.M{
		// 		"from":         config.DBCollection.MonstersDefaultCollection,
		// 		"localField":   "mDefaultId",
		// 		"foreignField": "_id",
		// 		"as":           "monsterDefault",
		// 	},
		// },
		// {"$unwind": "$monsterDefault"},
		// {
		// 	"$lookup": bson.M{
		// 		"from":         config.DBCollection.ItemCollection,
		// 		"localField":   "itemId",
		// 		"foreignField": "_id",
		// 		"as":           "item",
		// 	},
		// },
		// {"$unwind": "$item"},
		// {"$lookup": bson.M{
		// 	"from":         config.DBCollection.CategoryItemCollection,
		// 	"localField":   "item.itemCategoryId",
		// 	"foreignField": "_id",
		// 	"as":           "item.itemCategory",
		// },
		// },
		// {"$match": bson.M{"isActive": true}},
	}

	
	if long != "" && lat != "" {
		longConv, _ := strconv.ParseFloat(long, 64)
		latConv, _ := strconv.ParseFloat(lat, 64)
		var coordinates [2]float64
		coordinates[0] = longConv
		coordinates[1] = latConv
		objQuery = append(objQuery, bson.M{
			"$geoNear": bson.M{
				"near":          bson.M{"type": "Point", "coordinates": coordinates},
				"maxDistance":   distance,
				"distanceField": "dist.calculated",
				"spherical":     true,
			},
		})

	}else{

		objQuery = append(objQuery, bson.M{"$sort": bson.M{
			"order":-1,
		}})
	}

	objQuery = append(objQuery, bson.M{	
		"$lookup": bson.M{
		"from":         config.DBCollection.MonstersDefaultCollection,
		"localField":   "mDefaultId",
		"foreignField": "_id",
		"as":           "monsterDefault",
	},
	}, bson.M{"$unwind": "$monsterDefault",})


	
	objQuery = append(objQuery, bson.M{	
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
	}, bson.M{"$unwind": "$item"},)

	
	objQuery = append(objQuery, bson.M{	
		"$lookup": bson.M{
			"from":         config.DBCollection.CategoryItemCollection,
			"localField":   "item.itemCategoryId",
			"foreignField": "_id",
			"as":           "item.itemCategory",
		},
	},bson.M{"$match": bson.M{"isActive": true,},})
	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.All(&wildMonster)
	if errCount != nil {
		result := map[string]interface{}{
			"pages":       0,
			"pageIndex":   pageIndex,
			"pageLimit":   pageLimit,
			"wildMonster": []bson.M{},
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}
		utils.ResponseSuccess(ctx, result)
		return
	}

	wildMonsterCheck := []models.WildMonster{}
	pipeCount.All(&wildMonsterCheck)
	data := []bson.M{}
	couponItemColl := session.DB(config.DbName).C(config.DBCollection.CouponItemCollection)
	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)
	if len(wildMonster) > 0 {
		for k, v := range wildMonster {
			itemPlayer := []bson.M{}
			itemPlayerColl.Find(bson.M{"itemId": v["itemId"],
				"playerId": bson.ObjectIdHex(playerID),
			}).All(&itemPlayer)

			if len(itemPlayer) < v["itemStock"].(int) {
				wildMonster[k]["isGet"] = true
			} else {
				wildMonster[k]["isGet"] = false
			}

			zoneTypes := map[string]map[string]string{}
			zoneTypesQuery := bson.M{"_id": v["zoneTypeId"]}
			mongoErr := zoneTypeColl.Find(zoneTypesQuery).One(&zoneTypes)
			if mongoErr == nil {

				zoneTypeName := zoneTypes["zoneTypeName"]
				if zoneTypeName["eng"] == "InMall" {
					mall := bson.M{}
					errMall := mallColl.Find(bson.M{"_id": v["toMall"]}).One(&mall)
					if errMall == nil {
						wildMonster[k]["mall"] = mall
					}

					mallFloor := bson.M{}
					errMallFloor := mallFloorColl.Find(bson.M{"_id": v["toMallFloor"]}).One(&mallFloor)
					if errMallFloor == nil {
						wildMonster[k]["mallFloor"] = mallFloor
					}
				} else if zoneTypeName["eng"] == "InMall Shop" {
					mall := bson.M{}
					errMall := mallColl.Find(bson.M{"_id": v["toMall"]}).One(&mall)
					if errMall == nil {
						wildMonster[k]["mall"] = mall
					}

					mallFloor := bson.M{}
					errMallFloor := mallFloorColl.Find(bson.M{"_id": v["toMallFloor"]}).One(&mallFloor)
					if errMallFloor == nil {
						wildMonster[k]["mallFloor"] = mallFloor
					}

					shop := bson.M{}
					errShop := shopColl.Find(bson.M{"_id": v["toShop"]}).One(&shop)
					if errShop == nil {
						wildMonster[k]["shop"] = shop
					}
				} else if zoneTypeName["eng"] == "InMall Floor" {
					mall := bson.M{}
					errMall := mallColl.Find(bson.M{"_id": v["toMall"]}).One(&mall)
					if errMall == nil {
						wildMonster[k]["mall"] = mall
					}

					mallFloor := bson.M{}
					errMallFloor := mallFloorColl.Find(bson.M{"_id": v["toMallFloor"]}).One(&mallFloor)
					if errMallFloor == nil {
						wildMonster[k]["mallFloor"] = mallFloor
					}
				}

			}

			//StartDateTime := wildMonsterCheck[k].StartDateTime
			ExpiredDateTime := wildMonsterCheck[k].ExpiredDateTime
			if v["itemId"] != nil{
				couponCountMax, _ := couponItemColl.Find(bson.M{"itemId":v["itemId"] }).Count()
				couponCodeCount, _ := couponItemColl.Find(bson.M{
					"itemId":      v["itemId"] ,
					"itemPlayerId": bson.M{"$exists": false},
				}).Count()
				wildMonster[k]["couponCodeCount"]=couponCodeCount
				wildMonster[k]["couponCountMax"]=couponCountMax
			}else{
				wildMonster[k]["couponCodeCount"]=0
				wildMonster[k]["couponCountMax"]=0
			}

			if v["isReachDateTime"].(bool) == false {
				if v["count"].(int) > 0 {
					data = append(data, wildMonster[k])
				}
			} else {
				if v["count"].(int) > 0 {

					//now.Year() >StartDateTime.Year() &&
					//int(ExpiredDateTime.Month())
					if ExpiredDateTime.Year() > now.Year() {
						data = append(data, wildMonster[k])
					} else if ExpiredDateTime.Year() == now.Year() {

						if int(ExpiredDateTime.Month()) > int(now.Month()) {
							data = append(data, wildMonster[k])
						} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
							if ExpiredDateTime.Day() > now.Day() {
								data = append(data, wildMonster[k])
							} else if ExpiredDateTime.Day() == now.Day() {
								if ExpiredDateTime.Hour() > now.Hour() {
									data = append(data, wildMonster[k])
								} else {
									if ExpiredDateTime.Hour() == now.Hour() {

										if ExpiredDateTime.Minute() > now.Minute() {
											data = append(data, wildMonster[k])
										}
									}
								}
							}
						}
					}
				}
				// if StartDateTime.Before(now) || StartDateTime.Equal(now) {
				// 	if ExpiredDateTime.After(now) || ExpiredDateTime.Equal(now) {
				// 		if v["count"].(int) > 0 {
				// 			data = append(data, wildMonster[k])
				// 		}
				// 	}
				// }
			}

		}

	}
	// mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	// if countErr != nil {
	// 	result := map[string]interface{}{
	// 		"pages":       0,
	// 		"pageIndex":   pageIndex,
	// 		"pageLimit":   pageLimit,
	// 		"wildMonster": []bson.M{},
	// 		"paging": map[string]interface{}{
	// 			"next":     "",
	// 			"previous": "",
	// 		},
	// 	}
	// 	utils.ResponseSuccess(ctx, result)
	// 	return
	// }
	//count := mcount["count"].(int)
	//	pages := utils.CalcPages(count, limit)

	// originalLink := fmt.Sprintf("/api/v1/wildMonster/monstersInstantiate/%s", playerID)
	// linkF := originalLink + "?page=%d&limit=%d"
	//nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := bson.M{} //utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["wildMonster"] = data

	utils.ResponseSuccess(ctx, result)
}
