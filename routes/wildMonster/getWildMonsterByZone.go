package wildMonster

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"

	// "inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	"strconv"
)

// GetWildMonstersByZone will serve as router.
func GetWildMonstersByZone(ctx iris.Context) {
	zoneID := ctx.URLParam("zoneTypeId")
	ID := ctx.URLParam("id")
	monstersLimit := ctx.URLParamIntDefault("limit", 5)

	err := validation.Errors{
		"zoneTypeId": validation.Validate(zoneID, validation.Required, validation.NotNil, is.MongoID),
		"limit":      validation.Validate(monstersLimit, validation.NotNil),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	zoneTypeID, _ := strconv.Atoi(zoneID)
	query := bson.M{"zoneTypeId": zoneTypeID}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	query["zoneTypeId"] = bson.ObjectIdHex(zoneID)
	zoneTypeColl := session.DB(config.DbName).C(config.DBCollection.ZoneTypeCollection)

	zoneTypes := map[string]map[string]string{}
	zoneTypesQuery := bson.M{"_id": bson.ObjectIdHex(zoneID)}
	mongoErr := zoneTypeColl.Find(zoneTypesQuery).One(&zoneTypes)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}

	zoneTypeName := zoneTypes["zoneTypeName"]
	if zoneTypeName["eng"] != "World" {

		errId := validation.Errors{
			"id": validation.Validate(ID, is.MongoID),
		}.Filter()
		if errId != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
			return
		}
		if ID != "" {
			if zoneTypeName["eng"] == "InMall" {
				query["toMall"] = bson.ObjectIdHex(ID)
			}

			if zoneTypeName["eng"] == "InMall Floor" {
				query["toMallFloor"] = bson.ObjectIdHex(ID)
			}

			if zoneTypeName["eng"] == "InMall Shop" {
				query["toShop"] = bson.ObjectIdHex(ID)
			}
		}
	} else {
		query["zoneTypeId"] = bson.ObjectIdHex(zoneID)
	}

	//if zoneTypeID == enums.InMallShop.ToInt() {
	// Query inmall shop info.
	//	query["toShop"] = fieldID
	//} else if zoneTypeID == enums.InMallFloor.ToInt() {
	// Query inmall floor info.
	//	query["toMallFloor"] = fieldID
	//}

	query["isActive"] = true
	wildMonsterColl := session.DB(config.DbName).C(config.DBCollection.WildMonsterCollection)

	pipe := wildMonsterColl.Pipe([]bson.M{
		{"$match": query},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MonstersDefaultCollection,
				"localField":   "mDefaultId",
				"foreignField": "_id",
				"as":           "monsterDefault",
			},
		},
		{"$unwind": "$monsterDefault"},
		{"$limit": monstersLimit},
	})

	monsters := []bson.M{}
	if mongoErr := pipe.All(&monsters); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to pipe data", fmt.Sprint(mongoErr))
		return
	}

	result := map[string]interface{}{
		"limit":    monstersLimit,
		"monsters": monsters,
	}
	utils.ResponseSuccess(ctx, result)
}
