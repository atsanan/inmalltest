package paymentMPayCreditCards

import (

	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

)

func DeletePaymentMPayCreditCard(ctx iris.Context){
	ID := ctx.Params().Get("id")
	err := validation.Errors{
		"id": validation.Validate(ID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PaymentMPayCreditCardCollection)
	mongoErr := coll.Remove(bson.M{"_id": bson.ObjectIdHex(ID)})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to delete PaymentMPay Credit Card", fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, nil)


}
