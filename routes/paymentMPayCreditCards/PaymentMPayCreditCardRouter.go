package paymentMPayCreditCards

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, AddPaymentMPayCreditCard)
	routes.Get("/{playerId:string}", auth.TokenMDW, GetPaymentMPayCreditCard)
	routes.Get("/delete/{id:string}", auth.TokenMDW, DeletePaymentMPayCreditCard)


}
