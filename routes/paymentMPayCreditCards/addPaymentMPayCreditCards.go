package paymentMPayCreditCards

import (

	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"time"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

)

func AddPaymentMPayCreditCard(ctx iris.Context){

	playerID := ctx.PostValue("playerId")
	creditCardType := ctx.PostValue("creditCardType")
	name := ctx.PostValue("name")
	description := ctx.PostValue("description")
	tokenKey := ctx.PostValue("tokenKey")
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}


	_id := bson.NewObjectId()
	save:=bson.M{
		"_id":_id,
		"playerId":bson.ObjectIdHex(playerID),
		"creditCardType":creditCardType,
		"name":name,
		"description":description,
		"tokenKey":tokenKey,
		"isActive":true,
		"createAt":time.Now(),
	}
	
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PaymentMPayCreditCardCollection)

	mongoErr := coll.Insert(save)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	paymentMPayCreditCards := bson.M{}
	if findErr := coll.Find(bson.M{"_id": _id}).One(&paymentMPayCreditCards); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, paymentMPayCreditCards)
}