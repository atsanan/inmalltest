package privilegeGroups

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Get("/", auth.TokenMDW, getPrivilegeGroup)

}
