package privilegeGroups

import (

	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

//getPrivilegeGroup
func getPrivilegeGroup(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	getPrivilegeColl := session.DB(config.DbName).C(config.DBCollection.PrivilegeGroupsCollection)
	baseQuery := []bson.M{
		{"$sort": bson.M{"order": -1}},
		{"$match": bson.M{"isActive": true}},
	}

	mcount, countErr := utils.CalcPipeCount(ctx, baseQuery, getPrivilegeColl)
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
		return
	}

	count := mcount["count"].(int)

	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := getPrivilegeColl.Pipe(offsetQuery)
	privilegeGroup := []bson.M{}
	if mongoErr := pipe.All(&privilegeGroup); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}
	getPrivilegeDefaultColl := session.DB(config.DbName).C(config.DBCollection.PrivilegeDefaultsCollection)

	privilegeDefaultRsults := []bson.M{}

	for _, v := range privilegeGroup {
		privilegeDefault := []bson.M{}
		err := getPrivilegeDefaultColl.Find(bson.M{"privilegeGroupId": v["_id"],
			"isActive": true,
		}).Sort("-order").All(&privilegeDefault)
		if err == nil {
			privilegeDefaultRsults = append(privilegeDefaultRsults,
				bson.M{
					"id":                    v["_id"],
					"filenameLogo1":         v["filenameLogo1"],
					"filenameLogo2":         v["filenameLogo2"],
					"isActive":              v["isActive"],
					"order":                 v["order"],
					"privilegeGroupDetails": v["privilegeGroupDetails"],
					"privilegeGroupNames":   v["privilegeGroupNames"],
					"created_at":            v["created_at"],
					"updated_at":            v["updated_at"],
					"privilegeDefaults":     privilegeDefault,
				})
			//return
		}

		if err != nil {
			privilegeDefaultRsults = append(privilegeDefaultRsults,
				bson.M{
					"id":                    v["_id"],
					"filenameLogo1":         v["filenameLogo1"],
					"filenameLogo2":         v["filenameLogo2"],
					"isActive":              v["isActive"],
					"order":                 v["order"],
					"privilegeGroupDetails": v["privilegeGroupDetails"],
					"privilegeGroupNames":   v["privilegeGroupNames"],
					"created_at":            v["created_at"],
					"updated_at":            v["updated_at"],
					"privilegeDefaults":     nil,
				})
		}
	}

	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/privilegeGroup")
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(originalLink)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["privilegeGroups"] = privilegeDefaultRsults

	utils.ResponseSuccess(ctx, result)

}
