package teamPlayer

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddTeamPlayer route  will add teamPlayer to system.
func AddTeamPlayer(ctx iris.Context) {

	teamPlayer := models.TeamPlayer{}
	if parseErr := ctx.ReadJSON(&teamPlayer); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(parseErr))
		return
	}

	_id := bson.NewObjectId()
	update := bson.M{
		"_id":                  _id,
		"teamPlayerNameEng":    teamPlayer.TeamPlayerNameEng,
		"teamPlayerNameThai":   teamPlayer.TeamPlayerNameThai,
		"teamPlayerNameChi1":   teamPlayer.TeamPlayerNameChi1,
		"teamPlayerNameChi2":   teamPlayer.TeamPlayerNameChi2,
		"teamPlayerDetailEng":    teamPlayer.TeamPlayerDetailEng,
		"teamPlayerDetailThai": teamPlayer.TeamPlayerDetailThai,
		"teamPlayerDetailChi1": teamPlayer.TeamPlayerDetailChi1,
		"teamPlayerDetailChi2": teamPlayer.TeamPlayerDetailChi2,
		"createAt":             time.Now(),
	}
	validateErr := teamPlayer.ValidateAll()
	if validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.TeamPlayerCollection)

	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	teamPlayers := []models.TeamPlayer{}
	findErr := coll.Find(bson.M{"teamPlayerDetailEng": teamPlayer.TeamPlayerDetailEng}).All(&teamPlayers)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, teamPlayers)
}
