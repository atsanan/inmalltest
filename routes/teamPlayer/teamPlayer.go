package teamPlayer

import (
	"fmt"
	// "time"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
)

// TeamPlayer route  will fetch all TeamPlayers.
func TeamPlayer(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	teamPlayerNameEng := ctx.URLParam("teamPlayerNameEng")
	teamPlayerNameThai := ctx.URLParam("teamPlayerNameThai")
	teamPlayerNameChi1 := ctx.URLParam("teamPlayerNameChi1")
	teamPlayerNameChi2 := ctx.URLParam("teamPlayerNameChi2")
	teamPlayerDetailEng := ctx.URLParam("teamPlayerDetailEng")
	teamPlayerDetailThai := ctx.URLParam("teamPlayerDetailThai")
	teamPlayerDetailChi1 := ctx.URLParam("teamPlayerDetailChi1")
	teamPlayerDetailChi2 := ctx.URLParam("teamPlayerDetailChi2")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.TeamPlayerCollection)

	query := bson.M{}

	if teamPlayerNameEng != "" {
		query["teamPlayerNameEng"] = bson.M{"$regex": teamPlayerNameEng, "$options": "i"}
	}

	if teamPlayerNameThai != "" {
		query["teamPlayerNameThai"] = bson.M{"$regex": teamPlayerNameThai, "$options": "i"}
	}

	if teamPlayerNameChi1 != "" {
		query["teamPlayerNameChi1"] = bson.M{"$regex": teamPlayerNameChi1, "$options": "i"}
	}

	if teamPlayerDetailEng != "" {
		query["teamPlayerDetailEng"] = bson.M{"$regex": teamPlayerDetailEng, "$options": "i"}
	}
	if teamPlayerDetailThai != "" {
		query["teamPlayerDetailThai"] = bson.M{"$regex": teamPlayerDetailThai, "$options": "i"}
	}
	if teamPlayerDetailChi1 != "" {
		query["teamPlayerDetailChi1"] = bson.M{"$regex": teamPlayerDetailChi1, "$options": "i"}
	}
	if teamPlayerNameChi2 != "" {
		query["teamPlayerNameChi2"] = bson.M{"$regex": teamPlayerNameChi2, "$options": "i"}
	}
	if teamPlayerDetailChi2 != "" {
		query["teamPlayerDetailChi2"] = bson.M{"$regex": teamPlayerDetailChi2, "$options": "i"}
	}

	teamPlayers := []bson.M{}
	err := coll.Find(query).Skip(offset).Limit(limit).All(&teamPlayers)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	count, _ := coll.Count()
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v1/teamPlayer?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":       pages,
		"pageIndex":   page,
		"pageLimit":   limit,
		"teamPlayers": teamPlayers,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}
	utils.ResponseSuccess(ctx, result)
}
