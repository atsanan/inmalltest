package teamPlayer

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/mitchellh/mapstructure"
)

func validateParams(teamPlayerID string) error {
	err := validation.Errors{
		"id": validation.Validate(teamPlayerID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		return err
	}
	return nil
}

// func validate(teamPlayerName string, teamPlayerDetail string) error {
// 	teamPlayer := models.TeamPlayer{
// 		TeamPlayerName:   teamPlayerName,
// 		TeamPlayerDetail: teamPlayerDetail,
// 	}
// 	validateErr := teamPlayer.ValidateSome()
// 	if validateErr != nil || (teamPlayerDetail == "" && teamPlayerName == "") {
// 		return validateErr
// 	}

// 	return nil
// }

// UpdateTeamPlayer route  will add teamPlayer to system.
func UpdateTeamPlayer(ctx iris.Context) {
	teamPlayerID := ctx.Params().Get("id")

	if err := validateParams(teamPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	// validateErr := validate(teamPlayerName, teamPlayerDetail)
	// if validateErr != nil || (teamPlayerDetail == "" && teamPlayerName == "") {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validation error or missing body.", fmt.Sprint(validateErr))
	// 	return
	// }

	update := map[string]interface{}{}
	if parseErr := ctx.ReadJSON(&update); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(parseErr))
		return
	}

	TeamPlayer := models.TeamPlayer{}
	decodeErr := mapstructure.Decode(update, &TeamPlayer)
	if decodeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "decode error", fmt.Sprint(decodeErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.TeamPlayerCollection)
	// if teamPlayerName != "" {
	// 	update["teamPlayerName"] = teamPlayerName
	// }
	// if teamPlayerDetail != "" {
	// 	update["teamPlayerDetail"] = teamPlayerDetail
	// }

	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(teamPlayerID)}, bson.M{
		"$set":         update,
		"$currentDate": bson.M{"lastModified": true}})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	var teamPlayer = models.TeamPlayer{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(teamPlayerID)}).One(&teamPlayer)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, teamPlayer)
}
