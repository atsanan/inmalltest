package sticker

import (
	"fmt"
	// "time"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

// GetMonsterStickerByToMonster will serve as router.
func GetMonsterStickerByToMonster(ctx iris.Context) {
	mPlayerID := ctx.Params().Get("mPlayerId")

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	if validErr := utils.ValidateMongoID("mPlayerId", mPlayerID); validErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validErr))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterStickerColl := session.DB(config.DbName).C(config.DBCollection.MonsterStickerCollection)

	baseQuery := []bson.M{
		{"$match": bson.M{"sendToMPlayerId": bson.ObjectIdHex(mPlayerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.StickerStoreCollection,
				"localField":   "stickerStoreId",
				"foreignField": "_id",
				"as":           "stickerStore",
			},
		},
		{"$unwind": "$stickerStore"},
	}
	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})

	monsterStickers := []bson.M{}
	if mongoErr := monsterStickerColl.Pipe(offsetQuery).All(&monsterStickers); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}
	mcount, countErr := utils.CalcPipeCount(ctx, baseQuery, monsterStickerColl)
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
		return
	}
	pages := utils.CalcPagesByPipeResult(mcount, limit)

	originalLink := fmt.Sprintf("/api/v1/sticker/byTo/%s", mPlayerID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)

	result["stickers"] = monsterStickers
	utils.ResponseSuccess(ctx, result)
}
