package sticker

import (
	"fmt"
	"time"

	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/go-ozzo/ozzo-validation"
)

func monsterStickerValidate(ctx iris.Context, monsterSticker *models.MonsterSticker) error {

	if parseErr := ctx.ReadJSON(&monsterSticker); parseErr != nil {
		return parseErr
	}

	if validateErr := monsterSticker.ValidationMonsterSticker(); validateErr != nil {
		return validateErr
	}

	return nil
}

// SendStickerToFriend will serve as router.
func SendStickerToFriend(ctx iris.Context) {
	monsterSticker := models.MonsterSticker{}

	stickerStoreID := ctx.PostValue("stickerStoreId")
	sendFromPlayerID := ctx.PostValue("sendFromPlayerId")
	sendToMPlayerID := ctx.PostValue("sendToMPlayerId")
	channelID := ctx.PostValue("channelId")
	errMessage := validation.Errors{
		"stickerStoreId":   validation.Validate(stickerStoreID, validation.Required),
		"sendFromPlayerId": validation.Validate(sendFromPlayerID, validation.Required),
		"sendToMPlayerId":  validation.Validate(sendToMPlayerID, validation.Required),
		"channelId":        validation.Validate(channelID, validation.Required),
	}.Filter()
	if errMessage != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMessage))
		return
	}

	// if validErr := monsterStickerValidate(ctx, &monsterSticker); validErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validErr))
	// 	return
	// }

	monsterSticker.ID = bson.NewObjectId()
	monsterSticker.StickerStoreID = bson.ObjectIdHex(stickerStoreID)
	monsterSticker.SendFromPlayerID = bson.ObjectIdHex(sendFromPlayerID)
	monsterSticker.SendToMPlayerID = bson.ObjectIdHex(sendToMPlayerID)
	monsterSticker.ChannelID = bson.ObjectIdHex(channelID)
	monsterSticker.CreateAt = time.Now()

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterStickerColl := session.DB(config.DbName).C(config.DBCollection.MonsterStickerCollection)
	if mongoErr := monsterStickerColl.Insert(monsterSticker); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	stickerResult := bson.M{}
	if mongoErr := monsterStickerColl.Find(bson.M{"_id": monsterSticker.ID}).One(&stickerResult); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, stickerResult)
}
