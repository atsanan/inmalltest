package sticker

import (
	"fmt"
	// "time"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

// GetMonsterStickerByChannel will serve as router.
func GetMonsterStickerByChannel(ctx iris.Context) {
	channelID := ctx.Params().Get("channelId")

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	if validErr := utils.ValidateMongoID("channelId", channelID); validErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validErr))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterStickerColl := session.DB(config.DbName).C(config.DBCollection.MonsterStickerCollection)

	baseQuery := []bson.M{
		{"$match": bson.M{"channelId": bson.ObjectIdHex(channelID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.StickerStoreCollection,
				"localField":   "stickerStoreId",
				"foreignField": "_id",
				"as":           "stickerStore",
			},
		},
		{"$unwind": "$stickerStore"},
	}
	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := monsterStickerColl.Pipe(offsetQuery)
	resp := []bson.M{}
	if mongoErr := pipe.All(&resp); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Pipe data fail", fmt.Sprint(mongoErr))
		return
	}

	mcount, countErr := utils.CalcPipeCount(ctx, baseQuery, monsterStickerColl)
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Pipe count fail", fmt.Sprint(countErr))
		return
	}
	pages := utils.CalcPagesByPipeResult(mcount, limit)

	originalLink := fmt.Sprintf("/api/v1/sticker/byChannel/%s", channelID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)

	result["stickers"] = resp
	utils.ResponseSuccess(ctx, result)
}
