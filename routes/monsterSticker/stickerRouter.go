package sticker

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Get("/byChannel/{channelId: string}", auth.TokenMDW, GetMonsterStickerByChannel)
	routes.Get("/byFrom/{playerId: string}", auth.TokenMDW, GetMonsterStickerByFromPlayer)
	routes.Get("/byTo/{mPlayerId: string}", auth.TokenMDW, GetMonsterStickerByToMonster)
	routes.Post("/toFriend", auth.TokenMDW, SendStickerToFriend)
}
