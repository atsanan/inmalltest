package item

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, AddItemDefault)
	routes.Post("/{id: string}", auth.TokenMDW, UpdateItemDefault)
	routes.Get("/", auth.TokenMDW, GetItemDefault)
	routes.Get("/list", auth.TokenMDW, GetItemDefaultV2)
	routes.Get("/{itemId: string}", auth.TokenMDW, GetItemDetail)
}
