package item

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"strconv"
	"strings"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//GetItemDefault func
func GetItemDefault(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	fields := ctx.URLParamTrim("fields")
	mainTab := ctx.URLParam("mainTab")
	search := ctx.URLParam("search")
	subCategory := ctx.URLParam("itemSubCategory")
	isHighlight := ctx.URLParamTrim("isHighlight")
	split := strings.Split(fields, ",")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemCollection)

	objQuery := []bson.M{}
	objQuery = append(objQuery, bson.M{"$match": bson.M{"isActive": true}})

	for _, value := range split {
		if value == "itemCategoryId" {
			objQuery = append(objQuery, bson.M{
				"$lookup": bson.M{
					"from":         config.DBCollection.CategoryItemCollection,
					"localField":   "itemCategoryId",
					"foreignField": "_id",
					"as":           "itemCategory",
				},
			}, bson.M{"$unwind": "$itemCategory"})
		}
	}

	params := ctx.URLParams()
	sortResult := bson.M{}
	if params["sort"] != "" {
		sort := strings.Split(params["sort"], ",")
		lenSort := len(sort)
		if lenSort > 0 {
			for _, Sort := range sort {
				keys := strings.Split(Sort, ":")
				sortKey, sortValue := keys[0], keys[1]
				val, _ := strconv.Atoi(sortValue)
				if val > 0 {
					sortResult[sortKey] = 1
				} else {
					sortResult[sortKey] = -1
				}
			}
			objQuery = append(objQuery, bson.M{"$sort": sortResult})
		}
	}

	if mainTab != "" {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"mainTab": mainTab}})
	}

	if subCategory != "" {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"subCategory": subCategory}})
	}

	boolIsHighlight, _ := strconv.ParseBool(isHighlight)
	if isHighlight != "" {
		objQuery = append(objQuery, bson.M{
			"$match": bson.M{
				"isHighlight": boolIsHighlight,
			},
		})
	}

	
	if search !=""{
		quuerSearch:=[]bson.M{
			{
				"itemName.eng":bson.M{"$regex":search,"$options":"i",},
			},
			{
				"itemName.thai":bson.M{"$regex":search,"$options":"i",},
			},	
			{
				"itemName.chi1":bson.M{"$regex":search,"$options":"i",},
			},	
			{
				"itemName.chi2":bson.M{"$regex":search,"$options":"i",},
			},
			{
				"itemDetail.eng":bson.M{"$regex":search,"$options":"i",},
			},
			{
				"item.thai":bson.M{"$regex":search,"$options":"i",},
			},	
			{
				"itemName.chi1":bson.M{"$regex":search,"$options":"i",},
			},	
			{
				"itemName.chi2":bson.M{"$regex":search,"$options":"i",},
			},
		}

		objQuery = append(objQuery, bson.M{"$match": bson.M{"$or":quuerSearch,}})
	}


	objQuery = append(objQuery, bson.M{"$sort": bson.M{"itemOrder": -1}})

	findItem := []bson.M{}
	countItem := []bson.M{}

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.All(&countItem)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}

	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)

	count := len(countItem)
	pages := utils.CalcPages(count, limit)

	findErr := pipe.All(&findItem)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	shopColl := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	for k, v := range findItem {
		data :=bson.M{}
			if v["shopSelect"] != ""{
				shopColl.Pipe([]bson.M{
					{
						"$match":bson.M{"_id":v["shopSelect"]},
					},
					{
					"$project": bson.M{
						"itemShop":0,
						"address":0,
						"created_at":0,
						"filenameLogo1":0,
						"isActive":0,
						"isPromotionAvailable":0,
						"isSponser":0,
						"lastModified":0,
						"monstersCount":0,
						"mapReach":0,
						"mainTypes":0,
						"mallFloorId":0,
						"monsterRating":0,
						"order":0,
						"shopAssertId":0,
						"shopCategoryId":0,
						"shopDetail":0,
						"shopTel":0,
						"shopUrl":0,
						"updated_at":0,
						"workingTime":0,		
					},
				},
				}).One(&data)
			}
		findItem[k]["shop"]=data
	 }

	linkF := "/api/v1/item?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"items":     findItem,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}

//GetItemDefaultV2 func
func GetItemDefaultV2(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	fields := ctx.URLParamTrim("fields")
	mainTab := ctx.URLParam("mainTab")
	split := strings.Split(fields, ",")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemCollection)

	objQuery := []bson.M{}
	isCoupon := ctx.URLParam("isCoupon")

	linkF := "/api/v2/item/list?"
	boolIsCoupon, errBool := strconv.ParseBool(isCoupon)
	if errBool == nil {
		linkF += "&isCoupon=" + isCoupon
		objQuery = append(objQuery, bson.M{"$match": bson.M{"isActive": true, "isCoupon": boolIsCoupon}})
	} else {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"isActive": true}})
	}
	linkF += "page=%d&limit=%d"
	for _, value := range split {
		if value == "itemCategoryId" {
			objQuery = append(objQuery, bson.M{
				"$lookup": bson.M{
					"from":         config.DBCollection.CategoryItemCollection,
					"localField":   "itemCategoryId",
					"foreignField": "_id",
					"as":           "itemCategory",
				},
			}, bson.M{"$unwind": "$itemCategory"})
		}
	}

	params := ctx.URLParams()
	sortResult := bson.M{}
	if params["sort"] != "" {
		sort := strings.Split(params["sort"], ",")
		lenSort := len(sort)
		if lenSort > 0 {
			for _, Sort := range sort {
				keys := strings.Split(Sort, ":")
				sortKey, sortValue := keys[0], keys[1]
				val, _ := strconv.Atoi(sortValue)
				if val > 0 {
					sortResult[sortKey] = 1
				} else {
					sortResult[sortKey] = -1
				}
			}
			objQuery = append(objQuery, bson.M{"$sort": sortResult})
		}
	}

	if mainTab != "" {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"mainTab": mainTab}})
	}

	
	findItem := []bson.M{}
	countItem := []bson.M{}

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.All(&countItem)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}

	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)

	count := len(countItem)
	pages := utils.CalcPages(count, limit)

	findErr := pipe.All(&findItem)
	if findErr != nil {
		result := map[string]interface{}{
			"pages":     pages,
			"pageIndex": page,
			"pageLimit": limit,
			"items":     []bson.M{},
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}
	
		utils.ResponseSuccess(ctx, result)
		return
	}

	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"items":     findItem,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}
