package item

import (

	// "time"

	// "github.com/globalsign/mgo"

	"fmt"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
	"github.com/mitchellh/mapstructure"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// UpdateItemDefault func
func UpdateItemDefault(ctx iris.Context) {
	itemID := ctx.Params().Get("id")
	err := validation.Errors{
		"id": validation.Validate(itemID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	update := make(map[string]interface{})
	if rErr := ctx.ReadJSON(&update); rErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(rErr))
		return
	}

	data := bson.M{}
	ItemCategoryID, _ := update["itemCategoryId"].(string)
	ItemName, _ := update["itemName"].(string)
	errID := validation.Errors{
		"ItemCategoryID": validation.Validate(ItemCategoryID, is.MongoID),
		"ItemName":       validation.Validate(ItemName, validation.Length(2, 64)),
	}.Filter()

	if errID != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errID))
		return
	}

	for k, v := range update {
		if k == "itemCategoryId" {
			data["itemCategoryId"] = bson.ObjectIdHex(ItemCategoryID)
		} else {
			data[k] = v
		}

	}
	item := models.Item{}
	decodeErr := mapstructure.Decode(data, &item)
	if decodeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "decode error", fmt.Sprint(decodeErr))
		return
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemCollection)
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(itemID)}, bson.M{
		"$currentDate": bson.M{"lastModified": true},
		"$set":         data,
	})

	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "update errors", fmt.Sprint(mongoErr))
		return
	}

	findItem := bson.M{}
	pipe := coll.Pipe(
		[]bson.M{
			{"$match": bson.M{"_id": bson.ObjectIdHex(itemID)}},
			{
				"$lookup": bson.M{
					"from":         config.DBCollection.CategoryItemCollection,
					"localField":   "itemCategoryId",
					"foreignField": "_id",
					"as":           "itemCategory",
				},
			},
			{"$unwind": "$itemCategory"},
		})
	findErr := pipe.One(&findItem)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, findItem)
}
