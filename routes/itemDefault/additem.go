package item

import (

	// "time"

	// "github.com/globalsign/mgo"

	"fmt"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddItemDefault is
func AddItemDefault(ctx iris.Context) {
	item := models.Item{}
	if parseErr := ctx.ReadJSON(&item); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}
	if validateErr := item.ValidateAll(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}

	_id := bson.NewObjectId()

	update := bson.M{
		"_id":                _id,
		"itemNameEng":        item.ItemNameEng,
		"itemDetailEng":      item.ItemDetailEng,
		"itemNameThai":       item.ItemNameThai,
		"itemDetailThai":     item.ItemDetailThai,
		"itemNameChi1":       item.ItemNameChi1,
		"itemDetailChi1":     item.ItemDetailChi1,
		"itemNameChi2":       item.ItemNameChi2,
		"itemDetailChi2":     item.ItemDetailChi2,
		"itemEffectThai":     item.ItemEffectThai,
		"itemEffectEng":      item.ItemEffectEng,
		"itemEffectChi1":     item.ItemEffectChi1,
		"itemEffectChi2":     item.ItemEffectChi2,
		"coin":               item.Coin,
		"diamond":            item.Diamond,
		"isActive":           item.IsActive,
		"itemOrder":          item.ItemOrder,
		"itemAssetModel":     item.ItemAssetModel,
		"itemAssetImageSlot": item.ItemAssetImageSlot,
		"itemAssetVersion":   item.ItemAssetVersion,
		"itemCategoryId":     item.ItemCategoryID,
		"createAt":           time.Now(),
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemCollection)
	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	findItem := bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{"_id": _id}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.CategoryItemCollection,
				"localField":   "itemCategoryId",
				"foreignField": "_id",
				"as":           "itemCategory",
			},
		},
		{"$unwind": "$itemCategory"},
	}
	pipe := coll.Pipe(objQuery)
	findErr := pipe.One(&findItem)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, findItem)
}
