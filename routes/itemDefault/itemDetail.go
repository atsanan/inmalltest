package item

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func GetItemDetail(ctx iris.Context) {

	itemID := ctx.Params().Get("itemId")
	if validateErr := utils.ValidateMongoID("itemId", itemID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemCollection)

	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(itemID)}},
		{"$lookup": bson.M{
			"from":         config.DBCollection.CategoryItemCollection,
			"localField":   "itemCategoryId",
			"foreignField": "_id",
			"as":           "itemCategory",
		},
		},
		{"$unwind": "$itemCategory"},
	}

	result := bson.M{}
	pipe := coll.Pipe(objQuery)
	findErr := pipe.One(&result)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, result)
}
