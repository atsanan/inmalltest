package paymentMPlayItems

import (
	"fmt"
	//"strconv"

	// "log"
	//"time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
)

//ListPaymentMPlayItems router
func ListPaymentMPlayItems(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PaymentMPayItemsCollection)
	paymentMPlayItems:=[]bson.M{}
	objQuery:=[]bson.M{}
	
	mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	if countErr != nil {
		result := map[string]interface{}{
			"pages":     "",
			"pageIndex": page,
			"pageLimit": limit,
			"paymentMPlayItems": paymentMPlayItems,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}

	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)
	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})

	pipe := coll.Pipe(objQuery)

	findErr := pipe.All(&paymentMPlayItems)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	linkF := "/api/v1/paymentMPlayItems?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"paymentMPlayItems": paymentMPlayItems,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}