package auth

import (
	//"fmt"
	"inmallgame/app/data-access"
	//"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "log"

	"inmallgame/app/security"
	"inmallgame/app/utils"
)

// TokenMDW for validate jwt.
func TokenMDW(ctx iris.Context) {
	version := ctx.Params().Get("version")
	if version != "v2" {

		jwtHandler := middleware.New(middleware.Config{
			ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
				return []byte(utils.MySigningKey), nil
			},
			// When set, the middleware verifies that tokens are signed with the specific signing algorithm
			// If the signing method is not constant the ValidationKeyGetter callback can be used to implement additional checks
			// Important to avoid security issues described here: https://auth0.com/blog/2015/03/31/critical-vulnerabilities-in-json-web-token-libraries/
			SigningMethod: jwt.SigningMethodHS256,
			Expiration:    true,
		})

		// username, password, _ := ctx.Request().BasicAuth()
		apiKey := ctx.GetHeader(utils.APIKey)
		if apiKey == "inmall-1234" {
			ctx.Next()
		} else {
			err := VerifyToken(ctx, jwtHandler)
			if err == nil {
				user := ctx.Values().Get("jwt").(*jwt.Token)
				ctx.Values().Set("user", user.Claims)
				ctx.Next()
			}
		}
	} else {
		// tokenHeader := ctx.GetHeader("authorization")
		// splitted := strings.Split(tokenHeader, " ")

		// if len(splitted) != 2 {
		// 	utils.ResponseFailure(ctx, iris.StatusUnauthorized, "Unauthorized", fmt.Sprint(""))
		// 	return
		// }

		data := bson.M{}
		config := utils.ConfigParser(ctx)
		session := database.GetMgoSession()
		coll := session.DB(config.DbName).C(config.DBCollection.NetWorkAccessCollection)
		pipe := coll.Pipe([]bson.M{
			{
				"$lookup": bson.M{
					"from":         config.DBCollection.IpAccessCollection,
					"localField":   "_id",
					"foreignField": "networkAccessId",
					"as":           "ipAccess",
				},
			},
			{"$unwind": "$ipAccess"},
			{"$match": bson.M{
				"apiKey":   "HEykqG0mGMy06D6zIoCaPBdRWkgXFCI2rCtVM4EsTi1g0AlCWJAjcwOXheGKZIbNRX0hOHOpAbOC5JgOEeZd6NbR4HfvkBgwjCVNS1sZ1dIaZntURbEODuwKfpk65uvXrwa50Mtvq4aNL5w4FpaJkZJXZkDwCgM2tcIdofFFSnWbvkkrnQHnl8wcxTAKXqNFoZAJzkDmfhZLx03kob4iFwrTELQK3Jy29QtJ3uxLm2bgepknCx3VIwxZUo",
				"isActive": true,
				"$or": []bson.M{
					{
						"ipAccess.ipAddress": ctx.RemoteAddr(),
					},
					{
						"ipAccess.ipAddress": "0.0.0.0",
					},
				},
				"ipAccess.isActive": true,
			}},
		})
		findErr := pipe.One(&data)
		if findErr != nil {
			ctx.StatusCode(iris.StatusUnauthorized)
			ctx.JSON(iris.Map{"isSuccess": false, "message": "Unauthorized", "error": "", "ip": ctx.RemoteAddr()})
			//utils.ResponseFailure(ctx, iris.StatusUnauthorized, "Unauthorized", fmt.Sprint(""))
			return
		}

		ctx.Next()

	}
}
