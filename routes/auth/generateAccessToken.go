package auth

import (
	// "fmt"
	// "log"
	"time"
	// "encoding/json"

	"github.com/dgrijalva/jwt-go"

	"inmallgame/app/models"
	"inmallgame/app/utils"
)

// GenerateAccessToken for use by client authentication.
func GenerateAccessToken(user models.User) (signString string, err error) {
	// Create the Claims
	expireToken :=int64(0)// time.Now().Add(time.Hour * 24 * 365).Unix()
	claims := utils.MyCustomClaims{
		user.Email,
		user.Password,
		user.ID.Hex(),
		jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    "ahoo-studio.co.th",
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(utils.MySigningKey))
	if err != nil {
		return "", err
	}

	return ss, nil
}

// GenerateTokenByCustomClaim for use by refresh token.
func GenerateTokenByCustomClaim(user utils.MyCustomClaims) (signString string, err error) {
	// Create the Claims
	//expireToken := time.Now().Add(time.Hour * 24).Unix()
	expireToken :=int64(0)// time.Now().Add(time.Hour * 24 * 365).Unix()

	claims := utils.MyCustomClaims{
		user.Username,
		user.Password,
		user.ID,
		jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    "ahoo-studio.co.th",
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(utils.MySigningKey))
	if err != nil {
		return "", err
	}

	return ss, nil
}
