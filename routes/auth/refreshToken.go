package auth

import (
	"encoding/json"
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"github.com/kataras/iris"

	"log"

	"inmallgame/app/data-access"
	"inmallgame/app/helpers"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
)

// RefreshToken with old token and then return new token to client.
func RefreshToken(ctx iris.Context) {
	user := ctx.Values().Get("jwt").(*jwt.Token)

	my := utils.MyCustomClaims{}
	bytes, _ := json.Marshal(user.Claims)
	if err := json.Unmarshal(bytes, &my); err != nil {
		log.Print(err.Error())

		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(iris.Map{"message": err.Error()})
		return
	}

	ss, err := GenerateTokenByCustomClaim(my)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusInternalServerError, nil, fmt.Sprint(err))
		return
	}

	data := models.User{}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.UserCollection)
	errFind := coll.Find(bson.M{"_id": bson.ObjectIdHex(my.ID)}).One(&data)
	if errFind != nil {

		utils.ResponseFailure(ctx, iris.StatusInternalServerError, nil, fmt.Sprint(errFind))
		return
	}

	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(my.ID)}, bson.M{
		"$inc":         bson.M{"time": 1},
		"$currentDate": bson.M{"lastLogin": true},
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}
	checkLoginReeard := models.User{}
	if findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(my.ID)}).One(&checkLoginReeard); findErr != nil {
		return
	}
	isLoginReward, _, loginReward, index := helpers.CheckUpDateLoginReward(ctx, checkLoginReeard)

	errLoginReward := coll.Update(bson.M{"_id": bson.ObjectIdHex(my.ID)}, bson.M{
		"$set":         bson.M{"indexReward": index},
		"$currentDate": bson.M{"lastLoginReward": true},
	})
	if errLoginReward != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}
	ctx.JSON(iris.Map{"isSuccess": true, "data": ss, "isLoginReward": isLoginReward, "loginReward": loginReward})
	return

}
