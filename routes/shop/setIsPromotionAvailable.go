package shop

import (
	// "log"
	"time"
	// "encoding/json"
	// "strconv"
	"fmt"
	// "github.com/globalsign/mgo"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

// SetPromotionAvaliable will serve as router.
func SetPromotionAvaliable(ctx iris.Context) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	NewsColl := session.DB(config.DbName).C(config.DBCollection.NewsCollection)

	query := []bson.M{
		bson.M{"$lookup": bson.M{
			"from":         config.DBCollection.NewsCollection,
			"localField":   "_id",
			"foreignField": "shopId",
			"as":           "news",
		},
		},
		//{"$unwind": "$news"},
		{"$match": bson.M{"news": bson.M{"$size": 0}}},
	}

	pipe := coll.Pipe(query)

	shops := []bson.M{}
	if mongoErr := pipe.All(&shops); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}
	//oids := make([]bson.ObjectId, len(shops))

	if len(shops) > 0 {
		for _, v := range shops {
			// 	oids[i] = v["_id"].(bson.ObjectId)
			// }

			mongoErr := coll.Update(bson.M{"_id": v["_id"].(bson.ObjectId)}, bson.M{
				"$set":         bson.M{"isPromotionAvailable": false},
				"$currentDate": bson.M{"lastModified": true}})
			if mongoErr != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
				return
			}
		}
	}

	queryExpired := []bson.M{
		bson.M{"$lookup": bson.M{
			"from":         config.DBCollection.NewsCollection,
			"localField":   "_id",
			"foreignField": "shopId",
			"as":           "news",
		},
		},
		//{"$unwind": "$news"},
		{"$match": bson.M{"isPromotionAvailable": true}},
	}

	pipeExpired := coll.Pipe(queryExpired)
	shopsExpired := []bson.M{}
	if mongoExpired := pipeExpired.All(&shopsExpired); mongoExpired != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoExpired))
		return
	}

	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)

	if len(shopsExpired) > 0 {
		for _, v := range shopsExpired {
			News := []models.News{}
			NewsColl.Find(bson.M{"shopId": v["_id"].(bson.ObjectId)}).All(&News)

			if len(News) > 0 {
				countExpired := 0
				for k, vNews := range News {
					if vNews.IsReachDateTime == true {
						ExpiredDateTime := News[k].ExpiredDateTime
						if ExpiredDateTime.Year() < now.Year() {
							countExpired++
						} else if ExpiredDateTime.Year() == now.Year() {
							if int(ExpiredDateTime.Month()) < int(now.Month()) {
								countExpired++
							} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
								if ExpiredDateTime.Day() < now.Day() {
									countExpired++
								} else if ExpiredDateTime.Day() == now.Day() {
									if ExpiredDateTime.Hour() < now.Hour() {
										countExpired++
									} else {
										if ExpiredDateTime.Hour() == now.Hour() {

											if ExpiredDateTime.Minute() < now.Minute() {
												countExpired++
											}
										}
									}
								}
							}
						}
					}
				}

				if countExpired >= len(News) {
					coll.Update(bson.M{"_id": v["_id"].(bson.ObjectId)}, bson.M{
						"$set":         bson.M{"isPromotionAvailable": false},
						"$currentDate": bson.M{"lastModified": true}})
					// if mongoErr != nil {
					// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
					// 	return
					// }
				}

			}

		}
	}

	utils.ResponseSuccess(ctx, bson.M{"message": "success Update Shops", "shopsExpired": shopsExpired})

}
