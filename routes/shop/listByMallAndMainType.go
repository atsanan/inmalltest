package shop

import (

	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
)

// ListByMallAndMainType will serve as router.
func ListByMallAndMainType(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	mainTypes := ctx.URLParam("mainTypes")
	mallID := ctx.URLParam("mallId")
	search := ctx.URLParam("search")

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)

	baseQuery := []bson.M{
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "_id",
				"foreignField": "mallFloorId",
				"as":           "shop",
			},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallCollection,
				"localField":   "mallId",
				"foreignField": "_id",
				"as":           "mall",
			},
		},
		{"$unwind": "$shop"},
		{"$unwind": "$mall"},
	}

	if mainTypes != "" {
		baseQuery = append(baseQuery, bson.M{"$match": bson.M{"shop.mainTypes": mainTypes}})
	}

	if mallID != "" {
		baseQuery = append(baseQuery, bson.M{"$match": bson.M{"mallId": bson.ObjectIdHex(mallID)}})
	}

	if search != "" {
		baseQuery = append(baseQuery, bson.M{"$match": bson.M{"$or": []bson.M{
			bson.M{"shop.shopName.eng": bson.M{"$regex": search, "$options": "i"}},
			bson.M{"shop.shopName.thai": bson.M{"$regex": search, "$options": "i"}},
			bson.M{"shop.shopName.chi1": bson.M{"$regex": search, "$options": "i"}},
			bson.M{"shop.shopName.chi2": bson.M{"$regex": search, "$options": "i"}},
		},
		},
		},
		)
	}
	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := mallFloorColl.Pipe(offsetQuery)

	mallFloors := []bson.M{}
	if mongoErr := pipe.All(&mallFloors); mongoErr != nil {
		utils.ResponseSuccess(ctx,
			bson.M{
				"mallFloors": mallFloors,
				"pageIndex":  pageIndex,
				"pageLimit":  pageLimit,
				"pages":      1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return
	}

	mcount, countErr := utils.CalcPipeCount(ctx, baseQuery, mallFloorColl)
	if countErr != nil {
		utils.ResponseSuccess(ctx,
			bson.M{
				"mallFloors": mallFloors,
				"pageIndex":  pageIndex,
				"pageLimit":  pageLimit,
				"pages":      1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return
	}
	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	originalLink := "api/v1/shop/byMallAndMainType"
	linkF := originalLink + "?page=%d&limit=%d"
	if mallID != "" {
		linkF += "&mallId=" + mallID
	}

	if search != "" {
		linkF += "&search=" + search
	}

	if mainTypes != "" {
		linkF += "&mainTypes=" + mainTypes
	}

	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["mallFloors"] = mallFloors

	utils.ResponseSuccess(ctx, result)
}
