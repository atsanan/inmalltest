package shop

import (
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
)

// RatingMonster will serve as router.
func RatingMonster(ctx iris.Context) {
	// pageIndex, _ := ctx.URLParamInt("page")
	// pageLimit, _ := ctx.URLParamInt("limit")
	//offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	query := []bson.M{
		bson.M{"$lookup": bson.M{
			"from":         "channels",
			"localField":   "_id",
			"foreignField": "shopId",
			"as":           "channel",
		},
		},
		bson.M{
			"$lookup": bson.M{
				"from":         "joinChannels",
				"localField":   "channel._id",
				"foreignField": "channelId",
				"as":           "joinChannel",
			},
		},
		bson.M{"$project": bson.M{"_id": "$_id",
			"shopName": "$shopName",
			"size":     bson.M{"$size": "$joinChannel"},
		},
		},
		bson.M{"$sort": bson.M{
			"size": -1}},
	}
	pipe := coll.Pipe(query)

	shops := []bson.M{}
	if mongoErr := pipe.All(&shops); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}

	resultShop := []bson.M{}
	maxSize := float64(shops[0]["size"].(int))
	for _, v := range shops {
		calculate := 100 * (float64(v["size"].(int)) / maxSize)

		if calculate > 0 {
			rate := 0
			if calculate > 80 {
				rate = 5
			} else if calculate > 60 {
				rate = 4
			} else if calculate > 40 {
				rate = 3
			} else if calculate > 20 {
				rate = 2

			} else if calculate > 5 {
				rate = 1

			}

			coll.Update(bson.M{"_id": v["_id"]},
				bson.M{
					"$set": bson.M{"monsterRating": rate,
						"monstersCount": v["size"].(int),
					},
					"$currentDate": bson.M{"lastModified": true}})
			queryShop := bson.M{}
			coll.Find(bson.M{"_id": v["_id"]}).One(&queryShop)

			resultShop = append(resultShop, queryShop)

		}
	}
	// originalLink := "api/v1/shop/byMallAndMainType"
	// linkF := originalLink + "?page=%d&limit=%d"
	// nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	// result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	// result["mallFloors"] = mallFloors
	utils.ResponseSuccess(ctx, bson.M{
		"shops": resultShop})

}
