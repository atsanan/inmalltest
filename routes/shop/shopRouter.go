package shop

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// ServeRouter will serve as router handler.
func ServeRouter(shopRoutes router.Party) {
	// For authorize user.
	shopRoutes.Get("/byFloorId/{mallFloorId: string}", auth.TokenMDW, ListByFloorID)
	shopRoutes.Get("/byFloorKey/{mallFloorKey: string}", auth.TokenMDW, ListByFloorKey)
	shopRoutes.Get("/byMall/{mallId: string}", auth.TokenMDW, ListByMall)
	shopRoutes.Get("/byMallAndMainType/", auth.TokenMDW, ListByMallAndMainType)
	shopRoutes.Get("/byMall2/{mallId: string}", auth.TokenMDW, ListByMall2)
	shopRoutes.Get("/nearBy", auth.TokenMDW, NearbyShop)
	shopRoutes.Get("/{id: string}", auth.TokenMDW, shopDetail)
	shopRoutes.Get("/ratingMonster", RatingMonster)
	shopRoutes.Get("/setIsPromotionAvailable", SetPromotionAvaliable)
}
