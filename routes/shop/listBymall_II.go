package shop

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
)

// ListByMall will serve as router.
func ListByMall2(ctx iris.Context) {
	mallID := ctx.Params().Get("mallId")

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	if validateErr := utils.ValidateMongoID("mallId", mallID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)

	baseQuery := []bson.M{
		{"$match": bson.M{"mallId": bson.ObjectIdHex(mallID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "_id",
				"foreignField": "mallFloorId",
				"as":           "shop",
			},
		},
		//	{"$unwind": "$shop"},
	}
	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := mallFloorColl.Pipe(offsetQuery)

	mallFloors := []bson.M{}
	if mongoErr := pipe.All(&mallFloors); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	mcount, countErr := utils.CalcPipeCount(ctx, baseQuery, mallFloorColl)
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
		return
	}
	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/shop/byMall/%s", mallID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["mallFloors"] = mallFloors

	utils.ResponseSuccess(ctx, result)
}
