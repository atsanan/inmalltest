package shop

import (
	"fmt"

	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	// "github.com/novalagung/gubrak"

	// "inmallgame/app/models"

	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/kataras/iris"
)

// shopDetail will serve as router.
func shopDetail(ctx iris.Context) {
	ID := ctx.Params().Get("id")

	if validateErr := utils.ValidateMongoID("id", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	shop := bson.M{}

	err := coll.Find(bson.M{"_id": bson.ObjectIdHex(ID)}).One(&shop)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, shop)
}
