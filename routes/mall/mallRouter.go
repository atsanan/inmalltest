package mall

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Get("/list", auth.TokenMDW, GetMallList)
	routes.Get("/info/{mallId : string}", auth.TokenMDW, GetMallInfo)
	routes.Get("/{mallId : string}", GetMallInfo)
	routes.Get("/floor/list/{mallId : string}", auth.TokenMDW, GetMallFloorList)
	routes.Get("/floor/byLocation",auth.TokenMDW, GetMallFloorByLocation)

	routes.Post("/floor/info", auth.TokenMDW, GetMallFloorInfo)
	routes.Get("/nearBy", auth.TokenMDW, GetMallNearby)
	// For admin.
	routes.Post("/add", AddMall)
	routes.Post("/{mallId : string}", UpdateMall)
	routes.Post("/floor/add", FloorCreate)
	routes.Post("/floor/{mallFoorId:string}", FloorUpdate)
}
