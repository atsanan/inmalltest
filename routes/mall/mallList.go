package mall

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// GetMallList will serve as router.
func GetMallList(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	version := ctx.Params().Get("version")
	if pageLimit == 10 {
		pageLimit = 20
	}
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	malls := []bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallColl := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	if mongoErr := mallColl.Find(bson.M{"isActive": true}).Skip(offset).Limit(limit).All(&malls); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	for k, v := range malls {
		mallFloors := []bson.M{}
		mallFloorColl.Pipe([]bson.M{
			{"$match":
				bson.M{"mallId": v["_id"],},
			},
			{
				"$addFields" : bson.M{
					"convert" : bson.M{ "$toInt": "$mapIndoorFloorData" },
				},
			},
			{"$sort":bson.M{"convert":1},},
			{  "$project": bson.M{"convert":0},},
		}).All(&mallFloors)
		malls[k]["mallFloors"]=mallFloors
	}
	count, _ := mallColl.Count()
	pages := utils.CalcPages(count, limit)

	linkF := "api/" + version + "/mall/list?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["malls"] = malls

	utils.ResponseSuccess(ctx, result)
}
