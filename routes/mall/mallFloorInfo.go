package mall

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
)

// GetMallFloorInfo will serve as router.
func GetMallFloorInfo(ctx iris.Context) {
	mallFloorID := ctx.PostValue("mallFloorId")
	mapIndoorFloorKey := ctx.PostValue("mapIndoorFloorKey")

	if mallFloorID == "" && mapIndoorFloorKey == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint("Either of mallFloorId or mapIndoorFloorKey should not empty."))
		return
	}
	if mallFloorID != "" {
		if validateErr := utils.ValidateMongoID("mallFloorId", mallFloorID); validateErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
			return
		}
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)

	mallColl := session.DB(config.DbName).C(config.DBCollection.MallCollection)

	query := bson.M{}
	if mallFloorID != "" {
		query["$match"] = bson.M{"_id": bson.ObjectIdHex(mallFloorID)}
	} else {
		query["$match"] = bson.M{"mapIndoorFloorKey": mapIndoorFloorKey}
	}

	pipe := mallFloorColl.Pipe([]bson.M{
		query,
		// {
		// 	"$lookup": bson.M{
		// 		"from":         config.DBCollection.MallCollection,
		// 		"localField":   "mallId",
		// 		"foreignField": "_id",
		// 		"as":           "mall",
		// 	},
		// },
		// {"$unwind": "$mall"},
	})

	result := []bson.M{}
	if mongoErr := pipe.All(&result); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}
	for k, v := range result {
		mallFloors := []bson.M{}
		mallFloorColl.Pipe([]bson.M{
			{"$match":
				bson.M{"mallId": v["mallId"],},
			},
			{
				"$addFields" : bson.M{
					"convert" : bson.M{ "$toInt": "$mapIndoorFloorData" },
				},
			},
			{"$sort":bson.M{"convert":1},},
			{  "$project": bson.M{"convert":0},},
		}).All(&mallFloors)
		mall := bson.M{}
		mallColl.Find(bson.M{"_id": v["mallId"]}).One(&mall)
		mall["mallFloors"] = mallFloors
		result[k]["mall"] = mall
	}

	utils.ResponseSuccess(ctx, result)
}
