package mall

import (
	"fmt"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
)

// "log"
// "time"
// "encoding/json"
// "strconv"

// "github.com/globalsign/mgo"

// "github.com/novalagung/gubrak"

// "inmallgame/app/models"

// AddMall will server as router.
func AddMall(ctx iris.Context) {
	mall := models.Mall{}
	if parseErr := ctx.ReadJSON(&mall); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	if validateErr := mall.ValidateAll(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}

	GeoJSON := mall.Location

	if validateErr := GeoJSON.ValidateGeo(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}

	_id := bson.NewObjectId()
	update := bson.M{
		"_id":             _id,
		"mallNameEng":     mall.MallNameEng,
		"mallNameThai":    mall.MallNameThai,
		"mallNameChi1":    mall.MallNameChi1,
		"mallNameChi2":    mall.MallNameChi2,
		"mallDetailEng":   mall.MallDetailEng,
		"mallDetailThai":  mall.MallDetailThai,
		"mallDetailChi1":  mall.MallDetailChi1,
		"mallDetailChi2":  mall.MallDetailChi2,
		"mallAddressEng":  mall.MallAddressEng,
		"mallAddressThai": mall.MallAddressThai,
		"mallAddressChi1": mall.MallAddressChi1,
		"mallAddressChi2": mall.MallAddressChi2,
		"mapOutdoorId":    mall.MapOutdoorID,
		"mapIndoorId":     mall.MapIndoorID,
		"isActive":        mall.IsActive,
		"location":        mall.Location,
		"createAt":        time.Now(),
		"lastModified":    "",
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	// Create geosphere index.
	models.MallCreateIndex(coll)

	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	findItem := models.Mall{}
	findErr := coll.Find(bson.M{"_id": _id}).One(&findItem)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, findItem)
}
