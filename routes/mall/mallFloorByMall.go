package mall

import (
	"fmt"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
)

// GetMallList will serve as router.
func GetMallFloorList(ctx iris.Context) {
	mallID := ctx.Params().Get("mallId")

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	// if pageLimit == 10 {
	// 	pageLimit = 20
	// }
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	baseQuery := []bson.M{
		{"$match": bson.M{"mallId": bson.ObjectIdHex(mallID)}},
	}
	mcount, _ := utils.CalcPipeCount(ctx, baseQuery, mallFloorColl)
	pages := utils.CalcPagesByPipeResult(mcount, limit)

	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := mallFloorColl.Pipe(offsetQuery)

	mallFloors := []bson.M{}
	if mongoErr := pipe.All(&mallFloors); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}
	originalLink := fmt.Sprintf("api/v2/mall/floor/%s", mallID)
	linkF := originalLink + "?page=%d&limit=%d"

	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["mallFloors"] = mallFloors
	utils.ResponseSuccess(ctx, result)

}
