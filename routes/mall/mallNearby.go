package mall

import (
	"fmt"
	"strconv"

	"github.com/go-ozzo/ozzo-validation/is"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

//GetMallNearby router
func GetMallNearby(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	lat := ctx.URLParamTrim("lat")
	long := ctx.URLParamTrim("long")
	typeQuery := ctx.URLParamTrim("type")
	minDistance, _ := ctx.URLParamInt("minDistance")
	maxDistance, _ := ctx.URLParamInt("maxDistance")
	err := validation.Errors{
		"lat":  validation.Validate(lat, validation.Required, is.Float),
		"long": validation.Validate(long, validation.Required, is.Float),
	}.Filter()

	typeQ := "Point"
	if typeQuery != "" {
		typeQ = typeQuery
	}

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	minDis := 0
	if minDistance <= 0 {
		minDis = 1000
	} else {
		minDis = minDistance
	}

	maxDis := 0
	if maxDistance <= 0 {
		maxDis = 5000
	} else {
		maxDis = maxDistance
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	malls := []bson.M{}
	longConv, _ := strconv.ParseFloat(long, 64)
	latConv, _ := strconv.ParseFloat(lat, 64)
	var coordinates [2]float64
	coordinates[0] = longConv
	coordinates[1] = latConv
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallColl := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	if mongoErr := mallColl.Find(bson.M{
		"location": bson.M{"$near": bson.M{
			"$geometry":    bson.M{"type": typeQ, "coordinates": coordinates},
			"$minDistance": minDis,
			"$maxDistance": maxDis,
		}},
	}).Skip(offset).Limit(limit).All(&malls); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	count, errCount := mallColl.Find(bson.M{
		"location": bson.M{"$near": bson.M{
			"$geometry":    bson.M{"type": typeQ, "coordinates": coordinates},
			"$minDistance": minDis,
			"$maxDistance": maxDis,
		}},
	}).Count()

	if errCount != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v1/mall/nearBy?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["malls"] = malls

	utils.ResponseSuccess(ctx, result)
}
