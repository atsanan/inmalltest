package mall

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"github.com/mitchellh/mapstructure"
)

//UpdateMall routes
func UpdateMall(ctx iris.Context) {
	mallID := ctx.Params().Get("mallId")

	update := map[string]interface{}{}
	if parseErr := ctx.ReadJSON(&update); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	mall := models.Mall{}
	decodeErr := mapstructure.Decode(update, &mall)
	if decodeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(decodeErr))
		return
	}

	// if validateErr := mall.ValidateAll(); validateErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
	// 	return
	// }

	// GeoJSON := mall.Location

	// if validateErr := GeoJSON.ValidateGeo(); validateErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
	// 	return
	// }

	// if rErr := ctx.ReadJSON(&update); rErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(rErr))
	// 	return
	// }

	// update := bson.M{
	// 	"mapOutdoorId": mall.MapOutdoorID,
	// 	"mapIndoorId":  mall.MapIndoorID,
	// 	"isActive":     mall.IsActive,
	// 	"location":     mall.Location,
	// }

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(mallID)}, bson.M{
		"$currentDate": bson.M{"lastModified": true},
		"$set":         update,
	})

	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	findData := models.Mall{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(mallID)}).One(&findData)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, findData)
}
