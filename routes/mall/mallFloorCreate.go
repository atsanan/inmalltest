package mall

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//FloorCreate router
func FloorCreate(ctx iris.Context) {
	floor := models.MallFloor{}
	if parseErr := ctx.ReadJSON(&floor); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	if validateErr := floor.ValidateAll(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}

	_id := bson.NewObjectId()
	insert := bson.M{
		"_id":                 _id,
		"mallFloorNameEng":    floor.MallFloorNameEng,
		"mallFloorNameThai":   floor.MallFloorNameThai,
		"mallFloorNameChi1":   floor.MallFloorNameChi1,
		"mallFloorNameChi2":   floor.MallFloorNameChi2,
		"mallFloorDetailEng":  floor.MallFloorDetailEng,
		"mallFloorDetailThai": floor.MallFloorDetailThai,
		"mallFloorDetailChi1": floor.MallFloorDetailChi1,
		"mallFloorDetailChi2": floor.MallFloorDetailChi2,
		"mallId":              floor.MallID,
		"mapIndoorFloorKey":   floor.MapIndoorFloorKey,
		"mapIndoorFloorData":  floor.MapIndoorFloorData,
		"createAt":            time.Now(),
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	mall := models.Mall{}
	colMall := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	findErr := colMall.Find(bson.M{"_id": floor.MallID}).One(&mall)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch mallId", fmt.Sprint(findErr))
		return
	}

	coll := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	models.MallFloorCreateIndex(coll)

	mongoErr := coll.Insert(insert)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	pipe := coll.Pipe([]bson.M{
		{"$match": bson.M{"_id": _id}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallCollection,
				"localField":   "mallId",
				"foreignField": "_id",
				"as":           "mall",
			},
		},
		{"$unwind": "$mall"},
	},
	)

	findData := bson.M{}

	if err := pipe.One(&findData); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, findData)
}
