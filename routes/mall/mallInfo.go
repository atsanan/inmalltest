package mall

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

// GetMallInfo will serve as router.
func GetMallInfo(ctx iris.Context) {
	mallID := ctx.Params().Get("mallId")
	if validateErr := utils.ValidateMongoID("mallId", mallID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	mall := bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallColl := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	if mongoErr := mallColl.Find(bson.M{"_id": bson.ObjectIdHex(mallID)}).One(&mall); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}
	mallFloors := []bson.M{}
	mallFloorColl.Pipe([]bson.M{
		{"$match":
			bson.M{"mallId": mall["_id"],},
		},
		{
			"$addFields" : bson.M{
				"convert" : bson.M{ "$toInt": "$mapIndoorFloorData" },
			},
		},
		{"$sort":bson.M{"convert":1},},
		{  "$project": bson.M{"convert":0},},
	}).All(&mallFloors)
	mall["mallFloors"] = mallFloors

	utils.ResponseSuccess(ctx, mall)
}
