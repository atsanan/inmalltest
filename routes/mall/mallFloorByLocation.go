package mall

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"strconv"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

func GetMallFloorByLocation(ctx iris.Context) {
	lat := ctx.URLParamTrim("lat")
	long := ctx.URLParamTrim("long")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	// if pageLimit == 10 {
	// 	pageLimit = 20
	// }
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)
	err := validation.Errors{
		"lat":  validation.Validate(lat, validation.Required, is.Float),
		"long": validation.Validate(long, validation.Required, is.Float),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	longConv, _ := strconv.ParseFloat(long, 64)
	latConv, _ := strconv.ParseFloat(lat, 64)

	var coordinates [2]float64
	coordinates[0] = longConv
	coordinates[1] = latConv

	mall := bson.M{}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallColl := session.DB(config.DbName).C(config.DBCollection.MallCollection)
	if mongoErr := mallColl.Pipe([]bson.M{
		bson.M{
			"$geoNear": bson.M{
				"near":          bson.M{"type": "Point", "coordinates": coordinates},
				"maxDistance":   5000,
				"distanceField": "dist.calculated",
				"spherical":     true,
			},
		},
		bson.M{"$sort": bson.M{"dist.calculated": 1}},
		bson.M{"$match":bson.M{"isActive":true}},
	}).One(&mall); mongoErr != nil {
		result := map[string]interface{}{
			"pages":      page,
			"pageIndex":  pageIndex,
			"pageLimit":  pageLimit,
			"mallFloors": []bson.M{},
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}

	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	baseQuery := []bson.M{
		{"$match": bson.M{"mallId": mall["_id"]}},
	}
	mcount, _ := utils.CalcPipeCount(ctx, baseQuery, mallFloorColl)
	pages := utils.CalcPagesByPipeResult(mcount, limit)

	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := mallFloorColl.Pipe(offsetQuery)

	mallFloors := []bson.M{}
	if mongoErr := pipe.All(&mallFloors); mongoErr != nil {
		result := map[string]interface{}{
			"pages":      page,
			"pageIndex":  pageIndex,
			"pageLimit":  pageLimit,
			"mallFloors": []bson.M{},
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}
	originalLink := fmt.Sprintf("api/v2/mall/floor/")
	linkF := originalLink + "?page=%d&limit=%d"

	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["mallFloors"] = mallFloors
	utils.ResponseSuccess(ctx, result)

}
