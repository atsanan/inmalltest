package mall

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
	"github.com/mitchellh/mapstructure"
)

//FloorUpdate routes
func FloorUpdate(ctx iris.Context) {
	mallFoorID := ctx.Params().Get("mallFoorId")

	err := validation.Errors{
		"mallFoorId": validation.Validate(mallFoorID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	update := map[string]interface{}{}
	if parseErr := ctx.ReadJSON(&update); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	// if validateErr := floor.ValidateForUpdate(); validateErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
	// 	return
	// }

	floor := models.MallFloor{}
	decodeErr := mapstructure.Decode(update, &floor)
	if decodeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "decode error", fmt.Sprint(decodeErr))
		return
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	mallID, _ := update["mallId"].(string)
	if mallID != "" {
		update["mallId"] = bson.ObjectIdHex(mallID)
	}

	coll := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(mallFoorID)}, bson.M{
		"$currentDate": bson.M{"lastModified": true},
		"$set":         update,
	})

	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	pipe := coll.Pipe([]bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(mallFoorID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallCollection,
				"localField":   "mallId",
				"foreignField": "_id",
				"as":           "mall",
			},
		},
		{"$unwind": "$mall"},
	},
	)

	findData := models.MallFloorResponse{}

	if err := pipe.One(&findData); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, findData)
}
