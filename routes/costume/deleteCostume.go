package costume

import (
	"fmt"
	// "time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// DeleteCostume route  will delete costume from store provide for admin authorities.
func DeleteCostume(ctx iris.Context) {
	costumeID := ctx.Params().Get("id")
	if costumeID == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "id params cannot empty.", "")
		return
	}

	err := validation.Errors{
		"id": validation.Validate(costumeID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CostumeCollection)
	mongoErr := coll.Remove(bson.M{"_id": bson.ObjectIdHex(costumeID)})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to delete costume item", fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, nil)
}
