package costume

import (
	"fmt"
	// "time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

//Costume route  will fetch all costume from store.
func Costume(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CostumeCollection)

	costumes := []bson.M{}
	err := coll.Find(bson.M{}).Skip(offset).Limit(limit).All(&costumes)
	if err != nil {
		fmt.Println("Not found", err)

		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}

	count, _ := coll.Count()
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v1/costumeStore?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"costumes":  costumes,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}
	utils.ResponseSuccess(ctx, result)
}
