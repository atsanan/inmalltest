package costume

import (
	"fmt"
	// "time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// UpdateCostume route  will add costume to store provide for admin.
func UpdateCostume(ctx iris.Context) {
	costumeID := ctx.Params().Get("id")
	err := validation.Errors{
		"id": validation.Validate(costumeID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	update := map[string]interface{}{}
	if rErr := ctx.ReadJSON(&update); rErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(rErr))
		return
	}
	cost := models.Costume{}
	decodeErr := mapstructure.Decode(update, &cost)
	if decodeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(decodeErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CostumeCollection)
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(costumeID)}, bson.M{
		"$currentDate": bson.M{"lastModified": true},
		"$set":         update,
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	costume := models.Costume{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(costumeID)}).One(&costume)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, costume)
}
