package costume

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// CostumeDetail route  will add costume to store provide for admin.
func CostumeDetail(ctx iris.Context) {
	ID := ctx.Params().Get("id")
	if validateErr := utils.ValidateMongoID("costumeId", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CostumeCollection)

	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(ID)}},
	}

	result := bson.M{}
	pipe := coll.Pipe(objQuery)
	findErr := pipe.One(&result)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, result)
}
