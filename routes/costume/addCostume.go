package costume

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddCostume route  will add costume to store provide for admin.
func AddCostume(ctx iris.Context) {
	// config := utils.ConfigParser(ctx)

	costume := models.Costume{}
	if parseErr := ctx.ReadJSON(&costume); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(parseErr))
		return
	}
	if validateErr := costume.ValidateAll(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}
	_id := bson.NewObjectId()

	update := bson.M{
		"_id":               _id,
		"costumeNameEng":    costume.CostumeNameEng,
		"costumeDetailEng":  costume.CostumeDetailEng,
		"costumeNameThai":   costume.CostumeNameThai,
		"costumeDetailThai": costume.CostumeDetailThai,
		"costumeNameChi1":   costume.CostumeNameChi1,
		"costumeDetailChi1": costume.CostumeDetailChi1,
		"costumeNameChi2":   costume.CostumeNameChi2,
		"costumeDetailChi2": costume.CostumeDetailChi2,
		"coin":              costume.Coin,
		"diamond":           costume.Diamond,
		"isActive":          costume.IsActive,
		"createAt":          time.Now(),
		"lastModified":      time.Now(),
	}
	// if costume.CostumeDetail != "" {
	// 	update["costumeDetail"] = costume.CostumeDetail
	// }

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CostumeCollection)
	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	costumes := []models.Costume{}
	findErr := coll.Find(bson.M{"costumeNameEng": costume.CostumeNameEng}).All(&costumes)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, costumes)
}
