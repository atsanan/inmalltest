package couponDiamond

import (
	"fmt"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	validation "github.com/go-ozzo/ozzo-validation"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

func AddDimondItemPlayer(ctx iris.Context) {

	itemID := ctx.PostValue("itemId")
	playerID := ctx.PostValue("playerId")
	err := validation.Errors{
		"itemId":   validation.Validate(itemID, validation.Required, is.MongoID),
		"playerId": validation.Validate(playerID, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CouponAddonDiamondCollection)
	objQuery := []bson.M{}
	objQuery = append(objQuery, bson.M{"$match": bson.M{
		"itemId":   bson.ObjectIdHex(itemID),
		"playerId": bson.ObjectIdHex(playerID),
	},
	})
	couponPlayer := bson.M{}
	findErr := coll.Pipe(objQuery).One(&couponPlayer)

	if findErr == nil {
		ctx.JSON(iris.Map{"isSuccess": false, "data": bson.M{"diamondInclude": couponPlayer["diamondInclude"].(int),
		}})
		return
	}

	collItem := session.DB(config.DbName).C(config.DBCollection.ItemCollection)
	objQueryItem := []bson.M{}
	objQueryItem = append(objQueryItem, bson.M{"$match": bson.M{
		"isActive": true,
		"_id":      bson.ObjectIdHex(itemID),
	},
	})

	coupon := bson.M{}
	findItemErr := collItem.Pipe(objQueryItem).One(&coupon)
	if findItemErr != nil {
		utils.ResponseFailure(ctx, iris.StatusOK, nil, fmt.Sprint(findItemErr))
		return
	}
	_id := bson.NewObjectId()

	insert := bson.M{
		"_id":            _id,
		"itemId":         bson.ObjectIdHex(itemID),
		"playerId":       bson.ObjectIdHex(playerID),
		"diamondInclude": coupon["diamondInclude"].(int),
		"createAt":       time.Now(),
	}
	mongoErr := coll.Insert(insert)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}
	data := bson.M{}
	findDataErr := coll.Find(bson.M{"_id": _id}).One(&data)
	if findDataErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findDataErr))
		return
	}
	utils.ResponseSuccess(ctx, bson.M{"diamondInclude": coupon["diamondInclude"].(int)})
}
