package couponDiamond

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, AddDimondItemPlayer)
	routes.Post("/check", auth.TokenMDW, checkItemPlayer)
}
