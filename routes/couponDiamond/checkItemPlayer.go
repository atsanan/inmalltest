package couponDiamond

import (
	"fmt"
	"inmallgame/app/utils"

	validation "github.com/go-ozzo/ozzo-validation"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

func checkItemPlayer(ctx iris.Context) {

	itemID := ctx.PostValue("itemId")
	playerID := ctx.PostValue("playerId")
	err := validation.Errors{
		"itemId":   validation.Validate(itemID, validation.Required, is.MongoID),
		"playerId": validation.Validate(playerID, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CouponAddonDiamondCollection)
	objQuery := []bson.M{}
	objQuery = append(objQuery, bson.M{"$match": bson.M{
		"itemId":   bson.ObjectIdHex(itemID),
		"playerId": bson.ObjectIdHex(playerID),
	},
	})
	couponPlayer := bson.M{}
	findErr := coll.Pipe(objQuery).One(&couponPlayer)

	if findErr != nil {
		utils.ResponseSuccess(ctx, bson.M{"isExists": false})
		return
	}

	utils.ResponseSuccess(ctx, bson.M{"isExists": true})
}
