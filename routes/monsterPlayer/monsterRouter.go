package monster

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Get("/info", auth.TokenMDW, MPlayerInfo)
	routes.Post("/info/{mPlayerId: string}", auth.TokenMDW, UpdateMPlayerInfo)
	routes.Post("/pick/", auth.TokenMDW, CatchWildMonster)
	routes.Get("/inbag/{playerId: string}", auth.TokenMDW, AllMonsterInbag)         // not join channels.
	routes.Get("/inChannel/{playerId: string}", auth.TokenMDW, AllMonsterInChannel) //  join channels.
	routes.Get("/all/{playerId: string}", auth.TokenMDW, AllMonsterPlayer)          // all.
	routes.Get("/check/{playerId: string}/{mDefaultId: string}", auth.TokenMDW, CheckMonterPlayer)
	routes.Get("/length/{playerId: string}", auth.TokenMDW, GroupMonterPlayer)
	routes.Get("/removeByMDefault/{mDfaultId: string}", auth.TokenMDW, RemoveMonterPlayerByMDefault)
	routes.Post("/food/{mPlayerId: string}", auth.TokenMDW, UpdateMonsterFood)
	routes.Post("/happy/{mPlayerId: string}", auth.TokenMDW, UpdateMonsterHappiness)
	routes.Post("/health/{mPlayerId: string}", auth.TokenMDW, UpdateMonsterHealth)
	routes.Post("/quest1/{mPlayerId: string}", auth.TokenMDW, UpdateQuest1)
	routes.Post("/quest2/{mPlayerId: string}", auth.TokenMDW, UpdateQuest2)
	routes.Post("/quest3/{mPlayerId: string}", auth.TokenMDW, UpdateQuest3)
	routes.Post("/exp/{mPlayerId: string}", auth.TokenMDW, UpdateExp)
	routes.Post("/message/{mPlayerId: string}", auth.TokenMDW, UpdateMessage)
	routes.Get("/remove/{mPlayerId: string}", auth.TokenMDW, RemoveMonterPlayer)
	routes.Get("/countMonsterInChannelAndInstantiate/{playerId: string}", auth.TokenMDW, CountMonsterInChannelAndMonstersInstantiate)
	routes.Get("/checkJoinChannelByShop/{playerId: string}/{shopId: string}", auth.TokenMDW, CheckMonterPlayerShop)
}
