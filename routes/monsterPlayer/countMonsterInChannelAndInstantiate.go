package monster

import (

	// "github.com/globalsign/mgo"

	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// CountMonsterInChannelAndMonstersInstantiate for admin use.
func CountMonsterInChannelAndMonstersInstantiate(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")

	if err := utils.ValidateMongoID("playerId", playerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	wildMonstercoll := session.DB(config.DbName).C(config.DBCollection.WildMonsterCollection)

	baseQ := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.JoinChannelCollection,
				"localField":   "_id",
				"foreignField": "mPlayerId",
				"as":           "joinChannels",
			},
		},
		{"$match": bson.M{"joinChannels.0": bson.M{"$exists": true}}},
		{"$unwind": "$joinChannels"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ChannelCollection,
				"localField":   "joinChannels.channelId",
				"foreignField": "_id",
				"as":           "channel",
			},
		},
		{"$unwind": "$channel"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "channel.shopId",
				"foreignField": "_id",
				"as":           "shop",
			},
		},
		{"$unwind": "$shop"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallFloorCollection,
				"localField":   "shop.mallFloorId",
				"foreignField": "_id",
				"as":           "mallFloor",
			},
		},
		{"$unwind": "$mallFloor"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallCollection,
				"localField":   "mallFloor.mallId",
				"foreignField": "_id",
				"as":           "mall",
			},
		},
		{"$unwind": "$mall"},
		{"$sort": bson.M{"createAt": -1}},
	}
	pipe := monsterPlayerColl.Pipe(baseQ)
	monsterCount := []bson.M{}
	countMonsterInChannel := 0
	if err := pipe.All(&monsterCount); err == nil {
		countMonsterInChannel = len(monsterCount)
	}

	objQuery := []bson.M{
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MonstersDefaultCollection,
				"localField":   "mDefaultId",
				"foreignField": "_id",
				"as":           "monsterDefault",
			},
		},
		{"$unwind": "$monsterDefault"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
		{"$lookup": bson.M{
			"from":         config.DBCollection.CategoryItemCollection,
			"localField":   "item.itemCategoryId",
			"foreignField": "_id",
			"as":           "item.itemCategory",
		},
		},
		{"$match": bson.M{"isActive": true}},
	}

	wildMonster := []bson.M{}
	pipeCount := wildMonstercoll.Pipe(objQuery)
	errCount := pipeCount.All(&wildMonster)

	// isGet := 0
	// nonIsGet := 0
	countMonsterInstantiate := 0

	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)
	if errCount == nil {
		wildMonsterCheck := []models.WildMonster{}
		pipeCount.All(&wildMonsterCheck)
		for k, v := range wildMonster {
			//StartDateTime := wildMonsterCheck[k].StartDateTime
			ExpiredDateTime := wildMonsterCheck[k].ExpiredDateTime

			if v["isReachDateTime"].(bool) == false {
				if v["count"].(int) > 0 {
					countMonsterInstantiate += 1
				}
			} else {

				if ExpiredDateTime.Year() > now.Year() {
					if v["count"].(int) > 0 {
						countMonsterInstantiate += 1
					}
				} else if ExpiredDateTime.Year() == now.Year() {

					if int(ExpiredDateTime.Month()) > int(now.Month()) {
						if v["count"].(int) > 0 {
							countMonsterInstantiate += 1
						}
					} else if int(ExpiredDateTime.Month()) == int(now.Month()) {
						if ExpiredDateTime.Day() > now.Day() {
							if v["count"].(int) > 0 {
								countMonsterInstantiate += 1
							}
						} else if ExpiredDateTime.Day() == now.Day() {
							if ExpiredDateTime.Hour() > now.Hour() {
								if v["count"].(int) > 0 {
									countMonsterInstantiate += 1
								}
							} else {
								if ExpiredDateTime.Hour() == now.Hour() {

									if ExpiredDateTime.Minute() > now.Minute() {
										if v["count"].(int) > 0 {
											countMonsterInstantiate += 1
										}
									}
								}
							}
						}
					}
				}
				// if StartDateTime.Before(now) || StartDateTime.Equal(now) {
				// 	if ExpiredDateTime.After(now) || ExpiredDateTime.Equal(now) {

				// 	}
				// }
			}

		}

	}

	collMailBox := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	queryMailBox := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID),
			"isActive": true,
		}},
	}
	mailBox := []bson.M{}
	pipeMailBox := collMailBox.Pipe(queryMailBox)
	errMailBox := pipeMailBox.All(&mailBox)
	countMailBox := 0
	if errMailBox == nil {
		countMailBox = len(mailBox)
	}

	queryMonsterInBag := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.JoinChannelCollection,
				"localField":   "_id",
				"foreignField": "mPlayerId",
				"as":           "joinChannels",
			},
		},
		{"$match": bson.M{"joinChannels": bson.M{"$size": 0}}},
	}

	pipeMonsterInBag := monsterPlayerColl.Pipe(queryMonsterInBag)
	monsterInBagCount := []bson.M{}
	countMonsterInBag := 0
	if err := pipeMonsterInBag.All(&monsterInBagCount); err == nil {
		countMonsterInBag = len(monsterInBagCount)
	}

	utils.ResponseSuccess(ctx, bson.M{
		"countMonsterInChannel":   countMonsterInChannel,
		"countMonsterInstantiate": countMonsterInstantiate,
		"countMailBox":            countMailBox,
		"countMonsterInBag":       countMonsterInBag,
	})
}
