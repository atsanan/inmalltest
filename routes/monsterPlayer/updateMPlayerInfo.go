package monster

import (
	"fmt"
	"strconv"
	"time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"

	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
)

func validateRule(mPlayerName string) error {
	err := validation.Errors{
		"mPlayerName": validation.Validate(mPlayerName, validation.Length(2, 128)),
		//	"mPlayerHabit": validation.Validate(mPlayerHabit, is.Int),
	}.Filter()

	if err != nil {
		return err
	}

	return nil
}

// UpdateMPlayerInfo route handler.
func UpdateMPlayerInfo(ctx iris.Context) {
	mPlayerID := ctx.Params().Get("mPlayerId")
	if err := utils.ValidateMongoID("mPlayerId", mPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	var mPlayerName = ctx.PostValue("mPlayerName")
	mPlayerHabit := ctx.PostValue("mPlayerHabit")
	i64, _ := strconv.Atoi(mPlayerHabit)
	// if parseErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
	// 	return
	// }
	if err := validateRule(mPlayerName); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	var mPlayerNewDateTime = ctx.PostValue("mPlayerNewDateTime")
	var mPlayerLastDateTime = ctx.PostValue("mPlayerLastDateTime")
	var mPlayerMessage = ctx.PostValue("mPlayerMessage")
	update := bson.M{}
	if mPlayerName != "" {
		update["mPlayerName"] = mPlayerName
	}
	if mPlayerHabit != "" {
		update["mPlayerHabit"] = i64
	}

	if mPlayerNewDateTime != "" {
		mPlayerNewDate, err := time.Parse(time.RFC3339, mPlayerNewDateTime)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
			return
		}
		update["mPlayerNewDateTime"] = mPlayerNewDate
	}
	if mPlayerLastDateTime != "" {
		mPlayerLastDate, err := time.Parse(time.RFC3339, mPlayerLastDateTime)
		if err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
			return
		}
		update["mPlayerLastDateTime"] = mPlayerLastDate
	}

	if mPlayerMessage != "" {
		update["mPlayerMessage"] = mPlayerMessage
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	if mongoErr := monsterPlayerColl.Update(bson.M{"_id": bson.ObjectIdHex(mPlayerID)}, bson.M{
		"$set":         update,
		"$currentDate": bson.M{"lastModified": true},
	}); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	pipe := monsterPlayerColl.Pipe([]bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(mPlayerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MonstersDefaultCollection,
				"localField":   "mDefaultId",
				"foreignField": "_id",
				"as":           "monstersDefault",
			},
		},
		{"$unwind": "$monstersDefault"},
	})
	resp := bson.M{}
	if err := pipe.One(&resp); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, resp)
}
