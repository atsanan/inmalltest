package monster

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func RemoveMonterPlayerByMDefault(ctx iris.Context) {

	mDefaultID := ctx.Params().Get("mDfaultId")
	if err := utils.ValidateMongoID("mDfaultId", mDefaultID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	monster := bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MonstersDefaultCollection)
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)
	MonsterStickerColl := session.DB(config.DbName).C(config.DBCollection.MonsterStickerCollection)
	mongoErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(mDefaultID)}).One(&monster)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to find data", fmt.Sprint(mongoErr))
		return
	}

	monsterPlayer := []bson.M{}
	monsterPlayerErr := monsterPlayerColl.Find(bson.M{"mDefaultId": bson.ObjectIdHex(mDefaultID)}).All(&monsterPlayer)
	if monsterPlayerErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to find data", fmt.Sprint(monsterPlayerErr))
		return
	}

	for _, v := range monsterPlayer {
		joinChannelColl.RemoveAll(bson.M{"mPlayerId": v["_id"]})
		MonsterStickerColl.RemoveAll(bson.M{"sendToMPlayerId": v["_id"]})

	}
	monsterPlayerColl.RemoveAll(bson.M{"mDefaultId": bson.ObjectIdHex(mDefaultID)})
	utils.ResponseSuccess(ctx, bson.M{"message": "success remove monsterPlayer"})
}
