package monster

import (
	"fmt"
	"inmallgame/app/utils"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	validation "github.com/go-ozzo/ozzo-validation"
)

//UpdateMessage monster
func UpdateMessage(ctx iris.Context) {
	mPlayerID := ctx.Params().Get("mPlayerId")
	if err := utils.ValidateMongoID("mPlayerId", mPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	mPlayerMessage := ctx.PostValue("mPlayerMessage")

	errMessage := validation.Errors{
		"mPlayerMessage": validation.Validate(mPlayerMessage, validation.Required),
	}.Filter()
	if errMessage != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMessage))
		return
	}

	update := bson.M{
		"mPlayerMessage":        mPlayerMessage,
		"mPlayerQuest1Datetime": time.Now(),
	}

	ResponsMonster(ctx, mPlayerID, update)
}
