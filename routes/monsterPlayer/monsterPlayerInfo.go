package monster

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// MPlayerInfo route handler.
func MPlayerInfo(ctx iris.Context) {
	mPlayerID := ctx.URLParam("mPlayerId")
	if err := utils.ValidateMongoID("mPlayerId", mPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	coll := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	pipe := coll.Pipe([]bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(mPlayerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MonstersDefaultCollection,
				"localField":   "mDefaultId",
				"foreignField": "_id",
				"as":           "monstersDefault",
			},
		},
		{"$unwind": "$monstersDefault"},
	})
	resp := bson.M{}
	if err := pipe.One(&resp); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, resp)
}
