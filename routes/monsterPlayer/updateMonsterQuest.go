package monster

import (
	"fmt"
	"inmallgame/app/utils"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//UpdateQuest1 monster
func UpdateQuest1(ctx iris.Context) {
	mPlayerID := ctx.Params().Get("mPlayerId")
	if err := utils.ValidateMongoID("mPlayerId", mPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	mPlayerStatusQuest1, parseErr := ctx.PostValueInt("mPlayerStatusQuest1")
	if parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	update := bson.M{
		"mPlayerStatusQuest1":   mPlayerStatusQuest1,
		"mPlayerQuest1Datetime": time.Now(),
	}

	ResponsMonster(ctx, mPlayerID, update)
}

//UpdateQuest2 monster
func UpdateQuest2(ctx iris.Context) {
	mPlayerID := ctx.Params().Get("mPlayerId")
	if err := utils.ValidateMongoID("mPlayerId", mPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	mPlayerStatusQuest2, parseErr := ctx.PostValueInt("mPlayerStatusQuest2")
	if parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	update := bson.M{
		"mPlayerStatusQuest2":   mPlayerStatusQuest2,
		"mPlayerQuest2Datetime": time.Now(),
	}

	ResponsMonster(ctx, mPlayerID, update)
}

//UpdateQuest3 monster
func UpdateQuest3(ctx iris.Context) {
	mPlayerID := ctx.Params().Get("mPlayerId")
	if err := utils.ValidateMongoID("mPlayerId", mPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	mPlayerStatusQuest3, parseErr := ctx.PostValueInt("mPlayerStatusQuest3")
	if parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	update := bson.M{
		"mPlayerStatusQuest3":   mPlayerStatusQuest3,
		"mPlayerQuest3Datetime": time.Now(),
	}

	ResponsMonster(ctx, mPlayerID, update)
}
