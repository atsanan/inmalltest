package monster

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// CatchWildMonster for admin use.
func CatchWildMonster(ctx iris.Context) {
	MDefaultID := ctx.PostValue("mDefaultId")
	PlayerID := ctx.PostValue("playerId")
	err := validation.Errors{
		"mDefaultId": validation.Validate(MDefaultID, validation.Required, is.MongoID),
		"playerId":   validation.Validate(PlayerID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	_id := bson.NewObjectId()
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	monsterPlayer := models.MonsterPlayer{
		ID:         _id,
		MDefaultID: bson.ObjectIdHex(MDefaultID),
		PlayerID:   bson.ObjectIdHex(PlayerID),
		CreateAt: time.Now(),
	}

	if mongoErr := monsterPlayerColl.Insert(monsterPlayer); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	m := bson.M{}
	pipe := monsterPlayerColl.Pipe([]bson.M{
		{"$match": bson.M{"_id": _id}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MonstersDefaultCollection,
				"localField":   "mDefaultId",
				"foreignField": "_id",
				"as":           "monstersDefault",
			},
		},
		{"$unwind": "$monstersDefault"},
	})
	if findErr := pipe.One(&m); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, m)
}
