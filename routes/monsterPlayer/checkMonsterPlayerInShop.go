package monster

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

// "github.com/globalsign/mgo"
//	"github.com/globalsign/mgo/bson"
//	"github.com/kataras/iris"
// "github.com/novalagung/gubrak"
// "inmallgame/app/models"

// "log"
// "time"
// "encoding/json"
// "strconv"

func CheckMonterPlayerShop(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	shopID := ctx.Params().Get("shopId")
	err := validation.Errors{
		"playerd": validation.Validate(playerID, validation.Required, is.MongoID),
		"shopId":  validation.Validate(shopID, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)

	channels := bson.M{}
	query := []bson.M{
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.JoinChannelCollection,
				"localField":   "_id",
				"foreignField": "channelId",
				"as":           "joinChannel",
			},
		},
		{"$unwind": "$joinChannel"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MonsterPlayerCollection,
				"localField":   "joinChannel.mPlayerId",
				"foreignField": "_id",
				"as":           "mPlayer",
			},
		},
		{"$unwind": "$mPlayer"},
		{"$match": bson.M{
			"shopId":           bson.ObjectIdHex(shopID),
			"mPlayer.playerId": bson.ObjectIdHex(playerID),
		},
		},
	}
	// if findAvailableChannel {
	// 	query["$where"] = "this.capacity < this.maxCapacity"
	// }
	if mongoErr := coll.Pipe(query).One(&channels); mongoErr != nil {
		utils.ResponseSuccess(ctx, bson.M{"isExists": false,})
		return
	}

	utils.ResponseSuccess(ctx, bson.M{"isExists": true,})	


}
