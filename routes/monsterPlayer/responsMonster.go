package monster

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func ResponsMonster(ctx iris.Context, mPlayerID string, update interface{}) {

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	if mongoErr := monsterPlayerColl.Update(bson.M{"_id": bson.ObjectIdHex(mPlayerID)}, bson.M{
		"$set":         update,
		"$currentDate": bson.M{"lastModified": true},
	}); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	m := bson.M{}
	if findErr := monsterPlayerColl.Find(bson.M{"_id": bson.ObjectIdHex(mPlayerID)}).One(&m); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, m)

}
