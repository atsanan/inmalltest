package monster

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func GroupMonterPlayer(ctx iris.Context) {

	playerID := ctx.Params().Get("playerId")
	if err := utils.ValidateMongoID("playerId", playerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	baseQ := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
		{"$group": bson.M{"_id": "$mDefaultId", "count": bson.M{"$sum": 1}}},
	}
	pipe := monsterPlayerColl.Pipe(baseQ)
	resp := []bson.M{}
	if err := pipe.All(&resp); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	monsterDefaultColl := session.DB(config.DbName).C(config.DBCollection.MonstersDefaultCollection)
	baseCountQ := []bson.M{
		{"$match": bson.M{"mDefaultGroupId": bson.M{"$ne":-1},}},
	}
	pipeMax := monsterDefaultColl.Pipe(baseCountQ)
	max := []bson.M{}
	if err := pipeMax.All(&max); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	queryCount := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
		{"$group": bson.M{"_id":nil, "count": bson.M{"$sum": 1}}},
	}
	pipeCount := monsterPlayerColl.Pipe(queryCount)
	monsterCount :=bson.M{}
	if err := pipeCount.One(&monsterCount); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	monster := bson.M{
		"count": len(resp),
		"max":   len(max),
		"monsterCount":monsterCount["count"].(int),
	}

	utils.ResponseSuccess(ctx, monster)

}
