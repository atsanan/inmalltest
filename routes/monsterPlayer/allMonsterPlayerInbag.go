package monster

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AllMonsterInbag route handler.
func AllMonsterInbag(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	if err := utils.ValidateMongoID("playerId", playerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	params := ctx.URLParams()
	fieldSelection := utils.FieldsParams(params)

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)

	baseQ := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.JoinChannelCollection,
				"localField":   "_id",
				"foreignField": "mPlayerId",
				"as":           "joinChannels",
			},
		},
		{"$match": bson.M{"joinChannels": bson.M{"$size": 0}}},
	}
	mergeQ := MergeMDefaultIDLookup(fieldSelection, baseQ, config.DBCollection.MonstersDefaultCollection)
	offsetQ := append(mergeQ, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := monsterPlayerColl.Pipe(offsetQ)

	resp := []bson.M{}
	if err := pipe.All(&resp); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	//mcount, _ := utils.CalcPipeCount(ctx, mergeQ, monsterPlayerColl)
	count := len(resp)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/monster/inbag/%s", playerID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["monsters"] = resp

	utils.ResponseSuccess(ctx, result)
}
