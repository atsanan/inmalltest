package monster

import (
	"fmt"
	"time"

	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

// UpdateMonsterFood route handler.
func UpdateMonsterFood(ctx iris.Context) {
	mPlayerID := ctx.Params().Get("mPlayerId")
	if err := utils.ValidateMongoID("mPlayerId", mPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	mPlayerFoodID := ctx.PostValue("mPlayerFoodId")
	if err := utils.ValidateMongoID("mPlayerFoodId", mPlayerFoodID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	mPlayerStatusFood, parseErr := ctx.PostValueInt("mPlayerStatusFood")
	if parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	mPlayerMessage := ctx.PostValue("mPlayerMessage")

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	update := bson.M{
		"mPlayerStatusFood":   mPlayerStatusFood,
		"mPlayerFoodDatetime": time.Now(),
		"mPlayerFoodId":       bson.ObjectIdHex(mPlayerFoodID),
	}

	if mPlayerMessage != "" {
		update["mPlayerMessage"] = mPlayerMessage
	}

	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	if mongoErr := monsterPlayerColl.Update(
		bson.M{"_id": bson.ObjectIdHex(mPlayerID)}, bson.M{
			"$set":         update,
			"$currentDate": bson.M{"lastModified": true},
		}); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	m := bson.M{}
	if findErr := monsterPlayerColl.Find(bson.M{"_id": bson.ObjectIdHex(mPlayerID)}).One(&m); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, m)
}
