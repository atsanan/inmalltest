package monster

import (
	"fmt"
	"inmallgame/app/utils"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//UpdateExp monster
func UpdateExp(ctx iris.Context) {
	mPlayerID := ctx.Params().Get("mPlayerId")
	if err := utils.ValidateMongoID("mPlayerId", mPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	mPlayerStatusExp, parseErr := ctx.PostValueInt("mPlayerStatusExp")
	if parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	update := bson.M{
		"mPlayerStatusExp":      mPlayerStatusExp,
		"mPlayerQuest1Datetime": time.Now(),
	}

	ResponsMonster(ctx, mPlayerID, update)
}
