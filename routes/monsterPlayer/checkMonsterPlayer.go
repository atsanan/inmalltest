package monster

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

// "github.com/globalsign/mgo"
//	"github.com/globalsign/mgo/bson"
//	"github.com/kataras/iris"
// "github.com/novalagung/gubrak"
// "inmallgame/app/models"

// "log"
// "time"
// "encoding/json"
// "strconv"

func CheckMonterPlayer(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	mDefaultID := ctx.Params().Get("mDefaultId")
	if err := utils.ValidateMongoID("playerId", playerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	if err := utils.ValidateMongoID("mDefaultId", mDefaultID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	baseQ := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID), "mDefaultId": bson.ObjectIdHex(mDefaultID)}},
	}
	pipe := monsterPlayerColl.Pipe(baseQ)
	resp := []bson.M{}
	if err := pipe.All(&resp); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	count := len(resp)
	monsterStatus := bson.M{}
	if count == 0 {
		monsterStatus = bson.M{"monsterStatus": false}
	} else {
		monsterStatus = bson.M{"monsterStatus": true}
	}

	utils.ResponseSuccess(ctx, monsterStatus)
}
