package monster

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"github.com/novalagung/gubrak"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// MergeMDefaultIDLookup function
func MergeMDefaultIDLookup(fieldSelection []string, baseQuery []bson.M, lookupColl string) []bson.M {
	if fieldSelection != nil {
		if id, _ := gubrak.IndexOf(fieldSelection, "mDefaultId"); id >= 0 {
			baseQuery = append(baseQuery, bson.M{
				"$lookup": bson.M{
					"from":         lookupColl,
					"localField":   "mDefaultId",
					"foreignField": "_id",
					"as":           "monsterDefault",
				},
			}, bson.M{"$unwind": "$monsterDefault"})
		}
	}

	return baseQuery
}

// AllMonsterPlayer route handler.
func AllMonsterPlayer(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	if err := utils.ValidateMongoID("playerId", playerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	params := ctx.URLParams()
	fieldSelection := utils.FieldsParams(params)

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)

	baseQuery := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
	}

	mergeQuery := MergeMDefaultIDLookup(fieldSelection, baseQuery, config.DBCollection.MonstersDefaultCollection)

	offsetQuery := append(mergeQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := monsterPlayerColl.Pipe(offsetQuery)

	resp := []bson.M{}
	if err := pipe.All(&resp); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	count := len(resp)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/monster/all/%s", playerID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["monsters"] = resp

	utils.ResponseSuccess(ctx, result)
}
