package monster

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func RemoveMonterPlayer(ctx iris.Context) {
	mPlayerID := ctx.Params().Get("mPlayerId")
	if err := utils.ValidateMongoID("mPlayerId", mPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	monsterPlayerColl := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)
	MonsterStickerColl := session.DB(config.DbName).C(config.DBCollection.MonsterStickerCollection)
	if err := monsterPlayerColl.Remove(bson.M{"_id": bson.ObjectIdHex(mPlayerID)}); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to remove data", fmt.Sprint(err))
		return
	}

	joinChannelColl.RemoveAll(bson.M{"mPlayerId": bson.ObjectIdHex(mPlayerID)})

	MonsterStickerColl.RemoveAll(bson.M{"sendToMPlayerId": bson.ObjectIdHex(mPlayerID)})

	utils.ResponseSuccess(ctx, bson.M{"message": "success remove monsterPlayer"})

}
