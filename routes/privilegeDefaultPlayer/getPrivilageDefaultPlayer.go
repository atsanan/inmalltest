package privilegeDefaultPlayer

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

func getPrivilegeDefaultPlayer(ctx iris.Context) {

	playerID := ctx.Params().Get("playerId")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	if validateErr := utils.ValidateMongoID("playerId", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PrivilegeDefaultsPlayerCollection)

	baseQuery := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID),
			"isActive": true,
		}},
	}

	pcount, countErr := utils.CalcPipeCount(ctx, baseQuery, coll)
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
		return
	}

	count := pcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(offsetQuery)

	privileDefaultPlayer := []bson.M{}
	if mongoErr := pipe.All(&privileDefaultPlayer); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	originalLink := fmt.Sprintf("/api/v1/privilegeDefaultPlayers/%s", playerID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["privilegeDefaultPlayers"] = privileDefaultPlayer

	utils.ResponseSuccess(ctx, result)
}
