package privilegeDefaultPlayer

import (
	"fmt"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"time"
)

//addPrivilegePlayer
func addPrivilegePlayer(ctx iris.Context) {

	privilegeDefaultPlayerGroup := models.PrivilegeDefaultPlayerGroup{}
	if parseErr := ctx.ReadJSON(&privilegeDefaultPlayerGroup); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	if validateErr := privilegeDefaultPlayerGroup.ValidateAll(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}

	PrivilegeDefaultID := privilegeDefaultPlayerGroup.PrivilegeDefaultID
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PrivilegeDefaultsPlayerCollection)

	coll.RemoveAll(bson.M{"playerId": privilegeDefaultPlayerGroup.PlayerID})

	successID := []bson.ObjectId{}
	if len(PrivilegeDefaultID) > 0 {
		for _, v := range PrivilegeDefaultID {
			mongoErr := coll.Insert(bson.M{
				"playerId":           privilegeDefaultPlayerGroup.PlayerID,
				"privilegeDefaultId": v,
				"isActived":          true,
				"created_at":         time.Now(),
			})
			if mongoErr != nil {

				//utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
				return
			} else {
				successID = append(successID, v)
			}
		}

	} 

	utils.ResponseSuccess(ctx, bson.M{
		"playerId":           privilegeDefaultPlayerGroup.PlayerID,
		"privilegeDefaultId": successID,
	})
}
