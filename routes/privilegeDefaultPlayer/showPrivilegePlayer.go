package privilegeDefaultPlayer

import (
	"fmt"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

type PrivilegeShop struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty" `
	ExpiredDateTime time.Time     `json:"expiredDateTime" bson:"expiredDateTime"`
	StartDateTime   time.Time     `json:"startDateTime" bson:"startDateTime"`
}

func showPrivilegePlayer(ctx iris.Context) {

	playerID := ctx.Params().Get("playerId")
	mallID := ctx.URLParamTrim("mallId")
	if validateErr := utils.ValidateMongoID("playerId", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PrivilegeDefaultsPlayerCollection)
	privilegeShopsCollection := session.DB(config.DbName).C(config.DBCollection.PrivilegeShopsCollection)

	privilegeDefaultsCollection := session.DB(config.DbName).C(config.DBCollection.PrivilegeDefaultsCollection)
	ShopCollection := session.DB(config.DbName).C(config.DBCollection.ShopCollection)

	baseQuery := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
	}

	if mallID != "" {
		dataShop := []bson.M{}
		addQuery := []bson.M{
			bson.M{
				"$lookup": bson.M{
					"from":         config.DBCollection.ShopCollection,
					"localField":   "shopId",
					"foreignField": "_id",
					"as":           "shop",
				},
			},
			bson.M{
				"$unwind": "$shop",
			},
			bson.M{
				"$lookup": bson.M{
					"from":         config.DBCollection.MallFloorCollection,
					"localField":   "shop.mallFloorId",
					"foreignField": "_id",
					"as":           "mallFloor",
				},
			},
			bson.M{
				"$unwind": "$mallFloor",
			},
			bson.M{"$match": bson.M{
				"mallFloor.mallId": bson.ObjectIdHex(mallID),
			}},
		}
		if shopErr := privilegeShopsCollection.Pipe(addQuery).All(&dataShop); shopErr != nil {
		}

		oids := make([]bson.ObjectId, len(dataShop))
		if len(dataShop) > 0 {
			for i := range dataShop {
				oids[i] = dataShop[i]["privilegeDefaultId"].(bson.ObjectId)
			}
		}

		baseQuery = append(baseQuery,
			bson.M{"$match": bson.M{
				"privilegeDefaultId": bson.M{
					"$in": oids,
				},
			}})
	}
	// pcount, countErr := utils.CalcPipeCount(ctx, baseQuery, coll)
	// if countErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
	// 	return
	// }

	//offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(baseQuery)

	privileDefaultPlayer := []bson.M{}
	if mongoErr := pipe.All(&privileDefaultPlayer); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	data := []bson.M{}
	now := time.Now()
	for k, v := range privileDefaultPlayer {

		pd := bson.M{}
		privilegeDefaultsCollection.Find(bson.M{"_id": v["privilegeDefaultId"]}).One(&pd)
		privileDefaultPlayer[k]["privilegeDefault"] = pd
		ps := []bson.M{}
		privilegeShopsCollection.Find(bson.M{
			"privilegeDefaultId": v["privilegeDefaultId"],
			"isActive":           true,
		}).All(&ps)

		psDate := []PrivilegeShop{}
		privilegeShopsCollection.Find(bson.M{
			"privilegeDefaultId": v["privilegeDefaultId"],
			"isActive":           true,
		}).All(&psDate)

		if len(ps) > 0 {
			dataPs := []bson.M{}
			for kps, vps := range ps {
				if vps["isReachDateTime"].(bool) == false {
					shop := bson.M{}
					ShopCollection.Find(bson.M{"_id": vps["shopId"]}).One(&shop)
					ps[kps]["shop"] = shop
					dataPs = append(dataPs, ps[kps])
				} else {
					StartDateTime := psDate[kps].StartDateTime
					ExpiredDateTime := psDate[kps].ExpiredDateTime
					if StartDateTime.Before(now) || StartDateTime.Equal(now) {
						if ExpiredDateTime.After(now) || ExpiredDateTime.Equal(now) {
							shop := bson.M{}
							ShopCollection.Find(bson.M{"_id": vps["shopId"]}).One(&shop)
							ps[kps]["shop"] = shop
							dataPs = append(dataPs, ps[kps])
						}
					}

				}
			}

			privileDefaultPlayer[k]["privilegeShops"] = dataPs
			data = append(data, privileDefaultPlayer[k])
		}
	}
	pages := utils.CalcPages(len(data), limit)
	originalLink := fmt.Sprintf("/api/v1/privilegeDefaultPlayers/showPrivilegePlayer/%s", playerID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)

	if len(data) > offset+limit {
		result["privilegeDefaultPlayers"] = data[offset : offset+limit]
	} else {
		if offset < len(data) {
			result["privilegeDefaultPlayers"] = data[offset:len(data)]
		} else {
			result["privilegeDefaultPlayers"] = []bson.M{}
		}

	}
	utils.ResponseSuccess(ctx, result)
}
