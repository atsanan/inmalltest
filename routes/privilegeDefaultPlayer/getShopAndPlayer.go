package privilegeDefaultPlayer

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

func getShopAndPlayer(ctx iris.Context) {

	playerID := ctx.Params().Get("playerId")
	shopID := ctx.Params().Get("shopId")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	if validateErr := utils.ValidateMongoID("playerId", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	if validateErr := utils.ValidateMongoID("shopId", shopID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PrivilegeShopsCollection)

	privileDefaultPlayer := []bson.M{}
	baseQuery := []bson.M{
		{"$match": bson.M{"shopId": bson.ObjectIdHex(shopID),
						"isActive":true,
					},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PrivilegeDefaultsCollection,
				"localField":   "privilegeDefaultId",
				"foreignField": "_id",
				"as":           "privilegeDefault",
			},
		},
		{"$unwind": "$privilegeDefault"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PrivilegeDefaultsPlayerCollection,
				"localField":   "privilegeDefaultId",
				"foreignField": "privilegeDefaultId",
				"as":           "privilegeDefaultPlayers",
			},
		},
	    {"$unwind": "$privilegeDefaultPlayers"},
		{"$match": bson.M{"privilegeDefaultPlayers.playerId": bson.ObjectIdHex(playerID)}},
	}

	pcount, countErr := utils.CalcPipeCount(ctx, baseQuery, coll)
	if countErr != nil {

		utils.ResponseSuccess(ctx,
			bson.M{"privilegeShops": privileDefaultPlayer,
				"pageIndex": pageIndex,
				"pageLimit": pageLimit,
				"pages":     1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		//utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
		return
	}

	count := pcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(offsetQuery)

	if mongoErr := pipe.All(&privileDefaultPlayer); mongoErr != nil {

		utils.ResponseSuccess(ctx,
			bson.M{"privilegeShops": privileDefaultPlayer,
				"pageIndex": pageIndex,
				"pageLimit": pageLimit,
				"pages":     1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		//utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	originalLink := fmt.Sprintf("/api/v1/privilegeDefaultPlayers/%s/%s", shopID, playerID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["privilegeShops"] = privileDefaultPlayer

	utils.ResponseSuccess(ctx, result)
}
