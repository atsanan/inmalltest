package privilegeDefaultPlayer

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, addPrivilegePlayer)
	routes.Get("/{playerId:string}", auth.TokenMDW, getPrivilegeDefaultPlayer)
	routes.Get("/shopAndPlayer/{shopId:string}/{playerId:string}", auth.TokenMDW, getShopAndPlayer)
	routes.Get("/showPrivilegePlayer/{playerId:string}", auth.TokenMDW, showPrivilegePlayer)

}
