package stickerstore

import (
	"fmt"
	// "time"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
)

// GetStoreList will serve as router.
func GetStoreList(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	stickerColl := session.DB(config.DbName).C(config.DBCollection.StickerStoreCollection)

	storeResults := []bson.M{}
	if mongoErr := stickerColl.Find(bson.M{}).Skip(offset).Limit(limit).All(&storeResults); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}

	count, _ := stickerColl.Count()
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v1/stickerStore/list?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["stickers"] = storeResults

	utils.ResponseSuccess(ctx, result)
}
