package stickerstore

import (
	"fmt"
	// "time"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	"github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

func validateStickerStireFields(ctx iris.Context, jsonData map[string]interface{}) error {
	stickerStore := models.StickerStore{}
	if decodeErr := mapstructure.Decode(jsonData, &stickerStore); decodeErr != nil {
		return decodeErr
	}

	if validateErr := stickerStore.ValidateType(); validateErr != nil {
		return validateErr
	}

	return nil
}

// UpdateStickerStore will serve as router.
func UpdateStickerStore(ctx iris.Context) {
	stickerStoreID := ctx.Params().Get("stickerStoreId")
	if validErr := utils.ValidateMongoID("stickerStoreId", stickerStoreID); validErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validErr))
		return
	}

	jsonData := map[string]interface{}{}
	if parseErr := ctx.ReadJSON(&jsonData); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}
	if validFieldsErr := validateStickerStireFields(ctx, jsonData); validFieldsErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validFieldsErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	stickerColl := session.DB(config.DbName).C(config.DBCollection.StickerStoreCollection)
	if mongoErr := stickerColl.Update(
		bson.M{"_id": bson.ObjectIdHex(stickerStoreID)},
		bson.M{"$set": jsonData, "$currentDate": bson.M{"lastModified": true}}); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	storeResult := bson.M{}
	if mongoErr := stickerColl.Find(bson.M{"_id": bson.ObjectIdHex(stickerStoreID)}).One(&storeResult); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, storeResult)
}
