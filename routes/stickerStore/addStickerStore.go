package stickerstore

import (
	"fmt"
	"time"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

// AddStickerStore will serve as router.
func AddStickerStore(ctx iris.Context) {
	stickerStore := models.StickerStore{}

	if parseErr := ctx.ReadJSON(&stickerStore); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	if validateErr := stickerStore.ValidateStickerStore(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	stickerStore.ID = bson.NewObjectId()
	stickerStore.CreateAt = time.Now()

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	stickerColl := session.DB(config.DbName).C(config.DBCollection.StickerStoreCollection)
	if mongoErr := stickerColl.Insert(stickerStore); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	storeResult := bson.M{}
	if mongoErr := stickerColl.Find(bson.M{"_id": stickerStore.ID}).One(&storeResult); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, storeResult)
}
