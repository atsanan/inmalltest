package stickerstore

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Get("/list", auth.TokenMDW, GetStoreList)

	// For admin.
	routes.Post("/add", AddStickerStore)
	routes.Post("/update/{stickerStoreId : string}", UpdateStickerStore)
}
