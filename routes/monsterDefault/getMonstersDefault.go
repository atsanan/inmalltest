package monsterDefault

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
)

// GetMonstersDefault for admin use.
func GetMonstersDefault(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	params := ctx.URLParams()
	sortData := utils.SortParamsAsStrings(params)

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	monsters := []bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MonstersDefaultCollection)

	var mongoErr error
	if sortData != "" {
		mongoErr = coll.Find(bson.M{}).Sort(sortData).Skip(offset).Limit(limit).All(&monsters)
	} else {
		mongoErr = coll.Find(bson.M{}).Skip(offset).Limit(limit).All(&monsters)
	}
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	count, _ := coll.Count()
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v1/monsterDefault?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["monsters"] = monsters

	utils.ResponseSuccess(ctx, result)
}
