package monsterDefault

import (
	"fmt"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

func validateID(ctx iris.Context, mDefaultID string) error {
	err := validation.Errors{
		"id": validation.Validate(mDefaultID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		return err
	}

	return nil
}

// GetInfo for admin use.
func GetInfo(ctx iris.Context) {
	mID := ctx.Params().Get("id")
	err := validateID(ctx, mID)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	monster := bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MonstersDefaultCollection)
	mongoErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(mID)}).One(&monster)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to find data", fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, monster)
}
