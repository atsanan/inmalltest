package monsterDefault

import (
	"fmt"
	"log"
	"time"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	"github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

func validateAddDefaultMonster(ctx iris.Context, monster *map[string]interface{}) error {
	if parseErr := ctx.ReadJSON(&monster); parseErr != nil {
		return parseErr
	}

	mDefault := models.MonsterDefault{}
	if decodeErr := mapstructure.Decode(monster, &mDefault); decodeErr != nil {
		return decodeErr
	}

	if validateErr := mDefault.ValidateAll(); validateErr != nil {
		return validateErr
	}

	return nil
}

// AddMonsterDefault for admin use.
func AddMonsterDefault(ctx iris.Context) {
	monster := map[string]interface{}{}
	if err := validateAddDefaultMonster(ctx, &monster); err != nil {
		log.Println("validate fail", err)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	monster["_id"] = bson.NewObjectId()
	monster["createAt"] = time.Now()

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MonstersDefaultCollection)
	if mongoErr := coll.Insert(monster); mongoErr != nil {
		log.Println("mongoErr", mongoErr)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	monsterDefault := bson.M{}
	findErr := coll.Find(bson.M{"_id": monster["_id"]}).One(&monsterDefault)
	if findErr != nil {
		log.Println("findErr", findErr)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, monsterDefault)
}
