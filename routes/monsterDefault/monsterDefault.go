package monsterDefault

import (

	// "github.com/globalsign/mgo"

	// "github.com/novalagung/gubrak"

	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Get("/", auth.TokenMDW, GetMonstersDefault)
	routes.Get("/info/{id: string}", auth.TokenMDW, GetInfo)

	// For admin
	routes.Post("/add", AddMonsterDefault)
	routes.Post("/{id : string}", UpdateMonsterDefault)
}
