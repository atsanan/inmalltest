package monsterDefault

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

func validateBody(ctx iris.Context, mDefaultID string, monster *map[string]interface{}) error {
	err := validation.Errors{
		"id": validation.Validate(mDefaultID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		return err
	}

	if parseErr := ctx.ReadJSON(&monster); parseErr != nil {
		return parseErr
	}

	mDefault := models.MonsterDefault{}
	if decodeErr := mapstructure.Decode(monster, &mDefault); decodeErr != nil {
		return decodeErr
	}

	if validateErr := mDefault.ValidateRule(); validateErr != nil {
		return validateErr
	}

	return nil
}

// UpdateMonsterDefault for admin use.
func UpdateMonsterDefault(ctx iris.Context) {
	mID := ctx.Params().Get("id")

	monster := map[string]interface{}{}
	err := validateBody(ctx, mID, &monster)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MonstersDefaultCollection)

	mongoErr := coll.Update(
		bson.M{"_id": bson.ObjectIdHex(mID)},
		bson.M{
			"$set":         monster,
			"$currentDate": bson.M{"lastModified": true},
		})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	m := bson.M{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(mID)}).One(&m)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, m)
}
