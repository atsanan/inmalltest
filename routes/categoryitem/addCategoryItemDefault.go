package categoryitem

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddCategoryItemDefault for admin use.
func AddCategoryItemDefault(ctx iris.Context) {
	item := models.ItemCategory{}
	if parseErr := ctx.ReadJSON(&item); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	if validateErr := item.ValidateAll(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}

	_id := bson.NewObjectId()

	update := bson.M{
		"_id":               _id,
		"itemCategoryName":  item.ItemCategoryName,
		"itemCategoryOrder": item.ItemCategoryOrder,
		"createAt":          time.Now(),
		"lastModified":      time.Now(),
	}

	if item.ItemCategoryOrder != 0 {
		update["itemCategoryOrder"] = item.ItemCategoryOrder
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CategoryItemCollection)
	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	items := models.ItemCategory{}
	findErr := coll.Find(bson.M{"_id": _id}).One(&items)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, items)
}
