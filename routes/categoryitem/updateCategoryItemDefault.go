package categoryitem

import (
	"fmt"

	// "github.com/globalsign/mgo"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

func validateID(ctx iris.Context, categoryItemDefaultID string) error {
	err := validation.Errors{
		"id": validation.Validate(categoryItemDefaultID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		return err
	}

	return nil
}

// UpdateCategoryItemDefault for admin use.
func UpdateCategoryItemDefault(ctx iris.Context) {
	categoryItemID := ctx.Params().Get("id")
	err := validation.Errors{
		"id": validation.Validate(categoryItemID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	item := models.ItemCategory{}

	if rErr := ctx.ReadJSON(&item); rErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(rErr))
		return
	}

	if validateErr := item.ValidateAll(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CategoryItemCollection)

	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(categoryItemID)}, bson.M{
		"$currentDate": bson.M{"lastModified": true},
		"$set":         item,
	})

	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	ItemCategoryData := models.ItemCategory{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(categoryItemID)}).One(&ItemCategoryData)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, ItemCategoryData)
}
