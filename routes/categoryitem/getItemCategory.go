package categoryitem

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//GetCatagoryItem data list
func GetCatagoryItem(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.CategoryItemCollection)
	categoryItems := []bson.M{}
	err := coll.Find(bson.M{}).Skip(offset).Limit(limit).All(&categoryItems)
	if err != nil {
		fmt.Println("Not found", err)

		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}
	count, _ := coll.Count()
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v1/categoryItemDefault?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":         pages,
		"pageIndex":     page,
		"pageLimit":     limit,
		"categoryItems": categoryItems,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}
	utils.ResponseSuccess(ctx, result)
}
