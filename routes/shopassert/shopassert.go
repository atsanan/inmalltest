package shopassert

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func GetShopAssetDetail(ctx iris.Context) {

	ID := ctx.Params().Get("id")
	if validateErr := utils.ValidateMongoID("id", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopAssertsCollection)

	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(ID)}},
	}

	result := bson.M{}
	pipe := coll.Pipe(objQuery)
	findErr := pipe.One(&result)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, result)
}
