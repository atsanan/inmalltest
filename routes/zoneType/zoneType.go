package zoneType

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// GetZoneType for admin add data.
func GetZoneType(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	zoneTypes := []bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ZoneTypeCollection)
	mongoErr := coll.Find(bson.M{}).Skip(offset).Limit(limit).All(&zoneTypes)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}

	count, _ := coll.Count()
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v1/zonetype?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":     pages,
		"pageIndex": page,
		"pageLimit": limit,
		"zoneTypes": zoneTypes,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}
	utils.ResponseSuccess(ctx, result)
}
