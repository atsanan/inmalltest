package zoneType

import (
	"fmt"
	"inmallgame/app/enums"
	"log"
	"time"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	// "time"
	// "encoding/json"
	// "strconv"
)

func checkBody(ctx iris.Context, zoneType *models.ZoneType) error {
	if parseErr := ctx.ReadJSON(&zoneType); parseErr != nil {
		return parseErr
	}

	if zoneType.ZoneTypeID < enums.World.ToInt() || zoneType.ZoneTypeID > enums.InMallShop.ToInt() {
		return fmt.Errorf("ZoneTypeId must in range of zoneTypeEnum %d", zoneType.ZoneTypeID)
	}

	if err := zoneType.ValidateAll(); err != nil {
		return err
	}

	return nil
}

// AddZoneType for admin add data.
func AddZoneType(ctx iris.Context) {
	zoneType := models.ZoneType{}
	err := checkBody(ctx, &zoneType)
	if err != nil {
		log.Printf("Validate Err %s", err)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	zoneType.ID = bson.NewObjectId()
	zoneType.CreateAt = time.Now()

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ZoneTypeCollection)
	mongoErr := coll.Insert(zoneType)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	zoneTypes := bson.M{}
	if findErr := coll.Find(bson.M{"_id": zoneType.ID}).One(&zoneTypes); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, zoneTypes)
}
