package digitalBoard

import (
	"fmt"
	"inmallgame/app/utils"

	"github.com/kataras/iris"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
)

//DigitalBoardByShop func
func DigitalBoardByShop(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	shopID := ctx.Params().Get("shopId")
	if validateErr := utils.ValidateMongoID("mallId", shopID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.DigitalBoardCollection)

	objQuery := []bson.M{
		{"$match": bson.M{"isActive": true,
			"shopId": bson.ObjectIdHex(shopID),
		},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallCollection,
				"localField":   "mallId",
				"foreignField": "_id",
				"as":           "mall",
			},
		},
		{"$unwind": "$mall"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallFloorCollection,
				"localField":   "mallFloorId",
				"foreignField": "_id",
				"as":           "mallFloor",
			},
		},
		{"$unwind": "$mallFloor"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "shopId",
				"foreignField": "_id",
				"as":           "shop",
			},
		},
		{"$unwind": "$shop"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.DigitalBoardAssertsCollection,
				"localField":   "digitalBoardAssertId",
				"foreignField": "_id",
				"as":           "digitalBoardAssert",
			},
		},
		{"$unwind": "$digitalBoardAssert"},
	}
	// objQuery = append(objQuery,
	// 	bson.M{"$match": bson.M{"isActive": true,
	// 		"shopId": bson.ObjectIdHex(shopID),
	// 	}})

	data := []bson.M{}

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.All(&data)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}

	mcount, err := utils.CalcPipeCount(ctx, objQuery, coll)
	if err != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"digital":   data,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}
	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)

	findErr := pipe.All(&data)
	if findErr != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"digital":   data,
			"mcount":    mcount,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}
		utils.ResponseSuccess(ctx, result)
		return
	}

	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/digitalBoard/byShop/%s", shopID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["digital"] = data

	utils.ResponseSuccess(ctx, result)

}
