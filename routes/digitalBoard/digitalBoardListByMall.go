package digitalBoard

import (
	"fmt"
	"inmallgame/app/utils"

	"github.com/kataras/iris"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
)

//DigitalBoardByMall func
func DigitalBoardByMall(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	mallID := ctx.Params().Get("mallId")

	search := ctx.URLParam("search")

	if validateErr := utils.ValidateMongoID("mallId", mallID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.DigitalBoardCollection)

	shopColl := session.DB(config.DbName).C(config.DBCollection.ShopCollection)

	objQuery := []bson.M{
		{"$match": bson.M{
			"mallId":   bson.ObjectIdHex(mallID),
			"isActive": true}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallCollection,
				"localField":   "mallId",
				"foreignField": "_id",
				"as":           "mall",
			},
		},
		{"$unwind": "$mall"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallFloorCollection,
				"localField":   "mallFloorId",
				"foreignField": "_id",
				"as":           "mallFloor",
			},
		},
		{"$unwind": "$mallFloor"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.DigitalBoardAssertsCollection,
				"localField":   "digitalBoardAssertId",
				"foreignField": "_id",
				"as":           "digitalBoardAssert",
			},
		},
		{"$unwind": "$digitalBoardAssert"}}

	if mallID != "" {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"mallId": bson.ObjectIdHex(mallID)}})
	}

	shop := []bson.M{}

	if search != "" {

		mongoErr := shopColl.Pipe([]bson.M{
			bson.M{"$match": bson.M{"$or": []bson.M{
				bson.M{"shopName.eng": bson.M{"$regex": search,"$options":"i"}},
				bson.M{"shopName.thai": bson.M{"$regex": search,"$options":"i"}},
				bson.M{"shopName.chi1": bson.M{"$regex": search,"$options":"i"}},
				bson.M{"shopName.chi2": bson.M{"$regex": search,"$options":"i"}},
			},
			},
			},
		}).All(&shop)
		if mongoErr != nil {
			result := map[string]interface{}{
				"pages":     page,
				"pageIndex": pageIndex,
				"pageLimit": pageLimit,
				"shop":      shop,
				"digital":   []bson.M{},
				"paging": map[string]interface{}{
					"next":     "",
					"previous": "",
				},
			}

			utils.ResponseSuccess(ctx, result)
			return
		} else {
			queryShop := []bson.M{}
			if len(shop) > 0 {
				for _, v := range shop {
					queryShop = append(queryShop, bson.M{"mallFloorId": v["mallFloorId"]})
				}
				objQuery = append(objQuery, bson.M{"$match": bson.M{"$or": queryShop}})
			} else {
				result := map[string]interface{}{
					"pages":     page,
					"pageIndex": pageIndex,
					"pageLimit": pageLimit,
					"shop":      shop,
					"digital":   []bson.M{},
					"paging": map[string]interface{}{
						"next":     "",
						"previous": "",
					},
				}

				utils.ResponseSuccess(ctx, result)
				return
			}
		}

	}
	data := []bson.M{}

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.All(&data)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}

	mcount, err := utils.CalcPipeCount(ctx, objQuery, coll)
	if err != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"digital":   data,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}
	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)

	findErr := pipe.All(&data)
	if findErr != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"digital":   data,
			"mcount":    mcount,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}
		utils.ResponseSuccess(ctx, result)
		return
	}

	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("api/v1/digitalBoard/byMall/%s", mallID)
	linkF := originalLink + "?page=%d&limit=%d"

	if search != "" {
		linkF += "&search=" + search
	}

	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["digital"] = data

	utils.ResponseSuccess(ctx, result)

}
