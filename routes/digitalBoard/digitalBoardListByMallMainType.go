package digitalBoard

import (
	"fmt"
	"inmallgame/app/utils"

	"github.com/kataras/iris"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

//DigitalBoardByMallMainType func
func DigitalBoardByMallMainType(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	mainTypes := ctx.URLParam("mainTypes")
	mallID := ctx.URLParam("mallId")
	err := validation.Errors{
		"mallId": validation.Validate(mallID, is.MongoID),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.DigitalBoardCollection)

	objQuery := []bson.M{
		{"$match": bson.M{"isActive": true}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallCollection,
				"localField":   "mallId",
				"foreignField": "_id",
				"as":           "mall",
			},
		},
		{"$unwind": "$mall"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MallFloorCollection,
				"localField":   "mallFloorId",
				"foreignField": "_id",
				"as":           "mallFloor",
			},
		},
		{"$unwind": "$mallFloor"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "mallFloor._id",
				"foreignField": "mallFloorId",
				"as":           "shops",
			},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.DigitalBoardAssertsCollection,
				"localField":   "digitalBoardAssertId",
				"foreignField": "_id",
				"as":           "digitalBoardAssert",
			},
		},
		{"$unwind": "$digitalBoardAssert"}}

	if mainTypes != "" {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"shops.mainTypes": mainTypes}})
	}

	if mallID != "" {
		objQuery = append(objQuery, bson.M{"$match": bson.M{"mall._id": bson.ObjectIdHex(mallID)}})
	}

	data := []bson.M{}

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.All(&data)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}

	mcount, err := utils.CalcPipeCount(ctx, objQuery, coll)
	if err != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"digital":   data,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}
	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)

	findErr := pipe.All(&data)
	if findErr != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"digital":   data,
			"mcount":    mcount,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}
		utils.ResponseSuccess(ctx, result)
		return
	}

	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("api/v1/digitalBoard/byMall/%s", mallID)
	linkF := originalLink + "?page=%d&limit=%d"

	if mainTypes != "" {
		linkF += "&mainTypes=" + mainTypes
	}

	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["digital"] = data

	utils.ResponseSuccess(ctx, result)

}
