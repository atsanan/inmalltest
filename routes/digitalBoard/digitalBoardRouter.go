package digitalBoard

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	//routes.Post("/{id: string}", auth.TokenMDW, UpdateItemDefault)

	routes.Get("/byMall/{mallId:string}", auth.TokenMDW, DigitalBoardByMall)
	routes.Get("/byShop/{shopId:string}", auth.TokenMDW, DigitalBoardByShop)
	routes.Get("/NearBy/", auth.TokenMDW, DigitalBoardByNearBy)
	routes.Get("/byMall", auth.TokenMDW, DigitalBoardByMallMainType)
}
