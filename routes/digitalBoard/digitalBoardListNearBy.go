package digitalBoard

import (
	"fmt"
	"inmallgame/app/utils"
	"strconv"

	"github.com/kataras/iris"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

//DigitalBoardByNearBy func
func DigitalBoardByNearBy(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	maxDistance := ctx.URLParamIntDefault("maxDistance", 5000)
	lat := ctx.URLParamTrim("lat")
	long := ctx.URLParamTrim("long")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)
	err := validation.Errors{
		"lat":  validation.Validate(lat, validation.Required, is.Float),
		"long": validation.Validate(long, validation.Required, is.Float),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.DigitalBoardCollection)

	objQuery := []bson.M{}
	longConv, _ := strconv.ParseFloat(long, 64)
	latConv, _ := strconv.ParseFloat(lat, 64)
	var coordinates [2]float64
	coordinates[0] = longConv
	coordinates[1] = latConv
	objQuery = append(objQuery,
		bson.M{
			"$geoNear": bson.M{
				"near":          bson.M{"type": "Point", "coordinates": coordinates},
				"maxDistance":   maxDistance,
				"distanceField": "dist.calculated",
				"spherical":     true,
			},
		})

	data := []bson.M{}

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.All(&data)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}

	mcount, err := utils.CalcPipeCount(ctx, objQuery, coll)
	if err != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"digital":   data,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}
	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)

	findErr := pipe.All(&data)
	if findErr != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"digital":   data,
			"mcount":    mcount,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}
		utils.ResponseSuccess(ctx, result)
		return
	}

	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/digitalBoard/byShop/")
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["digital"] = data

	utils.ResponseSuccess(ctx, result)

}
