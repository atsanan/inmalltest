package shopcategories

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, AddShopCategories)
	routes.Post("/{id:string}", auth.TokenMDW, UpdateShopCategoies)
	routes.Get("/", auth.TokenMDW, GetShopCategories)
}
