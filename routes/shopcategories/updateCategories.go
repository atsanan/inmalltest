package shopcategories

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/mitchellh/mapstructure"
)

func validateParams(newsID string) error {
	err := validation.Errors{
		"id": validation.Validate(newsID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		return err
	}
	return nil
}

//UpdateShopCategoies router
func UpdateShopCategoies(ctx iris.Context) {
	ShopCategoriesID := ctx.Params().Get("id")

	if err := validateParams(ShopCategoriesID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	update := map[string]interface{}{}
	if parseErr := ctx.ReadJSON(&update); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(parseErr))
		return
	}

	shopCategory := models.ShopCategory{}
	decodeErr := mapstructure.Decode(update, &shopCategory)
	if decodeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "decode error", fmt.Sprint(decodeErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopCategoriesCollection)
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(ShopCategoriesID)}, bson.M{
		"$set":         update,
		"$currentDate": bson.M{"lastModified": true}})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	ShopCategory := bson.M{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(ShopCategoriesID)}).One(&ShopCategory)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, ShopCategory)
}
