package shopcategories

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddShopCategories for admin add data.
func AddShopCategories(ctx iris.Context) {
	shopCategory := models.ShopCategory{}
	if parseErr := ctx.ReadJSON(&shopCategory); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return

	}

	_id := bson.NewObjectId()
	update := bson.M{
		"_id": _id,
		"shopCategoryNameEng":  shopCategory.ShopCategoryNameEng,
		"shopCategoryNameThai": shopCategory.ShopCategoryNameThai,
		"shopCategoryNameChi1": shopCategory.ShopCategoryNameChi1,
		"shopCategoryNameChi2": shopCategory.ShopCategoryNameChi2,
		"createAt":             time.Now(),
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopCategoriesCollection)

	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	News := bson.M{}
	if findErr := coll.Find(bson.M{"_id": _id}).One(&News); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, News)

}
