package itemplayer

import (

	// "github.com/globalsign/mgo"

	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"inmallgame/app/models"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// DeleteItemPlayerList route.
func DeleteItemPlayerList(ctx iris.Context) {

	listIdItemPlayer := models.ListIdItemPlayer{}
	if parseErr := ctx.ReadJSON(&listIdItemPlayer); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	_, err := coll.RemoveAll(bson.M{"_id": bson.M{"$in": listIdItemPlayer.Id}})
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, listIdItemPlayer, fmt.Sprint(err))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{"message": "success delete itemPlayer"})

}
