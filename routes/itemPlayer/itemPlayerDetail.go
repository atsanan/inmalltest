package itemplayer

import (

	// "github.com/globalsign/mgo"

	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// ItemPlayerDetail route.
func ItemPlayerDetail(ctx iris.Context) {
	ID := ctx.Params().Get("id")
	if validateErr := utils.ValidateMongoID("itemPlayerId", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)

	itemPlayer := bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(ID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
		{"$lookup": bson.M{
			"from":         config.DBCollection.CategoryItemCollection,
			"localField":   "item.itemCategoryId",
			"foreignField": "_id",
			"as":           "item.itemCategory",
		},
		},
	}

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.One(&itemPlayer)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not founds", fmt.Sprint(errCount))
		return
	}

	utils.ResponseSuccess(ctx, itemPlayer)

}
