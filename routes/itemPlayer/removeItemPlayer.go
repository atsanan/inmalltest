package itemplayer

import (

	// "github.com/globalsign/mgo"

	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// RemoveItemPlayer route.
func RemoveItemPlayer(ctx iris.Context) {
	ID := ctx.Params().Get("id")
	if validateErr := utils.ValidateMongoID("itemPlayerId", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	mongoErr := coll.Remove(bson.M{"_id": bson.ObjectIdHex(ID)})

	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to remove data", fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{"message": "success remove itemPlayer"})
}
