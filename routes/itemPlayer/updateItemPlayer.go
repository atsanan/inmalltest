package itemplayer

import (
	"fmt"
	"inmallgame/app/utils"
	"strconv"
	"time"

	"inmallgame/app/data-access"

	"inmallgame/app/models"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//UpdateItemPlayer
func UpdateItemPlayer(ctx iris.Context) {
	ID := ctx.Params().Get("id")
	if validateErr := utils.ValidateMongoID("itemPlayerId", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	itemPlayerQuery := models.ItemPlayer{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(ID)}).One(&itemPlayerQuery)

	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(findErr))
		return
	}

	IsCoupon := ctx.PostValue("isCoupon")
	CouponGiftID := ctx.PostValueDefault("couponGiftId", itemPlayerQuery.CouponGiftID)
	CouponHashKey := ctx.PostValueDefault("couponHashKey", itemPlayerQuery.CouponHashKey)
	CouponQRImage := ctx.PostValueDefault("couponQRImage", itemPlayerQuery.CouponQRImage)
	CouponPassword := ctx.PostValueDefault("couponPassword", itemPlayerQuery.CouponPassword)
	couponDatetime1, _ := ctx.PostValueBool("couponDatetime1")
	couponDatetime2, _ := ctx.PostValueBool("couponDatetime2")
	couponDatetime3, _ := ctx.PostValueBool("couponDatetime3")
	update := bson.M{
		"couponGiftId":   CouponGiftID,
		"couponHashKey":  CouponHashKey,
		"couponQRImage":  CouponQRImage,
		"couponPassword": CouponPassword,
	}
	// if IsCoupon != nil {
	// 	update["isCoupon"] = IsCoupon
	// }
	isCoupon, errBool := strconv.ParseBool(IsCoupon)
	if errBool == nil {
		update["isCoupon"] = isCoupon
	}

	if couponDatetime1 == true {
		update["couponDatetime1"] = time.Now()
	}

	if couponDatetime2 == true {
		update["couponDatetime2"] = time.Now()
	}

	if couponDatetime3 == true {
		update["couponDatetime3"] = time.Now()
	}
	DiamondCheckIn, errDiamond := ctx.PostValueInt("diamondCheckIn")

	if errDiamond == nil {
		update["diamondCheckIn"] = DiamondCheckIn
	}
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(ID)},
		bson.M{
			"$currentDate": bson.M{"lastModified": true},
			"$set":         update,
		})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	itemPlayer := bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(ID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
		{"$lookup": bson.M{
			"from":         config.DBCollection.CategoryItemCollection,
			"localField":   "item.itemCategoryId",
			"foreignField": "_id",
			"as":           "item.itemCategory",
		},
		},
	}

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.One(&itemPlayer)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not founds", fmt.Sprint(errCount))
		return
	}

	utils.ResponseSuccess(ctx, itemPlayer)
}
