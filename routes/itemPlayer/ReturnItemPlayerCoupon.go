package itemplayer

import (

	// "github.com/globalsign/mgo"
	"inmallgame/app/utils"
	//"math"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	"fmt"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"

	// "github.com/globalsign/mgo/bson"
	// "github.com/kataras/iris"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

type itemPlayerReturn struct {
	ID              bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	PlayerID        bson.ObjectId `json:"playerId" bson:"playerId"`
	CouponDatetime1 time.Time     `json:"couponDatetime1" bson:"couponDatetime1"`
	CreateAt        time.Time     `json:"createAt" bson:"createAt"`
}

// ReturnItemPlayerCoupon route.
func ReturnItemPlayerCoupon(ctx iris.Context) {
	// hours, err := ctx.URLParamInt("hours")
	// if err != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
	// 	return
	// }
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	settingColl := session.DB(config.DbName).C(config.DBCollection.SettingCollection)
	couponItemColl := session.DB(config.DbName).C(config.DBCollection.CouponItemCollection)
	playerColl := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	itemPlayer := []itemPlayerReturn{}
	baseQuery := []bson.M{}
	baseQuery = append(baseQuery, bson.M{"$match": bson.M{
		"couponDatetime1": bson.M{"$exists": false},
		"$or":[]bson.M{
		{
			"isPlayerBuy": bson.M{"$exists": false},
		},
		{
			"isPlayerBuy":false,
		},
		},
		
	},
	})
	pipeCount := coll.Pipe(baseQuery)
	errCount := pipeCount.All(&itemPlayer)
	if errCount != nil {

		return
	}

	setting := bson.M{}

	pipeSetting := settingColl.Pipe([]bson.M{})
	errSetting := pipeSetting.One(&setting)
	if errSetting != nil {

		return
	}

	couponTimeOutHour := setting["couponTimeOutHours"].(int)
	location, _ := time.LoadLocation("Asia/Bangkok")
	now := time.Now().In(location)

	opt := option.WithCredentialsFile("inmallgotchimons.json")
	app, errfire := firebase.NewApp(context.Background(), nil, opt)
	if errfire != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
	}

	context := context.Background()
	client, errMes := app.Messaging(context)
	if errMes != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
	}

	stringMap := map[string]string{
		"title": setting["couponTimeOutMessageTitle"].(string),
		"body":  setting["couponTimeOutMessageBody"].(string),
	}
	data := []bson.M{}
	for _, v := range itemPlayer {
		createAt := v.CreateAt.In(location)
		hs := now.Sub(createAt).Hours()
		//hs, _ = math.Modf(hs)
		//ms := mf * 60
		if hs > float64(couponTimeOutHour) {
			player := bson.M{}
			pipePlayer := playerColl.Pipe([]bson.M{
				{
					"$match": bson.M{
						"_id": v.PlayerID,
					},
				},
				{
					"$lookup": bson.M{
						"from":         config.DBCollection.UserCollection,
						"localField":   "userId",
						"foreignField": "_id",
						"as":           "user",
					},
				},
				{"$unwind": "$user"},
				{
					"$group": bson.M{
						"_id":                     "$_id",
						"pushMessagesAccessToken": bson.M{"$first": "$user.pushMessagesAccessToken"},
					},
				},
			})
			errSetting := pipePlayer.One(&player)
			coll.Remove(bson.M{"_id": v.ID})
			couponItemColl.Update(bson.M{
				"itemPlayerId": v.ID,
			},
				bson.M{
					"$unset": bson.M{"datetimeGetCoupon": true, "itemPlayerId": true},
				})

			if errSetting == nil {
				if player["pushMessagesAccessToken"] != nil {
					message := &messaging.Message{
						Notification: &messaging.Notification{
							Title: setting["couponTimeOutMessageTitle"].(string),
							Body:  setting["couponTimeOutMessageBody"].(string),
							//Data:  dataJson,
						},
						Data:  stringMap,
						Token: player["pushMessagesAccessToken"].(string),
						//Topic: "InMallGotchiMons",
						//Condition: condition,
						//Topic:topic,
						//Condition: condition,
					}

					response, _ := client.Send(context, message)
					data = append(data, bson.M{
						"_id":      v.ID,
						"response": response,
					})
				} else {
					data = append(data, bson.M{
						"_id":      v.ID,
						"response": "",
					})
				}
			}
		}
	}

	itemPlayerDateTime := []itemPlayerReturn{}
	baseQuery2 := []bson.M{}
	baseQuery2 = append(baseQuery2, bson.M{"$match": bson.M{
		"couponDatetime1": bson.M{"$exists": true},
		"couponDatetime2": bson.M{"$exists": false},
		"$or":[]bson.M{
		{
			"isPlayerBuy": bson.M{"$exists": false},
		},
		{
			"isPlayerBuy":false,
		},
		},
	},
	})
	pipeCountDateTime := coll.Pipe(baseQuery2)
	errCountDateTime := pipeCountDateTime.All(&itemPlayerDateTime)
	if errCountDateTime != nil {
		return
	}
	//couponCountSecMax := setting["couponCountSecMax"].(int)
	for _, v := range itemPlayerDateTime {
		CouponDatetime1 := v.CouponDatetime1.In(location) //.Add(time.Second * time.Duration(couponCountSecMax))
		hs := now.Sub(CouponDatetime1).Hours()
		//hs, _ = math.Modf(hs)
		// data = append(data, bson.M{
		// 	"_id":      v.ID,
		// 	"response": "",
		// 	"couponDatetime1":CouponDatetime1,
		// 	"hs":hs,
		// 	"couponTimeOutHour":float64(couponTimeOutHour),
		// })
		//ms := mf * 60
		if hs > float64(couponTimeOutHour) {
			player := bson.M{}
			pipePlayer := playerColl.Pipe([]bson.M{
				{
					"$match": bson.M{
						"_id": v.PlayerID,
					},
				},
				{
					"$lookup": bson.M{
						"from":         config.DBCollection.UserCollection,
						"localField":   "userId",
						"foreignField": "_id",
						"as":           "user",
					},
				},
				{"$unwind": "$user"},
				{
					"$group": bson.M{
						"_id":                     "$_id",
						"pushMessagesAccessToken": bson.M{"$first": "$user.pushMessagesAccessToken"},
					},
				},
			})
			errSetting := pipePlayer.One(&player)
			coll.Remove(bson.M{"_id": v.ID})
			couponItemColl.Update(bson.M{
				"itemPlayerId": v.ID,
			},
				bson.M{
					"$unset": bson.M{"datetimeGetCoupon": true, "itemPlayerId": true},
				})

			if errSetting == nil {
				if player["pushMessagesAccessToken"] != nil {
					message := &messaging.Message{
						Notification: &messaging.Notification{
							Title: setting["couponTimeOutMessageTitle"].(string),
							Body:  setting["couponTimeOutMessageBody"].(string),
							//Data:  dataJson,
						},
						Data:  stringMap,
						Token: player["pushMessagesAccessToken"].(string),
						//Topic: "InMallGotchiMons",
						//Condition: condition,
						//Topic:topic,
						//Condition: condition,
					}

					response, _ := client.Send(context, message)
					data = append(data, bson.M{
						"_id":      v.ID,
						"response": response,
					})
				} else {
					data = append(data, bson.M{
						"_id":      v.ID,
						"response": "",
					})
				}
			}
		}
	}

	utils.ResponseSuccess(ctx, bson.M{"itemPlayers": data, "now": now,})
}
