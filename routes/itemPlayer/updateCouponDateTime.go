package itemplayer

import (
	"fmt"
	"time"

	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//UpdateCouponDateTime1
func UpdateCouponDateTime1(ctx iris.Context) {
	ID := ctx.Params().Get("id")
	if err := utils.ValidateMongoID("itemPlayerId", ID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	UpdateCouponDateTime(ctx, ID, bson.M{
		"couponDatetime1": time.Now(),
	})
}

//UpdateCouponDateTime2
func UpdateCouponDateTime2(ctx iris.Context) {
	ID := ctx.Params().Get("id")
	if err := utils.ValidateMongoID("itemPlayerId", ID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	UpdateCouponDateTime(ctx, ID, bson.M{
		"couponDatetime2": time.Now(),
	})
}

func UpdateCouponDateTime(ctx iris.Context, itemPlayerID string, update interface{}) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	errUpdate := coll.Update(bson.M{"_id": bson.ObjectIdHex(itemPlayerID)}, bson.M{
		"$set":         update,
		"$currentDate": bson.M{"lastModified": true},
	})
	if errUpdate != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not Update", fmt.Sprint("update"))
		return
	}

	itemPlayerUpdate := bson.M{}

	errCount := coll.Find(bson.M{"_id": bson.ObjectIdHex(itemPlayerID)}).One(&itemPlayerUpdate)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}
	utils.ResponseSuccess(ctx, itemPlayerUpdate)

}
