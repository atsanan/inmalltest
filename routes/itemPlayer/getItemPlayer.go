package itemplayer

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"strconv"
	"strings"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

// GetItemPlayer find by Player .
func GetItemPlayer(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	fields := ctx.URLParamTrim("fields")
	IsCoupon := ctx.URLParam("isCoupon")
	itemCategoryID := ctx.URLParamTrim("itemCategoryId")
	split := strings.Split(fields, ",")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	if playerID != "" {
		if validateErr := utils.ValidateMongoID("playerID", playerID); validateErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
			return
		}
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)

	itemPlayer := []bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID),
			"isActive": true,
			//"isCoupon": true,
		}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemShopCollection,
				"localField":   "itemId",
				"foreignField": "itemId",
				"as":           "itemShop",
			},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "itemShop.shopId",
				"foreignField": "_id",
				"as":           "shop",
			},
		},
	}



	isCoupon, errBool := strconv.ParseBool(IsCoupon)
	if errBool == nil {
		objQuery = append(objQuery, bson.M{"$match": bson.M{
			"isCoupon": isCoupon,
		}})
	}

	for _, value := range split {
		if value == "itemCategoryId" {

			errItemCat := validation.Errors{
				"itemCategoryId": validation.Validate(itemCategoryID, is.MongoID),
			}.Filter()
			if errItemCat != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errItemCat))
				return
			}
			queryItemCategory := bson.M{}

			if itemCategoryID != "" {
				queryItemCategory["item.itemCategoryId"] = bson.ObjectIdHex(itemCategoryID)
			}

			objQuery = append(objQuery, bson.M{
				"$lookup": bson.M{
					"from":         config.DBCollection.CategoryItemCollection,
					"localField":   "item.itemCategoryId",
					"foreignField": "_id",
					"as":           "item.itemCategory",
				},
			}, bson.M{"$unwind": "$item.itemCategory"},
				bson.M{"$match": queryItemCategory})
		}
	}



	itemPlayerCount := bson.M{}
	baseQuery := append(objQuery, bson.M{"$count": "count"})
	
	
	pipeCount := coll.Pipe(baseQuery)
	errCount := pipeCount.One(&itemPlayerCount)

	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}



	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})

	pipe := coll.Pipe(objQuery)

	err := pipe.All(&itemPlayer)
	if err != nil {
		fmt.Println("Not found", err)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(err))
		return
	}


	count := itemPlayerCount["count"].(int)
	pages := utils.CalcPages(count, limit)
	
	// shopColl := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	// itemColl := session.DB(config.DbName).C(config.DBCollection.ItemCollection)

	
	
	// for k, v := range itemPlayer {
	// 	data :=[]bson.M{}
	// 	shopColl.Pipe([]bson.M{
		
	// 		{
	// 		"$lookup": bson.M{
	// 			"from":         config.DBCollection.ItemShopCollection,
	// 			"localField":  "_id",
	// 			"foreignField": "shopId",
	// 			"as":           "itemShop",
	// 		},
	// 		},
	// 		{"$unwind": "$itemShop"},
	// 		{
	// 			"$match":bson.M{"itemShop.itemId":v["itemId"]},
	// 		},
	// 		{
	// 		"$project": bson.M{
	// 			"itemShop":0,
	// 			"address":0,
	// 			"created_at":0,
	// 			"filenameLogo1":0,
	// 			"isActive":0,
	// 			"isPromotionAvailable":0,
	// 			"isSponser":0,
	// 			"lastModified":0,
	// 			"monstersCount":0,
	// 			"mapReach":0,
	// 			"mainTypes":0,
	// 			"mallFloorId":0,
	// 			"monsterRating":0,
	// 			"order":0,
	// 			"shopAssertId":0,
	// 			"shopCategoryId":0,
	// 			"shopDetail":0,
	// 			"shopTel":0,
	// 			"shopUrl":0,
	// 			"updated_at":0,
	// 			"workingTime":0,		
	// 		},
	// 	},
	// 	}).All(&data)

	// 	utils.ResponseSuccess(ctx, bson.M{
	// 	"data":data,
	// 	})
	// 	return
	// 	item:=bson.M{}
	// 	itemColl.Pipe([]bson.M{
	// 		{
	// 			"$match":bson.M{"_id":v["itemId"]},
	// 		},
	// 	}).One(&item)

	// 	for a, b := range data {
	// 		if item["shopSelect"]==b["_id"]{
	// 			data[a]["isSelect"]=true
	// 		}else{
	// 			data[a]["isSelect"]=false
	// 		}
	// 	}
		
	// 	// data =append(data,bson.M{"shop":v["shop"]})
	// 	itemPlayer[k]["shop"]=data
	//  }
	linkF := "/api/v1/itemPlayer?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := map[string]interface{}{
		"pages":      pages,
		"pageIndex":  page,
		"pageLimit":  limit,
		"itemPlayer": itemPlayer,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	utils.ResponseSuccess(ctx, result)
}
