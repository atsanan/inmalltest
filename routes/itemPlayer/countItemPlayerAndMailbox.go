package itemplayer

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	//"time"
)

// CountItemPlayerAndMailBox
func CountItemPlayerAndMailBox(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	itemCategoryID := ctx.URLParam("itemCategoryId")
	mallID := ctx.URLParamTrim("mallId")
	if validateErr := utils.ValidateMongoID("playerID", playerID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	itemPlayer := []bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID),
			"isActive": true,
			"isCoupon": true,
		}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
	}

	pipe := coll.Pipe(objQuery)
	err := pipe.All(&itemPlayer)
	countItemPlayerCoupon := 0
	if err == nil {
		countItemPlayerCoupon = len(itemPlayer)

	
	}

	collMailBox := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	queryMailBox := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID),
			"isActive": true,
		}},
	}
	mailBox := []bson.M{}
	pipeMailBox := collMailBox.Pipe(queryMailBox)
	errMailBox := pipeMailBox.All(&mailBox)
	countMailBox := 0
	if errMailBox == nil {
		countMailBox = len(mailBox)
	}
	privilegeDefaultsPlayerColl := session.DB(config.DbName).C(config.DBCollection.PrivilegeDefaultsPlayerCollection)
	privilegeShopsCollection := session.DB(config.DbName).C(config.DBCollection.PrivilegeShopsCollection)
	privileDefaultPlayer := []bson.M{}
	baseQuery := []bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
	}

	if mallID != "" {
		dataShop := []bson.M{}
		addQuery := []bson.M{
			bson.M{
				"$lookup": bson.M{
					"from":         config.DBCollection.ShopCollection,
					"localField":   "shopId",
					"foreignField": "_id",
					"as":           "shop",
				},
			},
			bson.M{
				"$unwind": "$shop",
			},
			bson.M{
				"$lookup": bson.M{
					"from":         config.DBCollection.MallFloorCollection,
					"localField":   "shop.mallFloorId",
					"foreignField": "_id",
					"as":           "mallFloor",
				},
			},
			bson.M{
				"$unwind": "$mallFloor",
			},
			bson.M{"$match": bson.M{
				"mallFloor.mallId": bson.ObjectIdHex(mallID),
			}},
		}
		if shopErr := privilegeShopsCollection.Pipe(addQuery).All(&dataShop); shopErr != nil {
		}

		oids := make([]bson.ObjectId, len(dataShop))
		if len(dataShop) > 0 {
			for i := range dataShop {
				oids[i] = dataShop[i]["privilegeDefaultId"].(bson.ObjectId)
			}
		}

		baseQuery = append(baseQuery,
			bson.M{"$match": bson.M{
				"privilegeDefaultId": bson.M{
					"$in": oids,
				},
			}})
	}

	pipePrivilegeDefaultsPlayer := privilegeDefaultsPlayerColl.Pipe(baseQuery)
	if mongoErr := pipePrivilegeDefaultsPlayer.All(&privileDefaultPlayer); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	collShop := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	countPrivilege := 0
	
	for _, v := range privileDefaultPlayer {
		ps := []bson.M{}
		privilegeShopsCollection.Find(bson.M{
			"privilegeDefaultId": v["privilegeDefaultId"],
			"isActive":           true,
		}).All(&ps)

		
		
		if len(ps) > 0 {
		

			countPrivilege += 1
		}

	}

	//newsColl := session.DB(config.DbName).C(config.DBCollection.NewsCollection)

	ChannelCollection := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)
	collItemShop := session.DB(config.DbName).C(config.DBCollection.ItemShopCollection)
	// objQueryNews := []bson.M{
	// 	{
	// 		"$match": bson.M{"newsActive": true},
	// 	},
	// 	{
	// 		"$lookup": bson.M{
	// 			"from":         config.DBCollection.NewsTypeCollection,
	// 			"localField":   "newsType",
	// 			"foreignField": "_id",
	// 			"as":           "newsType",
	// 		},
	// 	},
	// 	{"$unwind": "$newsType"},
	// 	{
	// 		"$match": bson.M{"newsType.newsTypeName.eng": "Promotion"},
	// 	},
	// }
	// news := []bson.M{}
	// pipeNews := newsColl.Pipe(objQueryNews)
	// findErr := pipeNews.All(&news)
	// countPromotion := 0
	// if findErr == nil {
	// 	for _, v := range news {

	// 		shop := bson.M{}
	// 		err := collShop.Find(bson.M{"_id": v["shopId"]}).One(&shop)
	// 		if err == nil {

	// 			channelCheck := []bson.M{}
	// 			channel := []bson.M{}
	// 			ChannelCollection.Find(bson.M{"shopId": shop["_id"]}).All(&channel)
	// 			if len(channel) > 0 {

	// 				for _, vc := range channel {
	// 					jc := []bson.M{}
	// 					joinChannelColl.Pipe([]bson.M{
	// 						{
	// 							"$match": bson.M{"channelId": vc["_id"]},
	// 						},
	// 						{
	// 							"$lookup": bson.M{
	// 								"from":         config.DBCollection.MonsterPlayerCollection,
	// 								"localField":   "mPlayerId",
	// 								"foreignField": "_id",
	// 								"as":           "mPlayerId",
	// 							},
	// 						},
	// 						{"$unwind": "$mPlayerId"},
	// 						{
	// 							"$match": bson.M{"mPlayerId.playerId": bson.ObjectIdHex(playerID)},
	// 						},
	// 					}).All(&jc)

	// 					if len(jc) > 0 {
	// 						channelCheck = append(channelCheck, bson.M{"check": 1})
	// 					}
	// 				}
	// 				if len(channelCheck) > 0 {
	// 					countPromotion += 1
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	itemShop := []bson.M{}

	objQueryItemShop := []bson.M{
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
	}

	if itemCategoryID !=""{
		objQueryItemShop = append(objQuery, bson.M{"$match": bson.M{
			"item.itemCategoryId": bson.ObjectIdHex(itemCategoryID),
		},
		})

	}
	pipeItemShop := collItemShop.Pipe(objQueryItemShop)
	findErrItemShop := pipeItemShop.All(&itemShop)
	if findErrItemShop != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErrItemShop))
		return
	}
    countFavorite:=0
	for _, v := range itemShop {
		
		shop := bson.M{}
		err := collShop.Find(bson.M{"_id": v["shopId"]}).One(&shop)
		if err == nil {

			channelCheck := []bson.M{}
			channel := []bson.M{}
			ChannelCollection.Find(bson.M{"shopId": shop["_id"]}).All(&channel)
			if len(channel) > 0 {

				for kc, vc := range channel {
					jc := []bson.M{}
					joinChannelColl.Pipe([]bson.M{
						{
							"$match": bson.M{"channelId": vc["_id"]},
						},
						{
							"$lookup": bson.M{
								"from":         config.DBCollection.MonsterPlayerCollection,
								"localField":   "mPlayerId",
								"foreignField": "_id",
								"as":           "mPlayerId",
							},
						},
						{"$unwind": "$mPlayerId"},
						{
							"$match": bson.M{"mPlayerId.playerId": bson.ObjectIdHex(playerID)},
						},
					}).All(&jc)
					channel[kc]["joinChannel"] = jc
					if len(jc) > 0 {
						channelCheck = append(channelCheck, bson.M{"check": 1})
					}
				}

				if len(channelCheck) > 0 {
					countFavorite+=1
				}
			}
		}
	}
	utils.ResponseSuccess(ctx,
		bson.M{
			"countItemPlayerCoupon": countItemPlayerCoupon,
			"countMailBox":    countMailBox,
			"countPrivilege":  countPrivilege,
			//"countPromotion":  countPromotion,
			"countFavorite":countFavorite,
		})
}
