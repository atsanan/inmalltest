package itemplayer

import (
	"fmt"

	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//getItemDataShop
func getItemDataShop(ctx iris.Context) {
	ID := ctx.Params().Get("id")
	if validateErr := utils.ValidateMongoID("itemPlayerId", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)

	itemPlayer := bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{"_id": bson.ObjectIdHex(ID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "item.shopId",
				"foreignField": "_id",
				"as":           "shop",
			},
		},
	}

	pipeCount := coll.Pipe(objQuery)
	errCount := pipeCount.One(&itemPlayer)
	if errCount != nil {

		utils.ResponseSuccess(ctx, bson.M{"shop": []bson.M{},})
		return
	}

	utils.ResponseSuccess(ctx, itemPlayer)
}
