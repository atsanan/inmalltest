package itemplayer

import (

	// "github.com/globalsign/mgo"

	"fmt"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddItemPlayer route.
func AddItemPlayer(ctx iris.Context) {
	PlayerID := ctx.PostValue("playerId")
	ItemID := ctx.PostValue("itemId")
	Count, _ := ctx.PostValueInt("count")
	IsCoupon, _ := ctx.PostValueBool("isCoupon")
	CouponGiftID := ctx.PostValue("couponGiftId")
	CouponHashKey := ctx.PostValue("couponHashKey")
	CouponQRImage := ctx.PostValue("couponQRImage")
	CouponPassword := ctx.PostValue("couponPassword")

	if err := utils.ValidateMongoID("playerId", PlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	if err := utils.ValidateMongoID("itemId", ItemID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	itemPlayer := models.ItemPlayer{
		PlayerID:       bson.ObjectIdHex(PlayerID),
		ItemID:         bson.ObjectIdHex(ItemID),
		Count:          Count,
		IsCoupon:       IsCoupon,
		CouponGiftID:   CouponGiftID,
		CouponHashKey:  CouponHashKey,
		CouponQRImage:  CouponQRImage,
		CouponPassword: CouponPassword,
	}

	// if parseErr := ctx.ReadJSON(&itemPlayer); parseErr != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
	// 	return
	// }
	if validateErr := itemPlayer.ValidateAll(); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Validate fail.", fmt.Sprint(validateErr))
		return
	}
	_id := bson.NewObjectId()
	update := bson.M{
		"_id":            _id,
		"playerId":       itemPlayer.PlayerID,
		"itemId":         itemPlayer.ItemID,
		"count":          itemPlayer.Count,
		"isCoupon":       itemPlayer.IsCoupon,
		"couponGiftId":   itemPlayer.CouponGiftID,
		"couponHashKey":  itemPlayer.CouponHashKey,
		"couponQRImage":  itemPlayer.CouponQRImage,
		"couponPassword": itemPlayer.CouponPassword,
		"isActive":       true,
		"createAt":       time.Now(),
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)

	findItem := bson.M{}

	if IsCoupon == true {
		mongoErr := coll.Insert(update)
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			return
		}
		objQuery := []bson.M{
			{"$match": bson.M{"_id": _id}},
			{
				"$lookup": bson.M{
					"from":         config.DBCollection.ItemCollection,
					"localField":   "itemId",
					"foreignField": "_id",
					"as":           "item",
				},
			},
			{"$unwind": "$item"},
		}
		pipe := coll.Pipe(objQuery)
		findErr := pipe.One(&findItem)
		if findErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
			return
		}

	} else {
		itemErr := coll.Update(
			bson.M{"itemId": itemPlayer.ItemID,
				"playerId": itemPlayer.PlayerID,
				"isCoupon": false,
			},
			bson.M{
				"$set": bson.M{
					"couponGiftId":   CouponGiftID,
					"couponHashKey":  CouponHashKey,
					"couponQRImage":  CouponQRImage,
					"couponPassword": CouponPassword,
					"isCoupon":       false,
				},
				"$inc":         bson.M{"count": 1},
				"$currentDate": bson.M{"lastModified": true},
			})

		if itemErr != nil {
			if mongoErr := coll.Insert(update); mongoErr != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
				return
			}

			pipe := coll.Pipe([]bson.M{
				{"$match": bson.M{"_id": _id}},
				{
					"$lookup": bson.M{
						"from":         config.DBCollection.ItemCollection,
						"localField":   "itemId",
						"foreignField": "_id",
						"as":           "item",
					},
				},
				{"$unwind": "$item"},
			})
			err := pipe.One(&findItem)
			if err != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to pipe data", fmt.Sprint(err))
				return
			}
		} else {
			pipe := coll.Pipe([]bson.M{
				{"$match": bson.M{"itemId": itemPlayer.ItemID,
					"playerId": itemPlayer.PlayerID}},
				{
					"$lookup": bson.M{
						"from":         config.DBCollection.ItemCollection,
						"localField":   "itemId",
						"foreignField": "_id",
						"as":           "item",
					},
				},
				{"$unwind": "$item"},
			})
			err := pipe.One(&findItem)
			if err != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to pipe data", fmt.Sprint(err))
				return
			}
		}

	}

	utils.ResponseSuccess(ctx, findItem)
}
