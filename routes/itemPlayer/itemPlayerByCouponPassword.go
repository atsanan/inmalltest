package itemplayer

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/kataras/iris"
)

// GetItemPlayerByCouponPassword find by Player .
func GetItemPlayerByCouponPassword(ctx iris.Context) {

	couponPassword := ctx.Params().Get("couponPassword")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	err := validation.Errors{
		"couponPassword": validation.Validate(couponPassword, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return

	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)

	objQuery := []bson.M{

		{"$match": bson.M{"couponPassword": couponPassword, "isActive": true}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.CategoryItemCollection,
				"localField":   "item.itemCategoryId",
				"foreignField": "_id",
				"as":           "item.itemCategory",
			},
		},
		{"$unwind": "$item.itemCategory"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PlayerCollection,
				"localField":   "playerId",
				"foreignField": "_id",
				"as":           "player",
			},
		},
		{"$unwind": "$player"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.UserCollection,
				"localField":   "player.userId",
				"foreignField": "_id",
				"as":           "user",
			},
		},
		{"$unwind": "$user"},
	}
	itemPlayer := []bson.M{}
	mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	if countErr != nil {
		utils.ResponseSuccess(ctx,
			bson.M{"itemPlayer": itemPlayer,
				"pageIndex": pageIndex,
				"pageLimit": pageLimit,
				"pages":     1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return
	}

	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(objQuery)
	findErr := pipe.All(&itemPlayer)
	if findErr != nil {
		utils.ResponseSuccess(ctx,
			bson.M{
				"itemPlayer": itemPlayer,
				"pageIndex":  pageIndex,
				"pageLimit":  pageLimit,
				"pages":      1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return
	}

	linkF := "/api/v1/itemPlayer/byCouponPassword?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["itemPlayer"] = itemPlayer
	utils.ResponseSuccess(ctx, result)
}
