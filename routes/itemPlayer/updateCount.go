package itemplayer

import (
	"fmt"

	"inmallgame/app/models"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//UpdateCountItemPlayer
func UpdateCountItemPlayer(ctx iris.Context) {
	itemPlayerID := ctx.PostValue("itemPlayerId")
	count, _ := ctx.PostValueInt("count")
	if err := utils.ValidateMongoID("ItemPlayerID", itemPlayerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)

	itemPlayer := models.ItemPlayer{}

	errCount := coll.Find(bson.M{"_id": bson.ObjectIdHex(itemPlayerID)}).One(&itemPlayer)
	if errCount != nil {
		fmt.Println("Not found", errCount)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
		return
	}

	result := itemPlayer.Count + count

	if result <= 0 {
		coll.Remove(bson.M{"_id": bson.ObjectIdHex(itemPlayerID)})

		utils.ResponseSuccess(ctx, bson.M{})
	} else {
		errUpdate := coll.Update(bson.M{"_id": bson.ObjectIdHex(itemPlayerID)}, bson.M{
			"$inc": bson.M{"count": count},
		})
		if errUpdate != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not Update", fmt.Sprint("update"))
			return
		}

		itemPlayerUpdate := models.ItemPlayer{}

		errCount := coll.Find(bson.M{"_id": bson.ObjectIdHex(itemPlayerID)}).One(&itemPlayerUpdate)
		if errCount != nil {
			fmt.Println("Not found", errCount)
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(errCount))
			return
		}
		utils.ResponseSuccess(ctx, itemPlayerUpdate)

	}

}
