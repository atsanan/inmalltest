package itemplayer

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, AddItemPlayer)
	routes.Post("/deleteList", auth.TokenMDW, DeleteItemPlayerList)
	routes.Post("/softDeleteList", auth.TokenMDW, SoftDeleteItemPlayerList)
	routes.Post("/updateCount", auth.TokenMDW, UpdateCountItemPlayer)
	routes.Post("/{id:string}", auth.TokenMDW, UpdateItemPlayer)
	routes.Get("/remove/{id:string}", auth.TokenMDW, RemoveItemPlayer)
	routes.Get("/softdelete/{id:string}", auth.TokenMDW, SoftDeleteItemPlayer)
	routes.Get("/couponDatetime1/{id:string}", auth.TokenMDW, UpdateCouponDateTime1)
	routes.Get("/couponDatetime2/{id:string}", auth.TokenMDW, UpdateCouponDateTime2)
	routes.Get("/info/{id:string}", auth.TokenMDW, ItemPlayerDetail)
	routes.Get("/byCouponPassword/{couponPassword:string}", GetItemPlayerByCouponPassword)
	routes.Get("/{playerId:string}", auth.TokenMDW, GetItemPlayer)
	routes.Get("/shop/{id:string}", auth.TokenMDW, getItemDataShop)
	routes.Get("/countItemPlayerAndMailBox/{playerId:string}", auth.TokenMDW, CountItemPlayerAndMailBox)
	routes.Get("/countItemPlayer/{itemId:string}/{playerId:string}", auth.TokenMDW, CountItemPlayer)
	routes.Get("/checkReturnItemPlayerCoupon/", auth.TokenMDW, ReturnItemPlayerCoupon)
}
