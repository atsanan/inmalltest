package itemplayer

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

//CountItemPlayer
func CountItemPlayer(ctx iris.Context) {
	playerID := ctx.Params().Get("playerId")
	itemID := ctx.Params().Get("itemId")
	err := validation.Errors{
		"playerId": validation.Validate(playerID, validation.Required, is.MongoID),
		"itemId":   validation.Validate(itemID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	itemPlayerCount := []bson.M{}
	baseQuery := []bson.M{}
	baseQuery = append(baseQuery, bson.M{"$match": bson.M{
		"playerId": bson.ObjectIdHex(playerID),
		"itemId":   bson.ObjectIdHex(itemID),
	}})
	baseQuery = append(baseQuery, bson.M{
		"$lookup": bson.M{
			"from":         config.DBCollection.ItemCollection,
			"localField":   "itemId",
			"foreignField": "_id",
			"as":           "item",
		},
	}, bson.M{"$unwind": "$item"})
	pipeCount := coll.Pipe(baseQuery)
	errCount := pipeCount.All(&itemPlayerCount)
	if errCount != nil {
		utils.ResponseSuccess(ctx, bson.M{"itemPlayers": itemPlayerCount})
		return
	}
	utils.ResponseSuccess(ctx, bson.M{"itemPlayers": itemPlayerCount})
}
