package shopasserts

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddShopAsserts for admin add data.
func AddShopAsserts(ctx iris.Context) {
	shopAsserts := models.ShopAsserts{}
	if parseErr := ctx.ReadJSON(&shopAsserts); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return

	}

	_id := bson.NewObjectId()
	update := bson.M{
		"_id": _id,
		//	"shopAssertName":    shopAsserts.ShopAssertName,
		"ShopAssertNameEng":    shopAsserts.ShopAssertNameEng,
		"ShopAssertNameThai":   shopAsserts.ShopAssertNameThai,
		"ShopAssertNameChi1":   shopAsserts.ShopAssertNameChi1,
		"ShopAssertNameChi2":   shopAsserts.ShopAssertNameChi2,
		"shopAssertDetailEng":  shopAsserts.ShopAssertDetailEng,
		"shopAssertDetailThai": shopAsserts.ShopAssertDetailThai,
		"shopAssertDetailChi1": shopAsserts.ShopAssertDetailChi1,
		"shopAssertDetailChi2": shopAsserts.ShopAssertDetailChi2,
		"shopAssertVersion":    shopAsserts.ShopAssertVersion,
		"shopCategoryId":       shopAsserts.ShopCategoryID,
		"fileNameLogo1":        shopAsserts.FileNameLogo1,
		"fileNameLogo2":        shopAsserts.FileNameLogo2,
		"createAt":             time.Now(),
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopAssertsCollection)

	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	shopAssertsData := bson.M{}
	if findErr := coll.Find(bson.M{"_id": _id}).One(&shopAssertsData); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, shopAssertsData)

}
