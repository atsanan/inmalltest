package shopasserts

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	//routes.Get("/detail/{id:string}", auth.TokenMDW, GetShopAssetDetail)
	routes.Post("/add", auth.TokenMDW, AddShopAsserts)
	routes.Post("/{id:string}", auth.TokenMDW, UpdateShopAsserts)
	routes.Get("/", auth.TokenMDW, GetShopAsserts)

}
