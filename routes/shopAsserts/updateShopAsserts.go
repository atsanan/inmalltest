package shopasserts

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"github.com/mitchellh/mapstructure"
)

func validateParams(newsID string) error {
	err := validation.Errors{
		"id": validation.Validate(newsID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		return err
	}
	return nil
}

//UpdateShopAsserts router
func UpdateShopAsserts(ctx iris.Context) {
	ShopAssertsID := ctx.Params().Get("id")

	if err := validateParams(ShopAssertsID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	update := map[string]interface{}{}
	if parseErr := ctx.ReadJSON(&update); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Update costumeSelectId fail.", fmt.Sprint(parseErr))
		return
	}
	shopAds := models.ShopAds{}
	decodeErr := mapstructure.Decode(update, &shopAds)
	if decodeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "decode error", fmt.Sprint(decodeErr))
		return
	}
	if update["shopCategoryId"] != "" {
		WideItemID, _ := update["shopCategoryId"].(string)
		if WideItemID != "" {
			update["shopCategoryId"] = bson.ObjectIdHex(WideItemID)
		}
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopAssertsCollection)
	mongoErr := coll.Update(bson.M{"_id": bson.ObjectIdHex(ShopAssertsID)}, bson.M{
		"$set":         update,
		"$currentDate": bson.M{"lastModified": true}})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	shopAssertsData := bson.M{}
	findErr := coll.Find(bson.M{"_id": bson.ObjectIdHex(ShopAssertsID)}).One(&shopAssertsData)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, shopAssertsData)
}
