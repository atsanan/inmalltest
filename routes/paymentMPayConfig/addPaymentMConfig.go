package paymentMPayConfig

import (
 	"fmt"
// 	// "log"
	"time"
// 	// "encoding/json"
	"strconv"

// 	// "github.com/globalsign/mgo"

// 	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	// validation "github.com/go-ozzo/ozzo-validation"
   // "github.com/go-ozzo/ozzo-validation/is"

	"github.com/kataras/iris"

// 	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
// 	//"inmallgame/app/models"
 	"inmallgame/app/utils"
// 	"strconv"
// 
)


func addPaymentMPayConfig(ctx iris.Context) {

	projectCode := ctx.PostValue("projectCode")
	command := ctx.PostValue("command")
	sid := ctx.PostValue("sid")
	redirectUrl := ctx.PostValue("redirectUrl")
	merchantId := ctx.PostValueIntDefault("merchantId",34462)
	orderId := ctx.PostValue("orderId")
	currency := ctx.PostValue("currency")
	purchaseAmt := ctx.PostValue("purchaseAmt")
	productDesc := ctx.PostValue("productDesc")
	secretKey := ctx.PostValue("secretKey")
	smsFlag := ctx.PostValue("smsFlag")
	smsMobile := ctx.PostValue("smsMobile")
	mobileNo := ctx.PostValue("mobileNo")
	tokenKey := ctx.PostValue("tokenKey")
	orderExpire:=ctx.PostValueIntDefault("orderExpire",720)
	creditCardType:=ctx.PostValue("creditCardType")
	channelRefId:=ctx.PostValue("channelRefId")
	paymentMethod:=ctx.PostValue("paymentMethod")


	// err := validation.Errors{
	// 	"projectCode":       validation.Validate(projectCode, validation.Required),
	// 	"command":       validation.Validate(command, validation.Required),
	// 	"sid":       validation.Validate(sid, validation.Required),
	// 	"merchantId":       validation.Validate(merchantId, validation.Required),
	// 	"merchantId":       validation.Validate(merchantId, validation.Required),
	// 	"merchantId":       validation.Validate(merchantId, validation.Required),
	// 	"merchantId":       validation.Validate(merchantId, validation.Required),
	// 	"merchantId":       validation.Validate(merchantId, validation.Required),
	// }.Filter()

	// if err != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
	// 	return
	// }

	insert :=bson.M{}
	if projectCode !=""{
		insert["projectCode"]=projectCode 
	}else{
		insert["projectCode"]="inmall" 
	}

	if command !=""{
		insert["command"]=command 
	}else{
		insert["command"]="RequestOrderTepsApi" 
	}

	if sid !=""{
		insert["sid"]=sid 
	}else{
		insert["sid"]="217c23474995387b4c98deed1733091d415059e57ca1b10835195f60a5678fc4" 
	}
	
	if redirectUrl !=""{
		insert["redirectUrl"]=redirectUrl 
	}else{
		insert["redirectUrl"]="https://www.google.co.th/" 
	}

	pur, _ := strconv.ParseFloat(purchaseAmt, 64)
	insert["purchaseAmt"]= pur
	//mer := strconv.FormatInt(merchantId, 16)
	//mer:= strconv.FormatInt(merchantId, 16)
	insert["merchantId"]=merchantId 

	insert["orderId"]=orderId 
	insert["currency"]=currency 
	insert["productDesc"]=productDesc 
	if paymentMethod !=""{
		insert["paymentMethod"]=paymentMethod 
	}else{
		insert["paymentMethod"]="4" 
	}

	if secretKey !=""{
		insert["secretKey"]=secretKey 
	}else{
		insert["secretKey"]="SaltTEPS" 
	}

	if smsFlag !=""{
		insert["smsFlag"]=smsFlag 
	}else{
		insert["smsFlag"]="y"
	}

	
	if smsMobile !=""{
		insert["smsMobile"]=smsMobile 
	}else{
		insert["smsMobile"]=nil
	}

	if mobileNo !=""{
		insert["mobileNo"]=mobileNo 
	}else{
		insert["mobileNo"]=nil
	}

	if tokenKey !=""{
		insert["tokenKey"]=tokenKey 
	}else{
		insert["tokenKey"]=nil
	}

	
	insert["orderExpire"]=orderExpire 
	

	
	if creditCardType !=""{
		insert["creditCardType"]=creditCardType 
	}else{
		insert["creditCardType"]=nil
	}

	if channelRefId !=""{
		insert["channelRefId"]=channelRefId 
	}else{
		insert["channelRefId"]=nil
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PaymentMPayConfigCollection)

	_id := bson.NewObjectId()
	insert["_id"]=_id
	insert["created_at"]=time.Now()
	mongoErr := coll.Insert(insert)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	payment := bson.M{}
	findErr := coll.Find(bson.M{"_id":_id}).One(&payment)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}
	utils.ResponseSuccess(ctx, payment)
}