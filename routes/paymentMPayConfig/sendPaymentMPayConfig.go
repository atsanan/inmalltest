package paymentMPayConfig

import (
	//"bytes"
	"fmt"
	// "io/ioutil"
	//"log"
	//"net/http"

	"inmallgame/app/data-access"
	"github.com/kataras/iris"
	"inmallgame/app/utils"
	"github.com/globalsign/mgo/bson"
	// // "crypto/sha256"
	// // "strconv"
	"encoding/xml"
	// "encoding/json"
	// "net/url"
)

const (
	clientSecret string = "9ca19353e1fb861f6d3aed8af6803c0b"
	accessToken  string = "d42cd44868d2fe953329677aa1c71e1d80f0f4d7"
	siteHost     string = "https://sandbox.mpay.co.th/mpay-sandbox-api/InterfaceService?projectCode=TEPS&command=RequestOrderTepsApi&sid=217c23474995387b4c98deed1733091d415059e57ca1b10835195f60a5678fc4&redirectUrl=&merchantId=34462&orderId=wewqe&purchaseAmt=213100&curแrency=THB&paymentMethod=&productDesc=&smsMobile=&mobileNo=&smsFlag=Y&tokenKey=&orderExpire=60&integrityStr=adc6d9450ec04cf874e84649e8c8c1b0793df624295092723244275a2a0a4bf8"
	userID       string = "460"
)

type Response struct {
	XMLName  xml.Name `xml:"response" json:"response"`
	Status   string   `xml:"status" json:"status"`
	RespCode string   `xml:"respCode" json:"respCode"`
	RespDesc string   `xml:"respDesc" json:"respDesc"`
	SaleId      string   `xml:"saleId" json:"saleId"`
	EndPointUrl string   `xml:"endPointUrl" json:"endPointUrl"`

} 


type DataFormat struct {
    ProductList []struct {
        Sku      string `xml:"sku" json:"sku"`
        Quantity int    `xml:"quantity" json:"quantity"`
    } `xml:"Product" json:"products"`
}

func sendPlayment(ctx iris.Context) {
	

	// orderId := ctx.PostValue("orderId")
	// currency := ctx.PostValue("currency")
	// purchaseAmt := ctx.PostValue("purchaseAmt")
	// productDesc := ctx.PostValue("productDesc")
	// smsMobile := ctx.PostValue("smsMobile")
	// mobileNo := ctx.PostValue("mobileNo")
	// tokenKey:=ctx.PostValue("tokenKey")
	// creditCardType:=ctx.PostValue("creditCardType")
	// channelRefId:=ctx.PostValue("channelRefId")
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PaymentMPayConfigCollection)

	payment := bson.M{}
	findErr := coll.Find(bson.M{}).One(&payment)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}
	
	// sid :=payment["sid"].(string)
	// merchantId :=strconv.Itoa(payment["merchantId"].(int)) 
	// secretKey :=payment["secretKey"].(string)
	// str:=sid +merchantId+orderId+purchaseAmt+secretKey
	// orderExpire :=strconv.Itoa(payment["orderExpire"].(int))
	// sha256:=sha256.Sum256([]byte(str))
	// integrityStr:=fmt.Sprintf("%x",sha256)
	// paymentMethod :=ctx.PostValue("paymentMethod") 
	
	// isProduction:=payment["isProduction"].(bool)
	// urlConCat:=""
	// if isProduction ==true{
	// 	urlConCat =payment["urlProduction"].(string)
	// }else{
	// 	urlConCat =payment["urlDev"].(string)
	// }
	// requestUrl:=urlConCat+"?projectCode="+payment["projectCode"].(string)+"&command="+payment["command"].(string)+"&sid="+payment["sid"].(string)+"&smsFlag="+payment["smsFlag"].(string)+"&redirectUrl="+payment["redirectUrl"].(string)+"&paymentMethod="+paymentMethod+"&orderId="+orderId+"&currency="+currency+"&purchaseAmt="+purchaseAmt+"&productDesc="+productDesc+"&smsMobile="+smsMobile+"&mobileNo="+mobileNo+"&tokenKey="+tokenKey+"&creditCardType="+creditCardType+"&integrityStr="+integrityStr+"&orderExpire="+orderExpire+"&merchantId="+merchantId
	
	// if channelRefId !=""{
	// 	requestUrl+="&channelRefId="+channelRefId
	// }
	
	// encodeRequesUrl := requestUrl
	// sendUrl, _ := url.QueryUnescape(encodeRequesUrl)

	// resp, err := http.Get(sendUrl)
	// if err != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(err))
	// 	return
	// }

	// body, err := ioutil.ReadAll(resp.Body)
	// if err != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(err))
	// 	return
	// }
	
	
    // data := &Response{}
    // err = xml.Unmarshal(body, data)
    // if nil != err {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
    //     return
    // }

    // result, err := json.Marshal(data)
    // if nil != err {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
    //     return
    // }

	// dataReturn :=bson.M{}
	// err = json.Unmarshal([]byte(string(result)), &dataReturn)

	// if err != nil {
	// 	fmt.Printf("error: %v", err)
	// 	return
	// }

	// encodedValue := data.EndPointUrl
	// decodedValue, er := url.QueryUnescape(encodedValue)
	// if er != nil {
	// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(er))
	// 	return
	// }

	// dataReturn["endPointUrl"]=decodedValue
	// dataReturn["sendUrl"]=sendUrl
	utils.ResponseSuccess(ctx,bson.M{"a":"a"})
}
