package miniGame

import (
	"github.com/kataras/iris/core/router"

	//"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Get("/list", MiniGameList)
}