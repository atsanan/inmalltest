package miniGame

import (
	"fmt"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"
	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"inmallgame/app/data-access"
	//"inmallgame/app/models"
	"inmallgame/app/utils"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// GetMallList will serve as router.
func MiniGameList(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	version := ctx.Params().Get("version")
	if pageLimit == 10 {
		pageLimit = 20
	}
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	miniGame := []bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallColl := session.DB(config.DbName).C(config.DBCollection.MiniGameCollection)
	if mongoErr := mallColl.Find(bson.M{"isActive": true}).Skip(offset).Limit(limit).All(&miniGame); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	count, _ := mallColl.Count()
	pages := utils.CalcPages(count, limit)

	linkF := "api/" + version + "/miniGame/list?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["miniGame"] = miniGame

	utils.ResponseSuccess(ctx, result)
}
