package setting

import (
	"fmt"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	"net"
	"inmallgame/app/data-access"
)

func getMacAddrs() (macAddrs []string) {
    netInterfaces, err := net.Interfaces()
    if err != nil {
        fmt.Printf("fail to get net interfaces: %v", err)
        return macAddrs
    }

    for _, netInterface := range netInterfaces {
        macAddr := netInterface.HardwareAddr.String()
        if len(macAddr) == 0 {
            continue
        }

        macAddrs = append(macAddrs, macAddr)
    }
    return macAddrs
}

func getSetting(ctx iris.Context) {
	Settting := bson.M{}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.SettingCollection)
	findErr := coll.Find(bson.M{}).One(&Settting)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	Settting["deviceId"]="abc";
	utils.ResponseSuccess(ctx, Settting)
}

func getSettingLanguages(ctx iris.Context) {
	setttingLanguage := bson.M{}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.SettingLanguagesCollection)
	findErr := coll.Find(bson.M{}).One(&setttingLanguage)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}
	utils.ResponseSuccess(ctx, setttingLanguage)
}
