package setting

import (
	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Get("/", getSetting)
	routes.Get("/languages", getSettingLanguages)

}
