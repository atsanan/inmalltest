package newstype

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddNewsType for admin add data.
func AddNewsType(ctx iris.Context) {
	newsType := models.NewsType{}
	if parseErr := ctx.ReadJSON(&newsType); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return

	}

	_id := bson.NewObjectId()
	update := bson.M{
		"_id":                _id,
		"newsTypeNameEng":    newsType.NewsTypeNameEng,
		"newsTypeNameThai":   newsType.NewsTypeNameThai,
		"newsTypeNameChi1":   newsType.NewsTypeNameChi1,
		"newsTypeNameChi2":   newsType.NewsTypeNameChi2,
		"newsTypeDetailEng":  newsType.NewsTypeDetailEng,
		"newsTypeDetailThai": newsType.NewsTypeDetailThai,
		"newsTypeDetailChi1": newsType.NewsTypeDetailChi1,
		"newsTypeDetailChi2": newsType.NewsTypeDetailChi2,
		"createAt":           time.Now(),
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.NewsTypeCollection)

	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	News := bson.M{}
	if findErr := coll.Find(bson.M{"_id": _id}).One(&News); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, News)

}
