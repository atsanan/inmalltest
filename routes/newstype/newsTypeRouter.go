package newstype

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Post("/add", auth.TokenMDW, AddNewsType)
	routes.Post("/{id:string}", auth.TokenMDW, UpdateNewsType)
	routes.Get("/", auth.TokenMDW, GetNewsType)

}
