package channel

import (
	"fmt"
	"strconv"
	// "time"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
	// "github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

func validateID(ctx iris.Context, id string, isAuto string, availableChannel string) (findAuto bool, findAvailableChannel bool, err error) {
	if err := validation.Validate(id, validation.Required, is.MongoID); err != nil {
		return true, true, err
	}

	findAuto, err2 := strconv.ParseBool(isAuto)
	if err2 != nil {
		findAuto = true
	}
	findAvailableChannel, err3 := strconv.ParseBool(availableChannel)
	if err3 != nil {
		findAvailableChannel = true
	}
	return findAuto, findAvailableChannel, nil
}

// GetChannelsByShop will serve as router.
func GetChannelsByShop(ctx iris.Context) {
	shopID := ctx.Params().Get("id")

	isAuto := ctx.URLParam("isAuto")
	availableChannel := ctx.URLParam("availableChannel")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	findAuto, _, err := validateID(ctx, shopID, isAuto, availableChannel)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)

	channels := []models.Channel{}
	query := bson.M{"shopId": bson.ObjectIdHex(shopID), "isAuto": findAuto}
	// if findAvailableChannel {
	// 	query["$where"] = "this.capacity < this.maxCapacity"
	// }
	if mongoErr := coll.Find(query).Skip(offset).Limit(limit).All(&channels); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not found", fmt.Sprint(mongoErr))
		return
	}

	count := len(channels)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/channel/byShop/%s", shopID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["shops"] = channels

	utils.ResponseSuccess(ctx, result)
}
