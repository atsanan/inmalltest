package channel

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// For authorize user.
	routes.Post("/create", auth.TokenMDW, CreateChannel)
	routes.Post("/join", auth.TokenMDW, JoinChannel)
	routes.Post("/monsterLeave", auth.TokenMDW, LeaveChannel)
	routes.Get("/byShop/{id: string}", auth.TokenMDW, GetChannelsByShop)
	routes.Get("/byFloor/{id: string}", auth.TokenMDW, GetChannelsByFloor)
	routes.Get("/byMall/{id: string}", auth.TokenMDW, GetChannelsByMall)
	routes.Get("/byPlayer/{id: string}", auth.TokenMDW, GetChannelsByPlayer)
	routes.Get("/byChannel/{id: string}/{playerId: string}", auth.TokenMDW, GetPlayersByChannel)
	routes.Get("/byChannelNew/{id: string}/{playerId: string}", auth.TokenMDW, GetChannelByPlayernew)
}
