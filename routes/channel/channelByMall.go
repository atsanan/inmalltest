package channel

import (
	"fmt"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/mitchellh/mapstructure"
	"github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

//ByMallResult data
type ByMallResult struct {
	MallFloorName      string        `json:"mallFloorName" bson:"mallFloorName"`
	MallFloorDetail    string        `json:"mallFloorDetail" bson:"mallFloorDetail"`
	MapIndoorFloorKey  string        `json:"mapIndoorFloorKey" bson:"mapIndoorFloorKey"`
	MapIndoorFloorData string        `json:"mapIndoorFloorData" bson:"mapIndoorFloorData"`
	MallID             bson.ObjectId `json:"mallId" bson:"mallId"`
	CreateAt           time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified       time.Time     `json:"lastModified" bson:"lastModified,omitempty"`

	Channel models.Channel `json:"channel" bson:"channel"`
	Shop    models.Shop    `json:"shop" bson:"shop"`
}

// GetChannelsByMall will serve as router.
func GetChannelsByMall(ctx iris.Context) {
	mallID := ctx.Params().Get("id")

	isAuto := ctx.URLParam("isAuto")
	availableChannel := ctx.URLParam("availableChannel")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	findAuto, findAvailableChannel, err := validateID(ctx, mallID, isAuto, availableChannel)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)

	pipe := mallFloorColl.Pipe([]bson.M{
		{"$match": bson.M{"mallId": bson.ObjectIdHex(mallID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "_id",
				"foreignField": "mallFloorId",
				"as":           "shop",
			},
		},
		{"$unwind": "$shop"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ChannelCollection,
				"localField":   "shop._id",
				"foreignField": "shopId",
				"as":           "channel",
			},
		},
		{"$unwind": "$channel"},
		{"$match": bson.M{"channel.isAuto": findAuto}},
		{"$skip": offset},
		{"$limit": limit},
	})
	mallFloors := []ByMallResult{}
	if pipeErr := pipe.All(&mallFloors); pipeErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(pipeErr))
		return
	}

	mcount, _ := countByMallID(ctx, mallID, findAuto)
	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/channel/byMall/%s", mallID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)

	if findAvailableChannel {
		datas, _ := gubrak.Filter(mallFloors, func(each ByMallResult) bool {
			return each.Channel.Capacity < each.Channel.MaxCapacity
		})

		result["mallFloors"] = datas
		utils.ResponseSuccess(ctx, result)
		return
	}

	result["mallFloors"] = mallFloors
	utils.ResponseSuccess(ctx, result)
}

func countByMallID(ctx iris.Context, mallID string, isAuto bool) (bson.M, error) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallFloorColl := session.DB(config.DbName).C(config.DBCollection.MallFloorCollection)

	pipe := mallFloorColl.Pipe([]bson.M{
		{"$match": bson.M{"mallId": bson.ObjectIdHex(mallID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "_id",
				"foreignField": "mallFloorId",
				"as":           "shop",
			},
		},
		{"$unwind": "$shop"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ChannelCollection,
				"localField":   "shop._id",
				"foreignField": "shopId",
				"as":           "channel",
			},
		},
		{"$unwind": "$channel"},
		{"$match": bson.M{"channel.isAuto": isAuto}},
		{"$count": "count"},
	})

	results := bson.M{}
	err := pipe.One(&results)
	if err != nil {
		return nil, err
	}

	return results, nil
}
