package channel

import (
	"fmt"
	"time"

	// "strconv"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/mitchellh/mapstructure"
	"github.com/novalagung/gubrak"
	// "github.com/tidwall/gjson"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

//ByFloorResult data
type ByFloorResult struct {
	ID             bson.ObjectId  `json:"id" bson:"_id,omitempty" `
	ShopNameEng    string         `json:"shopNameEng" bson:"shopNameEng"`
	ShopNameThai   string         `json:"shopNameThai" bson:"shopNameThai"`
	ShopNameChi1   string         `json:"shopNameChi1" bson:"shopNameChi1"`
	ShopNameChi2   string         `json:"shopNameChi2" bson:"shopNameChi2"`
	ShopDetailEng  string         `json:"shopDetail" bson:"shopDetailEng"`
	ShopDetailThai string         `json:"shopDetailThai" bson:"shopDetailThai"`
	ShopDetailChi1 string         `json:"shopDetailChi1" bson:"shopDetailChi1"`
	ShopDetailChi2 string         `json:"shopDetailChi2" bson:"shopDetailChi2"`
	ShopTel        string         `json:"shopTel" bson:"shopTel"`
	ShopURL        string         `json:"shopUrl" bson:"shopUrl"`
	ShopCategoryID string         `json:"shopCategoryId" bson:"shopCategoryId"`
	ShopCode       string         `json:"shopCode" bson:"shopCode"`
	ShopFloor      int            `json:"shopFloor" bson:"shopFloor"`
	Location       models.GeoJSON `json:"mapReach" bson:"mapReach"`
	MapReach       bson.ObjectId  `json:"location" bson:"location"`
	MallFloorID    bson.ObjectId  `json:"mallFloorId" bson:"mallFloorId"`
	ShopAssetID    bson.ObjectId  `json:"shopAssetId" bson:"shopAssetId"`
	FilenameLogo1  string         `json:"filenameLogo1" bson:"filenameLogo1"`
	FilenameLogo2  string         `json:"filenameLogo2" bson:"filenameLogo2"`
	ShopAdsID      []string       `json:"shopAdsId" bson:"shopAdsId"`
	IsActive       bool           `json:"isActive" bson:"isActive"`
	ShopModelID     bson.ObjectId         `json:"shopModelId" bson:"shopModelId"`
	// MapLat      float32       `json:"mapLat" bson:"mapLat"`
	// MapLong     float32       `json:"mapLong" bson:"mapLong"`
	//ShopLogo string `json:"shopLogo" bson:"shopLogo"`
	// ShopID      int           `json:"shopId"`
	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`

	Channel models.Channel `json:"channel" bson:"channel"`
}

// GetChannelsByFloor will serve as router.
func GetChannelsByFloor(ctx iris.Context) {
	floorID := ctx.Params().Get("id")

	isAuto := ctx.URLParam("isAuto")
	availableChannel := ctx.URLParam("availableChannel")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	findAuto, findAvailableChannel, err := validateID(ctx, floorID, isAuto, availableChannel)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	shopColl := session.DB(config.DbName).C(config.DBCollection.ShopCollection)

	pipe := shopColl.Pipe([]bson.M{
		{"$match": bson.M{"mallFloorId": bson.ObjectIdHex(floorID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ChannelCollection,
				"localField":   "_id",
				"foreignField": "shopId",
				"as":           "channel",
			},
		},
		{"$unwind": "$channel"},
		// {"$match": bson.M{"channels": bson.M{"$not": bson.M{"$size": 0}}}},
		{"$match": bson.M{"channel.isAuto": findAuto}},
		{"$skip": offset},
		{"$limit": limit},
	})

	shops := []ByFloorResult{}
	if err := pipe.All(&shops); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	mcount, err := countAllItems(ctx, floorID, findAuto)
	count := 0

	if err != nil {
		count = 0
	} else {
		count = mcount["count"].(int)

	}
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/channel/byFloor/%s", floorID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)

	if findAvailableChannel {
		datas, _ := gubrak.Filter(shops, func(each ByFloorResult) bool {
			return each.Channel.Capacity < each.Channel.MaxCapacity
		})

		result["shops"] = datas
		utils.ResponseSuccess(ctx, result)
		return
	}

	result["shops"] = shops
	utils.ResponseSuccess(ctx, result)
}

func countAllItems(ctx iris.Context, floorID string, isAuto bool) (bson.M, error) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	shopColl := session.DB(config.DbName).C(config.DBCollection.ShopCollection)

	pipe := shopColl.Pipe([]bson.M{
		{"$match": bson.M{"mallFloorId": bson.ObjectIdHex(floorID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ChannelCollection,
				"localField":   "_id",
				"foreignField": "shopId",
				"as":           "channel",
			},
		},
		{"$unwind": "$channel"},
		// {"$match": bson.M{"channels": bson.M{"$not": bson.M{"$size": 0}}}},
		{"$match": bson.M{"channel.isAuto": isAuto}},
		{"$count": "count"},
	})

	shops := bson.M{}
	err := pipe.One(&shops)
	if err != nil {
		return nil, err
	}

	return shops, nil
}
