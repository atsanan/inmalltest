package channel

import (
	"fmt"
	// "strconv"
	// "time"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	// "github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
)

// GetChannelsByPlayer will serve as router.
func GetChannelsByPlayer(ctx iris.Context) {
	playerID := ctx.Params().Get("id")

	// isAuto := ctx.URLParam("isAuto")
	// availableChannel := ctx.URLParam("availableChannel")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	if err := validation.Validate(playerID, validation.Required, is.MongoID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)

	pipe := joinChannelColl.Pipe([]bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ChannelCollection,
				"localField":   "channelId",
				"foreignField": "_id",
				"as":           "channel",
			},
		},
		{"$unwind": "$channel"},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ShopCollection,
				"localField":   "channel.shopId",
				"foreignField": "_id",
				"as":           "shop",
			},
		},
		{"$unwind": "$shop"},
		{"$skip": offset},
		{"$limit": limit},
	})

	joinChannels := []bson.M{}
	if err := pipe.All(&joinChannels); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	mcount, _ := countByPlayer(ctx, playerID)
	count := mcount
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/channel/byPlayer/%s", playerID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["joinChannels"] = joinChannels

	utils.ResponseSuccess(ctx, result)
}

func countByPlayer(ctx iris.Context, playerID string) (int, error) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)

	pipe := joinChannelColl.Pipe([]bson.M{
		{"$match": bson.M{"playerId": bson.ObjectIdHex(playerID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ChannelCollection,
				"localField":   "channelId",
				"foreignField": "_id",
				"as":           "channel",
			},
		},
		{"$unwind": "$channel"},
		{"$count": "count"},
	})

	joinChannels := []bson.M{}
	err := pipe.All(&joinChannels)
	if err != nil {
		return 0, err
	}

	return len(joinChannels), nil
}
