package channel

import (
	"fmt"
	"time"

	"github.com/go-ozzo/ozzo-validation/is"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"

	// "github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
	// "github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

func validateJoinChannel(ctx iris.Context, mPlayerID string, channelID string) error {
	err := validation.Errors{
		"mPlayerId": validation.Validate(mPlayerID, validation.Required, is.MongoID),
		"channelId": validation.Validate(channelID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		return err
	}
	return nil
}

// JoinChannel serve as router. To create joinChannel object that increase or decrease by channel capacity.
func JoinChannel(ctx iris.Context) {
	mPlayerID := ctx.PostValue("mPlayerId")
	channelID := ctx.PostValue("channelId")

	if err := validateJoinChannel(ctx, mPlayerID, channelID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	myClaim, _ := utils.TokenParser(ctx)

	available, err := availableChannel(ctx, channelID)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Find available channel error", fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	playerColl := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	player := models.Player{}

	findErr := playerColl.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).One(&player)
	if findErr != nil {
		fmt.Println(findErr)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(findErr))
		return
	}

	if available {
		// ok, we can join channel.
		jChannel, createErr := createJoinChannel(ctx, channelID, mPlayerID, player.ID)
		if createErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(createErr))
			return
		}

		info := map[string]interface{}{
			"isJoin":      true,
			"joinChannel": jChannel,
		}

		utils.ResponseSuccess(ctx, info)
	} else {
		info := map[string]interface{}{
			"channelId": channelID,
			"mPlayerId": mPlayerID,
			"playerId":  myClaim.ID,
			"isJoin":    false,
		}
		utils.ResponseFailure(ctx, iris.StatusBadRequest, info, "Channel capacity not yet available")
		return
	}
}

func createJoinChannel(ctx iris.Context, channelID string, mPlayerID string, playerID bson.ObjectId) (*models.JoinChannel, error) {
	joinChannel := models.JoinChannel{}
	joinChannel.ID = bson.NewObjectId()
	joinChannel.ChannelID = bson.ObjectIdHex(channelID)
	joinChannel.MPlayerID = bson.ObjectIdHex(mPlayerID)
	joinChannel.PlayerID = playerID
	joinChannel.CreateAt = time.Now()
	joinChannel.JoinDateTime = time.Now()

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	channelColl := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)

	if err := joinChannelColl.Insert(joinChannel); err != nil {
		return nil, err
	}

	// Increase capacity in channel model.
	channelColl.Update(bson.M{"_id": bson.ObjectIdHex(channelID)}, bson.M{"$inc": bson.M{"capacity": 1}})

	jChannel := models.JoinChannel{}
	if err := joinChannelColl.Find(bson.M{"_id": joinChannel.ID}).One(&jChannel); err != nil {
		return nil, err
	}

	return &jChannel, nil
}

func availableChannel(ctx iris.Context, channelID string) (bool, error) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	channelColl := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)

	result := models.Channel{}
	if mongoErr := channelColl.Find(bson.M{"_id": bson.ObjectIdHex(channelID)}).One(&result); mongoErr != nil {
		println(1)
		return false, mongoErr
	}

	count, countErr := joinChannelColl.Find(bson.M{"channelId": bson.ObjectIdHex(channelID)}).Count()
	if countErr != nil {
		println(2)
		return false, countErr
	}

	if count != result.Capacity {
		fmt.Println("WTF", "something went wrong.")
	}
	if count < result.MaxCapacity {
		return true, nil
	}

	return false, nil
}
