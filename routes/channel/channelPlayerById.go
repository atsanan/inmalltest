package channel

import (

	// "strconv"
	// "time"

	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/mitchellh/mapstructure"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

// GetChannelsByPlayer will serve as router.
func GetPlayersByChannel(ctx iris.Context) {

	channelID := ctx.Params().Get("id")
	playerID := ctx.Params().Get("playerId")
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit := ctx.URLParamIntDefault("limit",50)
	err := validation.Errors{
		"playerId":  validation.Validate(playerID, validation.Required, is.MongoID),
		"channelId": validation.Validate(channelID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.MonsterPlayerCollection)
	objQuery := []bson.M{
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.JoinChannelCollection,
				"localField":   "_id",
				"foreignField": "mPlayerId",
				"as":           "joinChannels",
			},
		},
		{"$unwind": "$joinChannels"},
		{"$match": bson.M{"joinChannels.channelId": bson.ObjectIdHex(channelID)}},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.MonstersDefaultCollection,
				"localField":   "mDefaultId",
				"foreignField": "_id",
				"as":           "monsterDefault",
			},
		},
		{"$unwind": "$monsterDefault"},
		{"$skip": offset},
		{"$limit": limit},
	}
	pipe := coll.Pipe(objQuery)

	resp := []bson.M{}
	mcount, countErr := utils.CalcPipeCount(ctx, objQuery, coll)
	if countErr != nil {
		utils.ResponseSuccess(ctx,
			bson.M{
				"monsters": resp,
				"pageIndex": pageIndex,
				"pageLimit": pageLimit,
				"pages":     1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return
	}

	objQuery = append(objQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})

	if err := pipe.All(&resp); err != nil {
		utils.ResponseSuccess(ctx,
			bson.M{
				"monsters": resp,
				"pageIndex": pageIndex,
				"pageLimit": pageLimit,
				"pages":     1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return	
	}

	//monsterRsults := []bson.M{}
	for k, v := range resp {
		isPlayer := true
		if v["playerId"] == bson.ObjectIdHex(playerID) {
			isPlayer = true
		} else {
			isPlayer = false
		}

		resp[k]["isPlayer"] = isPlayer

		//monsterRsults = append(monsterRsults,
		// bson.M{
		// 	"id":                       v["_id"],
		// 	"joinChannels":             v["joinChannels"],
		// 	"mDefaultId":               v["mDefaultId"],
		// 	"mPlayerFoodDatetime":      v["mPlayerFoodDatetime"],
		// 	"mPlayerFoodId":            v["mPlayerFoodId"],
		// 	"mPlayerHabit":             v["mPlayerHabit"],
		// 	"mPlayerHappinessDatetime": v["mPlayerHappinessDatetime"],
		// 	"mPlayerHappyId":           v["mPlayerHappyId"],
		// 	"mPlayerHealthDatetime":    v["mPlayerHealthDatetime"],
		// 	"mPlayerHealthId":          v["mPlayerHealthId"],
		// 	"mPlayerLastDateTime":      v["mPlayerLastDateTime"],
		// 	"mPlayerMessage":           v["mPlayerMessage"],
		// 	"mPlayerName":              v["mPlayerName"],
		// 	"mPlayerNewDateTime":       v["mPlayerNewDateTime"],
		// 	"mPlayerStatusFood":        v["mPlayerStatusFood"],
		// 	"mPlayerStatusHappiness":   v["mPlayerStatusHappiness"],
		// 	"mPlayerStatusHealth":      v["mPlayerStatusHealth"],
		// 	"playerId":                 v["playerId"],
		// 	"mPlayerStatusQuest1":      v["mPlayerStatusQuest1"],
		// 	"mPlayerQuest1Datetime":    v["mPlayerQuest1Datetime"],
		// 	"mPlayerStatusQuest2":      v["mPlayerStatusQuest2"],
		// 	"mPlayerQuest2Datetime":    v["mPlayerQuest2Datetime"],
		// 	"mPlayerStatusQuest3":      v["mPlayerStatusQuest3"],
		// 	"mPlayerQuest3Datetime":    v["mPlayerQuest3Datetime"],
		// 	"mPlayerStatusExp":         v["mPlayerStatusExp"],
		// 	"mPlayerExpDatetime":       v["mPlayerExpDatetime"],
		// 	"isPlayer":                 isPlayer,
		// 	"monsterDefault":           v["monsterDefault"],
		// })
	}

	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/channel/byChannel/%s/%s", channelID, playerID)
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["monsters"] = resp

	utils.ResponseSuccess(ctx, result)

}
