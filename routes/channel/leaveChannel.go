package channel

import (
	"fmt"

	"github.com/go-ozzo/ozzo-validation/is"
	// "time"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
	// "github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
)

func validateLeaveChannel(ctx iris.Context, mPlayerID string, channelID string) error {
	err := validation.Errors{
		"mPlayerId": validation.Validate(mPlayerID, validation.Required, is.MongoID),
		"channelId": validation.Validate(channelID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		return err
	}

	return nil
}

// LeaveChannel serve as router. To remove joinChannel object when monster leave.
func LeaveChannel(ctx iris.Context) {
	mPlayerID := ctx.PostValue("mPlayerId")
	channelID := ctx.PostValue("channelId")

	if err := validateLeaveChannel(ctx, mPlayerID, channelID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	// myClaim, _ := utils.TokenParser(ctx)
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	channelColl := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)
	joinChannelColl := session.DB(config.DbName).C(config.DBCollection.JoinChannelCollection)

	if mongoErr := joinChannelColl.Remove(bson.M{
		"channelId": bson.ObjectIdHex(channelID),
		"mPlayerId": bson.ObjectIdHex(mPlayerID),
	}); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}

	// Decrease capacity in channel model.
	channelColl.Update(bson.M{"_id": bson.ObjectIdHex(channelID)}, bson.M{"$inc": bson.M{"capacity": -1}})

	count, countErr := joinChannelColl.Find(bson.M{"channelId": bson.ObjectIdHex(channelID)}).Count()
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
		return
	}

	if count == 0 {
		// ok, we will delete channel.
		if err := channelColl.Remove(bson.M{"_id": bson.ObjectIdHex(channelID)}); err != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
			return
		}

		info := map[string]interface{}{
			"isMonsterLeave":  true,
			"isRemoveChannel": true,
		}
		utils.ResponseSuccess(ctx, info)
	} else {
		info := map[string]interface{}{
			"isMonsterLeave":  true,
			"isRemoveChannel": false,
		}
		utils.ResponseSuccess(ctx, info)
	}
}
