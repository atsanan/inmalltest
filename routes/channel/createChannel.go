package channel

import (
	"fmt"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	// "github.com/mitchellh/mapstructure"

	"inmallgame/app/data-access"
	"inmallgame/app/models"
	"inmallgame/app/utils"
)

func validateCreateChannel(ctx iris.Context) (map[string]interface{}, error) {
	mchannel := map[string]interface{}{}
	if err := ctx.ReadJSON(&mchannel); err != nil {
		return nil, err
	}

	joinNow := mchannel["joinNow"]
	mPlayerID := mchannel["mPlayerId"]
	// channel := mchannel["channel"]

	err := validation.Errors{
		"joinNow":   validation.Validate(joinNow, validation.NotNil),
		"mPlayerId": validation.Validate(mPlayerID, validation.Required),
	}.Filter()
	if err != nil {
		return nil, err
	}

	join, ok := joinNow.(bool)
	if !ok {
		return nil, fmt.Errorf("%s", "joinNow must be boolean")
	}
	if join {
		if err := validation.Validate(mPlayerID, validation.Required, is.MongoID); err != nil {
			return nil, fmt.Errorf("mPlayerID %s", err)
		}
	}

	return mchannel, nil
}

// CreateChannel serve as router.
func CreateChannel(ctx iris.Context) {
	mchannel, err := validateCreateChannel(ctx)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	myClaim, _ := utils.TokenParser(ctx)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	playerColl := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	player := models.Player{}

	findErr := playerColl.Find(bson.M{"userId": bson.ObjectIdHex(myClaim.ID)}).One(&player)
	if findErr != nil {
		fmt.Println(findErr)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(findErr))
		return
	}

	coll := session.DB(config.DbName).C(config.DBCollection.ChannelCollection)

	channel := mchannel["channel"].(map[string]interface{})
	result := models.Channel{}
	result.ID = bson.NewObjectId()
	result.ChannelName = channel["channelName"].(string)
	result.ChannelPassword = channel["channelPassword"].(string)
	isAuto, ok := channel["isAuto"].(bool)
	if ok {
		result.IsAuto = isAuto
	} else {
		result.IsAuto = true
	}
	shopID, ok := channel["shopId"].(string)
	if ok {
		result.ShopID = bson.ObjectIdHex(shopID)
	}
	if result.MaxCapacity == 0 {
		result.MaxCapacity = 25
	}
	result.CreateAt = time.Now()
	if err := result.ValidateAll(); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	if mongoErr := coll.Insert(result); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Insert Fail", fmt.Sprint(mongoErr))
		return
	}
	createChannel := models.Channel{}
	if mongoErr := coll.Find(bson.M{"_id": result.ID}).One(&createChannel); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}

	joinNow := mchannel["joinNow"]
	mPlayerID := mchannel["mPlayerId"]
	join, _ := joinNow.(bool)
	mPlayer, _ := mPlayerID.(string)
	if join {
		jChannel, createErr := createJoinChannel(ctx, createChannel.ID.Hex(), mPlayer, player.ID)
		if createErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(createErr))
			return
		}

		info := map[string]interface{}{
			"isJoin":      true,
			"joinChannel": jChannel,
			"channel":     createChannel,
		}
		utils.ResponseSuccess(ctx, info)
	} else {
		utils.ResponseSuccess(ctx, createChannel)
	}
}
