package shopads

import (
	"fmt"
	"time"

	// "github.com/globalsign/mgo"
	"inmallgame/app/models"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
	// "github.com/novalagung/gubrak"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"
)

// AddShopAds for admin add data.
func AddShopAds(ctx iris.Context) {
	shopAds := models.ShopAds{}
	if parseErr := ctx.ReadJSON(&shopAds); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return

	}

	_id := bson.NewObjectId()
	update := bson.M{
		"_id":               _id,
		"shopAdsNameEng":    shopAds.ShopAdsNameEng,
		"shopAdsNameThai":   shopAds.ShopAdsNameThai,
		"shopAdsNameChi1":   shopAds.ShopAdsNameChi1,
		"shopAdsNameChi2":   shopAds.ShopAdsNameChi2,
		"shopAdsImage":      shopAds.ShopAdsImage,
		"shopAdsImageLists": shopAds.ShopAdsImageLists,
		"createAt":          time.Now(),
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ShopAdsCollection)

	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	shopShopModelsData := bson.M{}
	if findErr := coll.Find(bson.M{"_id": _id}).One(&shopShopModelsData); findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, shopShopModelsData)

}
