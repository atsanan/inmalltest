package shopads

import (
	"github.com/kataras/iris/core/router"

	"inmallgame/routes/auth"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/add", auth.TokenMDW, AddShopAds)
	routes.Post("/{id:string}", auth.TokenMDW, UpdateShopAds)
	routes.Get("/", auth.TokenMDW, GetShopAds)
}
