package shopads

import (
	"fmt"
	// "log"
	// "time"
	// "encoding/json"
	// "strconv"

	// "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
)

// GetShopAds will serve as router.
func GetShopAds(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	coll := session.DB(config.DbName).C(config.DBCollection.ShopAdsCollection)

	baseQuery := []bson.M{}
	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(offsetQuery)

	shopAds := []bson.M{}
	if mongoErr := pipe.All(&shopAds); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	mcount, countErr := utils.CalcPipeCount(ctx, baseQuery, coll)
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
		return
	}
	count := mcount["count"].(int)
	pages := utils.CalcPages(count, limit)

	originalLink := fmt.Sprintf("/api/v1/shopAds/")
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["shopAds"] = shopAds

	utils.ResponseSuccess(ctx, result)
}
