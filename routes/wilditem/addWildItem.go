package wilditem

import (
	"fmt"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

func validateWildItem(ctx iris.Context, wideItem *models.WildItem) error {
	if parseErr := ctx.ReadJSON(&wideItem); parseErr != nil {
		return parseErr
	}
	if validateErr := wideItem.ValidateAll(); validateErr != nil {
		return validateErr
	}

	return nil
}

//AddWildItem for admin use.
func AddWildItem(ctx iris.Context) {
	wideItem := models.WildItem{}

	if parseErr := validateWildItem(ctx, &wideItem); parseErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(parseErr))
		return
	}

	_id := bson.NewObjectId()
	update := bson.M{
		"_id":                _id,
		"itemId":             wideItem.ItemID,
		"isActive":           wideItem.IsActive,
		"isReachDateTime":    wideItem.IsReachDateTime,
		"startDateTime":      wideItem.StartDateTime,
		"expiredDateTime":    wideItem.ExpiredDateTime,
		"count":              wideItem.Count,
		"toMall":             wideItem.ToMall,
		"toMallFloor":        wideItem.ToMallFloor,
		"toShop":             wideItem.ToShop,
		"zoneTypeId":         wideItem.ZoneTypeID,
		"couponGiftId":       wideItem.CouponGiftID,
		"couponGiftNameThai": wideItem.CouponGiftNameThai,
		"couponGiftNameEng":  wideItem.CouponGiftNameEng,
		"couponGiftNameChi1": wideItem.CouponGiftNameChi1,
		"couponGiftNameChi2": wideItem.CouponGiftNameChi2,
		"isGoldenMinutes":    wideItem.IsGoldenMinutes,
		"createAt":           time.Now(),
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.WildItemCollection)
	mongoErr := coll.Insert(update)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
		return
	}

	items := []models.WildItem{}
	findErr := coll.Find(bson.M{"_id": _id}).All(&items)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, items)
}
