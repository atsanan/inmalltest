package wilditem

import (
	"fmt"
	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"github.com/kataras/iris"

	"inmallgame/app/data-access"
)

//ItemPick struct.
type ItemPick struct {
	WideItemID bson.ObjectId `json:"wildItemId" bson:"wildItemId"`
	PlayerID   bson.ObjectId `json:"playerId" bson:"playerId"`
}

func validateCatchWildItem(wildItemID string, playerID string) error {
	err := validation.Errors{
		"wildItemId": validation.Validate(wildItemID, validation.Required, is.MongoID),
		"playerId":   validation.Validate(playerID, validation.Required, is.MongoID),
	}.Filter()
	if err != nil {
		return err
	}

	return nil
}

//CatchWildItem routes
func CatchWildItem(ctx iris.Context) {
	WildItemID := ctx.PostValue("wildItemId")
	playerID := ctx.PostValue("playerId")
	IsCoupon, _ := ctx.PostValueBool("isCoupon")
	CouponGiftID := ctx.PostValue("couponGiftId")
	CouponHashKey := ctx.PostValue("couponHashKey")
	CouponQRImage := ctx.PostValue("couponQRImage")
	CouponPassword := ctx.PostValue("couponPassword")

	if err := validateCatchWildItem(WildItemID, playerID); err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	// Find wildItem.
	wildItem := models.WildItem{}
	wItemColl := session.DB(config.DbName).C(config.DBCollection.WildItemCollection)
	findErr := wItemColl.Find(bson.M{"_id": bson.ObjectIdHex(WildItemID)}).One(&wildItem)
	if findErr != nil {
		fmt.Println(findErr)
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(findErr))
		return
	}
	_id := bson.NewObjectId()
	itemplayer := models.ItemPlayer{
		ID:             _id,
		PlayerID:       bson.ObjectIdHex(playerID),
		ItemID:         wildItem.ItemID,
		IsCoupon:       IsCoupon,
		CouponGiftID:   CouponGiftID,
		CouponHashKey:  CouponHashKey,
		CouponQRImage:  CouponQRImage,
		CouponPassword: CouponPassword,
		Count:          1,
		IsActive :true,
		CreateAt:       time.Now(),
	}

	itemPlayerColl := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	resp := bson.M{}
	if wildItem.Count > 0 {
		if IsCoupon == true {
			if mongoErr := itemPlayerColl.Insert(itemplayer); mongoErr != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
				return
			}

			pipe := itemPlayerColl.Pipe([]bson.M{
				{"$match": bson.M{"_id": _id}},
				{
					"$lookup": bson.M{
						"from":         config.DBCollection.ItemCollection,
						"localField":   "itemId",
						"foreignField": "_id",
						"as":           "item",
					},
				},
				{"$unwind": "$item"},
				{
					"$lookup": bson.M{
						"from":         config.DBCollection.CategoryItemCollection,
						"localField":   "item.itemCategoryId",
						"foreignField": "_id",
						"as":           "item.itemCategory",
					},
				},
				{"$unwind": "$item.itemCategory"},
			})
			err := pipe.One(&resp)
			if err != nil {
				utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to pipe data", fmt.Sprint(err))
				return
			}
		} else if wildItem.IsToItemPlayer == 1 {

			itemErr := itemPlayerColl.Update(
				bson.M{"itemId": wildItem.ItemID,
					"playerId": bson.ObjectIdHex(playerID),
					"isCoupon": false,
				},
				bson.M{
					"$set": bson.M{
						"couponGiftId":   CouponGiftID,
						"couponHashKey":  CouponHashKey,
						"couponQRImage":  CouponQRImage,
						"couponPassword": CouponPassword,
						"isCoupon":       false,
					},
					"$inc":         bson.M{"count": 1},
					"$currentDate": bson.M{"lastModified": true},
				})

			if itemErr != nil {
				if mongoErr := itemPlayerColl.Insert(itemplayer); mongoErr != nil {
					utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
					return
				}

				pipe := itemPlayerColl.Pipe([]bson.M{
					{"$match": bson.M{"itemId": _id}},
					{
						"$lookup": bson.M{
							"from":         config.DBCollection.ItemCollection,
							"localField":   "itemId",
							"foreignField": "_id",
							"as":           "item",
						},
					},
					{"$unwind": "$item"},
					{
						"$lookup": bson.M{
							"from":         config.DBCollection.CategoryItemCollection,
							"localField":   "item.itemCategoryId",
							"foreignField": "_id",
							"as":           "item.itemCategory",
						},
					},
					{"$unwind": "$item.itemCategory"},
				})
				err := pipe.One(&resp)
				if err != nil {
					utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to pipe data", fmt.Sprint(err))
					return
				}

			}

		}

		mongoErr := wItemColl.Update(bson.M{"_id": bson.ObjectIdHex(WildItemID)},
			bson.M{"$inc": bson.M{"count": -1}})

		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "update errors", fmt.Sprint(mongoErr))
			return
		}

	} else {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint("item cannot pick"))
		return

	}

	utils.ResponseSuccess(ctx, resp)

}
