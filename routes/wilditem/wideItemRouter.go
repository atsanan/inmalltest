package wilditem

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	// // For authorize user.
	routes.Get("/byZone", auth.TokenMDW, GetWildItemByZone)
	routes.Get("/count/{itemId:string}", GetWildItemCountAndCountMax)
	routes.Post("/pick", auth.TokenMDW, CatchWildItem)
	// For admin
	routes.Post("/", AddWildItem)
}
