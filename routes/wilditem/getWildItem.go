package wilditem

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"

	"inmallgame/app/data-access"
	//"inmallgame/app/enums"
	"inmallgame/app/utils"
)

//GetWildItemByZone routes
func GetWildItemByZone(ctx iris.Context) {
	zoneID := ctx.URLParam("zoneTypeId")
	ID := ctx.URLParam("id")
	itemLimit := ctx.URLParamIntDefault("limit", 5)

	err := validation.Errors{
		"zoneTypeId": validation.Validate(zoneID, validation.Required, validation.NotNil, is.MongoID),
		"limit":      validation.Validate(itemLimit, validation.NotNil),
	}.Filter()
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	//zoneTypeID, _ := strconv.Atoi(zoneID)
	query := bson.M{}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()

	//if zoneTypeID == enums.InMallShop.ToInt() {
	// Query inmall shop info.

	zoneTypeColl := session.DB(config.DbName).C(config.DBCollection.ZoneTypeCollection)
	zoneTypes := map[string]map[string]string{}
	zoneTypesQuery := bson.M{"_id": bson.ObjectIdHex(zoneID)}
	mongoErr := zoneTypeColl.Find(zoneTypesQuery).One(&zoneTypes)
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(mongoErr))
		return
	}

	zoneTypeName := zoneTypes["zoneTypeName"]
	if zoneTypeName["eng"] != "World" {

		errId := validation.Errors{
			"id": validation.Validate(ID, is.MongoID),
		}.Filter()
		if errId != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
			return
		}
		if ID != "" {
			if zoneTypeName["eng"] == "InMall" {
				query["toMall"] = bson.ObjectIdHex(ID)
			}

			if zoneTypeName["eng"] == "InMall Floor" {
				query["toMallFloor"] = bson.ObjectIdHex(ID)
			}

			if zoneTypeName["eng"] == "InMall Shop" {
				query["toShop"] = bson.ObjectIdHex(ID)
			}
		}
	} else {
		query["zoneTypeId"] = bson.ObjectIdHex(zoneID)
	}
	//} //else if zoneTypeID == enums.InMallFloor.ToInt() {
	// Query inmall floor info.
	//query["toMallFloor"] = bson.ObjectIdHex(fieldID)
	//}

	wildItemColl := session.DB(config.DbName).C(config.DBCollection.WildItemCollection)

	pipe := wildItemColl.Pipe([]bson.M{
		{"$match": query},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.ItemCollection,
				"localField":   "itemId",
				"foreignField": "_id",
				"as":           "item",
			},
		},
		{"$unwind": "$item"},
		{"$limit": itemLimit},
	})

	items := []bson.M{}
	if mongoErr := pipe.All(&items); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to pipe data", fmt.Sprint(mongoErr))
		return
	}

	result := map[string]interface{}{
		"limit": itemLimit,
		"items": items,
	}
	utils.ResponseSuccess(ctx, result)

}
