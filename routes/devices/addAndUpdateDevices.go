package devices

import (
	"fmt"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"

	validation "github.com/go-ozzo/ozzo-validation"
)

//AddOrUpdateDevice func
func AddOrUpdateDevice(ctx iris.Context) {

	deviceID := ctx.PostValue("deviceId")
	deviceName := ctx.PostValue("deviceName")
	deviceType := ctx.PostValue("deviceType")
	deviceModel := ctx.PostValue("deviceModel")
	deviceOS := ctx.PostValue("deviceOS")
	err := validation.Errors{
		"deviceId": validation.Validate(deviceID, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.DeviceCollection)
	data := bson.M{
		"deviceId":    deviceID,
		"deviceName":  deviceName,
		"deviceType":  deviceType,
		"deviceModel": deviceModel,
		"deviceOS":    deviceOS,
	}
	objQuery := []bson.M{
		{"$match": bson.M{"deviceId": deviceID}},
	}

	result := bson.M{}
	pipe := coll.Pipe(objQuery)
	findErr := pipe.One(&result)
	if findErr != nil {
		data["privacyPolicyVersion"] = -1
		data["createAt"] = time.Now()
		mongoErr := coll.Insert(data)
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			return
		}
		result = data
	} else {
		mongoErr := coll.Update(bson.M{"deviceId": deviceID}, bson.M{
			"$currentDate": bson.M{"lastModified": true},
			"$set":         data,
		})
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
			return
		}
		result = data
	}

	returnData := bson.M{}
	pipe = coll.Pipe(objQuery)
	findErr = pipe.One(&returnData)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, returnData)
}
