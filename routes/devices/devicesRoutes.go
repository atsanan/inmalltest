package devices

import (
	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Post("/addAndUpdate/", AddOrUpdateDevice)

	routes.Post("/updatePrivacy/{deviceId:string}", UpdatePrivacy)
}
