package devices

import (
	"fmt"
	"strconv"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/kataras/iris"

	// "github.com/novalagung/gubrak"

	"inmallgame/app/data-access"
	// "inmallgame/app/models"
	"inmallgame/app/utils"
)

//UpdatePrivacy func
func UpdatePrivacy(ctx iris.Context) {
	privacyPolicyVersion := ctx.PostValue("privacyPolicyVersion")
	deviceID := ctx.Params().Get("deviceId")
	err := validation.Errors{
		"deviceId":             validation.Validate(deviceID, validation.Required),
		"privacyPolicyVersion": validation.Validate(privacyPolicyVersion, validation.Required),
	}.Filter()
	val, _ := strconv.Atoi(privacyPolicyVersion)
	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.DeviceCollection)

	mongoErr := coll.Update(bson.M{"deviceId": deviceID}, bson.M{
		"$currentDate": bson.M{"lastModified": true},
		"$set":         bson.M{"privacyPolicyVersion": val},
	})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to update data", fmt.Sprint(mongoErr))
		return
	}

	result := bson.M{}
	objQuery := []bson.M{
		{"$match": bson.M{"deviceId": deviceID}},
	}
	pipe := coll.Pipe(objQuery)
	findErr := pipe.One(&result)
	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch new data", fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, result)
}
