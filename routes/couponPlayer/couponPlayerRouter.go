package couponPlayer

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Get("/list", auth.TokenMDW, GetCouponPlayer)
	routes.Get("/detail", auth.TokenMDW, CouponPlayerDetail)
	routes.Get("/delete", auth.TokenMDW, CouponPlayerDelete)
	routes.Post("/sendCoupon", auth.TokenMDW, SendCouponPlayer)
}
