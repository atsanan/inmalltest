package couponPlayer

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/kataras/iris"

	"github.com/globalsign/mgo/bson"
)

func CouponPlayerDelete(ctx iris.Context) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	_, mongoErr := coll.RemoveAll(bson.M{"uniqueId": bson.M{"$exists": true}})
	if mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to delete costume item", fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, nil)
}
