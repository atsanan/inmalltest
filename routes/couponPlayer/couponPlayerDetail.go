package couponPlayer

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/kataras/iris"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

func CouponPlayerDetail(ctx iris.Context) {
	ID := ctx.URLParamTrim("id")
	err := validation.Errors{
		"id": validation.Validate(ID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	objQuery := []bson.M{}
	objQuery = append(objQuery, bson.M{"$match": bson.M{
		"_id": bson.ObjectIdHex(ID),
	},
	})
	couponPlayer := bson.M{}
	findErr := coll.Pipe(objQuery).One(&couponPlayer)

	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(findErr))
		return
	}

	utils.ResponseSuccess(ctx, bson.M{"couponPlayer": couponPlayer})

}
