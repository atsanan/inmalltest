package couponPlayer

import (
	"fmt"
	"inmallgame/app/utils"

	"inmallgame/app/data-access"

	"github.com/kataras/iris"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
)

func GetCouponPlayer(ctx iris.Context) {

	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)
	uniqueID := ctx.URLParamTrim("uniqueId")

	err := validation.Errors{
		"uniqueId": validation.Validate(uniqueID, validation.Required),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	couponPlayer := []bson.M{}
	baseQuery := []bson.M{
		{
			"$match": bson.M{
				"uniqueId": uniqueID,
			},
		},
	}
	mcount, countErr := utils.CalcPipeCount(ctx, baseQuery, coll)
	if countErr != nil {
		result := map[string]interface{}{
			"pages":     page,
			"pageIndex": pageIndex,
			"pageLimit": pageLimit,
			"couponPlayer":   couponPlayer,
			"paging": map[string]interface{}{
				"next":     "",
				"previous": "",
			},
		}

		utils.ResponseSuccess(ctx, result)
		return
	}
	count := mcount["count"].(int)

	pages := utils.CalcPages(count, limit)

	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	if mongoErr := coll.Pipe(offsetQuery).All(&couponPlayer); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	linkF := "/api/v2/couponPlayer/list?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["couponPlayer"] = couponPlayer

	utils.ResponseSuccess(ctx, result)

}
