package couponPlayer

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

func SendCouponPlayer(ctx iris.Context) {
	ItemPlayerID := ctx.PostValue("itemPlayerId")
	PlayerID := ctx.PostValue("playerId")
	ToID := ctx.PostValue("toId")

	err := validation.Errors{
		"itemPlayerId":   validation.Validate(ItemPlayerID, validation.Required, is.MongoID),
		"playerId": validation.Validate(PlayerID, validation.Required, is.MongoID),
		"toId":     validation.Validate(ToID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.ItemPlayerCollection)
	objQuery := []bson.M{}
	objQuery = append(objQuery, bson.M{"$match": bson.M{
		"_id":          bson.ObjectIdHex(ItemPlayerID),
		"playerId":        bson.ObjectIdHex(PlayerID),
		"couponDatetime1": bson.M{"$exists": false},
	},
	})
	couponPlayer := bson.M{}
	findErr := coll.Pipe(objQuery).One(&couponPlayer)

	if findErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(findErr))
		return
	}

	friendQuery := []bson.M{}
	friendQuery = append(friendQuery, bson.M{"$match": bson.M{
		"itemId":         couponPlayer["itemId"],
		"playerId":        bson.ObjectIdHex(ToID),
		"couponDatetime1": bson.M{"$exists": false},
	},
	})

	couponPlayerFreiend := bson.M{}
	errFriend := coll.Pipe(friendQuery).One(&couponPlayerFreiend)

	if errFriend == nil {
		utils.ResponseFailure(ctx, iris.StatusOK, nil, "player have Item")
		return
	}
	errUpdate := coll.Update(bson.M{
		"_id":          bson.ObjectIdHex(ItemPlayerID),
		"playerId":        bson.ObjectIdHex(PlayerID),
		"couponDatetime1": bson.M{"$exists": false}},
		bson.M{
			"$set": bson.M{
				"playerId": bson.ObjectIdHex(ToID),
			},
		})
	if errUpdate != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Not Update", fmt.Sprint("update"))
		return
	}
	utils.ResponseSuccess(ctx, bson.M{"message": "send coupon Success"})

}
