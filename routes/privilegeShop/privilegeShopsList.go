package privilegeShop

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//PrivilegeShopList
func PrivilegeShopList(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PrivilegeShopsCollection)
	baseQuery := []bson.M{

		{"$match": bson.M{"isActive": true}},
	}

	mcount, _ := utils.CalcPipeCount(ctx, baseQuery, coll)
	pages := utils.CalcPagesByPipeResult(mcount, limit)

	baseQuery = append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	pipe := coll.Pipe(baseQuery)
	privilegeShops := []bson.M{}
	if mongoErr := pipe.All(&privilegeShops); mongoErr != nil {

		utils.ResponseSuccess(ctx,
			bson.M{"privilegeShops": privilegeShops,
				"pageIndex": pageIndex,
				"pageLimit": pageLimit,
				"pages":     1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		//utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	originalLink := fmt.Sprintf("/api/v2/privilegeShops")
	linkF := originalLink + "?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, page, limit, nextPage, previousPage)
	result["privilegeShops"] = privilegeShops

	utils.ResponseSuccess(ctx, result)
}
