package privilegeShop

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"
	"strconv"

	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/kataras/iris"
)

//neayByPrivilegeShop
func neayByPrivilegeShop(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	lat := ctx.URLParamTrim("lat")
	long := ctx.URLParamTrim("long")
	typeQuery := ctx.URLParamTrim("type")
	// minDistance, _ := ctx.URLParamInt("minDistance")
	// maxDistance, _ := ctx.URLParamInt("maxDistance")
	err := validation.Errors{
		"lat":  validation.Validate(lat, validation.Required, is.Float),
		"long": validation.Validate(long, validation.Required, is.Float),
	}.Filter()

	typeQ := "Point"
	if typeQuery != "" {
		typeQ = typeQuery
	}

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	malls := []bson.M{}
	longConv, _ := strconv.ParseFloat(long, 64)
	latConv, _ := strconv.ParseFloat(lat, 64)
	var coordinates [2]float64
	coordinates[0] = longConv
	coordinates[1] = latConv
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallColl := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	baseQuery := []bson.M{
		bson.M{
			"$geoNear": bson.M{
				"near":          bson.M{"type": typeQ, "coordinates": coordinates},
				"maxDistance":   5000,
				"distanceField": "dist.calculated",
				"spherical":     true,
			},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PrivilegeShopsCollection,
				"localField":   "_id",
				"foreignField": "shopId",
				"as":           "privilegeShops",
			},
		},
	}
	mcount, countErr := utils.CalcPipeCount(ctx, baseQuery, mallColl)
	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(countErr))
		return
	}
	count := mcount["count"].(int)

	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	if mongoErr := mallColl.Pipe(offsetQuery).All(&malls); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v1/privilegeShop/nearBy?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["shops"] = malls

	utils.ResponseSuccess(ctx, result)
}

//neayByLocation
func neayByLocation(ctx iris.Context) {
	pageIndex, _ := ctx.URLParamInt("page")
	pageLimit, _ := ctx.URLParamInt("limit")
	lat := ctx.URLParamTrim("lat")
	long := ctx.URLParamTrim("long")
	typeQuery := ctx.URLParamTrim("type")
	// minDistance, _ := ctx.URLParamInt("minDistance")
	// maxDistance, _ := ctx.URLParamInt("maxDistance")
	err := validation.Errors{
		"lat":  validation.Validate(lat, validation.Required, is.Float),
		"long": validation.Validate(long, validation.Required, is.Float),
	}.Filter()

	typeQ := "Point"
	if typeQuery != "" {
		typeQ = typeQuery
	}

	if err != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}

	offset, limit, page := utils.CalcOffsetQuery(pageIndex, pageLimit)

	privilegeShops := []bson.M{}
	longConv, _ := strconv.ParseFloat(long, 64)
	latConv, _ := strconv.ParseFloat(lat, 64)
	var coordinates [2]float64
	coordinates[0] = longConv
	coordinates[1] = latConv
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	mallColl := session.DB(config.DbName).C(config.DBCollection.ShopCollection)
	baseQuery := []bson.M{
		{
			"$geoNear": bson.M{
				"near":          bson.M{"type": typeQ, "coordinates": coordinates},
				"maxDistance":   5000,
				"distanceField": "dist.calculated",
				"spherical":     true,
			},
		},
		{
			"$lookup": bson.M{
				"from":         config.DBCollection.PrivilegeShopsCollection,
				"localField":   "_id",
				"foreignField": "shopId",
				"as":           "privilegeShops",
			},
			//			{"$unwind": "$privilegeShops"},
		},
		{"$unwind": "$privilegeShops"},
		{"$match": bson.M{"isActive": true}},
		{
			"$group": bson.M{
				"_id":                  "$privilegeShops._id",
				"privilegeShopTitles":  bson.M{"$first": "$privilegeShops.privilegeShopTitles"},
				"privilegeShopDetails": bson.M{"$first": "$privilegeShops.privilegeShopDetails"},
				"startDateTime":        bson.M{"$first": "$privilegeShops.startDateTime"},
				"expiredDateTime":      bson.M{"$first": "$privilegeShops.expiredDateTime"},
				"shopId":               bson.M{"$first": "$privilegeShops.shopId"},
				"privilegeDefaultId":   bson.M{"$first": "$privilegeShops.privilegeDefaultId"},
				"order":                bson.M{"$first": "$privilegeShops.order"},
				"isActive":             bson.M{"$first": "$privilegeShops.isActive"},
				"isReachDateTime":      bson.M{"$first": "$privilegeShops.isReachDateTime"},
				"isGoldenMinutes":      bson.M{"$first": "$privilegeShops.isGoldenMinutes"},
				"created_at":           bson.M{"$first": "$privilegeShops.created_at"},
				"updated_at":           bson.M{"$first": "$privilegeShops.updated_at"},
			},
		},
	}
	mcount, countErr := utils.CalcPipeCount(ctx, baseQuery, mallColl)
	if countErr != nil {

		utils.ResponseSuccess(ctx,
			bson.M{"privilegeShops": privilegeShops,
				"pageIndex": pageIndex,
				"pageLimit": pageLimit,
				"pages":     1,
				"paging": bson.M{
					"next":     "",
					"previous": "",
				}})
		return

	}
	count := mcount["count"].(int)

	offsetQuery := append(baseQuery, bson.M{"$skip": offset}, bson.M{"$limit": limit})
	if mongoErr := mallColl.Pipe(offsetQuery).All(&privilegeShops); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	if countErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(err))
		return
	}
	pages := utils.CalcPages(count, limit)

	linkF := "/api/v2/privilegeShops/byLocation?page=%d&limit=%d"
	nextPage, previousPage := utils.WithPaging(page, pages, limit)(linkF)
	result := utils.PagingResult(pages, pageIndex, pageLimit, nextPage, previousPage)
	result["privilegeShops"] = privilegeShops

	utils.ResponseSuccess(ctx, result)
}
