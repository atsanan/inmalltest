package privilegeShop

import (
	"inmallgame/routes/auth"

	"github.com/kataras/iris/core/router"
)

// InitRouter for manage routes.
func InitRouter(routes router.Party) {
	routes.Get("/nearBy", auth.TokenMDW, neayByPrivilegeShop)
	routes.Get("/byLocation", auth.TokenMDW, neayByLocation)
	routes.Get("/list", auth.TokenMDW, PrivilegeShopList)
	routes.Get("/{id:string}", auth.TokenMDW, PrivilegeShopDetail)
}
