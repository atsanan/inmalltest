package privilegeShop

import (
	"fmt"
	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

//PrivilegeShopDetail
func PrivilegeShopDetail(ctx iris.Context) {
	ID := ctx.Params().Get("id")
	if validateErr := utils.ValidateMongoID("id", ID); validateErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(validateErr))
		return
	}

	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	coll := session.DB(config.DbName).C(config.DBCollection.PrivilegeShopsCollection)
	baseQuery := []bson.M{
		{"$match": bson.M{
			"_id":      bson.ObjectIdHex(ID),
			"isActive": true}},
	}
	pipe := coll.Pipe(baseQuery)
	privilegeShops := bson.M{}
	if mongoErr := pipe.One(&privilegeShops); mongoErr != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to fetch data", fmt.Sprint(mongoErr))
		return
	}

	utils.ResponseSuccess(ctx, privilegeShops)

}
