package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

// TestMonsterDefaultAPI serve as test router.
func TestMonsterDefaultAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("Get MonsterDefault Info", func(t *testing.T) {
		e.GET("/api/v1/monsterDefault/info/5b332284bdfdd84f1fbd6793").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).Body().Contains("data")
	})

	t.Run("Get MonsterDefault List", func(t *testing.T) {
		e.GET("/api/v1/monsterDefault").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).Body().Contains("monsters")

		e.GET("/api/v1/monsterDefault").
			WithQuery("sort", "mDefaultGroupLevel:1").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).Body().Contains("monsters")
	})

	t.Run("Add Default Monster", func(t *testing.T) {
		form := map[string]interface{}{
			"mDefaultName":           "mon",
			"mDefaultTypeId":         1,
			"mDefaultJobLevel":       1,
			"mDefaultGroupId":        1,
			"mDefaultAssetImageSlot": "1",
			"mDefaultAssetModel":     "1",
			"mDefaultAssetVersion":   1,
			"mDefaultGroupLevel":     1,
			"mDefaultTextDetail":     "bobo1",
			"MDefaultOrder":          1,
			"MDefaultJobCategories":  []int{0},
		}

		e.POST("/api/v1/monsterDefault/add").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("mDefaultName")
	})

	t.Run("Update Default Monster", func(t *testing.T) {
		form := map[string]interface{}{
			"mDefaultTextDetail": "",
			"mDefaultOrder":      10,
			"mDefaultGroupId":    1,
		}

		e.POST("/api/v1/monsterDefault/5b332284bdfdd84f1fbd6793").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("mDefaultName")
	})
}
