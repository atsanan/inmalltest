package tests

import (
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"

	"inmallgame/app/utils"
)

// TestUserAPI handle api test.
func TestUserAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("Get user", func(t *testing.T) {
		e.GET("/api/v1/user").
			WithHeader(utils.APIKey, "inmall-1234").
			WithQuery("userId", "5b4329f0bdfdd84f1f01c209").
			Expect().Status(httptest.StatusOK).
			Body().Contains("firstname")
	})

	t.Run("Update user", func(t *testing.T) {
		form := map[string]string{
			"name":      "mzget",
			"Firstname": "mzget",
			"Lastname":  "test",
			"Gender":    "male",
		}

		e.POST("/api/v1/user").
			WithHeader(utils.APIKey, "inmall-1234").
			WithQuery("userId", "5b4329f0bdfdd84f1f01c209").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("firstname")
	})
}
