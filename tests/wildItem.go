package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

// TestWildItemAPI handle api test.
func TestWildItemAPI(t *testing.T, e *httpexpect.Expect) {
	// WildIitem
	t.Run("Get WildItem byZone", func(t *testing.T) {
		e.GET("/api/v1/wildItem/byZone").
			WithHeader(utils.APIKey, "inmall-1234").
			WithQuery("zoneTypeId", "2").
			WithQuery("id", "5b34bbe41a58da6a606c9c89").
			Expect().Status(httptest.StatusOK).
			Body().Contains("items").Contains("limit")
	})

	t.Run("Pick WildItem in bag", func(t *testing.T) {
		e.POST("/api/v1/wildItem/pick").
			WithHeader(utils.APIKey, "inmall-1234").
			WithFormField("wildItemId", "5b51e37daaea3c30c4e9e2d4").
			WithFormField("playerId", "5b2ca882bdfdd84f1fa5fe44").
			Expect().Status(httptest.StatusOK).
			Body().Contains("item")

	})

	t.Run("Add WildItem (Backend)", func(t *testing.T) {
		form := map[string]interface{}{
			"itemId":          "5b482e7caaea3c2b44158ede",
			"zoneTypeId":      2,
			"toShop":          "5b34bbe41a58da6a606c9c89",
			"toMallFloor":     "5b472671aaea3c3044450098",
			"count":           6,
			"expiredDateTime": "2018-01-01T00:00:00Z",
			"startDateTime":   "2018-01-01T00:00:00Z",
			"isReachDateTime": true,
			"isAtive":         true,
			"isGoldenMinutes": true,
		}

		e.POST("/api/v1/wildItem").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("data")
	})
}
