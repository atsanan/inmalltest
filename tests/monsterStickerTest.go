package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

// TestMonsterStickerAPI handle api test.
func TestMonsterStickerAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("sticker/byChannel/{channelId}", func(t *testing.T) {
		e.GET("/api/v1/sticker/byChannel/5b3a1a9f083713dbfb387280").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("stickers").Contains("stickerStore")
	})

	t.Run("sticker/byFrom/{playerId}", func(t *testing.T) {
		e.GET("/api/v1/sticker/byFrom/5b2ca882bdfdd84f1fa5fe44").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("stickers").Contains("stickerStore")
	})

	t.Run("sticker/byTo/{mPlayerId}", func(t *testing.T) {
		e.GET("/api/v1/sticker/byTo/5b35ee0508371345dc8e4dff").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("stickers").Contains("stickerStore")
	})

	t.Run("sticker/toFriend", func(t *testing.T) {
		form := map[string]string{
			"stickerStoreId":   "5b43112e0837137851003525",
			"channelId":        "5b3a1a9f083713dbfb387280",
			"sendFromPlayerId": "5b2ca882bdfdd84f1fa5fe44",
			"sendToMPlayerId":  "5b35ee0508371345dc8e4dff",
		}

		e.POST("/api/v1/sticker/toFriend").
			WithHeader(utils.APIKey, "inmall-1234").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("data")
	})
}
