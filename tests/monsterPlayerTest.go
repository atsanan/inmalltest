package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

// TestMonsterPlayerAPI handle api test.
func TestMonsterPlayerAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("monsterPlayer/info", func(t *testing.T) {
		e.GET("/api/v1/monster/info").
			WithHeader(utils.APIKey, "inmall-1234").
			WithQuery("mPlayerId", "5b4710930837139ebc444bae").
			Expect().Status(httptest.StatusOK).
			Body().Contains("mPlayerName").Contains("monstersDefault")
	})

	t.Run("monsterPlayer/all", func(t *testing.T) {
		e.GET("/api/v1/monster/all/5b2ca882bdfdd84f1fa5fe44").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("monsters")

		e.GET("/api/v1/monster/all/5b2ca882bdfdd84f1fa5fe44").
			WithHeader(utils.APIKey, "inmall-1234").
			WithQuery("fields", "mDefaultId").
			Expect().Status(httptest.StatusOK).
			Body().Contains("monsters").Contains("monsterDefault")
	})

	t.Run("monsterPlayer/inbag", func(t *testing.T) {
		e.GET("/api/v1/monster/inbag/5b2ca882bdfdd84f1fa5fe44").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("monsters")

		e.GET("/api/v1/monster/inbag/5b2ca882bdfdd84f1fa5fe44").
			WithHeader(utils.APIKey, "inmall-1234").
			WithQuery("fields", "mDefaultId").
			Expect().Status(httptest.StatusOK).
			Body().Contains("monsters").Contains("monsterDefault")
	})

	t.Run("monsterPlayer/inChannel", func(t *testing.T) {
		e.GET("/api/v1/monster/inChannel/5b2ca882bdfdd84f1fa5fe44").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("monsters")

		e.GET("/api/v1/monster/inChannel/5b2ca882bdfdd84f1fa5fe44").
			WithHeader(utils.APIKey, "inmall-1234").
			WithQuery("fields", "mDefaultId").
			Expect().Status(httptest.StatusOK).
			Body().Contains("monsters").Contains("monsterDefault")
	})

	t.Run("UpdateMonsterFood", func(t *testing.T) {
		e.POST("/api/v1/monster/food/5b4710930837139ebc444bae").
			WithHeader(utils.APIKey, "inmall-1234").
			WithFormField("mPlayerStatusFood", 10).
			WithFormField("mPlayerFoodId", "5b444820aaea3c175828cbac").
			Expect().Status(httptest.StatusOK).
			Body().
			Contains("mPlayerStatusFood").
			Contains("mPlayerFoodDatetime").
			Contains("mPlayerFoodId")
	})

	t.Run("UpdateMonsterHappiness", func(t *testing.T) {
		e.POST("/api/v1/monster/happy/5b4710930837139ebc444bae").
			WithHeader(utils.APIKey, "inmall-1234").
			WithFormField("mPlayerStatusHappiness", 10).
			WithFormField("mPlayerHappyId", "5b444923aaea3c2d30cfc577").
			Expect().Status(httptest.StatusOK).
			Body().
			Contains("mPlayerStatusHappiness").
			Contains("mPlayerHappinessDatetime").
			Contains("mPlayerHappyId")
	})

	t.Run("UpdateMonsterHealth", func(t *testing.T) {
		e.POST("/api/v1/monster/health/5b4710930837139ebc444bae").
			WithHeader(utils.APIKey, "inmall-1234").
			WithFormField("mPlayerStatusHealth", 10).
			WithFormField("mPlayerHealthId", "5b446d78aaea3c18e0354477").
			Expect().Status(httptest.StatusOK).
			Body().
			Contains("mPlayerStatusHealth").
			Contains("mPlayerHealthDatetime").
			Contains("mPlayerHealthId")
	})

	t.Run("UpdateMonsterPlayer/info", func(t *testing.T) {
		e.POST("/api/v1/monster/info/5b4710930837139ebc444bae").
			WithHeader(utils.APIKey, "inmall-1234").
			WithFormField("mPlayerHabit", 10).
			Expect().Status(httptest.StatusOK).
			Body().Contains("mPlayerName").Contains("monstersDefault")
	})
}
