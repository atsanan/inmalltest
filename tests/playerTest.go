package tests

import (
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"

	"inmallgame/app/utils"
)

// TestPlayerAPI handle api test.
func TestPlayerAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("Get All Player", func(t *testing.T) {
		e.GET("/api/v1/player/all").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().
			Status(httptest.StatusOK).
			Body().Contains("players")
	})

	t.Run("Get Player Info", func(t *testing.T) {
		e.GET("/api/v1/player/info").
			WithHeader(utils.APIKey, "inmall-1234").
			WithQuery("fields", "player,user").
			WithQuery("userId", "5b4329f0bdfdd84f1f01c209").
			Expect().
			Status(httptest.StatusOK).
			Body().Contains("user").Contains("player")
	})

	t.Run("Update Player Info", func(t *testing.T) {
		form := map[string]interface{}{
			"coin": 1,
		}
		e.POST("/api/v1/player/5b2ca882bdfdd84f1fa5fe44").
			WithHeader(utils.APIKey, "inmall-1234").
			WithJSON(form).
			Expect().
			Status(httptest.StatusOK).
			Body().Contains("playerName")
	})
}
