package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

// TestWildMonsterAPI handle api test.
func TestWildMonsterAPI(t *testing.T, e *httpexpect.Expect) {
	// WildMonster
	t.Run("wildMonster/byZone", func(t *testing.T) {
		e.GET("/api/v1/wildMonster/byZone").
			WithHeader(utils.APIKey, "inmall-1234").
			WithQuery("zoneTypeId", "1").
			WithQuery("id", "5b34bbe41a58da6a606c9c89").
			Expect().Status(httptest.StatusOK).
			Body().Contains("monsters").Contains("limit")
	})

	t.Run("wildMonster/pick", func(t *testing.T) {
		e.POST("/api/v1/wildMonster/pick").
			WithHeader(utils.APIKey, "inmall-1234").
			WithFormField("wildMonsterId", "5b34c069bdfdd84f1fc77c7f").
			WithFormField("playerId", "5b2ca882bdfdd84f1fa5fe44").
			Expect().Status(httptest.StatusOK).
			Body().Contains("Cannot catch wildMonster because out of limit")
	})

	t.Run("wildMonster/add", func(t *testing.T) {
		form := map[string]interface{}{
			"mDefaultId": "5b332284bdfdd84f1fbd6793",
			"count":      2,
			"zoneTypeId": 1,
		}

		e.POST("/api/v1/wildMonster").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("zoneTypeId")
	})
}
