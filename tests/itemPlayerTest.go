package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

//TestItemPlayerAPI  serve as test router.
func TestItemPlayerAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("Get item in bag", func(t *testing.T) {
		e.GET("/api/v1/itemPlayer/5b2ca882bdfdd84f1fa5fe44").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("data")
	})

	t.Run("Add item in bag", func(t *testing.T) {
		form := map[string]interface{}{
			"playerId":       "5b2ca882bdfdd84f1fa5fe44",
			"itemId":         "5b446d78aaea3c18e0354477",
			"count":          2,
			"couponPassword": "1234",
			"couponGirfID":   "test",
			"couponHashKey":  "test",
			"couponQRImage":  "https://github.com/ahoo-studio/inmall-api/wiki/Items-API",
		}

		e.POST("/api/v1/itemPlayer/add").
			WithJSON(form).
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("data")
	})

}
