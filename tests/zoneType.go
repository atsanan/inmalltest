package tests

import (
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"

	"inmallgame/app/utils"
)

// TestZoneTypeAPI handle api test.
func TestZoneTypeAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("Get ZoneType", func(t *testing.T) {
		e.GET("/api/v1/zonetype").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().
			Status(httptest.StatusOK).
			Body().Contains("zoneTypes")
	})
	t.Run("Add ZoneType", func(t *testing.T) {
		form := map[string]interface{}{
			"zoneTypeName": "InMall Shop",
			"zoneTypeId":   3,
			"isAtive":      false,
		}
		e.POST("/api/v1/zonetype").
			WithJSON(form).
			Expect().
			Status(httptest.StatusOK).
			Body().Contains("zoneTypeName")
	})
}
