package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/globalsign/mgo/bson"

	"github.com/kataras/iris/httptest"

	"github.com/iris-contrib/httpexpect"
)

//TestMallAPI  serve as test router.
func TestMallAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("Get malls list", func(t *testing.T) {
		e.GET("/api/v1/mall/list").
			WithQuery("page", 1).
			WithQuery("limit", 1).
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("malls")
	})

	t.Run("Get malls info", func(t *testing.T) {
		e.GET("/api/v1/mall/info/5b3dd9c012455826051945ce").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("mallName")
	})

	t.Run("Post mall floor info", func(t *testing.T) {
		e.POST("/api/v1/mall/floor/info").
			WithHeader(utils.APIKey, "inmall-1234").
			WithFormField("mallFloorId", "5b4720d0aaea3c233cfa5816").
			Expect().Status(httptest.StatusOK).
			Body().Contains("mall")
	})

	t.Run("Add mall (backend)", func(t *testing.T) {
		var coordinates [2]float64
		coordinates[0] = 100.5586294
		coordinates[1] = 13.8164014
		form := map[string]interface{}{
			"mallName":     "Test",
			"mallDetail":   "Test",
			"mallAddress":  "Test",
			"mapOutdoorId": "Test",
			"mapIndoorId":  "EIM-1dcaab70-7cc6-4673-8f9a-4f78dc9409cb",
			"isActive":     true,
			"location": bson.M{
				"type":        "Point",
				"coordinates": coordinates,
			},
		}
		e.POST("/api/v1/mall/add").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("mall")
	})

	t.Run("Update mall (backend)", func(t *testing.T) {

		var coordinates [2]float64
		coordinates[0] = 100.5586294
		coordinates[1] = 13.8164014
		form := map[string]interface{}{
			"mallName":     "TestUpdate",
			"mallDetail":   "TestUpdate",
			"mallAddress":  "TestUpdate",
			"mapOutdoorId": "TestUpdate",
			"mapIndoorId":  "EIM-1dcaab70-7cc6-4673-8f9a-4f78dc9409cb",
			"isActive":     true,
			"location": bson.M{
				"type":        "Point",
				"coordinates": coordinates,
			},
		}
		e.POST("/api/v1/mall/5b59aab2aaea3c407c7096d0").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("mall")
	})

	t.Run("Update mall (backend)", func(t *testing.T) {
		form := map[string]interface{}{
			"mallFloorName":      "test",
			"mallFloorDetail":    "test",
			"mapIndoorFloorData": "test",
			"mapIndoorFloorKey":  "9",
			"mallId":             "5b472e76aaea3c2f40f5ed69",
		}

		e.POST("/api/v1/mall/floor/add").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("mall")
	})

	t.Run("Update mall floor (backend)", func(t *testing.T) {
		form := map[string]interface{}{
			"mallFloorName":      "Siam_discovery-9",
			"mallFloorDetail":    "Siam_discovery-9",
			"mapIndoorFloorData": "Siam_discovery-9",
		}

		e.POST("/api/v1/mall/floor/5b59ae9eaaea3c1e88b70d91").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("mall")
	})

	t.Run("Mall nearBy", func(t *testing.T) {
		e.GET("/api/v1/mall/nearBy").
			WithQuery("long", 100.5488514).
			WithQuery("lat", 13.8001134).
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("malls")
	})
}
