package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

// TestShopAPI serve as test router.
func TestShopAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("shop/byFloorId", func(t *testing.T) {
		e.GET("/api/v1/shop/byFloorId/5b34bc381a58da6a606c9c8e").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("mallFloors").Contains("mall").Contains("shop")
	})
	t.Run("shop/byFloorKey", func(t *testing.T) {
		e.GET("/api/v1/shop/byFloorKey/Siam_discovery_G").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("mallFloors").Contains("mall").Contains("shop")
	})
	t.Run("shop/byMall", func(t *testing.T) {
		e.GET("/api/v1/shop/byMall/5b3dd9c012455826051945ce").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("mallFloors").Contains("mall").Contains("shop")
	})
}
