package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

//TestItemDefaultAPI  serve as test router.
func TestItemDefaultAPI(t *testing.T, e *httpexpect.Expect) {

	t.Run("Add item Default", func(t *testing.T) {
		form := map[string]interface{}{
			"itemName":           "test",
			"itemDetail":         "test",
			"itemEffect":         "test",
			"coin":               1,
			"diamond":            1,
			"isActive":           false,
			"itemOrder":          0,
			"itemAssetModel":     "ItemAssetModel",
			"itemAssetImageSlot": "test",
			"itemAssetVersion":   "1.0.2",
			"itemCategoryId":     "5b433c30aaea3c124c983cdb",
		}

		e.POST("/api/v1/itemDefault/add").
			WithHeader(utils.APIKey, "inmall-1234").
			WithJSON(form).Expect().
			Status(httptest.StatusOK).
			Body().Contains("data")
	})

	t.Run("Update ItemDefault", func(t *testing.T) {
		form := map[string]interface{}{
			"itemName":           "test",
			"itemDetail":         "test",
			"itemEffect":         "test",
			"coin":               2,
			"diamond":            2,
			"isActive":           false,
			"itemOrder":          0,
			"itemAssetModel":     "ItemAssetModel",
			"itemAssetImageSlot": "test",
			"itemAssetVersion":   "1.0.2",
			"itemCategoryId":     "5b433c30aaea3c124c983cdb",
		}

		e.POST("/api/v1/itemDefault/5b51a7c4aaea3c1968f62236").
			WithJSON(form).
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("data")
	})

	t.Run("Get default item", func(t *testing.T) {
		e.GET("/api/v1/itemDefault/").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("items")
	})

}
