package tests

import (
	"inmallgame/app/utils"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

// TestStickerStoreAPI handle api test.
func TestStickerStoreAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("stickerStore/list", func(t *testing.T) {
		e.GET("/api/v1/stickerStore/list").
			WithHeader(utils.APIKey, "inmall-1234").
			Expect().Status(httptest.StatusOK).
			Body().Contains("stickers")
	})

	t.Run("stickerStore/add (backend)", func(t *testing.T) {
		form := map[string]interface{}{
			"stickerStoreName":  "Pidgey",
			"coin":              0,
			"diamond":           0,
			"isActive":          false,
			"stickerStoreImage": "https://vignette.wikia.nocookie.net/pokemon/images/5/55/016Pidgey.png/revision/latest/scale-to-width-down/200?cb=20140328192030",
			"stickerStoreLevel": 1,
		}

		e.POST("/api/v1/stickerStore/add").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("data")
	})

	t.Run("Update StickerStore (backend)", func(t *testing.T) {
		form := map[string]interface{}{
			"stickerStoreDetail": "Pidgey resembles a small, plump-bodied bird. It is brown in color, with a cream-colored throat and belly. The tips of its wings share the same color. Both its feet and beak are a pinkish-gray color. Its plumage is fairly nondescript, particularly compared to its evolutions, Pidgeotto and Pidgeot. It has black markings around its eyes and a small crest of brown and cream feathers above its eyes.",
			"coin":               0,
			"diamond":            0,
		}

		e.POST("/api/v1/stickerStore/update/5b56d525083713fd27986e98").
			WithJSON(form).
			Expect().Status(httptest.StatusOK).
			Body().Contains("data")
	})
}
