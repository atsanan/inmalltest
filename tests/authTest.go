package tests

import (
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris/httptest"
)

// TestAuthAPI handle api test.
func TestAuthAPI(t *testing.T, e *httpexpect.Expect) {
	t.Run("auth/login", func(t *testing.T) {
		e.POST("/api/v1/auth/login").
			WithFormField("email", "rattajak.n@gmail.com").
			WithFormField("password", "12345678").
			Expect().Status(httptest.StatusOK).
			Body().Contains("data")
	})

	t.Run("auth/register", func(t *testing.T) {
		e.POST("/api/v1/auth/register").
			WithFormField("email", "rattajak.n@gmail.com").
			WithFormField("password", "12345678").
			Expect().Status(httptest.StatusBadRequest).
			Body().Contains("Email already used.")
	})

	t.Run("auth/registerOrLogin/facebook", func(t *testing.T) {
		e.POST("/api/v1/auth/registerOrLogin/facebook").
			WithFormField("authenToken", "EAAfXdugZBuNwBAHiLuMUi9XJlzSBjylCY981BSBmjZAWTH4oqTKT6UR3xsEAaTBtoRysZClFqHE1Jk2yR12oiC4ZCUGrdNZBGKdCRCGRZCKK2Edv8oHEioLWxmqHPO45YLicSAbn6H18Ljldr6unhYSgkGQpLI2zFSaereYEwvOLJ9UGGMcdqLeAI62ULZCRmwZD").
			Expect().Status(httptest.StatusBadRequest).
			Body().Contains("Get facebook data fail")
	})

	t.Run("auth/registerOrLogin/google", func(t *testing.T) {
		e.POST("/api/v1/auth/registerOrLogin/google").
			WithFormField("authenToken", "ya29.GlsDBh3Tc54tS_jOE4KRzBlHirVrK13OkYiXm76V4f1lSKAfsqt7A_GkhmUbMywMX9SVtdRnQvXXzeUR55FEQT-MFxvKGEfsFqrtQQZISksFTaSKmDNKwv-PisMy").
			Expect().Status(httptest.StatusBadRequest).
			Body().Contains("Get googleapis data fail")
	})

	t.Run("auth/verifyAccount", func(t *testing.T) {
		e.POST("/api/v1/auth/verifyAccount").
			WithFormField("secret", "ShBO6oyriYqq3cvuUkvvK0lC26--bkgNV9MNunBFxRFF1mGZjg==").
			Expect().Status(httptest.StatusOK).
			Body().Contains("data")
	})

	t.Run("auth/resendEmail", func(t *testing.T) {
		e.POST("/api/v1/auth/resendEmail").
			WithFormField("email", "rattajak.n5@gmail.com").
			Expect().Status(httptest.StatusBadRequest).
			Body().Contains("not found")
	})
}
