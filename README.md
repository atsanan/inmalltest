# go-iris-inmallgame
Go, iris framework for fun.

### Get Start
> Live reload.  
$ gin -a 8080 run *.go  
> Test suite.  
$ go test -v ./...

### Deployment
Just review `conf.json` file to make sure your `Env` configuration is `production` and check `DbPD` for your database connection.

### API spec
Please review `InMallGame API v(x.x.x).md`  
### OR review at
* https://github.com/ahoo-studio/inmall-api/wiki/InMall-API-Specification

### Postman Doc
* https://documenter.getpostman.com/view/448442/RWEnkatQ

### For build docker
$ docker build -t mzget/go-iris-inmallgame:[tag] ./
### For run docker
$ docker run -it --rm -p 8080:8080 --name my-running-app mzget/go-iris-inmallgame:0.0.1  
$ docker run -d -p 8081:8080 --name inmallgame mzget/go-iris-inmallgame:[tag]
### Docker cloud.
$ docker push mzget/go-iris-inmallgame