package models

import (
	"github.com/globalsign/mgo/bson"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// FacebookType for specific authenTypeId.
const FacebookType int = 1

// TraditionalType for specific authenTypeId.
const TraditionalType int = -1

// GoogleType for specific authenTypeId.
const GoogleType int = 2
const LineType int = 3

// UserID       int           `json:"userId" bson:"userId"`

// User model.
type User struct {
	ID                      bson.ObjectId `json:"id" bson:"_id,omitempty" `
	Name                    string        `json:"name" bson:"name"`
	Firstname               string        `json:"firstname" bson:"firstname"`
	Lastname                string        `json:"lastname" bson:"lastname"`
	Email                   string        `json:"email" bson:"email"`
	Password                string        `json:"password" bson:"password"`
	Gender                  string        `json:"gender" bson:"gender"`
	Birthdate               string        `json:"birthdate" bson:"birthdate"`
	Tel                     string        `json:"tel" bson:"tel"`
	AuthenTypeID            int           `json:"authenTypeId" bson:"authenTypeId"`
	AuthenID                string        `json:"authenId" bson:"authenId"`
	AuthenToken             string        `json:"authenToken" bson:"authenToken"`
	Verified                bool          `json:"verified" bson:"verified"`
	VerifiedAt              time.Time     `json:"verifiedAt" bson:"verifiedAt,omitempty"`
	Picture                 string        `json:"picture" bson:"picture"`
	PushMessagesAccessToken string        `json:"pushMessagesAccessToken" bson:"pushMessagesAccessToken"`
	Roles                   []string      `json:"roles" bson:"roles"`
	Time                    int           `json:"time" bson:"time"`
	DeviceID                string        `json:"deviceId" bson:"deviceId"`
	CreateAt                time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified            time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
	LastLogin               time.Time     `json:"lastLogin" bson:"lastLogin"`
	LastLoginReward         time.Time     `json:"lastLoginReward" bson:"lastLoginReward"`
	IndexReward         int     `json:"indexReward" bson:"indexReward"`
}

// Validate email and password.
func (a User) Validate() error {
	return validation.ValidateStruct(&a,
		// Street cannot be empty, and the length must between 5 and 50
		validation.Field(&a.Email, validation.Required, is.Email),
		// City cannot be empty, and the length must between 5 and 50
		validation.Field(&a.Password, validation.Required, validation.Length(8, 32)),
	)
}
