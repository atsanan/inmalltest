package models

import (
	"fmt"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// MallFloor model.
type MallFloor struct {
	ID                  bson.ObjectId `json:"id" bson:"_id,omitempty" `
	MallFloorNameEng    string        `json:"mallFloorNameEng" bson:"mallFloorNameEng"`
	MallFloorNameThai   string        `json:"mallFloorNameThai" bson:"mallFloorNameThai"`
	MallFloorNameChi1   string        `json:"mallFloorNameChi1" bson:"mallFloorNameChi1"`
	MallFloorNameChi2   string        `json:"mallFloorNameChi2" bson:"mallFloorNameChi2"`
	MallFloorDetailEng  string        `json:"mallFloorDetailEng" bson:"mallFloorDetailEng"`
	MallFloorDetailThai string        `json:"mallFloorDetailThai" bson:"mallFloorDetailThai"`
	MallFloorDetailChi1 string        `json:"mallFloorDetailChi1" bson:"mallFloorDetailChi1"`
	MallFloorDetailChi2 string        `json:"mallFloorDetailChi2" bson:"mallFloorDetailChi2"`
	MapIndoorFloorKey   string        `json:"mapIndoorFloorKey" bson:"mapIndoorFloorKey"`
	MapIndoorFloorData  string        `json:"mapIndoorFloorData" bson:"mapIndoorFloorData"`
	MallID              bson.ObjectId `json:"mallId" bson:"mallId"`
	// MallFloorID        int           `json:"mallFloorId"`
	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

//MallFloorResponse data
type MallFloorResponse struct {
	ID                 bson.ObjectId `json:"id" bson:"_id,omitempty" `
	MallFloorName      string        `json:"mallFloorName" bson:"mallFloorName"`
	MallFloorDetail    string        `json:"mallFloorDetail" bson:"mallFloorDetail"`
	MapIndoorFloorKey  string        `json:"mapIndoorFloorKey" bson:"mapIndoorFloorKey"`
	MapIndoorFloorData string        `json:"mapIndoorFloorData" bson:"mapIndoorFloorData"`
	MallID             bson.ObjectId `json:"mallId" bson:"mallId"`
	// MallFloorID        int           `json:"mallFloorId"`
	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
	Mall         Mall      `json:"mall" bson:"mall"`
}

// ValidateAll fun
func (c MallFloor) ValidateAll() error {
	return validation.ValidateStruct(&c,
		//validation.Field(&c.MallFloorName, validation.Required, validation.Length(2, 64)),
		validation.Field(&c.MallID, validation.Required),
	)
}

// ValidateForUpdate fun
func (c MallFloor) ValidateForUpdate() error {
	return validation.ValidateStruct(&c) //validation.Field(&c.MallFloorName, validation.Required, validation.Length(2, 64)),

}

// MallFloorCreateIndex just for add mapIndoorFloorKey index in background job.
func MallFloorCreateIndex(coll *mgo.Collection) {
	index := mgo.Index{
		Key:        []string{"mapIndoorFloorKey"},
		Background: true,
	}
	createIndexErr := coll.EnsureIndex(index)
	if createIndexErr != nil {
		fmt.Println("createIndexErr: ", createIndexErr)
	}
}
