package models

import (
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// Item model.
type Item struct {
	ID                 bson.ObjectId `json:"id" bson:"_id,omitempty" `
	ItemNameEng        string        `json:"itemNameEng" bson:"itemNameEng"`
	ItemDetailEng      string        `json:"itemDetailEng" bson:"itemDetailEng"`
	ItemEffectEng      string        `json:"itemEffectEng" bson:"itemEffectEng"`
	ItemNameThai       string        `json:"itemNameThai" bson:"itemNameThai"`
	ItemDetailThai     string        `json:"itemDetailThai" bson:"itemDetailThai"`
	ItemEffectThai     string        `json:"itemEffectThai" bson:"itemEffectThai"`
	ItemNameChi1       string        `json:"itemNameChi1" bson:"itemNameChi1"`
	ItemDetailChi1     string        `json:"itemDetailChi1" bson:"itemDetailChi1"`
	ItemEffectChi1     string        `json:"itemEffectchi1" bson:"itemEffectchi1"`
	ItemNameChi2       string        `json:"itemNameChi2" bson:"itemNameChi2"`
	ItemDetailChi2     string        `json:"itemDetailChi2" bson:"itemDetailChi2"`
	ItemEffectChi2     string        `json:"itemEffectchi2" bson:"itemEffectchi2"`
	Coin               int           `json:"coin" bson:"coin"`
	Diamond            int           `json:"diamond" bson:"diamond"`
	IsActive           bool          `json:"isActive" bson:"isActive"`
	ItemOrder          int           `json:"itemOrder" bson:"itemOrder"`
	ItemAssetModel     string        `json:"itemAssetModel" bson:"itemAssetModel"`
	ItemAssetImageSlot string        `json:"itemAssetImageSlot" bson:"itemAssetImageSlot"`
	ItemAssetVersion   string        `json:"itemAssetVersion" bson:"itemAssetVersion"`
	ItemCategoryID     bson.ObjectId `json:"itemCategoryId" bson:"itemCategoryId"`
	CreateAt           time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified       time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}

// ItemResult model.
type ItemResult struct {
	ID                 bson.ObjectId `json:"id" bson:"_id,omitempty" `
	ItemNameThai       string        `json:"itemNameThai" bson:"itemNameThai"`
	ItemDetailThai     string        `json:"itemDetailThai" bson:"itemDetailThai"`
	ItemNameEng        string        `json:"itemNameEng" bson:"itemNameEng"`
	ItemDetailEng      string        `json:"itemDetailEng" bson:"itemDetailEng"`
	ItemNameChi1       string        `json:"itemNameChi1" bson:"itemNameChi1"`
	ItemDetailChi1     string        `json:"itemDetailChi1" bson:"itemDetailChi1"`
	ItemNameChi2       string        `json:"itemNameChi2" bson:"itemNameChi2"`
	ItemDetailChi2     string        `json:"itemDetailChi2" bson:"itemDetailChi2"`
	ItemEffect         string        `json:"itemEffect" bson:"itemEffect"`
	Coin               int           `json:"coin" bson:"coin"`
	Diamond            int           `json:"diamond" bson:"diamond"`
	IsActive           bool          `json:"isActive" bson:"isActive"`
	ItemOrder          int           `json:"itemOrder" bson:"itemOrder"`
	ItemAssetModel     string        `json:"itemAssetModel" bson:"itemAssetModel"`
	ItemAssetImageSlot string        `json:"itemAssetImageSlot" bson:"itemAssetImageSlot"`
	ItemAssetVersion   string        `json:"itemAssetVersion" bson:"itemAssetVersion"`
	ItemCategoryID     bson.ObjectId `json:"itemCategoryId" bson:"itemCategoryId"`
	ItemCategory       ItemCategory  `json:"itemCategory" bson:"itemCategory"`
	CreateAt           time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified       time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll fun
func (c Item) ValidateAll() error {
	return validation.ValidateStruct(&c,
		// validation.Field(&c.ItemName, validation.Required, validation.Length(2, 64)),
		validation.Field(&c.Coin, validation.Required),
		validation.Field(&c.Diamond, validation.Required),
		validation.Field(&c.ItemAssetModel, validation.Required),
		validation.Field(&c.ItemAssetImageSlot, validation.Required),
		validation.Field(&c.ItemAssetVersion, validation.Required),
		validation.Field(&c.ItemCategoryID, validation.Required),
	)
}

// ValidateForUpdate fun
func (c Item) ValidateForUpdate() error {
	return validation.ValidateStruct(&c) // validation.Field(&c.ItemName, validation.Length(2, 64)),

}
