package models

import (
	"github.com/globalsign/mgo/bson"

	"github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// ZoneType model.
type ZoneType struct {
	ID           bson.ObjectId `json:"id" bson:"_id,omitempty" `
	ZoneTypeName string        `json:"zoneTypeName" bson:"zoneTypeName"`
	IsAtive      bool          `json:"isAtive" bson:"isAtive"`
	ZoneTypeID   int           `json:"zoneTypeId" bson:"zoneTypeId"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll for check all keys require.
func (z ZoneType) ValidateAll() error {
	return validation.ValidateStruct(&z,
		validation.Field(&z.ZoneTypeName, validation.Required, validation.Length(2, 32)),
		validation.Field(&z.ZoneTypeID, validation.NotNil),
		validation.Field(&z.IsAtive, validation.NotNil),
	)
}
