package models

import (
	"github.com/globalsign/mgo/bson"

	"github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// MonsterDefault model.
type MonsterDefault struct {
	ID                        bson.ObjectId   `json:"id" bson:"_id,omitempty" `
	MDefaultName              []string        `json:"mDefaultName" bson:"mDefaultName"`
	MDefaultTypeID            int             `json:"mDefaultTypeId" bson:"mDefaultTypeId"`
	MDefaultJobCategories     []bson.ObjectId `json:"mDefaultJobCategories" bson:"mDefaultJobCategories"`
	MDefaultJobLevel          int             `json:"mDefaultJobLevel" bson:"mDefaultJobLevel"`
	MDefaultGroupID           int             `json:"mDefaultGroupId" bson:"mDefaultGroupId"`
	MDefaultGroupLevel        int             `json:"mDefaultGroupLevel" bson:"mDefaultGroupLevel"`
	MDefaultTextDetail        []string        `json:"mDefaultTextDetail" bson:"mDefaultTextDetail"`
	MDefaultAssetModelAndroid string          `json:"mDefaultAssetModelAndroid" bson:"mDefaultAssetModelAndroid"`
	MDefaultAssetModelIOS     string          `json:"mDefaultAssetModelIOS" bson:"mDefaultAssetModelIOS"`
	MDefaultAssetImageSlot    string          `json:"mDefaultAssetImageSlot1" bson:"mDefaultAssetImageSlot1"`
	MDefaultAssetImageSlot2   string          `json:"mDefaultAssetImageSlot2" bson:"mDefaultAssetImageSlot2"`
	MDefaultAssetImageSlot3   string          `json:"mDefaultAssetImageSlot3" bson:"mDefaultAssetImageSlot3"`

	MDefaultAssetVersion int `json:"mDefaultAssetVersion" bson:"mDefaultAssetVersion"`
	MDefaultOrder        int `json:"mDefaultOrder" bson:"mDefaultOrder"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll use for validate all key of MonsterDefault.
func (m MonsterDefault) ValidateAll() error {
	return validation.ValidateStruct(&m,
		//validation.Field(&m.MDefaultName, validation.Required, validation.Length(2, 128)),
		validation.Field(&m.MDefaultTypeID, validation.Required),
		validation.Field(&m.MDefaultJobCategories, validation.Required, validation.Length(0, 128)),
		validation.Field(&m.MDefaultJobLevel, validation.Required),
		validation.Field(&m.MDefaultGroupID, validation.Required),
		validation.Field(&m.MDefaultGroupLevel, validation.Required),
		validation.Field(&m.MDefaultTextDetail, validation.Length(2, 128)), // Optional.
		//validation.Field(&m.MDefaultAssetModel, validation.Required, validation.Length(1, 128)),
		validation.Field(&m.MDefaultAssetImageSlot, validation.Required, validation.Length(1, 128)),
		validation.Field(&m.MDefaultAssetVersion, validation.Required),
	)
}

// ValidateRule for non require fields.
func (m MonsterDefault) ValidateRule() error {
	return validation.ValidateStruct(&m,
		//	validation.Field(&m.MDefaultName, validation.Length(2, 128)),
		validation.Field(&m.MDefaultJobCategories, validation.Length(0, 128)),
		validation.Field(&m.MDefaultTextDetail, validation.Length(2, 128)),
		//validation.Field(&m.MDefaultAssetModel, validation.Length(1, 128)),
		validation.Field(&m.MDefaultAssetImageSlot, validation.Length(1, 128)),
	)
}
