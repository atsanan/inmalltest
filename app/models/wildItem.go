package models

import (
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

//WildItem model.
type WildItem struct {
	ID                 bson.ObjectId `json:"id" bson:"_id,omitempty"`
	ItemID             bson.ObjectId `json:"itemId" bson:"itemId"`
	IsReachDateTime    bool          `json:"isReachDateTime"`
	IsActive           bool          `json:"isActive" bson:"isActive"`
	StartDateTime      time.Time     `json:"startDateTime" bson:"startDateTime"`
	ExpiredDateTime    time.Time     `json:"expiredDateTime" bson:"expiredDateTime"`
	Count              int           `json:"count" bson:"count"`
	ToMall             bson.ObjectId `json:"toMall" bson:"toMall"`
	ToMallFloor        bson.ObjectId `json:"toMallFloor"  bson:"toMallFloor"`
	ToShop             bson.ObjectId `json:"toShop" bson:"toShop"`
	ZoneTypeID         int           `json:"zoneTypeId" bson:"zoneTypeId"`
	CouponGiftID       string        `json:"couponGiftId" bson:"couponGiftId"`
	CouponGiftNameEng  string        `json:"couponGiftNameEng" bson:"couponGiftNameEng"`
	CouponGiftNameThai string        `json:"couponGiftNameThai" bson:"couponGiftNameThai"`
	CouponGiftNameChi1 string        `json:"couponGiftNameChi1" bson:"couponGiftNameChi1"`
	CouponGiftNameChi2 string        `json:"couponGiftNameChi2" bson:"couponGiftNameChi2"`
	IsGoldenMinutes    bool          `json:"isGoldenMinutes" bson:"isGoldenMinutes"`
	IsToItemPlayer    int          `json:"isToItemPlayer" bson:"isToItemPlayer"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll use for validate all key of WildMonster.
func (m WildItem) ValidateAll() error {
	return validation.ValidateStruct(&m,
		//	validation.Field(&m.ItemID, validation.Required),
		validation.Field(&m.ToShop, validation.Required),
		validation.Field(&m.ToMallFloor, validation.Required),
		validation.Field(&m.Count, validation.Required, validation.NotNil),
		validation.Field(&m.ZoneTypeID, validation.Required, validation.NotNil),
		validation.Field(&m.ExpiredDateTime, validation.Required),
		validation.Field(&m.StartDateTime, validation.Required),
		validation.Field(&m.IsReachDateTime, validation.NotNil),
		validation.Field(&m.IsActive, validation.NotNil),
		validation.Field(&m.IsGoldenMinutes, validation.NotNil),
	)
}
