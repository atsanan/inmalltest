package models

import (
	"github.com/globalsign/mgo/bson"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// Shop model.
type Shop struct {
	ID             bson.ObjectId `json:"id" bson:"_id,omitempty" `
	ShopNameEng    string        `json:"shopNameEng" bson:"shopNameEng"`
	ShopNameThai   string        `json:"shopNameThai" bson:"shopNameThai"`
	ShopNameChi1   string        `json:"shopNameChi1" bson:"shopNameChi1"`
	ShopNameChi2   string        `json:"shopNameChi2" bson:"shopNameChi2"`
	ShopDetailEng  string        `json:"shopDetail" bson:"shopDetailEng"`
	ShopDetailThai string        `json:"shopDetailThai" bson:"shopDetailThai"`
	ShopDetailChi1 string        `json:"shopDetailChi1" bson:"shopDetailChi1"`
	ShopDetailChi2 string        `json:"shopDetailChi2" bson:"shopDetailChi2"`
	ShopTel        string        `json:"shopTel" bson:"shopTel"`
	ShopURL        string        `json:"shopUrl" bson:"shopUrl"`
	ShopCategoryID string        `json:"shopCategoryId" bson:"shopCategoryId"`
	ShopCode       string        `json:"shopCode" bson:"shopCode"`
	ShopFloor      int           `json:"shopFloor" bson:"shopFloor"`
	Location       GeoJSON       `json:"mapReach" bson:"mapReach"`
	MapReach       bson.ObjectId       `json:"location" bson:"location"`
	MallFloorID    bson.ObjectId `json:"mallFloorId" bson:"mallFloorId"`
	ShopAssetID    bson.ObjectId `json:"shopAssetId" bson:"shopAssetId"`
	FilenameLogo1  string        `json:"filenameLogo1" bson:"filenameLogo1"`
	FilenameLogo2  string        `json:"filenameLogo2" bson:"filenameLogo2"`
	ShopAdsID      []string      `json:"shopAdsId" bson:"shopAdsId"`
	IsActive       bool          `json:"isActive" bson:"isActive"`
	ShopModelID     bson.ObjectId         `json:"shopModelId" bson:"shopModelId"`
	// ShopID      int           `json:"shopId"`
	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}
