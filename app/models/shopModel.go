package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"
)

// ShopModels model.
type ShopModels struct {
	ID                    bson.ObjectId `json:"id" bson:"_id,omitempty"`
	ShopModeNameEng       string        `json:"shopModeNameEng" bson:"shopModeNameEng"`
	ShopModeNameThai      string        `json:"shopModeNameThai" bson:"shopModeNameThai"`
	ShopModeNameChi1      string        `json:"shopModeNameChi1" bson:"shopModeNameChi1"`
	ShopModeNameChi2      string        `json:"shopModeNameChi2" bson:"shopModeNameChi2"`
	ShopModeDetailEng     string        `json:"shopModeDetailEng" bson:"shopModeDetailEng"`
	ShopModeDetailThai    string        `json:"shopModeDetailThai" bson:"shopModeDetailThai"`
	ShopModeDetailChi1    string        `json:"shopModeDetailChi1" bson:"shopModeDetailChi1"`
	ShopModeDetailChi2    string        `json:"shopModeDetailChi2" bson:"shopModeDetailChi2"`
	ShopModeAssertMode    string        `json:"shopModeAssertMode" bson:"shopModeAssertMode"`
	ShopModeAssertVersion string        `json:"shopModeAssertVersion" bson:"shopModeAssertVersion"`
	CreateAt              time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified          time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}
