package models

import (
	"github.com/globalsign/mgo/bson"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// AuthenType model.
type AuthenType struct {
	ID             bson.ObjectId `json:"id" bson:"_id,omitempty" `
	AuthenTypeID   int           `json:"authenTypeId"`
	AuthenTypeName string        `json:"authenTypeName"`
	IsActive       bool          `json:"isActive"`
	CreateAt       time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified   time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}
