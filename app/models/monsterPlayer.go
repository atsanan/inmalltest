package models

import (
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// MonsterPlayer model.
type MonsterPlayer struct {
	ID                       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	MDefaultID               bson.ObjectId `json:"mDefaultId" bson:"mDefaultId,omitempty"`
	PlayerID                 bson.ObjectId `json:"playerId" bson:"playerId"`
	MPlayerName              string        `json:"mPlayerName" bson:"mPlayerName"`
	MPlayerNewDateTime       time.Time     `json:"mPlayerNewDateTime" bson:"mPlayerNewDateTime"`
	MPlayerLastDateTime      time.Time     `json:"mPlayerLastDateTime" bson:"mPlayerLastDateTime"`
	MPlayerHabit             int           `json:"mPlayerHabit" bson:"mPlayerHabit"`
	MPlayerStatusFood        int           `json:"mPlayerStatusFood" bson:"mPlayerStatusFood"`
	MPlayerStatusHappiness   int           `json:"mPlayerStatusHappiness" bson:"mPlayerStatusHappiness"`
	MPlayerStatusHealth      int           `json:"mPlayerStatusHealth" bson:"mPlayerStatusHealth"`
	MPlayerFoodDatetime      time.Time     `json:"mPlayerFoodDatetime" bson:"mPlayerFoodDatetime"`
	MPlayerHealthDatetime    time.Time     `json:"mPlayerHealthDatetime" bson:"mPlayerHealthDatetime"`
	ChannelID                bson.ObjectId `json:"channelId" bson:"channelId,omitempty"`
	MPlayerFoodID            bson.ObjectId `json:"mPlayerFoodId" bson:"mPlayerFoodId,omitempty"`
	MPlayerHealthID          bson.ObjectId `json:"mPlayerHealthId" bson:"mPlayerHealthId,omitempty"`
	MPlayerHappyID           bson.ObjectId `json:"mPlayerHappyId" bson:"mPlayerHappyId,omitempty"`
	MPlayerHappinessDatetime time.Time     `json:"mPlayerHappinessDatetime" bson:"mPlayerHappinessDatetime"`
	MPlayerMessage           string        `json:"mPlayerMessage" bson:"mPlayerMessage"`
	MPlayerStatusQuest1      int           `json:"mPlayerStatusQuest1" bson:"mPlayerStatusQuest1"`
	MPlayerQuest1Datetime    time.Time     `json:"mPlayerQuest1Datetime" bson:"mPlayerQuest1Datetime"`
	MPlayerStatusQuest2      int           `json:"mPlayerStatusQuest2" bson:"mPlayerStatusQuest2"`
	MPlayerQuest2Datetime    time.Time     `json:"mPlayerQuest2Datetime" bson:"mPlayerQuest2Datetime"`
	MPlayerStatusQuest3      int           `json:"mPlayerStatusQuest3" bson:"mPlayerStatusQuest3"`
	MPlayerQuest3Datetime    time.Time     `json:"mPlayerQuest3Datetime" bson:"mPlayerQuest3Datetime"`
	MPlayerStatusExp         int           `json:"mPlayerStatusExp" bson:"mPlayerStatusExp"`
	MPlayerExpDatetime       time.Time     `json:"mPlayerExpDatetime" bson:"mPlayerExpDatetime"`
	//MDefaultID               bson.ObjectId `json:"mDefaultId" bson:"mDefaultId"`
	// MPlayerID              int           `json:"mPlayerId"`
	CreateAt     time.Time `json:"createAt" bson:"createAt"`
	LastModified time.Time `json:"lastModified" bson:"lastModified"`
}

// ValidateRule for non require fields.
func (m MonsterPlayer) ValidateRule() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.MPlayerName, validation.Length(2, 128)),
	)
}
