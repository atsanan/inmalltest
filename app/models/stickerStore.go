package models

import (
	"github.com/globalsign/mgo/bson"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// StickerStore model.
type StickerStore struct {
	ID                     bson.ObjectId `json:"id" bson:"_id,omitempty"`
	StickerStoreNameEng    string        `json:"stickerStoreNameEng" bson:"stickerStoreNameEng"`
	StickerStoreNameThai   string        `json:"stickerStoreNameThai" bson:"stickerStoreNameThai"`
	StickerStoreNameChi1   string        `json:"stickerStoreNameChi1" bson:"stickerStoreNameChi1"`
	StickerStoreNameChi2   string        `json:"stickerStoreNameChi2" bson:"stickerStoreNameChi2"`
	StickerStoreDetailEng  string        `json:"stickerStoreDetailEng" bson:"stickerStoreDetailEng"`
	StickerStoreDetailThai string        `json:"stickerStoreDetailThai" bson:"stickerStoreDetailThai"`
	StickerStoreDetailChi1 string        `json:"stickerStoreDetailChi1" bson:"stickerStoreDetailChi1"`
	StickerStoreDetailChi2 string        `json:"stickerStoreDetailChi2" bson:"stickerStoreDetailChi2"`
	StickerStoreImage      string        `json:"stickerStoreImage" bson:"stickerStoreImage"`
	StickerStoreLevel      int           `json:"stickerStoreLevel" bson:"stickerStoreLevel"`
	Coin                   int           `json:"coin" bson:"coin"`
	Diamond                int           `json:"diamond" bson:"diamond"`
	IsActive               bool          `json:"isActive" bson:"isActive"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateStickerStore is default validation rule.
func (s StickerStore) ValidateStickerStore() error {
	return validation.ValidateStruct(&s,
		// validation.Field(&s.StickerStoreName, validation.Required, validation.Length(2, 128)),
		// validation.Field(&s.StickerStoreDetail, validation.Length(2, 2048)), // optional.
		validation.Field(&s.StickerStoreLevel, validation.Required),
		validation.Field(&s.StickerStoreImage, validation.Required, is.URL),

		validation.Field(&s.Coin, validation.NotNil),
		validation.Field(&s.Diamond, validation.NotNil),
		validation.Field(&s.IsActive, validation.NotNil),
	)
}

// ValidateType is optinals validation rule.
func (s StickerStore) ValidateType() error {
	return validation.ValidateStruct(&s,
		// validation.Field(&s.StickerStoreName, validation.Length(2, 128)),
		// validation.Field(&s.StickerStoreDetail, validation.Length(2, 2048)),
		// // validation.Field(&s.StickerStoreLevel),
		validation.Field(&s.StickerStoreImage, is.URL),
		// validation.Field(&s.Coin),
		// validation.Field(&s.Diamond),
		// validation.Field(&s.IsActive),
	)
}
