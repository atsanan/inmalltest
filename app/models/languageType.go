package models

import (
	"github.com/globalsign/mgo/bson"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// LanguageType model.
type LanguageType struct {
	ID           bson.ObjectId `json:"id" bson:"_id,omitempty" `
	LanguageID   int           `json:"languageId"`
	LanguageName string        `json:"languageName"`
	IsAtive      bool          `json:"isAtive"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}
