package models

import (
	"github.com/globalsign/mgo/bson"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// Player model.
type Player struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty" `
	UserID          bson.ObjectId `json:"userId" bson:"userId"`
	TeamPlayerID    int           `json:"teamPlayerId" bson:"teamPlayerId"`
	PlayerName      string        `json:"playerName" bson:"playerName"`
	Coin            int           `json:"coin" bson:"coin"`
	Diamond         int           `json:"diamond" bson:"diamond"`
	CostumeSelectID bson.ObjectId           `json:"costumeSelectId" bson:"costumeSelectId"`
	CostumeList     []bson.ObjectId         `json:"costumeList" bson:"costumeList"`
	Gender          string        `json:"gender" bson:"gender"`
	PushMessagesAccessToken string        `json:"pushMessagesAccessToken" bson:"pushMessagesAccessToken"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateCoinDiamond for validate coin, diamond.
func (p Player) ValidateCoinDiamond() error {
	return validation.ValidateStruct(&p,
		// Street cannot be empty, and the length must between 5 and 50
		validation.Field(&p.Coin, is.Int),
		// City cannot be empty, and the length must between 5 and 50
		validation.Field(&p.Diamond, is.Int),
	)
}

// ValidateAll use for validate all keys except userId, Id.
func (p Player) ValidateAll() error {
	return validation.ValidateStruct(&p,
		validation.Field(&p.PlayerName, validation.Length(2, 32)),
		validation.Field(&p.CostumeList, validation.Length(0, 100)),
		validation.Field(&p.Gender, validation.Length(0, 16)),
	)
}

func (p Player) ValidateAllCostumeList() error {
	return validation.ValidateStruct(&p,
		validation.Field(&p.CostumeList, validation.Required),
	)
}
