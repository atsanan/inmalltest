package models

import (
	"github.com/globalsign/mgo/bson"

	"github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// TeamPlayer model.
type TeamPlayer struct {
	ID                   bson.ObjectId `json:"id" bson:"_id,omitempty" `
	TeamPlayerNameEng    string        `json:"teamPlayerNameEng" bson:"teamPlayerNameEng"`
	TeamPlayerNameThai   string        `json:"teamPlayerNameThai" bson:"teamPlayerNameThai"`
	TeamPlayerNameChi1   string        `json:"teamPlayerNameChi1" bson:"teamPlayerNameChi1"`
	TeamPlayerNameChi2   string        `json:"teamPlayerNameChi2" bson:"teamPlayerNameChi2"`
	TeamPlayerDetailEng  string        `json:"teamPlayerDetailEng" bson:"teamPlayerDetailEng"`
	TeamPlayerDetailThai string        `json:"teamPlayerDetailThai" bson:"teamPlayerDetailThai"`
	TeamPlayerDetailChi1 string        `json:"teamPlayerDetailChi1" bson:"teamPlayerDetailChi1"`
	TeamPlayerDetailChi2 string        `json:"teamPlayerDetailChi2" bson:"teamPlayerDetailChi2"`
	CreateAt             time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified         time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll use for validate all keys.
func (t TeamPlayer) ValidateAll() error {
	return validation.ValidateStruct(&t) // validation.Field(&t.TeamPlayerName, validation.Required, validation.Length(2, 32)),
	//validation.Field(&t.TeamPlayerDetail, validation.Length(2, 32)),

}

// ValidateSome use for validate some keys.
func (t TeamPlayer) ValidateSome() error {
	return validation.ValidateStruct(&t) // validation.Field(&t.TeamPlayerName, validation.Length(2, 32)),
	//validation.Field(&t.TeamPlayerDetail, validation.Length(2, 32)),

}
