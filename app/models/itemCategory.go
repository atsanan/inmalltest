package models

import (
	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// ItemCategory model.
type ItemCategory struct {
	ID                bson.ObjectId `json:"id" bson:"_id,omitempty" `
	ItemCategoryName  string        `json:"itemCategoryName" bson:"itemCategoryName"`
	ItemCategoryOrder int           `json:"itemCategoryOrder" bson:"itemCategoryOrder"`

	// ItemCategoryID    int           `json:"itemCategoryId"`
	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll fun
func (c ItemCategory) ValidateAll() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ItemCategoryName, validation.Required, validation.Length(2, 64)),
		validation.Field(&c.ItemCategoryOrder, validation.NotNil),
	)
}
