package models

import (
	"github.com/globalsign/mgo/bson"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// JoinChannel model.
type JoinChannel struct {
	ID           bson.ObjectId `json:"id" bson:"_id,omitempty" `
	ChannelID    bson.ObjectId `json:"channelId" bson:"channelId"`
	PlayerID     bson.ObjectId `json:"playerId" bson:"playerId"`
	MPlayerID    bson.ObjectId `json:"mPlayerId" bson:"mPlayerId"`
	JoinDateTime time.Time     `json:"joinDateTime" bson:"joinDateTime"`
	// JoinchannelID int           `json:"joinchannelId"`
	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}
