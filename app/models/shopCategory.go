package models

import (
	"github.com/globalsign/mgo/bson"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// ShopCategory model.
type ShopCategory struct {
	ID bson.ObjectId `json:"id" bson:"_id,omitempty" `
	//	ShopCategoryID   int           `json:"shopCategoryId"`
	ShopCategoryNameEng  string    `json:"shopCategoryNameEng" bson:"shopCategoryNameEng"`
	ShopCategoryNameThai string    `json:"shopCategoryNameThai" bson:"shopCategoryNameThai"`
	ShopCategoryNameChi1 string    `json:"shopCategoryNameChi1" bson:"shopCategoryNameChi1"`
	ShopCategoryNameChi2 string    `json:"shopCategoryNameChi2" bson:"shopCategoryNameChi2"`
	CreateAt             time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified         time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}
