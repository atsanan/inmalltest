package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"
)

//MailBox model.
type MailBox struct {
	ID            bson.ObjectId   `json:"_id" bson:"_id,omitempty"`
	ListUser      []bson.ObjectId `json:"listUser" bson:"listUser"`
	ListIdMailBox []bson.ObjectId `json:"listIdMailBox" bson:"listIdMailBox"`
	Title         string          `json:"title" bson:"title"`
	Detail        string          `json:"detail" bson:"detail"`
	LinkListname  string          `json:"linklistname" bson:"linklistname"`
	CreateAt      time.Time       `json:"createAt" bson:"createAt,omitempty"`
}
