package models

import (
	"github.com/globalsign/mgo/bson"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// ShopAds model.
type ShopAds struct {
	ID bson.ObjectId `json:"id" bson:"_id,omitempty" `
	//ShopAdsID   int           `json:"shopAdsId"`
	ShopAdsNameEng    string   `json:"shopAdsNameEng" bson:"shopAdsNameEng"`
	ShopAdsNameThai   string   `json:"shopAdsNameThai" bson:"shopAdsNameThai"`
	ShopAdsNameChi1   string   `json:"shopAdsNameChi1" bson:"shopAdsNameChi1"`
	ShopAdsNameChi2   string   `json:"shopAdsNameChi2" bson:"shopAdsNameChi2"`
	ShopAdsImage      string   `json:"shopAdsImage" bson:"shopAdsImage"`
	ShopAdsImageLists []string `json:"shopAdsImageLists" bson:"shopAdsImageLists"`
	//ShopID          int    `json:"shopId"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}
