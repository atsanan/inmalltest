package models

import (
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"

	//"github.com/go-ozzo/ozzo-validation"

	"time"
)

// ItemPlayer model.
type ItemPlayer struct {
	ID             bson.ObjectId `json:"id" bson:"_id,omitempty"`
	PlayerID       bson.ObjectId `json:"playerId" bson:"playerId"`
	ItemID         bson.ObjectId `json:"itemId" bson:"itemId"`
	Count          int           `json:"count" bson:"count"`
	IsCoupon       bool          `json:"isCoupon" bson:"isCoupon"`
	CouponGiftID   string        `json:"couponGiftId" bson:"couponGiftId"`
	CouponHashKey  string        `json:"couponHashKey" bson:"couponHashKey"`
	CouponQRImage  string        `json:"couponQRImage" bson:"couponQRImage"`
	CouponPassword string        `json:"couponPassword" bson:"couponPassword"`
	IsActive       bool          `json:"isActive" bson:"isActive"`
	CreateAt       time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified   time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}

// ItemPlayerResult model.
type ItemPlayerResult struct {
	ID             bson.ObjectId `json:"id" bson:"_id,omitempty"`
	PlayerID       bson.ObjectId `json:"playerId" bson:"playerId"`
	ItemID         bson.ObjectId `json:"itemId" bson:"itemId"`
	Count          int           `json:"count" bson:"count"`
	IsCoupon       bool          `json:"isCoupon" bson:"isCoupon"`
	CouponGiftID   string        `json:"CouponGiftId" bson:"CouponGiftId"`
	CouponHashKey  string        `json:"couponHashKey" bson:"couponHashKey"`
	CouponQRImage  string        `json:"couponQRImage" bson:"couponQRImage"`
	CouponPassword string        `json:"couponPassword" bson:"couponPassword"`
	CreateAt       time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified   time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
	Item           ItemResult    `json:"item" bson:"item"`
}

// ValidateAll use for validate all key of ItemPlayer.
func (c ItemPlayer) ValidateAll() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.PlayerID, validation.Required),
		validation.Field(&c.ItemID, validation.Required),
		validation.Field(&c.Count, validation.Required),
		validation.Field(&c.CouponQRImage),
	)
}


type ListIdItemPlayer struct {
	Id []bson.ObjectId `json:"listIdItemPlayer" bson:"listIdItemPlayer"`
}
