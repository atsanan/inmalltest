package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
	// "github.com/go-ozzo/ozzo-validation/is"
)

// Chat model.
type Chat struct {
	ID           bson.ObjectId `json:"id" bson:"_id,omitempty"`
	PlayerID     bson.ObjectId `json:"playerId" bson:"playerId"`
	FriendID     bson.ObjectId `json:"friendId" bson:"friendId"`
	Message      string        `json:"message" bson:"message"`
	MessageCode  string        `json:"messageCode" bson:"messageCode"`
	CreateAt     time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}
