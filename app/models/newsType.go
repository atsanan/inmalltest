package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

//NewsType sturct
type NewsType struct {
	ID                 bson.ObjectId `json:"id" bson:"_id,omitempty"`
	NewsTypeNameEng    string        `json:"newsTypeNameEng" bson:"newsTypeNameEng"`
	NewsTypeNameThai   string        `json:"newsTypeNameThai" bson:"newsTypeNameThai"`
	NewsTypeNameChi1   string        `json:"newsTypeNameChi1" bson:"newsTypeNameChi1"`
	NewsTypeNameChi2   string        `json:"newsTypeNameChi2" bson:"newsTypeNameChi2"`
	NewsTypeDetailEng  string        `json:"newsTypeDetailEng" bson:"newsTypeNameEng"`
	NewsTypeDetailThai string        `json:"newsTypeDetailThai" bson:"newsTypeNameThai"`
	NewsTypeDetailChi1 string        `json:"newsTypeDetailChi1" bson:"newsTypeNameChi1"`
	NewsTypeDetailChi2 string        `json:"newsTypeDetailChi2" bson:"newsTypeNameChi2"`
	CreateAt           time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified       time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}
