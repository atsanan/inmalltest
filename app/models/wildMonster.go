package models

import (
	"github.com/globalsign/mgo/bson"

	"github.com/go-ozzo/ozzo-validation"

	"time"
)

// WildMonster model.
type WildMonster struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty" `
	MDefaultID      bson.ObjectId `json:"mDefaultId" bson:"mDefaultId,omitempty"`
	ToShop          string        `json:"toShop" bson:"toShop"`
	ToMallFloor     string        `json:"toMallFloor" bson:"toMallFloor"`
	Count           int           `json:"count" bson:"count"`
	ZoneTypeID      int           `json:"zoneTypeId" bson:"zoneTypeId"`
	ExpiredDateTime time.Time     `json:"expiredDateTime" bson:"expiredDateTime"`
	StartDateTime   time.Time     `json:"startDateTime" bson:"startDateTime"`
	IsReachDateTime bool          `json:"isReachDateTime" bson:"isReachDateTime"`
	IsActive        bool          `json:"isActive" bson:"isActive"`
	IsPickToBag     bool          `json:"isPickToBag" bson:"isPickToBag"`
	IsGoldenMinutes bool          `json:"isGoldenMinutes" bson:"isGoldenMinutes"`
	ItemID          bson.ObjectId `json:"itemId" bson:"itemId"`
	// MWildID         int           `json:"mWildId" bson:"mWildId"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

type WildMonsterReturn struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty" `
	MDefaultID      bson.ObjectId `json:"mDefaultId" bson:"mDefaultId,omitempty"`
	ToMall          bson.ObjectId `json:"toMall" bson:"toMall"`
	ToShop          bson.ObjectId `json:"toShop" bson:"toShop"`
	ToMallFloor     bson.ObjectId `json:"toMallFloor" bson:"toMallFloor"`
	Count           int           `json:"count" bson:"count"`
	ZoneTypeID      bson.ObjectId `json:"zoneTypeId" bson:"zoneTypeId"`
	ExpiredDateTime time.Time     `json:"expiredDateTime" bson:"expiredDateTime"`
	StartDateTime   time.Time     `json:"startDateTime" bson:"startDateTime"`
	IsReachDateTime bool          `json:"isReachDateTime" bson:"isReachDateTime"`
	IsActive        bool          `json:"isActive" bson:"isActive"`
	IsPickToBag     bool          `json:"isPickToBag" bson:"isPickToBag"`
	IsGoldenMinutes bool          `json:"isGoldenMinutes" bson:"isGoldenMinutes"`
	ItemID          bson.ObjectId `json:"itemId" bson:"itemId"`
	PicPercent      int           `json:"picPercent" bson:"picPercent"`
	ItemStock       int           `json:"itemStock" bson:"itemStock"`
	// MWildID         int           `json:"mWildId" bson:"mWildId"`
	MonsterDefault MonsterDefault `json:"monsterDefault" bson:"monsterDefault"`
	CreateAt       time.Time      `json:"createAt" bson:"createAt,omitempty"`
	LastModified   time.Time      `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll use for validate all key of WildMonster.
func (m WildMonster) ValidateAll() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.MDefaultID, validation.Required),
		validation.Field(&m.Count, validation.Required),
		validation.Field(&m.ZoneTypeID, validation.Required),

		validation.Field(&m.ExpiredDateTime, validation.Skip, validation.Date(time.RFC3339)),
		validation.Field(&m.StartDateTime, validation.Skip, validation.Date(time.RFC3339)),
		validation.Field(&m.IsReachDateTime, validation.NotNil),
		validation.Field(&m.IsActive, validation.NotNil),
		validation.Field(&m.IsGoldenMinutes, validation.NotNil),
	)
}
