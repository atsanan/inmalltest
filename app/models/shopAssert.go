package models

import (
	"github.com/globalsign/mgo/bson"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// ShopAsserts model.
type ShopAsserts struct {
	ID bson.ObjectId `json:"id" bson:"_id,omitempty" `
	//	ShopAssetID      int           `json:"shopAssetId" bson:"shopAssetId"`
	ShopAssertName       string        `json:"shopAssertName" bson:"shopAssertName"`
	ShopAssertNameEng    string        `json:"shopAssertNameEng" bson:"shopAssertNameEng"`
	ShopAssertNameThai   string        `json:"shopAssertNameThai" bson:"shopAssertNameThai"`
	ShopAssertNameChi1   string        `json:"shopAssertNameChi1" bson:"shopAssertNameChi1"`
	ShopAssertNameChi2   string        `json:"shopAssertNameChi2" bson:"shopAssertNameChi2"`
	ShopAssertDetail     string        `json:"shopAssertDetail" bson:"shopAssertDetail"`
	ShopAssertDetailEng  string        `json:"shopAssertDetailEng" bson:"shopAssertDetailEng"`
	ShopAssertDetailThai string        `json:"shopAssertDetailThai" bson:"shopAssertDetailThai"`
	ShopAssertDetailChi1 string        `json:"shopAssertDetailChi1" bson:"shopAssertDetailChi1"`
	ShopAssertDetailChi2 string        `json:"shopAssertDetailChi2" bson:"shopAssertDetailChi2"`
	ShopAssertModel      string        `json:"shopAssertModel" bson:"shopAssertModel"`
	ShopAssertVersion    int           `json:"shopAssertVersion" bson:"shopAssertVersion"`
	ShopCategoryID       bson.ObjectId `json:"shopCategoryId" bson:"shopCategoryId"`
	FileNameLogo1        string        `json:"fileNameLogo1" bson:"fileNameLogo1"`
	FileNameLogo2        string        `json:"fileNameLogo2" bson:"fileNameLogo2"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}
