package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

//News sturct
type News struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty"`
	NewsNameEng     string        `json:"newsNameEng" bson:"newsNameEng"`
	NewsNameThai    string        `json:"newsNameThai" bson:"newsNameThai"`
	NewsNameChi1    string        `json:"newsNameChi1" bson:"newsNameChi1"`
	NewsNameChi2    string        `json:"newsNameChi2" bson:"newsNameChi2"`
	NewsDetailEng   string        `json:"newsDetailEng" bson:"newsDetailEng"`
	NewsDetailThai  string        `json:"newsDetailThai" bson:"newsDetailThai"`
	NewsDetailChi1  string        `json:"newsDetailChi1" bson:"newsDetailChi1"`
	NewsDetailChi2  string        `json:"newsDetailChi2" bson:"newsDetailChi2"`
	NewsActive      bool          `json:"newsActive" bson:"newsActive"`
	Gender          string        `json:"gender" bson:"gender"`
	StartRangeAge   int           `json:"startRangeAge" bson:"startRangeAge"`
	EndRangeAge     int           `json:"endRangeAge" bson:"endRangeAge"`
	FilenameImage1  string        `json:"filenameImage1" bson:"filenameImage1"`
	FilenameImage2  string        `json:"filenameImage2" bson:"filenameImage2"`
	ShopID          bson.ObjectId `json:"shopId" bson:"shopId"`
	Location        GeoJSON       `json:"location" bson:"location"`
	WildItemID      bson.ObjectId `json:"wildItemId" bson:"wildItemId"`
	NewsTypeID      bson.ObjectId `json:"newsTypeId" bson:"newsTypeId"`
	ExpiredDateTime time.Time     `json:"expiredDateTime" bson:"expiredDateTime"`
	StartDateTime   time.Time     `json:"startDateTime" bson:"startDateTime"`
	IsReachDateTime bool          `json:"isReachDateTime" bson:"isReachDateTime"`
	CreateAt        time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified    time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}
