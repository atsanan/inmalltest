package models

import (
	"github.com/globalsign/mgo/bson"
	"github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// Channel model.
type Channel struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty" `
	ChannelName     string        `json:"channelName" bson:"channelName"`
	ChannelPassword string        `json:"channelPassword" bson:"channelPassword"`
	IsAuto          bool          `json:"isAuto" bson:"isAuto"`
	ShopID          bson.ObjectId `json:"shopId" bson:"shopId,omitempty"`
	MaxCapacity     int           `json:"maxCapacity" bson:"maxCapacity"`
	Capacity        int           `json:"capacity" bson:"capacity"`
	// ChannelID       int           `json:"channelId" bson:"channelId"`
	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll use for create new costume.
func (c Channel) ValidateAll() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ChannelName, validation.Required, validation.Length(2, 64)),
		validation.Field(&c.ChannelPassword, validation.Required, validation.Length(4, 32)),
	)
}
