package models

import (
	"github.com/globalsign/mgo/bson"

	"github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// MonsterSticker model.
type MonsterSticker struct {
	ID               bson.ObjectId `json:"id" bson:"_id,omitempty"`
	StickerStoreID   bson.ObjectId `json:"stickerStoreId" bson:"stickerStoreId"`
	SendFromPlayerID bson.ObjectId `json:"sendFromPlayerId" bson:"sendFromPlayerId"`
	SendToMPlayerID  bson.ObjectId `json:"sendToMPlayerId" bson:"sendToMPlayerId"`
	ChannelID        bson.ObjectId `json:"channelId" bson:"channelId"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidationMonsterSticker for all fields.
func (m MonsterSticker) ValidationMonsterSticker() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.StickerStoreID, validation.Required),
		validation.Field(&m.SendFromPlayerID, validation.Required),
		validation.Field(&m.SendToMPlayerID, validation.Required),
		validation.Field(&m.ChannelID, validation.Required),
	)
}
