package models

import (
	"github.com/globalsign/mgo/bson"

	"github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// Costume model.
type Costume struct {
	ID bson.ObjectId `json:"id" bson:"_id,omitempty" `
	//	CostumeID     int           `json:"costumeId" bson:"costumeId"`
	// CostumeName   string `json:"costumeName" bson:"costumeName"`
	CostumeNameEng    string `json:"costumeNameEng" bson:"costumeNameEng"`
	CostumeDetailEng  string `json:"costumeDetailEng" bson:"costumeDetailEng"`
	CostumeNameThai   string `json:"costumeNameThai" bson:"costumeNameThai"`
	CostumeDetailThai string `json:"costumeDetailThai" bson:"costumeDetailThai"`
	CostumeNameChi1   string `json:"costumeNameChi1" bson:"costumeNameChi1"`
	CostumeDetailChi1 string `json:"costumeDetailChi1" bson:"costumeDetailChi1"`
	CostumeNameChi2   string `json:"costumeNameChi2" bson:"costumeNameChi2"`
	CostumeDetailChi2 string `json:"costumeDetailChi2" bson:"costumeDetailChi2"`

	Coin     int  `json:"coin" bson:"coin"`
	Diamond  int  `json:"diamond" bson:"diamond"`
	IsActive bool `json:"isActive" bson:"isActive"`

	CreateAt     time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll use for create new costume.
func (c Costume) ValidateAll() error {
	return validation.ValidateStruct(&c,
		// validation.Field(&c.CostumeNameThai,
		// 	validation.Required,       // not empty
		// 	validation.Length(2, 128), // length between 5 and 100
		// ),
		// validation.Field(&c.CostumeDetailThai, validation.Length(0, 128)),
		validation.Field(&c.Coin, validation.Required),
		validation.Field(&c.Diamond, validation.Required),
		validation.Field(&c.IsActive, validation.Required),
	)
}
