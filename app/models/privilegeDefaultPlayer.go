package models

import (
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"
)

//PrivilegeDefaultPlayerGroup
type PrivilegeDefaultPlayerGroup struct {
	ID                 bson.ObjectId   `json:"id" bson:"_id,omitempty" `
	PlayerID           bson.ObjectId   `json:"playerId" bson:"playerId"`
	PrivilegeDefaultID []bson.ObjectId `json:"privilegeDefaultId" bson:"privilegeDefaultId"`
}

// ValidateAll use for create new costume.
func (c PrivilegeDefaultPlayerGroup) ValidateAll() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.PlayerID, validation.Required),
		//validation.Field(&c.PrivilegeDefaultID, validation.Required),
	)
}
