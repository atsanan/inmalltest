package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

// Friend model.
type Friend struct {
	ID           bson.ObjectId `json:"id" bson:"_id,omitempty" `
	PlayerID     bson.ObjectId `json:"playerId" bson:"playerId"`
	FriendID     bson.ObjectId `json:"friendId" bson:"friendId"`
	FriendStatus string        `json:"friendStatus" bson:"friendStatus"`
	CreateAt     time.Time     `json:"createAt" bson:"createAt,omitempty"`
	LastModified time.Time     `json:"lastModified" bson:"lastModified,omitempty"`
}
