package models

import (
	"fmt"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	validation "github.com/go-ozzo/ozzo-validation"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"

	"time"
)

// GeoJSON model.
type GeoJSON struct {
	GeoType     string    `json:"type" bson:"type"`
	Coordinates []float32 `json:"coordinates" bson:"coordinates"`
}

// Mall model.
type Mall struct {
	ID bson.ObjectId `json:"id" bson:"_id,omitempty" `

	MallNameEng     string    `json:"mallNameEng" bson:"mallNameEng"`
	MallDetailEng   string    `json:"mallDetailEng" bson:"mallDetailEng"`
	MallAddressEng  string    `json:"mallAddressEng" bson:"mallAddressEng"`
	MallNameThai    string    `json:"mallNameThai" bson:"mallNameThai"`
	MallDetailThai  string    `json:"mallDetailThai" bson:"mallDetailThai"`
	MallAddressThai string    `json:"mallAddressThai" bson:"mallAddressThai"`
	MallNameChi1    string    `json:"mallNameChi1" bson:"mallNameChi1"`
	MallDetailChi1  string    `json:"mallDetailChi1" bson:"mallDetailChi1"`
	MallAddressChi1 string    `json:"mallAddressChi1" bson:"mallAddressChi1"`
	MallNameChi2    string    `json:"mallNameChi2" bson:"mallNameChi2"`
	MallDetailChi2  string    `json:"mallDetailChi2" bson:"mallDetailChi2"`
	MallAddressChi2 string    `json:"mallAddressChi2" bson:"mallAddressChi2"`
	MapOutdoorID    string    `json:"mapOutdoorId" bson:"mapOutdoorId"`
	MapIndoorID     string    `json:"mapIndoorId" bson:"mapIndoorId"`
	IsActive        bool      `json:"isActive" bson:"isActive"`
	Location        GeoJSON   `json:"location" bson:"location"`
	FileNameLogo1   string    `json:"fileNameLogo1" bson:"fileNameLogo1"`
	FileNameLogo2   string    `json:"fileNameLogo2" bson:"fileNameLogo2"`
	CreateAt        time.Time `json:"createAt" bson:"createAt,omitempty"`
	LastModified    time.Time `json:"lastModified" bson:"lastModified,omitempty"`
}

// ValidateAll fun
func (c Mall) ValidateAll() error {
	return validation.ValidateStruct(&c,
		// validation.Field(&c.MallName, validation.Required, validation.Length(2, 64)),
		validation.Field(&c.MapIndoorID, validation.Required),
		validation.Field(&c.IsActive, validation.Required),
		validation.Field(&c.Location, validation.Required),
	)
}

// ValidateGeo fun
func (c GeoJSON) ValidateGeo() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.GeoType, validation.Required, validation.Length(2, 64)),
		validation.Field(&c.Coordinates, validation.Required, validation.Length(2, 64)),
	)
}

// MallCreateIndex just for add geonear json index in background job.
func MallCreateIndex(coll *mgo.Collection) {
	index := mgo.Index{
		Key:        []string{"$2dsphere:location"},
		Background: true,
	}
	createIndexErr := coll.EnsureIndex(index)
	if createIndexErr != nil {
		fmt.Println("createIndexErr: ", createIndexErr)
	}
}
