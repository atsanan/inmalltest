package utils

// APIVersion const.
const APIVersion string = "api-version"

// APIKey const.
const APIKey string = "api-key"

// MasterKey const.
const MasterKey string = "master-key"
