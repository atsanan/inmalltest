package utils_test

import (
	"testing"

	"inmallgame/app/utils"
)

func TestResponseForm(t *testing.T) {
	t.Run("Test ResponseSuccess json form.", func(t *testing.T) {
		got := utils.SuccessResult("result")

		if got["isSuccess"] != true {
			t.Errorf("got '%s' want 'true'", got)
		}
		if got["data"] != "result" {
			t.Errorf("got '%s' want 'result'", got)
		}
	})

	t.Run("Test ResponseFail json form.", func(t *testing.T) {
		wantMessage := "some thing went wrong"
		wantErr := "WTF"
		got := utils.FailResult(wantMessage, wantErr)

		if got["isSuccess"] != false {
			t.Errorf("got '%s' want false", got)
		}
		if got["message"] != wantMessage {
			t.Errorf("got '%s' want '%s'", got, wantMessage)
		}
		if got["error"] != wantErr {
			t.Errorf("got '%s' want '%s'", got, wantErr)
		}
	})
}
