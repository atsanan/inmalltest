package utils

import (
	"github.com/kataras/iris"
)

// SuccessResult json form.
func SuccessResult(data interface{}) iris.Map {
	return iris.Map{"isSuccess": true, "data": data}
}

// FailResult json form.
func FailResult(message interface{}, err string) iris.Map {
	return iris.Map{"isSuccess": false, "message": message, "error": err}
}

// ResponseSuccess when everything ok.
func ResponseSuccess(ctx iris.Context, data interface{}) {
	ctx.JSON(SuccessResult(data))
}

// ResponseFailure when everything so sad.
func ResponseFailure(ctx iris.Context, statusCode int, data interface{}, err string) {
	ctx.StatusCode(statusCode)
	ctx.JSON(FailResult(data, err))
}
