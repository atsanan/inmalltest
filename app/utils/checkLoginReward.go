package utils

import (

	// "log"

	// "encoding/json"

	"inmallgame/app/models"
)

func CheckUpDateLoginReward(user models.User) (bool, map[string]interface{}) {
	isLoginReward := true
	currentDate := map[string]interface{}{}
	currentDate["lastLogin"] = true
	dateLastLoginReward := user.LastLoginReward
	LastLoginReward := dateLastLoginReward.Format("2006-01-02")

	dateLastLogin := user.LastLogin
	LastLogin := dateLastLogin.Format("2006-01-02")
	if LastLoginReward == LastLogin {
		isLoginReward = false
	} else {
		currentDate["lastLoginReward"] = true
	}

	return isLoginReward, currentDate

}

func test() bool {
	return true
}
