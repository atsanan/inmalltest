package utils_test

import (
	"fmt"
	"os"
	"path"
	"testing"

	"inmallgame/app/utils"
)

func TestConfig(t *testing.T) {

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	p := path.Join(dir, "../../conf.json")
	fmt.Println(dir, p)

	if err != nil {
		t.Errorf("got '%s'", err)
	}

	t.Run("Read configuration files.", func(t *testing.T) {
		got := utils.GetConfig(p)
		want := "inmallgame"

		if got.DbName != want {
			t.Errorf("got '%s' want '%s'", got, want)
		}
	})
}
