package utils

import (
	"fmt"
	"math"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kataras/iris"
)

// CalcPages count.
func CalcPages(count int, limit int) int {
	f := float64(count) / float64(limit)
	pages := int(math.Ceil(f))
	return pages
}

// CalcPagesByPipeResult count.
func CalcPagesByPipeResult(mcount map[string]interface{}, limit int) int {
	if mcount["count"] != nil {
		count := mcount["count"].(int)
		f := float64(count) / float64(limit)
		pages := int(math.Ceil(f))
		return pages
	}

	return 0
}

// CalcOffsetQuery will provide all item for skip, limit query.
func CalcOffsetQuery(pageID int, pageLimit int) (offset int, limit int, page int) {
	if pageID < 0 {
		page = 0
	} else {
		page = pageID
	}
	if pageLimit <= 0 {
		limit = 10
	} else {
		limit = pageLimit
	}

	offset = page * limit

	return offset, limit, page
}

// FnString use for composition fn type.
type FnString func(string) (string, string)

// WithPaging will provide next, previous links.
func WithPaging(page int, pages int, limit int) FnString {
	return func(link string) (next string, previous string) {
		var nextPage = ""
		if page < (pages - 1) {
			nextPage = fmt.Sprintf(link, page+1, limit)
		}
		var previousPage = ""
		if page > 0 {
			previousPage = fmt.Sprintf(link, page-1, limit)
		}

		return nextPage, previousPage
	}
}

// PagingResult will construct and return pagingResult map.
func PagingResult(pageTotal int, pageIndex int, pageLimit int, nextPage string, previousPage string) map[string]interface{} {
	result := map[string]interface{}{
		"pages":     pageTotal,
		"pageIndex": pageIndex,
		"pageLimit": pageLimit,
		"paging": map[string]interface{}{
			"next":     nextPage,
			"previous": previousPage,
		},
	}

	return result
}

// CalcPipeCount will return map["count"] from pipe operation.
func CalcPipeCount(ctx iris.Context, baseQuery []bson.M, collection *mgo.Collection) (bson.M, error) {
	baseQuery = append(baseQuery, bson.M{"$count": "count"})
	pipe := collection.Pipe(baseQuery)

	result := bson.M{}
	err := pipe.One(&result)
	if err != nil || result["count"] == nil {
		return nil, err
	}

	return result, nil
}
