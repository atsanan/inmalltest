package utils

import (
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

// ValidateMongoID is utility function.
func ValidateMongoID(key string, ID string) error {
	err := validation.Errors{
		key: validation.Validate(ID, validation.Required, is.MongoID),
	}.Filter()

	if err != nil {
		return err
	}

	return nil
}
