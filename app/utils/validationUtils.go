package utils

import (
	"github.com/go-ozzo/ozzo-validation"
)

// NotEmptyString is utility function.
func NotEmptyString(key string, value string) error {
	err := validation.Errors{
		key: validation.Validate(value, validation.NilOrNotEmpty),
	}.Filter()

	if err != nil {
		return err
	}

	return nil
}
