package utils_test

import (
	"testing"

	"inmallgame/app/utils"
)

func TestCalculationPages(t *testing.T) {
	t.Run("Test calculate all page.", func(t *testing.T) {
		got := utils.CalcPages(100, 10)
		want := 10

		if got != want {
			t.Errorf("got '%d' want '%d'", got, want)
		}
	})

	t.Run("Test calculate all page.", func(t *testing.T) {
		got := utils.CalcPages(101, 10)
		want := 11

		if got != want {
			t.Errorf("got '%d' want '%d'", got, want)
		}
	})
}

func TestCalcPagesByPipeResult(t *testing.T) {
	t.Run("Test CalcPagesByPipeResult.", func(t *testing.T) {
		data := map[string]interface{}{
			"count": 10,
		}
		got := utils.CalcPagesByPipeResult(data, 10)
		want := 1

		if got != want {
			t.Errorf("got '%d' want '%d'", got, want)
		}
	})

	t.Run("Test CalcPagesByPipeResult.", func(t *testing.T) {
		data := map[string]interface{}{}
		got := utils.CalcPagesByPipeResult(data, 10)
		want := 0

		if got != want {
			t.Errorf("got '%d' want '%d'", got, want)
		}
	})
}

func TestCalculationOffsetQuery(t *testing.T) {
	t.Run("Test calculate offset query.", func(t *testing.T) {
		offset, limit, page := utils.CalcOffsetQuery(0, 10)
		wantOffset := 0
		wantLimit := 10
		wantPage := 0

		if wantOffset != offset {
			t.Errorf("gotOffset '%d' wantOffset '%d'", offset, wantOffset)
		}
		if limit != wantLimit {
			t.Errorf("gotLimit '%d' wantLimit '%d'", limit, wantLimit)
		}
		if page != wantPage {
			t.Errorf("gotPage '%d' wantPage '%d'", page, wantPage)
		}
	})

	t.Run("Test calculate offset query.", func(t *testing.T) {
		offset, limit, page := utils.CalcOffsetQuery(1, 10)
		wantOffset := 10
		wantLimit := 10
		wantPage := 1

		if wantOffset != offset {
			t.Errorf("gotOffset '%d' wantOffset '%d'", offset, wantOffset)
		}
		if limit != wantLimit {
			t.Errorf("gotLimit '%d' wantLimit '%d'", limit, wantLimit)
		}
		if page != wantPage {
			t.Errorf("gotPage '%d' wantPage '%d'", page, wantPage)
		}
	})
}

func TestWithPaging(t *testing.T) {
	t.Run("Test with paging.", func(t *testing.T) {

		linkF := "testLink?page=%d&limit=%d"

		nextPage, previousPage := utils.WithPaging(0, 10, 10)(linkF)
		wantNextPage := "testLink?page=1&limit=10"
		wantPreviousPage := ""

		if nextPage != wantNextPage {
			t.Errorf("got nextPage '%s' want nextPage '%s'", nextPage, wantNextPage)
		}
		if previousPage != wantPreviousPage {
			t.Errorf("got previousPage '%s' want wantPreviousPage '%s'", previousPage, wantPreviousPage)
		}
	})

	t.Run("Test with paging.", func(t *testing.T) {
		linkF := "testLink?page=%d&limit=%d"

		nextPage, previousPage := utils.WithPaging(1, 10, 10)(linkF)
		wantNextPage := "testLink?page=2&limit=10"
		wantPreviousPage := "testLink?page=0&limit=10"

		if nextPage != wantNextPage {
			t.Errorf("got nextPage '%s' want nextPage '%s'", nextPage, wantNextPage)
		}
		if previousPage != wantPreviousPage {
			t.Errorf("got previousPage '%s' want wantPreviousPage '%s'", previousPage, wantPreviousPage)
		}
	})
}

func TestPagingResult(t *testing.T) {
	t.Run("Test pagingResult.", func(t *testing.T) {

		gotResult := utils.PagingResult(100, 10, 200, "next", "previous")
		want := map[string]interface{}{
			"pages":     100,
			"pageIndex": 10,
			"pageLimit": 200,
			"paging": map[string]interface{}{
				"next":     "next",
				"previous": "previous",
			},
		}

		if gotResult["pages"] != want["pages"] {
			t.Errorf("got nextPage '%s' want nextPage '%s'", gotResult, want)
		}
		if gotResult["pageIndex"] != want["pageIndex"] {
			t.Errorf("got nextPage '%s' want nextPage '%s'", gotResult, want)
		}
		if gotResult["pageLimit"] != want["pageLimit"] {
			t.Errorf("got nextPage '%s' want nextPage '%s'", gotResult, want)
		}
		if gotResult["paging"] == nil {
			t.Errorf("got nextPage '%s' want nextPage '%s'", gotResult, want)
		}
	})
}
