package utils

import (
	"fmt"
	"strconv"
	"strings"
)

// SortParamsAsMap will handle sort query params.
func SortParamsAsMap(params map[string]string) map[string]int {
	sort := params["sort"]
	if sort != "" {
		result := make(map[string]int)

		fields := strings.Split(sort, ",")
		fieldLen := len(fields)
		if fieldLen > 0 {
			for i := 0; i < fieldLen; i++ {
				keys := strings.Split(fields[i], ":")
				sortKey, sortValue := keys[0], keys[1]
				val, _ := strconv.Atoi(sortValue)
				result[sortKey] = val
			}
		}

		return result
	}

	return nil
}

// SortParamsAsStrings params2 will handle sort query params.
func SortParamsAsStrings(params map[string]string) string {
	sort := params["sort"]
	if sort != "" {
		result := ""

		fields := strings.Split(sort, ",")
		fieldLen := len(fields)
		if fieldLen > 0 {
			for i := 0; i < fieldLen; i++ {
				keys := strings.Split(fields[i], ":")
				sortKey, sortValue := keys[0], keys[1]
				val, _ := strconv.Atoi(sortValue)
				if val > 0 {
					result += fmt.Sprintf(" %s", sortKey)
				} else {
					result += fmt.Sprintf(" -%s", sortKey)
				}
			}
		}

		split := strings.Fields(result)
		return strings.Join(split, ", ")
	}

	return ""
}

// FieldsParams will return fields selection from query params.
func FieldsParams(params map[string]string) []string {
	fields := params["fields"]
	if fields != "" {
		selects := strings.Split(fields, ",")
		return selects
	}

	return nil
}
