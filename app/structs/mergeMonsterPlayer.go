package appStruct

import (
	"time"
)

type MergeMonsterPlayer struct {
	mPlayerId              int
	mPlayerName            string
	mPlayerNewDateTime     time.Time
	mPlayerLastDateTime    time.Time
	mPlayerHabit           int
	mPlayerStatusHunger    int
	mPlayerStatusHappiness int
	mPlayerStatusTreat     int
	mDefaultId             int
	mDefaultName           string
	mDefaultTypeId         int
	mDefaultJobCategory    []int
	mDefaultJobLevel       int
	mDefaultGroupId        int
	mDefaultGroupLevel     int
	mDefaultTextDetail     string
	mDefaultAssetModel     string
	mDefaultAssetImageSlot string
	mDefaultAssetVersion   int
}
