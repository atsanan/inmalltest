package enums

// ZoneTypeEnum utility.
type ZoneTypeEnum int

const (
	// World as 0
	World ZoneTypeEnum = 0
	// InMall as 1
	InMall ZoneTypeEnum = 1
	// InMallFloor as 2
	InMallFloor ZoneTypeEnum = 2
	// InMallShop as 3
	InMallShop ZoneTypeEnum = 3
)

func (zone ZoneTypeEnum) String() string {
	// declare an array of strings
	// ... operator counts how many
	// items in the array (7)
	names := [...]string{
		"World",
		"InMall",
		"InMallFloor",
		"InMallShop",
	}
	// → `day`: It's one of the
	// values of Weekday constants.
	// If the constant is Sunday,
	// then day is 0.
	// prevent panicking in case of
	// `day` is out of range of Weekday
	if zone < World || zone > InMallShop {
		return "Unknown"
	}
	// return the name of a Weekday
	// constant from the names array
	// above.
	return names[zone]
}

func (zone ZoneTypeEnum) ToInt() int {
	return int(zone)
}
