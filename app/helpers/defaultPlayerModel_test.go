package helpers_test

import (
	"testing"

	"github.com/globalsign/mgo/bson"

	"inmallgame/app/helpers"
)

func TestDefaultPlayerModel(t *testing.T) {
	t.Run("Create default player model.", func(t *testing.T) {
		got := helpers.DefaultPlayerModel(bson.NewObjectId())
		want := "undefined"

		if got.PlayerName != want {
			t.Errorf("got '%v' want '%s'", got, want)
		}
	})
}
