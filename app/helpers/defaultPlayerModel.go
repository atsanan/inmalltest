package helpers

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

// DefaultPlayerModel generate and return new Player.
func DefaultPlayerModel(userID bson.ObjectId) bson.M {
	player := bson.M{
		"playerName":   "undefined",
		"teamPlayerId": "",
		"coin":         0,
		"diamond":      0,
		//CostumeSelectID: bson.NewObjectId(),
		"costumeSelectId": "",
		"costumeList":     []bson.M{},
		"gender":          "",
		"createAt":        time.Now(),
		"lastModified":    time.Now(),
		"userId":          userID,
	}

	return player
}
