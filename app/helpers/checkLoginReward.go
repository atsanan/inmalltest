package helpers

import (

	// "log"

	// "encoding/json"

	"inmallgame/app/models"
	"inmallgame/app/utils"
	"time"

	"inmallgame/app/data-access"

	"github.com/kataras/iris"

	"github.com/globalsign/mgo/bson"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"

	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

func CheckUpDateLoginReward(ctx iris.Context, user models.User) (bool, map[string]interface{}, map[string]interface{}, int) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	LoginRewardColl := session.DB(config.DbName).C(config.DBCollection.LoginRewardCollection)
	MailBoxColl := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	PlayerColl := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	isLoginReward := true
	currentDate := map[string]interface{}{}
	currentDate["lastLogin"] = true
	currentDate["lastLoginReward"] = true

	dateLastLoginReward := user.LastLoginReward
	LastLoginReward := dateLastLoginReward.Format("2006-01-02")

	dateLastLogin := user.LastLogin
	LastLogin := dateLastLogin.Format("2006-01-02")

	opt := option.WithCredentialsFile("inmallgotchimons.json")
	app, errfire := firebase.NewApp(context.Background(), nil, opt)
	if errfire != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
	}

	loginReturn := bson.M{}

	context := context.Background()
	client, errMes := app.Messaging(context)
	if errMes != nil {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
	}

	if LastLoginReward == LastLogin {
		isLoginReward = false
	} else {

		loginReward := []bson.M{}
		index := 0

		player := bson.M{}
		if findErr := PlayerColl.Find(bson.M{"userId": user.ID}).One(&player); findErr != nil {
			isLoginReward = false
		} else if findErr := LoginRewardColl.Find(bson.M{"isActive": true}).All(&loginReward); findErr != nil {
			isLoginReward = false

		} else {

			if len(loginReward) > 0 {
				if user.IndexReward >= len(loginReward) {
					user.IndexReward = 0
				} else {

					index = user.IndexReward
					user.IndexReward += 1

				}

				loginReturn = loginReward[index]
				create := bson.M{
					"playerId":      player["_id"],
					"userId":        user.ID,
					"title":         loginReward[index]["title"],
					"detail":        loginReward[index]["detail"],
					"itemId":        loginReward[index]["itemId"],
					"itemActive":    loginReward[index]["itemActive"],
					"mDefaultId":    loginReward[index]["mDefaultId"],
					"monsterActive": loginReward[index]["monsterActive"],
					"diamond":       loginReward[index]["diamond"],
					"diamondActive": loginReward[index]["diamondActive"],
					"imageName":     loginReward[index]["imageName"],
					"thumbnail":     loginReward[index]["thumbnail"],
					"linkListName":  loginReward[index]["linkListName"],
					"isActive":      true,
					"createAt":      time.Now(),
				}

				mongoErr := MailBoxColl.Insert(create)
				if mongoErr != nil {
					utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
					//return
				}

				stringMap := map[string]string{
					"type":         "inbox",
					"by":           "system",
					"linkListName": loginReward[index]["linkListName"].(string),
				}

				message := &messaging.Message{
					Notification: &messaging.Notification{
						Title: loginReward[index]["title"].(string),
						Body:  loginReward[index]["detail"].(string),
						//Data:  dataJson,
					},
					Data:  stringMap,
					Token: user.PushMessagesAccessToken,
					//Topic: "InMallGotchiMons",
					//Condition: condition,
					//Topic:topic,
					//Condition: condition,
				}

				_, errSend := client.Send(context, message)
				if errSend == nil {
					utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errSend))
				}
			} else {
				isLoginReward = false
			}
		}
		//if user.IndexReward
	}

	return isLoginReward, currentDate, loginReturn, user.IndexReward

}

func CreateLoginReward(ctx iris.Context, user models.User) (bool, map[string]interface{}, int) {
	config := utils.ConfigParser(ctx)
	session := database.GetMgoSession()
	PlayerColl := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
	LoginRewardColl := session.DB(config.DbName).C(config.DBCollection.LoginRewardCollection)
	MailBoxColl := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
	player := bson.M{}

	PlayerColl.Find(bson.M{"userId": user.ID}).One(&player)

	loginReward := []bson.M{}
	LoginRewardColl.Find(bson.M{"isActive": true}).All(&loginReward)
	isLoginReward := true
	index := 0
	loginReturn := bson.M{}
	if len(loginReward) > 0 {
		if user.IndexReward >= len(loginReward) {
			user.IndexReward = 0
		} else {

			index = user.IndexReward
			user.IndexReward += 1

		}
		
		loginReturn = loginReward[index]
		create := bson.M{
			"playerId":      player["_id"],
			"userId":        user.ID,
			"title":         loginReward[index]["title"],
			"detail":        loginReward[index]["detail"],
			"itemId":        loginReward[index]["itemId"],
			"itemActive":    loginReward[index]["itemActive"],
			"mDefaultId":    loginReward[index]["mDefaultId"],
			"monsterActive": loginReward[index]["monsterActive"],
			"diamond":       loginReward[index]["diamond"],
			"diamondActive": loginReward[index]["diamondActive"],
			"imageName":     loginReward[index]["imageName"],
			"thumbnail":     loginReward[index]["thumbnail"],
			"linkListName":  loginReward[index]["linkListName"],
			"isActive":      true,
			"createAt":      time.Now(),
		}

		mongoErr := MailBoxColl.Insert(create)
		if mongoErr != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
			//return
		}
		opt := option.WithCredentialsFile("inmallgotchimons.json")
		app, errfire := firebase.NewApp(context.Background(), nil, opt)
		if errfire != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
		}

		context := context.Background()
		client, errMes := app.Messaging(context)
		if errMes != nil {
			utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
		}
		stringMap := map[string]string{
			"type":         "inbox",
			"by":           "system",
			"linkListName": loginReward[index]["linkListName"].(string),
		}

		message := &messaging.Message{
			Notification: &messaging.Notification{
				Title: loginReward[index]["title"].(string),
				Body:  loginReward[index]["detail"].(string),
				//Data:  dataJson,
			},
			Data:  stringMap,
			Token: user.PushMessagesAccessToken,
			//Topic: "InMallGotchiMons",
			//Condition: condition,
			//Topic:topic,
			//Condition: condition,
		}

		_, errSend := client.Send(context, message)
		if errSend == nil {
			//	currentDate["lastLoginReward"] = true
		}
	} else {
		isLoginReward = false
	}
	return isLoginReward, loginReturn, user.IndexReward

}



// func CreateLoginRewardTest(ctx iris.Context, user models.User) (bool, map[string]interface{}, int) {
// 	config := utils.ConfigParser(ctx)
// 	session := database.GetMgoSession()
// 	PlayerColl := session.DB(config.DbName).C(config.DBCollection.PlayerCollection)
// 	LoginRewardColl := session.DB(config.DbName).C(config.DBCollection.LoginRewardCollection)
// 	//MailBoxColl := session.DB(config.DbName).C(config.DBCollection.MailBoxCollection)
// 	player := bson.M{}

// 	PlayerColl.Find(bson.M{"userId": user.ID}).One(&player)

// 	loginReward := []bson.M{}
// 	LoginRewardColl.Find(bson.M{"isActive": true}).All(&loginReward)
// 	isLoginReward := true
// 	index := 0
// 	loginReturn := bson.M{}
// 	loginReturn = loginReward[index]
// 	// if len(loginReward) > 0 {
// 	// 	if user.IndexReward >= len(loginReward) {
// 	// 		user.IndexReward = 0
// 	// 	} else {

// 	// 		index = user.IndexReward
// 	// 		user.IndexReward += 1

// 	// 	}

// 		// create := bson.M{
// 		// 	"playerId":      player["_id"],
// 		// 	"userId":        user.ID,
// 		// 	"title":         loginReward[index]["title"],
// 		// 	"detail":        loginReward[index]["detail"],
// 		// 	"itemId":        loginReward[index]["itemId"],
// 		// 	"itemActive":    loginReward[index]["itemActive"],
// 		// 	"mDefaultId":    loginReward[index]["mDefaultId"],
// 		// 	"monsterActive": loginReward[index]["monsterActive"],
// 		// 	"diamond":       loginReward[index]["diamond"],
// 		// 	"diamondActive": loginReward[index]["diamondActive"],
// 		// 	"imageName":     loginReward[index]["imageName"],
// 		// 	"thumbnail":     loginReward[index]["thumbnail"],
// 		// 	"linkListName":  loginReward[index]["linkListName"],
// 		// 	"isActive":      true,
// 		// 	"createAt":      time.Now(),
// 		// }

// 		// mongoErr := MailBoxColl.Insert(create)
// 		// if mongoErr != nil {
// 		// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "Fail to insert data", fmt.Sprint(mongoErr))
// 		// 	//return
// 		// }
// 		// opt := option.WithCredentialsFile("inmallgotchimons.json")
// 		// app, errfire := firebase.NewApp(context.Background(), nil, opt)
// 		// if errfire != nil {
// 		// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, "error getting Messaging client: %v\n", fmt.Sprint(errfire))
// 		// }

// 		// context := context.Background()
// 		// client, errMes := app.Messaging(context)
// 		// if errMes != nil {
// 		// 	utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, fmt.Sprint(errMes))
// 		// }
// 		// stringMap := map[string]string{
// 		// 	"type":         "inbox",
// 		// 	"by":           "system",
// 		// 	"linkListName": loginReward[index]["linkListName"].(string),
// 		// }

// 		// message := &messaging.Message{
// 		// 	Notification: &messaging.Notification{
// 		// 		Title: loginReward[index]["title"].(string),
// 		// 		Body:  loginReward[index]["detail"].(string),
// 		// 		//Data:  dataJson,
// 		// 	},
// 		// 	Data:  stringMap,
// 		// 	Token: user.PushMessagesAccessToken,
// 		// 	//Topic: "InMallGotchiMons",
// 		// 	//Condition: condition,
// 		// 	//Topic:topic,
// 		// 	//Condition: condition,
// 		// }

// 		// _, errSend := client.Send(context, message)
// 		// if errSend == nil {
// 		// 	//	currentDate["lastLoginReward"] = true
// 		// }
// 	// } else {
// 	// 	isLoginReward = false
// 	// }
// 	return isLoginReward, loginReturn, user.IndexReward

// }
