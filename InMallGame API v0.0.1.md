API overview
============
0. Authentication
1. Players
2. costumes
3. TeamPlayer
4. Sticker
5. Monsters
6. Zone type
7. Items
8. Mall
9. Channels
10. Users
11. [PrivacyPolicy](###%2010.%20PrivacyPolicy)

## Methods
***
### 0. Authentication

##### 0.1 Register or login (Microsoft Azure AD B2C)
    POST /api/v1/auth/registerOrLogin/microsoftAD
###### Query parameters:
| Field | Description | Optional |
| ------ | ----------------------------------|---------|
| `JWTToken` |Login Microsoft Azure ADB2C|No|
> **Note** 
**Register setup default**
Player -> playerName = "undefined"
Player -> teamPlayerId = -1
Player -> coin = 500
Player -> diamond = 0
Player -> costumeSelectId = -1
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        isLogin : bool,
        playerId : string,
    }
    errorCode : int,
    errorMessage : string
}
```
##### 0.2 Register or login (Facebook)
    POST /api/v1/auth/registerOrLogin/facebook
###### Query parameters:
| Field | Description | Optional |
| ------ | ----------------------------------|---------|
| `authenToken` |Facebook user access token|No|
> **Note** 
**Register setup default**
`Player -> authenTypeId = 1`
Player -> playerName = "undefined"
Player -> teamPlayerId = -1
Player -> coin = 500
Player -> diamond = 0
Player -> costumeSelectId = -1
###### Returns:
```json
{
    "data": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im5hdHRhcG9uLnJAbGl2ZS5jb20iLCJpZCI6IjViMmNhODgxYmRmZGQ4NGYxZmE1ZmU0MCIsImV4cCI6MTUyOTc0MDA1NSwiaWF0IjoxNTI5NjUzNjU1LCJpc3MiOiJhaG9vLXN0dWRpby5jby50aCJ9.NXuT7_IH8ScQI9riJpkO0fHZ_3OjUFyS80Ua_vHS0q4",
    "isSuccess": true
}
```

### 1. Players
##### 1.1 Getdata player info
    POST /api/v1/player/info
###### Query parameters:
| Field | Description | Optional | Auth
| ------ | ----------------------|---|---|
###### Returns:
```json
{
    "data": {
        "player": {
            "id": "5b2ca882bdfdd84f1fa5fe44",
            "userId": "5b2ca881bdfdd84f1fa5fe40",
            "playerName": "undefined",
            "teamPlayerId": -1,
            "coin": 501,
            "diamond": 10,
            "costumeSelectId": 1,
            "costumeList": [
                1,
                2
            ],
            "createAt": "0001-01-01T00:00:00Z",
            "lastModified": "2018-06-22T11:10:52.845Z"
        },
        "user": {
            "id": "5b2ca881bdfdd84f1fa5fe40",
            "name": "Nattapon Rattajak",
            "firstname": "Nattapon",
            "lastname": "Rattajak",
            "gender": "male",
            "birthdate": "11/06/1989",
            "password": "",
            "email": "nattapon.r@live.com",
            "tel": "",
            "authenTypeId": 1,
            "authenId": "10203051208741885",
            "authenToken": "",
            "verified": true,
            "verifiedAt": "2018-06-22T07:47:35.04Z",
            "createAt": "2018-06-22T07:47:35.04Z",
            "lastModified": "0001-01-01T00:00:00Z"
        }
    },
    "isSuccess": true
}
```

##### 1.2 Getdata and update player coin/diamond
    GET /api/v1/player/coinAndDiamond 
        **(Deprecated please use /player/info instead.)**
    POST /api/v1/player/coinAndDiamond
###### Query parameters:
| Field | Description | Optional | Auth
| ------ | ----------------------|---|---|
| coin |Coin is the currency, that use in game can get in play|Yes (Post)| Yes
| diamond |Diamond is rare currnecy, that is used for in-app purchase|Yes (Post)| Yes
###### Returns:
```json
{
    "data": {
        "_id": "5b2ca882bdfdd84f1fa5fe44",
        "coin": 501,
        "diamond": 10
    },
    "isSuccess": true
}
```

##### 1.3 Update player costumes (select/update/new)
    GET /api/v1/player/costume
    POST /api/v1/player/costume
    POST /api/v1/player/costume/new 
        **(buy new costume)**
    
###### Query parameters:
| Field | Description | Optional | Auth
| ------ | ------------------------------|----|---------|
| costumeSelectId |ID of the selected costume|No (Post)| Yes

###### Returns:
```json
{
    "data": {
        "id": "5b2ca882bdfdd84f1fa5fe44",
        "userId": "5b2ca881bdfdd84f1fa5fe40",
        "playerName": "undefined",
        "teamPlayerId": -1,
        "coin": 501,
        "diamond": 10,
        "costumeSelectId": 2,
        "costumeList": [
            1,
            2
        ],
        "createAt": "0001-01-01T00:00:00Z",
        "lastModified": "2018-06-22T10:59:44.144Z"
    },
    "isSuccess": true
}
```

##### 1.4 Update player.teamplayer (update)
    GET /api/v1/player/teamplayer 
        **(Deprecated please use /player/info instead.)**
    POST /api/v1/player/teamplayer
    
###### Query parameters:
| Field | Description | Optional | Auth
| ------ | -----------|---|---|
| teamPlayerId |Team type of game|No| Yes
###### Returns:
```json
{
    "data": {
        "id": "5b2ca882bdfdd84f1fa5fe44",
        "userId": "5b2ca881bdfdd84f1fa5fe40",
        "playerName": "undefined",
        "teamPlayerId": 1,
        "coin": 501,
        "diamond": 10,
        "costumeSelectId": 1,
        "costumeList": [
            1,
            2
        ],
        "createAt": "0001-01-01T00:00:00Z",
        "lastModified": "2018-06-22T11:58:12.412Z"
    },
    "isSuccess": true
}
```

##### 1.5 Getdata players list (backend)
    GET /api/v1/players?page=0&limit=10
        ** Get all players.
   
###### Query parameters:
| Field | Description | Optional | Default | Authen
| --- | -------------|---|---|---|
| page |Current index of page|Yes | 0 | YES
| limit |Maximum number of page|Yes | 10 | YES
###### Returns:
```json
{
    "data": {
        "pageIndex": 0,
        "pageLimit": 1,
        "pages": 2,
        "paging": {
            "next": "/api/v1/players?page=1&limit=1",
            "previous": ""
        },
        "players": [
            {
                "id": "5b2ca882bdfdd84f1fa5fe44",
                "userId": "5b2ca881bdfdd84f1fa5fe40",
                "playerName": "undefined",
                "teamPlayerId": 1,
                "coin": 501,
                "diamond": 10,
                "costumeSelectId": 1,
                "costumeList": [
                    1,
                    2
                ],
                "createAt": "0001-01-01T00:00:00Z",
                "lastModified": "2018-06-22T11:58:12.412Z"
            }
        ]
    },
    "isSuccess": true
}
``` 

##### 1.6 Delete player (backend) 
    DELETE /api/v1/player/{id} **Not yet implemented**
> **Note** 
Delete data table "MonsterPlayer" where playerId
Delete data table "ItemPlayer" where playerId
Delete data table "JoinChannel" where playerId
Delete data table "MonsterStickers" where sendFromId or sendToId
###### Query parameters:
###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
``` 

##### 1.7 Update player (backend)
    POST /api/v1/player/{id : string}
###### Query parameters:
| Field | Description | Optional | Auth
| --- | ------ |---|---|
| playerName |Name of the player| No | admin
| teamPlayerId |Team's ID from selected| No |
| coin |Current coins of player| No |
| diamond |Current diamonds of player| No |
| costumeSelectId |Selected costume ID| No |
| costumeList |All Costume's ID, that player have| No |
###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
``` 
### 2. costumes
##### 2.1 Getdata costumes store list
    POST /api/v1/costumesStore/list
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| pageIndex |Current index of page|Yes | 0 |
| pageLimit |Maximum of page|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        costumes :
        [
            {
                costumeId : int,
                costumeName : string,
                costumeDetail : string,
                coin : int, 
                diamond : int,
                isActive : bool
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 2.2 Add costumes store (backend)
    POST /api/v1/costumesStore/add
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| costumeName |Name of the costume|No |
| costumeDetail |Detail about this costume|Yes |
| coin |Costume price in coins|No |
| diamond |Costume price in diamonds|No |
| isActive |Can interact with|No |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        costumeId : int,
    }
    errorCode : int, 
    errorMessage : string
}
```
##### 2.3 Delete costumes store (backend)
    DELETE /api/v1/costumesStore/{id}
###### Query parameters:
###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
##### 2.4 Update costumes store (backend)
    POST /api/v1/costumesStore/update
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| costumeId |ID of a costume|No |
| costumeName |Name of a costume|Yes |
| costumeDetail |Detail of this costume|Yes |
| coin |Price of a costume by coins|Yes |
| diamond |Price of a costume by diamonds|Yes |
| isActive |Can buy or not|Yes |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        costumeId : int,
    }
    errorCode : int, 
    errorMessage : string
}
```
### 3. TeamPlayer
##### 3.1 Getdata teamplayer list
    POST /api/v1/teamplayer/list
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| pageIndex |current page|Yes | 0 |
| pageLimit |maximum page|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        players :
        [
            {
                teamPlayerId : string,
                teamPlayerName : string,
                teamPlayerDetail : string,
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
```
##### 3.3 Add teamplayer (backend)
    POST /api/v1/teamplayer/add
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| teamPlayerName |Name of a team|No |
| teamPlayerDetail |Detail of this team|Yes |

###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        teamPlayerId : int,
    }
    errorCode : int, 
    errorMessage : string
}
```

##### 3.4 Update teamplayer (backend)
    POST /api/v1/teamplayer/update
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| teamPlayerId |Team's ID|No |
| teamPlayerName |Name of a team|Yes |
| teamPlayerDetail |Detail of this team|Yes |

###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        teamPlayerId : int,
    }
    errorCode : int, 
    errorMessage : string
}
```
### 4. Sticker
##### 4.1 Getdata sticker list by channel
    POST /api/v1/sticker/byChannel
> **Note** 
POST byChannel : where inChannel=channelId and limit

###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| channelId |ID of channel|No | |
| pageIndex |Current channels page|Yes | 0 |
| pageLimit |Maximum of channels page|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        stickers :
        [
            {
                mStickerId : int,
                stickerStoreId : int,
                sendFromId : int,
                sendToId : int, 
                inChannel : int,
                stickerStoreName : string,
                stickerStoreDetail : int,
                stickerStoreLevel : int,
                stickerStoreImage : string,
                coin : int, 
                diamond : int,
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
```
##### 4.2 Getdata sticker list by monster
    POST /api/v1/sticker/byTo
    POST /api/v1/sticker/byFrom
> **Note** 
POST byTo : where sendFromId=mPlayerId and limit
POST byFrom : where sendToId=mPlayerId and limit

###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| mPlayerId |Get player's monster ID|No ||
| pageIndex |Current page|Yes | 0 |
| pageLimit |Maximum page|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        stickers :
        [
            {
                mStickerId : int,
                stickerStoreId : int,
                sendFromId : int,
                sendToId : int, 
                inChannel : int,
                stickerStoreName : string,
                stickerStoreDetail : int,
                stickerStoreLevel : int,
                stickerStoreImage : string,
                coin : int, 
                diamond : int,
            }, ...
        ]
    }

    errorCode : int, 
    errorMessage : string
}
```
##### 4.3 Add sticker to friend
    POST /api/v1/sticker/toFried

###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| stickerStoreId |ID of this sticker in store|No |
| sendFromId |This sticker is sent grom whose's ID|No |
| sendToId |ID that this sticker is sent to|No |
| inChannel |which channel|No |
###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
##### 4.4 Getdata sticker store list
    POST /api/v1/stickerStore/list
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| pageIndex |Current page of sticker|Yes | 0 |
| pageLimit |Maximum page of sticker|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        stickers :
        [
            {
                stickerStoreId : int,
                stickerStoreName : string,
                stickerStoreDetail : string,
                stickerStoreLevel : int, 
                stickerStoreImage : string,
                coin : int,
                diamond : int
                isActive : bool
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 4.5 Add sticker store (backend)
    POST /api/v1/stickerStore/add
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| stickerStoreName |Name of the sticker in the store|No |
| costumeDetail |Costume’s detail|Yes |
| stickerStoreDetail |Sticker’s detail|Yes |
| stickerStoreLevel |Sticker’s level|No |
| stickerStoreImage |Path of the Sticker’s image|No |
| coin |Price of the sticker by coins|No |
| diamond |Price of the sticker by diamonds|No |
| isActive |Can interract or not|Yes |

###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
##### 4.6 Update sticker store(backend)
    POST /api/v1/stickerStore/update
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| stickerStoreId |ID of the sticker in the store|No |
| stickerStoreName |Name of the sticker in the store|Yes |
| costumeDetail |Costume's detail|Yes |
| stickerStoreDetail |Sticker's detail|Yes |
| stickerStoreLevel |Sticker's level|Yes |
| stickerStoreImage |Path of the Sticker's image|Yes |
| coin |Price of the sticker by coins|Yes |
| diamond |Price of the sticker by diamonds|Yes |
| isActive |Can interract or not|Yes |

###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
### 5. Monsters
##### 5.1 Getdata New monsters
    POST /api/v1/newMonster/byZone
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| zoneTypeId |ID of the zone's type|No |  |
| id |ID of the monster|No |  |
| limit |Maimum of number of monster|Yes | 5 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        limit : int, 
        monsters :
        [
            {
                mAddonId : int,
                isGoldenMinutes : bool,
                mDefaultId : int,
                mDefaultName : string,
                mDefaultTypeId : int,
                mDefaultJobCategory : [int],
                mDefaultJobLevel : int, 
                mDefaultGroupId : int,
                mDefaultGroupLevel : int,
                mDefaultTextDetail : text,
                mDefaultAssetModel : string,
                mDefaultAssetImageSlot : string, 
                mDefaultAssetVersion : int
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 5.2 Pick New monsters to bag
    POST /api/v1/newMonster/pick
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| mAddonId |Monster's ID|No |
| playerId |Player's ID|No |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        isPick : bool, 
        monster :  
        {
            mPlayerId : int,
            mPlayerName : string,
            mPlayerNewDateTime : datetime, 
            mPlayerLastDateTime :  datetime, 
            mPlayerHabit : int, 
            mPlayerStatusHunger : int,
            mPlayerStatusHappiness : int,
            mPlayerStatusTreat : int,
            mDefaultId : int,
            mDefaultName : string,
            mDefaultTypeId : int,
            mDefaultJobCategory : [int],
            mDefaultJobLevel : int, 
            mDefaultGroupId : int,
            mDefaultGroupLevel : int,
            mDefaultTextDetail : text,
            mDefaultAssetModel : string,
            mDefaultAssetImageSlot : string, 
            mDefaultAssetVersion : int
        }
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 5.3 Getdata Monster info
    POST /api/v1/monster/info
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| mPlayerId |ID of the monster of this player|No |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        monster :  
        {
            mPlayerId : int,
            mPlayerName : string,
            mPlayerNewDateTime : datetime, 
            mPlayerLastDateTime :  datetime, 
            mPlayerHabit : int, 
            mPlayerStatusHunger : int,
            mPlayerStatusHappiness : int,
            mPlayerStatusTreat : int,
            mDefaultId : int,
            mDefaultName : string,
            mDefaultTypeId : int,
            mDefaultJobCategory : [int],
            mDefaultJobLevel : int, 
            mDefaultGroupId : int,
            mDefaultGroupLevel : int,
            mDefaultTextDetail : text,
            mDefaultAssetModel : string,
            mDefaultAssetImageSlot : string, 
            mDefaultAssetVersion : int
        }
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 5.4 Update data foodAddCode/foodAddSet Monster 
    POST /api/v1/monster/updateFoodAdd
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| mPlayerId |Monster's ID of this player|No |
| mPlayerFoodAddCode |Add food's Code to monster|Yes |
| mPlayerFoodAddSec |Add time in seconds to this monster|Yes |

###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
##### 5.5 Update count view Monster 
##### 5.6 Getdata Monsters in bag
    POST /api/v1/monster/list
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| playerId |Player's ID|No | 0 |
| pageIndex |Current page of monster in bag|Yes | 0 |
| pageLimit |Maximum page of the bag|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        monsters :
        [
            {
                mPlayerId : int,
                mPlayerName : string,
                mPlayerNewDateTime : datetime, 
                mPlayerLastDateTime :  datetime, 
                mPlayerHabit : int, 
                mPlayerStatusHunger : int,
                mPlayerStatusHappiness : int,
                mPlayerStatusTreat : int,
                mDefaultId : int,
                mDefaultName : string,
                mDefaultTypeId : int,
                mDefaultJobCategory : [int],
                mDefaultJobLevel : int, 
                mDefaultGroupId : int,
                mDefaultGroupLevel : int,
                mDefaultTextDetail : text,
                mDefaultAssetModel : string,
                mDefaultAssetImageSlot : string, 
                mDefaultAssetVersion : int
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 5.7 Getdata default monster info
    POST /api/v1/monsterDefault/info
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| mDefaultId |Default monster's ID of a player|No |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        monster :  
        {
            mDefaultId : int,
            mDefaultName : string,
            mDefaultTypeId : int,
            mDefaultJobCategory : [int],
            mDefaultJobLevel : int, 
            mDefaultGroupId : int,
            mDefaultGroupLevel : int,
            mDefaultTextDetail : text,
            mDefaultAssetModel : string,
            mDefaultAssetImageSlot : string, 
            mDefaultAssetVersion : int
        }
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 5.8 Getdata default monsters list 
    POST /api/v1/monsterDefault/list
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| pageIndex |Current page of monsters list|Yes | 0 |
| pageLimit |Maximum page of monsters list|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        monsters :
        [
            {
                mDefaultId : int,
                mDefaultName : string,
                mDefaultTypeId : int,
                mDefaultJobCategory : [int],
                mDefaultJobLevel : int, 
                mDefaultGroupId : int,
                mDefaultGroupLevel : int,
                mDefaultTextDetail : text,
                mDefaultAssetModel : string,
                mDefaultAssetImageSlot : string, 
                mDefaultAssetVersion : int
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 5.9 Add default monsters  (backend)
    POST /api/v1/monsterDefault/add
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| mDefaultName |Name of default monster|No |
| mDefaultTypeId |ID of default monster|No |
| mDefaultJobCategory |Job Category of this default monster|No |
| mDefaultJobLevel |Job level of this default monster|No |
| mDefaultGroupId |Group's ID of this default monster|No |
| mDefaultGroupLevel |Group's level of this default monster|No |
| mDefaultTextDetail |Detail in text of this default monster|Yes |
| mDefaultAssetImageSlot |Path of the Image slot|No |
| mDefaultAssetVersion |Version of this monster asset|No |

###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
##### 5.10 Update default monsters (backend)
    POST /api/v1/monsterDefault/update
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| mDefaultId |ID of this default monster|No |
| mDefaultName |Name of default monster|No |
| mDefaultTypeId |ID of default monster|No |
| mDefaultJobCategory |Job Category of this default monster|No |
| mDefaultJobLevel |Job level of this default monster	|No |
| mDefaultGroupId |Group’s ID of this default monster|No |
| mDefaultGroupLevel |Group’s level of this default monster|No |
| mDefaultTextDetail |Detail in text of this default monster|Yes |
| mDefaultAssetImageSlot |Path of the Image slot|No |
| mDefaultAssetVersion |Version of this monster asset|No |

###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
### 6. Zone type
##### 6.1 Getdata Zone type list
    POST /api/v1/zonetype/list
> **Note** 
0 : WORLD
1 : MALL
2 : SHOP

###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| pageIndex |Current page of zone type list|Yes | 0 |
| pageLimit |Maximum page of zone type list|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        stickers :
        [
            {
                zoneTypeId : int,
                zoneTypeName : int,
                isActive : int,
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
```
### 7. Items
##### 7.1 Getdata items in bag
##### 7.2 Getdata Coupon passworld
##### 7.3 Scan Coupon
##### 7.4 Add item in bag
##### 7.5 Add default item  (backend)
    POST /api/v1/itemDefault/add
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| itemName |Name of a item|No |
| itemDetail |Detail of a item|Yes |
| itemEffect |Effect of this item|Yes |
| coin |Coins that player get|No |
| diamond |Diamonds that player get|No |
| isActive |Can interact or not|Yes |
| itemOrder |Orfer  of this item|Yes |
| itemAssetModel |Path of this item's model asset|No |
| itemAssetImageSlot |Path of Image slot asset|No |
| itemAssetVersion |Version of this item asset|No |
| itemCategoryId |Id of this item category|No |
###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
##### 7.6 Update default item (backend)
    POST /api/v1/itemDefault/update
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| itemId |ID of this item|No |
| itemName |Name of this item|No |
| itemDetail |Detail of this item|Yes |
| itemEffect |Effect of this item|Yes |
| coin |Price of this item in coins|No |
| diamond |Price of this item in diamonds|No |
| isActive |Can interact or not|Yes |
| itemOrder |Order of this item |Yes |
| itemAssetModel |Path of this item model asset|No |
| itemAssetImageSlot |Path of Image slot asset|No |
| itemAssetVersion |Version of this item asset|No |
| itemCategoryId |Id of this item category|No |
###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
##### 7.7 Add default category item  (backend)
    POST /api/v1/categoryItemDefault/add
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| itemCategoryName |Name of this item category|No |
| itemCategoryOrder |Order of this item category|Yes |
###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
##### 7.8 Update default category item  (backend)
    POST /api/v1/categoryItemDefault/update
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| itemCategoryId |ID of this item category|No |
| itemCategoryName |Name of this item category|No |
| itemCategoryOrder |Order of this item category|Yes |
###### Returns:
```javascript
{
    isSuccess : bool,
    errorCode : int, 
    errorMessage : string
}
```
### 8. Mall
##### 8.1 Getdata malls 
    POST /api/v1/mall/list
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| pageIndex |Current page of Malls list|Yes | 0 |
| pageLimit |Maximum page of Malls list|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        malls :
        [
            {
                mallId : int,
                mallName : string,
                mallDetail : text,
                mallAddress : text,
                mapLat : float,
                mapLong : float,
                mapOutdoorId : string,
                mapIndoorId : string,
                isActive : bool
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 8.2 Getdata mall info
    POST /api/v1/mall/info
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| mallId |ID of this mall|No | 0 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        mall :
        {
            mallId : int,
            mallName : string,
            mallDetail : text,
            mallAddress : text,
            mapLat : float,
            mapLong : float,
            mapOutdoorId : string,
            mapIndoorId : string,
            isActive : bool
            floors :
            [
                {
                    mallFloorId : int,
                    mallFloorName : string,
                    mallFloorDetail : string,
                    mapIndoorFloorKey : string,
                    mapIndoorFloorData : string,
                }, ...
            ],
        }
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 8.2 Getdata floor info
    POST /api/v1/mall/floor/info
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| mallFloorId |ID of floor in mall|Yes |
| mapIndoorFloorKey |Key of the indoor map on floor in mall|Yes |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        mallId : int,
        mallName : string,
        mallDetail : text,
        mallAddress : text,
        mapLat : float,
        mapLong : float,
        mapOutdoorId : string,
        mapIndoorId : string,
        floors :
        [
            {
                mallFloorId : int,
                mallFloorName : string,
                mallFloorDetail : string,
                mapIndoorFloorKey : string,
                mapIndoorFloorData : string,
            }, ...
        ],
        floor :
        {
            mallFloorId : int,
            mallFloorName : string,
            mallFloorDetail : string,
            mapIndoorFloorKey : string,
            mapIndoorFloorData : string,
        }
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 8.3 Getdata shops

    POST /api/v1/shop/list/byFloor
    
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| mallFloorId |ID of floor in mall|No | |
| pageIndex |Current page of shops on this floor|Yes | 0 |
| pageLimit |Maximum page of shops on this floor|Yes | 10 |

    POST /api/v1/shop/list/byMall
    
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| mallId |ID of this mall|No |  |
| pageIndex |Current page of shops on this floor|Yes | 0 |
| pageLimit |Maximum page of shops on this floor|Yes | 10 |

###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        mallId : int,
        mallFloorId : int,
        shops :
        [
            {
                shopId : int,
                shopName : string,
                shopDetail : string,
                shopFloor : int,
                mapLat : float,
                mapLong : float,
                mallFloorId : int,
                shopAssetId : int,
                shopLogo : string,
                shopAssetName : string,
                shopAssetDetail : string,
                shopAssetModel : string,
                shopAssetVersion : int,
                shopCategorieId : int,
                shopCategorieName : string
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 8.4 Add mall  (backend)
##### 8.5 Update mall (backend)
##### 8.6 Add floor  (backend)
##### 8.7 Update floor (backend)
### 9. Channels
##### 9.1 Getdata channels list
    POST /api/v1/channel/list/byShop
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| shopId |ID of this shop|No |  |
| pageIndex |Current page of channels|Yes | 0 |
| pageLimit |Maximum page of channels|Yes | 10 |

    POST /api/v1/channel/list/byFloor
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| mallFloorId |ID of this floor in mall|No |  |
| pageIndex |Currnt floor in mall|Yes | 0 |
| pageLimit |Maximum floor in mall|Yes | 10 |

    POST /api/v1/channel/list/byMall
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| mallId ||No |  |
| isAuto ||Yes | true |
| isLessChannelMax ||Yes | true |
| pageIndex ||Yes | 0 |
| pageLimit ||Yes | 10 |

    POST /api/v1/channel/list/byPlayer
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| playerId |ID of this mall|No |  |
| isAuto |Auto generate or not|Yes | true |
| isLessChannelMax |True when the current channel is under maximum setting|Yes | true |
| pageIndex |Current channel |Yes | 0 |
| pageLimit |Maximum channel|Yes | 10 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        mallId : int,
        mallFloorId : int,
        shops :
        [
            {
                channelId : int,
                channelName : string, 
                channelPassword : string, 
                isAuto : bool,
                shopId : int,
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 9.2 Getdata channel
    POST /api/v1/channel/join
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---|---|
| shopId |ID of the shop|No |  |
| pageIndex |Current channel in shop|Yes | 0 |
| pageLimit |Maximu, channel in shop|Yes | 10 |

###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        pageIndex : int,
        pageLength : int,
        pageLimit : int, 
        pageTotal : int,
        mallId : int,
        mallFloorId : int,
        shopId : int,
        channelId : int,
        channelName : string,
        channelName : string,
        channels :
        [
            {
                JoinchannelId : int,
                playerId : int,
                mPlayerId : int,
                joinDateTime : int,
            }, ...
        ]
    }
    errorCode : int, 
    errorMessage : string
}
``` 
##### 9.3 Create channel
    POST /api/v1/channel/create
###### Query parameters:
| Field | Description | Optional | Default |
| --- | -------------|---| --- |
| channelName |Name of the channel|No | |
| channelPassword |Password of the channel|No | |
| isAuto |Auto generate or not|Yes | |
| shopId |ID of the shop|Yes | |
| joinNow |Can join or not|No | false |
| playerId |ID of the player|No | -1 |
| mPlayerId |ID of the monster|No | -1 |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        channelId : int,
    }
    errorCode : int, 
    errorMessage : string
}
```
##### 9.4 Join channel
    POST /api/v1/channel/join
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| channelId |ID of the channel|No |
| playerId |ID of the player|No |
| mPlayerId |monster's ID of the player|No |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        isJoin : bool, 
        channelId : int,
        playerId : int,
        mPlayerId : int,
    }
    errorCode : int, 
    errorMessage : string
}
```
##### 9.5 Monster leave channel 
    POST /api/v1/channel/monsterLeave
###### Query parameters:
| Field | Description | Optional |
| --- | -------------|---|
| channelId |ID of the Channel|No |
| mPlayerId |Monster's ID of the player|No |
###### Returns:
```javascript
{
    isSuccess : bool,
    data :
    {
        isMonsterLeave : bool, 
        isRemoveChannel : bool, 
    }
    errorCode : int, 
    errorMessage : string
}
```
### 10 Users
| Field | Description | Optional | Auth
| --- | ------ |---|---|
| firstName |First name of the player, that gets from register | No | 
| lastName |Last name of the player, that gets from register| No |
| email |Email of the player, that gets from register| No |
| tel |Tel of the player, that gets from register| No |

### 11. PrivacyPolicy



