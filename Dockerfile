FROM golang:1.10.3

WORKDIR /go/src/app
# Copy the local package files to the container's workspace.
ADD . /go/src/inmallgame
COPY . .

RUN go get -d -v ./...
# RUN go install -v ./...
RUN go build -o myapp . 

CMD ["./myapp"]
LABEL Name=inmallgame
EXPOSE 8080