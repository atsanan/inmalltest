package main

import (
	"github.com/didip/tollbooth"
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
	"github.com/kataras/iris/core/host"
	"github.com/kataras/iris/middleware/logger"
	"github.com/kataras/iris/middleware/recover"

	// prometheusMiddleware "github.com/iris-contrib/middleware/prometheus"
	// "github.com/prometheus/client_golang/prometheus"

	opentracing "github.com/opentracing/opentracing-go"
	jaeger "github.com/uber/jaeger-client-go"
	jaegerConfig "github.com/uber/jaeger-client-go/config"

	"inmalltest/app/data-access"
	"inmalltest/app/security"

	// "inmallgame/app/security/basicauth"
	"inmallgame/app/utils"

	"fmt"
	"inmallgame/routes/auth"
	"inmallgame/routes/categoryitem"
	"inmallgame/routes/channel"
	"inmallgame/routes/chat"
	"inmallgame/routes/costume"
	"inmallgame/routes/coupon"
	"inmallgame/routes/couponDiamond"
	"inmallgame/routes/couponPlayer"
	"inmallgame/routes/devices"
	"inmallgame/routes/digitalBoard"
	"inmallgame/routes/friend"
	"inmallgame/routes/historyPlayer"
	"inmallgame/routes/itemDefault"
	"inmallgame/routes/itemPlayer"
	"inmallgame/routes/qrCode"
	"inmallgame/routes/mailBox"
	"inmallgame/routes/mall"
	"inmallgame/routes/monsterDefault"
	"inmallgame/routes/monsterPlayer"
	"inmallgame/routes/monsterSticker"
	"inmallgame/routes/miniGame"
	"inmalltest/routes/paymentMPayConfig"
	"inmallgame/routes/paymentMPayCreditCards"
	"inmallgame/routes/news"
	"inmallgame/routes/newstype"
	"inmalltest/routes/paymentMPlayItems"
	"inmallgame/routes/player"
	"inmalltest/routes/privilegeDefaultPlayer"
	"inmallgame/routes/privilegeGroups"
	"inmallgame/routes/privilegeShop"
	"inmallgame/routes/setting"
	"inmallgame/routes/shop"
	"inmallgame/routes/shopads"
	"inmallgame/routes/shopassert"
	"inmallgame/routes/shopasserts"
	"inmallgame/routes/shopcategories"
	"inmallgame/routes/shopmode"
	"inmallgame/routes/stickerStore"
	"inmallgame/routes/teamPlayer"
	"inmallgame/routes/tellFriend"
	"inmallgame/routes/user"
	"inmallgame/routes/wildMonster"
	"inmallgame/routes/wilditem"
	"inmallgame/routes/zoneType"
	"io"
	"os"
	"path"
	// "strings"
)

// initJaeger returns an instance of Jaeger Tracer that samples 100% of traces and logs all spans to stdout.
func initJaeger(service string) (opentracing.Tracer, io.Closer) {
	cfg := &jaegerConfig.Configuration{
		Sampler: &jaegerConfig.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &jaegerConfig.ReporterConfig{
			LogSpans: true,
		},
	}
	tracer, closer, err := cfg.New(service, jaegerConfig.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	return tracer, closer
}

//APIVersionMDW routes
func APIVersionMDW(ctx iris.Context) {
	// SupportVersions of api version management.
	supportVersions := []string{"v0.1", "v1","v0.2", "v2"}

	version := ctx.Params().Get("version")
	if version == "" {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, "Missing api version string.")
		return
	}
	var contained = utils.Contains(supportVersions, version)
	if contained == false {
		utils.ResponseFailure(ctx, iris.StatusBadRequest, nil, "Api version not match.")
		return
	}

	ctx.Next()
}

func notFoundHandler(ctx iris.Context) {
	ctx.HTML("Custom route for 404 not found http code, here you can render a view, html, json <b>any valid response</b>.")
}

func configureHost(su *host.Supervisor) {
	// here we have full access to the host that will be created
	// inside the `app.Run` function or `NewHost`.
	//
	// we're registering a shutdown "event" callback here:
	su.RegisterOnShutdown(func() {
		println("server is closed")
	})
	// su.RegisterOnError
	// su.RegisterOnServe
}

func newApp(configuration utils.Configuration) *iris.Application {
	app := iris.New()
	app.Logger().SetLevel("debug")
	// Optionally, add two built'n handlers
	// that can recover from any http-relative panics
	// and log the requests to the terminal.
	app.Use(recover.New())
	app.Use(logger.New())

	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"}, // allows everything, use that to change the hosts.
		AllowCredentials: true,
	})

	// first parameter is the request path
	// second is the system directory
	app.StaticWeb("/static", "./assets")

	// tracer := opentracing.GlobalTracer()
	tracer, closer := initJaeger("hello-world")
	defer closer.Close()

	span := tracer.StartSpan("limit-and-setconfig")

	// prom := prometheusMiddleware.New("serviceName", 300, 1200, 5000)
	// app.Use(prom.ServeHTTP)

	// limit-and-setconfig.
	limiter := tollbooth.NewLimiter(1000, nil)
	app.Use(func(ctx iris.Context) {
		if httpError := middleware.LimitHandler(ctx, limiter); httpError != nil {
			utils.ResponseFailure(ctx, httpError.StatusCode, nil, httpError.Message)
			ctx.StopExecution()
			return
		}
		ctx.Values().Set("config", configuration)
		ctx.Next()
	})

	apiRoutes := app.Party("/api/{version: string}", crs).AllowMethods(iris.MethodOptions)
	apiRoutes.Use(APIVersionMDW)
	apiRoutes.Get("/user", auth.TokenMDW, user.GetUser)
	apiRoutes.Post("/user", auth.TokenMDW, user.PostUser)
	apiRoutes.Post("/user/{id:string}", auth.TokenMDW, user.UpdateUser)
	// Sub urls from `api/v1`.
	authRoutes := apiRoutes.Party("/auth")
	authRoutes.Post("/login", user.Login)
	authRoutes.Post("/register", user.Register)
	authRoutes.Post("/registerOrLogin/microsoftAD", user.MicrosoftAD) // No test
	authRoutes.Post("/registerOrLogin/facebook", user.FacebookLogin)
	authRoutes.Post("/registerOrLogin/google", user.GoogleLogin)
	authRoutes.Get("/refreshToken", auth.TokenMDW, auth.RefreshToken)
	authRoutes.Post("/verifyAccount", user.VerifyAccount)
	authRoutes.Post("/resendEmail", user.ResendActivationEmail)
	authRoutes.Post("/registerOrLogin/googleFormData", user.GoogleFormData)
	authRoutes.Post("/registerOrLogin/facebookFormData", user.FacebookFormData)
	authRoutes.Post("/registerOrLogin/lineFormData", user.LineFormData)

	imageGoogleRoutes := apiRoutes.Party("/image")
	imageGoogleRoutes.Get("/{playerId:string}", user.ImageUrl)
	playerRoutes := apiRoutes.Party("/player")
	player.InitRouter(playerRoutes)

	costumeStoreRoutes := apiRoutes.Party("/costumeStore")
	// For authorized user.
	costumeStoreRoutes.Get("/", costume.Costume)
	// For admin
	costumeStoreRoutes.Post("/{id: string}", costume.UpdateCostume)
	costumeStoreRoutes.Delete("/{id: string}", costume.DeleteCostume)
	costumeStoreRoutes.Post("/add", costume.AddCostume)
	costumeStoreRoutes.Get("/{id: string}", costume.CostumeDetail)

	teamPlayerRoutes := apiRoutes.Party("/teamPlayer")
	// For authorized user.
	teamPlayerRoutes.Get("/", teamPlayer.TeamPlayer)
	// For admin
	teamPlayerRoutes.Post("/add", teamPlayer.AddTeamPlayer)
	teamPlayerRoutes.Post("/{id: string}", teamPlayer.UpdateTeamPlayer)

	zoneTypeRoutes := apiRoutes.Party("/zonetype")
	// For authorize user.
	zoneTypeRoutes.Get("/", auth.TokenMDW, zoneType.GetZoneType)
	// For admin
	zoneTypeRoutes.Post("/", zoneType.AddZoneType)

	monsterRoutes := apiRoutes.Party("/monster")
	monster.InitRouter(monsterRoutes)

	newsRoutes := apiRoutes.Party("/news")
	news.InitRouter(newsRoutes)

	
	promotionRoutes := apiRoutes.Party("/promotion")
	news.InitRouter(promotionRoutes)

	newsTypeRoutes := apiRoutes.Party("/newsType")
	newstype.InitRouter(newsTypeRoutes)

	monsterDefaultRoutes := apiRoutes.Party("/monsterDefault")
	monsterDefault.InitRouter(monsterDefaultRoutes)

	wildMonsterRoutes := apiRoutes.Party("/wildMonster")
	wildMonster.InitRouter(wildMonsterRoutes)

	channelRoutes := apiRoutes.Party("/channel")
	channel.InitRouter(channelRoutes)

	mallRoutes := apiRoutes.Party("/mall")
	mall.InitRouter(mallRoutes)

	shopRoutes := apiRoutes.Party("/shop")
	shop.ServeRouter(shopRoutes)
	shopAssertsRoutes := apiRoutes.Party("/shopAsserts")
	shopasserts.InitRouter(shopAssertsRoutes)
	shopCategoriesRoutes := apiRoutes.Party("/shopCategories")
	shopcategories.InitRouter(shopCategoriesRoutes)
	shopModeRouter := apiRoutes.Party("/shopMode")
	shopmode.InitRouter(shopModeRouter)
	shopAdsRouter := apiRoutes.Party("/shopAds")
	shopads.InitRouter(shopAdsRouter)

	stickerStoreRoutes := apiRoutes.Party("/stickerStore")
	stickerstore.InitRouter(stickerStoreRoutes)

	stickerRoutes := apiRoutes.Party("/sticker")
	sticker.InitRouter(stickerRoutes)

	categoryItemDefaultRoutes := apiRoutes.Party("/categoryItemDefault")
	// For admin
	categoryItemDefaultRoutes.Post("/add", categoryitem.AddCategoryItemDefault)
	categoryItemDefaultRoutes.Post("/{id: string}", categoryitem.UpdateCategoryItemDefault)
	categoryItemDefaultRoutes.Get("/", categoryitem.GetCatagoryItem)
	ItemRoutes := apiRoutes.Party("/itemDefault")
	item.InitRouter(ItemRoutes)

	ItemPlayerRoutes := apiRoutes.Party("/itemPlayer")
	itemplayer.InitRouter(ItemPlayerRoutes)

	wildItemRoutes := apiRoutes.Party("/wildItem")
	wilditem.InitRouter(wildItemRoutes)
	friendRoutes := apiRoutes.Party("/friends")
	friend.InitRouter(friendRoutes)

	chatRoutes := apiRoutes.Party("/chat")
	chat.InitRouter(chatRoutes)

	privilegeGroupsRoutes := apiRoutes.Party("/privilegeGroups")
	privilegeGroups.InitRouter(privilegeGroupsRoutes)

	privilegeDefaultPlayerRoutes := apiRoutes.Party("/privilegeDefaultPlayers")
	privilegeDefaultPlayer.InitRouter(privilegeDefaultPlayerRoutes)

	privilegeShopRoutes := apiRoutes.Party("/privilegeShops")
	privilegeShop.InitRouter(privilegeShopRoutes)

	settingRoutes := apiRoutes.Party("/setting")
	setting.InitRouter(settingRoutes)

	mailBoxRoutes := apiRoutes.Party("/mailBox")
	mailBox.InitRouter(mailBoxRoutes)

	digitalBoardRoutes := apiRoutes.Party("/digitalBoard")
	digitalBoard.InitRouter(digitalBoardRoutes)

	devicesRoutes := apiRoutes.Party("/device")
	devices.InitRouter(devicesRoutes)
	shopAssertRoutes := apiRoutes.Party("/shopAssert")
	shopAssertRoutes.Get("/{id:string}", shopassert.GetShopAssetDetail)

	couponRoutes := apiRoutes.Party("/coupon")
	coupon.InitRouter(couponRoutes)
	
	couponPlayerRoutes := apiRoutes.Party("/couponPlayer")
	couponPlayer.InitRouter(couponPlayerRoutes)

	couponDiamondRoutes := apiRoutes.Party("/couponDiamond")
	couponDiamond.InitRouter(couponDiamondRoutes)

	miniGameRoutes := apiRoutes.Party("/miniGame")
	miniGame.InitRouter(miniGameRoutes)

	
	tellFriendRoutes := apiRoutes.Party("/tellFriend")
	tellFriend.InitRouter(tellFriendRoutes)

	
	vouchersRoutes := apiRoutes.Party("/vouchers")
	coupon.InitRouter(vouchersRoutes)
	
	qrCodeRoutes := apiRoutes.Party("/qrCode")
	qrCode.InitRouter(qrCodeRoutes)

	historyPlayerRoutes := apiRoutes.Party("/historyPlayer")
	historyPlayer.InitRouter(historyPlayerRoutes)
	
	paymentMPlayItemsRoutes := apiRoutes.Party("/paymentMPlayItems")
	paymentMPlayItems.InitRouter(paymentMPlayItemsRoutes)

	paymentMPayConfigRoutes:= apiRoutes.Party("/payment")
	paymentMPayConfig.InitRouter(paymentMPayConfigRoutes)

	paymentMPayCreditCardsRoutes:= apiRoutes.Party("/paymentMPayCreditCards")
	paymentMPayCreditCards.InitRouter(paymentMPayCreditCardsRoutes)
	/* Official mongodb client.

		client := database.Connect()
	 	defer client.Disconnect(context.Background())

		app.Get("/ping", func(ctx iris.Context) {
			// Database name and collection name
			coll := client.Database(database.CARTOON).Collection(database.PROGRAMS)
			cursor, err := coll.Find(context.Background(), bson.NewDocument())
			if err != nil {
				log.Fatal(err)
			}
			for cursor.Next(context.Background()) {
				elem := bson.NewDocument()
				if err := cursor.Decode(elem); err != nil {
					log.Fatal(err)
				}

				// do something with elem....
				log.Fatal("result", elem.ToExtJSON(true))
				ctx.WriteString(elem.ToExtJSON(true))
			}
		})
	*/
	app.Get("/", func(ctx iris.Context) {
		var body = ctx.Request().Body
		var form = ctx.Request().Form

		fmt.Println(body, form)

		ctx.WriteString("InMallGame API: hello all developer!")
	})

	span.Finish()

	return app
}

func main() {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	p := path.Join(dir, "conf.json")
	fmt.Println(dir, p)

	configuration := utils.GetConfig(p)

	// Database connection
	session := database.MgoConnect(configuration)
	defer session.Close()

	app := newApp(configuration)
	// app.Get("/metrics", iris.FromStd(prometheus.Handler()))

	// registers a custom handler for 404 not found http (error) status code,
	// fires when route not found or manually by ctx.StatusCode(iris.StatusNotFound).
	app.OnErrorCode(iris.StatusNotFound, notFoundHandler)
	// var sb strings.Builder
	// sb.WriteString(":")
	// sb.WriteString(configuration.Port)
	app.Configure(iris.WithConfiguration(iris.Configuration{
		DisableAutoFireStatusCode: true,
	}))
	app.Run(iris.Addr(":"+configuration.Port, configureHost), iris.WithoutServerError(iris.ErrServerClosed))
}
