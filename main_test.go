package main

import (
	"fmt"
	"os"
	"path"
	"testing"

	"github.com/kataras/iris/httptest"

	"inmallgame/app/data-access"
	"inmallgame/app/utils"

	"inmallgame/tests"
)

// $ go test -v
func TestMain(t *testing.T) {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	p := path.Join(dir, "conf.json")
	fmt.Println(dir, p)

	configuration := utils.GetConfig(p)

	// Database connection
	session := database.MgoConnect(configuration)
	defer session.Close()

	app := newApp(configuration)
	e := httptest.New(t, app)

	// redirects to /admin without basic auth
	e.GET("/").Expect().Status(httptest.StatusOK).Body().Equal("InMallGame API: hello all developer!")
	// without basic auth
	// e.GET("/admin").Expect().Status(httptest.StatusUnauthorized)

	// // with valid basic auth
	// e.GET("/admin").WithBasicAuth("myusername", "mypassword").Expect().
	// 	Status(httptest.StatusOK).Body().Equal("/admin myusername:mypassword")
	// e.GET("/admin/profile").WithBasicAuth("myusername", "mypassword").Expect().
	// 	Status(httptest.StatusOK).Body().Equal("/admin/profile myusername:mypassword")
	// e.GET("/admin/settings").WithBasicAuth("myusername", "mypassword").Expect().
	// 	Status(httptest.StatusOK).Body().Equal("/admin/settings myusername:mypassword")

	// // with invalid basic auth
	// e.GET("/admin/settings").WithBasicAuth("invalidusername", "invalidpassword").
	// 	Expect().Status(httptest.StatusUnauthorized)

	tests.TestZoneTypeAPI(t, e)
	tests.TestMonsterDefaultAPI(t, e)
	tests.TestMonsterPlayerAPI(t, e)

	tests.TestAuthAPI(t, e)
	tests.TestUserAPI(t, e)
	tests.TestPlayerAPI(t, e)
	tests.TestWildMonsterAPI(t, e)
	tests.TestMonsterStickerAPI(t, e)
	tests.TestStickerStoreAPI(t, e)
	tests.TestShopAPI(t, e)
	tests.TestItemDefaultAPI(t, e)
	tests.TestItemPlayerAPI(t, e)
	tests.TestWildItemAPI(t, e)
	tests.TestMallAPI(t, e)
}
